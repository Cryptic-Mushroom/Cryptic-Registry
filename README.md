# Cryptic Registry

Cryptic Registry is a comprehensive library mod that aims to automate several annoying factors when modding Minecraft, including tags, data generation, and registration.

## The Problem

Minecraft modding comes with a lot of caveats. Mojang has been pushing for using static data in the form of JSON files ever since the inception of datapacks, and that has since come to fruition with the necessity of implementing several "game objects" through this system. This can be great for interacting with datapacks and other mods, but provides an infuriating problem: You must manually account for changes in your code by also adding them to the necessary data files or generators. This is made even more of a problem when in some cases (i.e. Blocks) have several spots in data where they must be accounted for, those being localization files, block states and models, block item models, and tags, just to name a few.

## The Solution

I've decided to take the liberty of going through hell and back to implement a system that is capable of automating these gruesome tasks for you. I present: Cryptic Registry. Cryptic Registry was originally only a part of the Midnight mod as a method to speed up much of the data generation we need to do for our mod, but I have since decided to implement it as its own library mod under a public open source license so that no one has to go through the same torture I subjected myself to.
