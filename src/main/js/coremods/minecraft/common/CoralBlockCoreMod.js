// noinspection DuplicatedCode, ES6ConvertVarToLetConst

var Opcodes = Java.type('org.objectweb.asm.Opcodes');
var FieldInsnNode = Java.type('org.objectweb.asm.tree.FieldInsnNode');
var FieldNode = Java.type('org.objectweb.asm.tree.FieldNode');
var FrameNode = Java.type('org.objectweb.asm.tree.FrameNode');
var InsnList = Java.type('org.objectweb.asm.tree.InsnList');
var InsnNode = Java.type('org.objectweb.asm.tree.InsnNode');
var JumpInsnNode = Java.type('org.objectweb.asm.tree.JumpInsnNode');
var LabelNode = Java.type('org.objectweb.asm.tree.LabelNode');
var LocalVariableNode = Java.type('org.objectweb.asm.tree.LocalVariableNode');
var MethodInsnNode = Java.type('org.objectweb.asm.tree.MethodInsnNode');
var MethodNode = Java.type('org.objectweb.asm.tree.MethodNode');
var TypeInsnNode = Java.type('org.objectweb.asm.tree.TypeInsnNode');
var VarInsnNode = Java.type('org.objectweb.asm.tree.VarInsnNode');

var ASMAPI = Java.type('net.minecraftforge.coremod.api.ASMAPI');

// noinspection JSUnusedGlobalSymbols
/**
 * The CoralBlockCoreMod coremod hooks into the CoralBlock, CoralFanBlock, CoralPlantBlock, and CoralWallFanBlock
 * classes and injects the ASM bytecode presented in the transformers returned by this method.
 *
 * @returns {{save: {transformer: (function(*): *), target: {names: (function(*): string[]), type: string}}}} The transformers to use for this coremod.
 */
function initializeCoreMod() {
    return {
        'save': {
            'target': {
                'type': 'CLASS',
                'names': function (_) {
                    return [
                        'net.minecraft.world.level.block.CoralBlock',
                        'net.minecraft.world.level.block.CoralFanBlock',
                        'net.minecraft.world.level.block.CoralPlantBlock',
                        'net.minecraft.world.level.block.CoralWallFanBlock'
                    ];
                }
            },
            'transformer': deadBlock
        }
    };
}

/**
 * This function transforms the given class (defined in the coremod) to account custom implementations of the dead
 * block. If the instanceof check succeeds, we attempt to call our own (cached) dead block rather than getting the
 * original field from the class.
 *
 * @param clazz The class's bytecode that will be transformed by this coremod.
 * @returns {*} The transformed class.
 */
function deadBlock(clazz) {
    // get field name for dead block
    var deadBlockField = 'deadBlock';
    switch (clazz.name) {
        case 'net/minecraft/world/level/block/CoralBlock':
            deadBlockField = ASMAPI.mapField('f_52128_');
            break;
        case 'net/minecraft/world/level/block/CoralFanBlock':
            deadBlockField = ASMAPI.mapField('f_52149_');
            break;
        case 'net/minecraft/world/level/block/CoralPlantBlock':
            deadBlockField = ASMAPI.mapField('f_52172_');
            break;
        case 'net/minecraft/world/level/block/CoralWallFanBlock':
            deadBlockField = ASMAPI.mapField('f_52200_');
            break;
        default:
            ASMAPI.log('ERROR', 'Failed to get mapped field name for "deadBlock" in class {}! This is very bad.', clazz.name.replaceAll('/', '.'));
            break;
    }

    ASMAPI.log('DEBUG', 'Got mapped field name: "deadBlock" to "{}" for class {}', deadBlockField, clazz.name.replaceAll('/', '.'));

    // replace all field calls
    for (var i = 0; i < clazz.methods.size(); i++) {
        var method = clazz.methods.get(i);
        for (var j = 0; j < method.instructions.size(); j++) {
            var insn = method.instructions.get(j);
            if (insn.getOpcode() === Opcodes.GETFIELD && insn.name.equals(deadBlockField) && insn.desc.equals('Lnet/minecraft/world/level/block/Block;')) {
                ASMAPI.log('DEBUG', 'Replacing deadBlock field call in {}', clazz.name + '.' + method.name + method.desc);
                method.instructions.set(insn, ASMAPI.buildMethodCall(clazz.name, 'cmreg$deadBlock', '()Lnet/minecraft/world/level/block/Block;', ASMAPI.MethodType.VIRTUAL));
            }
        }
    }

    // create a protected field with our cached dead block
    clazz.fields.add(new FieldNode(Opcodes.ACC_PROTECTED, 'cmreg$deadBlock', 'Lnet/minecraft/world/level/block/Block;', null, null));

    // create our method
    var cmreg$deadBlock = new MethodNode(Opcodes.ACC_PRIVATE, 'cmreg$deadBlock', '()Lnet/minecraft/world/level/block/Block;', null, null);
    var list = new InsnList();
    var start = new LabelNode();
    list.add(start);

    // this instanceof CoralHasCustomDeadBlock
    var hasCustomdeadBlock = new LabelNode();
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new TypeInsnNode(Opcodes.INSTANCEOF, 'com/crypticmushroom/minecraft/registry/api/block/CoralHasCustomDeadBlock'));
    list.add(new JumpInsnNode(Opcodes.IFNE, hasCustomdeadBlock));
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new FieldInsnNode(Opcodes.GETFIELD, clazz.name, deadBlockField, 'Lnet/minecraft/world/level/block/Block;'));
    list.add(new InsnNode(Opcodes.ARETURN));

    // if this.cmreg$deadBlock != null, skip to 'deadBlockInitialized' label
    var deadBlockInitialized = new LabelNode();
    list.add(hasCustomdeadBlock);
    list.add(new FrameNode(Opcodes.F_SAME, 0, null, 0, null));
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new FieldInsnNode(Opcodes.GETFIELD, clazz.name, 'cmreg$deadBlock', 'Lnet/minecraft/world/level/block/Block;'));
    list.add(new JumpInsnNode(Opcodes.IFNONNULL, deadBlockInitialized));

    // this.cmreg$deadBlock = ((CoralHasCustomDeadBlock) this).getDeadBlock();
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new TypeInsnNode(Opcodes.CHECKCAST, 'com/crypticmushroom/minecraft/registry/api/block/CoralHasCustomDeadBlock'));
    list.add(ASMAPI.buildMethodCall('com/crypticmushroom/minecraft/registry/api/block/CoralHasCustomDeadBlock', 'getDeadBlock', '()Lnet/minecraft/world/level/block/Block;', ASMAPI.MethodType.INTERFACE));
    list.add(new FieldInsnNode(Opcodes.PUTFIELD, clazz.name, 'cmreg$deadBlock', 'Lnet/minecraft/world/level/block/Block;'));

    // return this.cmreg$deadBlock
    list.add(deadBlockInitialized);
    list.add(new FrameNode(Opcodes.F_SAME, 0, null, 0, null));
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new FieldInsnNode(Opcodes.GETFIELD, clazz.name, 'cmreg$deadBlock', 'Lnet/minecraft/world/level/block/Block;'));
    list.add(new InsnNode(Opcodes.ARETURN));

    var end = new LabelNode();
    list.add(end);

    cmreg$deadBlock.instructions = list;
    cmreg$deadBlock.localVariables = [new LocalVariableNode('this', 'L' + clazz.name + ';', null, start, end, 0)];
    cmreg$deadBlock.maxStack = 2;
    cmreg$deadBlock.maxLocals = 1;

    clazz.methods.add(cmreg$deadBlock);

    //ASMAPI.log('DEBUG', '{}\n{}', clazz.name.replaceAll('/', '.'), ASMAPI.classNodeToString(clazz));
    return clazz;
}
