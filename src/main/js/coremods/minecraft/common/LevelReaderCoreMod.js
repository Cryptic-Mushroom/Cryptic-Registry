// noinspection ES6ConvertVarToLetConst

var Opcodes = Java.type('org.objectweb.asm.Opcodes');
var FrameNode = Java.type('org.objectweb.asm.tree.FrameNode');
var InsnList = Java.type('org.objectweb.asm.tree.InsnList');
var InsnNode = Java.type('org.objectweb.asm.tree.InsnNode');
var JumpInsnNode = Java.type('org.objectweb.asm.tree.JumpInsnNode');
var LabelNode = Java.type('org.objectweb.asm.tree.LabelNode');
var MethodNode = Java.type('org.objectweb.asm.tree.MethodNode');
var TypeInsnNode = Java.type('org.objectweb.asm.tree.TypeInsnNode');
var VarInsnNode = Java.type('org.objectweb.asm.tree.VarInsnNode');

var ASMAPI = Java.type('net.minecraftforge.coremod.api.ASMAPI');

// noinspection JSUnusedGlobalSymbols
/**
 * The LevelReader coremod hooks into the LevelReader class and injects the ASM bytecode presented in the transformers
 * returned by this method.
 *
 * @returns {{save: {target: {type: string, class: string, methodDesc: string, methodName: string}, transformer: (function(MethodNode): MethodNode)}}} The transformers to use for this coremod.
 */
function initializeCoreMod() {
    return {
        'save': {
            'target': {
                'type': 'METHOD',
                'class': 'net.minecraft.world.level.LevelReader',
                'methodName': 'm_6171_',
                'methodDesc': '(Lnet/minecraft/core/BlockPos;Lnet/minecraft/world/level/ColorResolver;)I'
            },
            'transformer': getBlockTint
        }
    };
}

/**
 * This function transforms the getBlockTint(BlockPos, ColorResolver) method to account for the subtype
 * CrypticColorResolver. If the instanceof check succeeds, we return the result of CrypticColorResolver.getColor()
 * instead of continuing on with the original method.
 *
 * This is the following Java code that is injected at the top of the method, including mappings and implied imports:
 * <pre>
 *     if (colorResolver instanceof CrypticColorResolver) {
 *         return ((CrypticColorResolver) colorResolver).getColor(this.getBiome(blockPos), blockPos.getX(), blockPos.getZ());
 *     }
 * </pre>
 *
 * This coremod will retroactively work with other coremods and mixins because the original code is never reached while
 * functionally being the same, barring the redirection of ColorResolver.getColor() to CrypticColorResolver.getColor().
 *
 * @param method The method's bytecode that will be transformed by this coremod.
 * @returns {*} The transformed method.
 */
function getBlockTint(method) {
    var list = new InsnList();

    // if color resolver is not a CrypticColorResolver, skip all of this
    var isNotCrypticColorResolver = new LabelNode();
    list.add(new VarInsnNode(Opcodes.ALOAD, 2));
    list.add(new TypeInsnNode(Opcodes.INSTANCEOF, 'com/crypticmushroom/minecraft/registry/api/biome/CrypticColorResolver'));
    list.add(new JumpInsnNode(Opcodes.IFEQ, isNotCrypticColorResolver));

    // return ((CrypticColorResolver) colorResolver).getColor(this.getBiome(blockPos), blockPos.getX(), blockPos.getZ())
    list.add(new LabelNode());
    list.add(new VarInsnNode(Opcodes.ALOAD, 2));
    list.add(new TypeInsnNode(Opcodes.CHECKCAST, 'com/crypticmushroom/minecraft/registry/api/biome/CrypticColorResolver')); // (CrypticColorResolver) colorResolver
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new VarInsnNode(Opcodes.ALOAD, 1));
    list.add(ASMAPI.buildMethodCall('net/minecraft/world/level/LevelReader', ASMAPI.mapMethod('m_204166_'), '(Lnet/minecraft/core/BlockPos;)Lnet/minecraft/core/Holder;', ASMAPI.MethodType.INTERFACE)); // this.getBiome(blockPos)
    list.add(new VarInsnNode(Opcodes.ALOAD, 1));
    list.add(ASMAPI.buildMethodCall('net/minecraft/core/BlockPos', ASMAPI.mapMethod('m_121983_'), '()I', ASMAPI.MethodType.VIRTUAL)); // blockPos.getX()
    list.add(new InsnNode(Opcodes.I2D));
    list.add(new VarInsnNode(Opcodes.ALOAD, 1));
    list.add(ASMAPI.buildMethodCall('net/minecraft/core/BlockPos', ASMAPI.mapMethod('m_122015_'), '()I', ASMAPI.MethodType.VIRTUAL)); // blockPos.getZ()
    list.add(new InsnNode(Opcodes.I2D));
    list.add(ASMAPI.buildMethodCall('com/crypticmushroom/minecraft/registry/api/biome/CrypticColorResolver', 'getColor', '(Lnet/minecraft/core/Holder;DD)I', ASMAPI.MethodType.INTERFACE)); // method call
    list.add(new InsnNode(Opcodes.IRETURN)); // return (int) result

    list.add(isNotCrypticColorResolver);
    list.add(new FrameNode(Opcodes.F_SAME, 0, null, 0, null));

    // insert AFTER the first instruction. this is important so that our work goes after the first label.
    method.instructions.insert(method.instructions.getFirst(), list);

    //ASMAPI.log('DEBUG', '{}.{}\n{}', 'net.minecraft.world.level.LevelReader', method.name.replaceAll('/', '.'), ASMAPI.methodNodeToString(method));
    return method;
}
