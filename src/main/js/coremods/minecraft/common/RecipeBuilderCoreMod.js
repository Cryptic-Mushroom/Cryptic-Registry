// noinspection ES6ConvertVarToLetConst

var Opcodes = Java.type('org.objectweb.asm.Opcodes');
var MethodNode = Java.type('org.objectweb.asm.tree.MethodNode');
var VarInsnNode = Java.type('org.objectweb.asm.tree.VarInsnNode');

var ASMAPI = Java.type('net.minecraftforge.coremod.api.ASMAPI');

// noinspection JSUnusedGlobalSymbols
/**
 * The RecipeBuilder coremod hooks into the Item class and injects the ASM bytecode presented in the transformers
 * returned by this method.
 *
 * @returns {{save: {target: {type: string, class: string, methodDesc: string, methodName: string}, transformer: (function(MethodNode): MethodNode)}}} The transformers to use for this coremod.
 */
function initializeCoreMod() {
    return {
        'save': {
            'target': {
                'type': 'METHOD',
                'class': 'net.minecraft.data.recipes.RecipeBuilder',
                'methodName': 'm_176500_',
                'methodDesc': '(Ljava/util/function/Consumer;Ljava/lang/String;)V'
            },
            'transformer': save
        }
    };
}

/**
 * This function transforms the save(Consumer, String) method to account for the subtype CrypticRecipeWriter. After the
 * instanceof check succeeds, the resourcelocation1 variable is replaced with the version presented in the injected
 * bytecode. Because the variable is not marked as final, we are free to re-assign it without manipulating its
 * initialization.
 *
 * This is the following Java code that is injected after resourcelocation1 is first assigned, including mappings and
 * implied imports:
 * <pre>
 *     resourcelocation1 = RecipeBuilderHooks.replaceResLoc(resourcelocation1, finishedRecipeConsumer, recipeId);
 * </pre>
 *
 * @param method The method's bytecode that will be transformed by this coremod.
 * @returns {*} The transformed method.
 */
function save(method) {
    // RecipeBuilderHooks.replaceResLoc(resourcelocation1, finishedRecipeConsumer, recipeId);
    var list = ASMAPI.listOf([
        new VarInsnNode(Opcodes.ALOAD, 4), // resourcelocation1
        new VarInsnNode(Opcodes.ALOAD, 1), // finishedRecipeConsumer
        new VarInsnNode(Opcodes.ALOAD, 2), // recipeId
        ASMAPI.buildMethodCall('com/crypticmushroom/minecraft/registry/coremod/hook/RecipeBuilderHooks', 'replaceResLoc', '(Lnet/minecraft/resources/ResourceLocation;Ljava/util/function/Consumer;Ljava/lang/String;)Lnet/minecraft/resources/ResourceLocation;', ASMAPI.MethodType.STATIC),
        new VarInsnNode(Opcodes.ASTORE, 4) // resourcelocation1
    ]);

    // look for the instruction where the resourcelocation1 variable is first set
    var instructions = method.instructions;
    var insn;
    for (var i = 0; i < instructions.size(); i++) {
        insn = instructions.get(i);
        if (insn.getOpcode() === Opcodes.ASTORE && insn.var === 4) {
            break;
        }
    }

    // insert our instructions right after the variable is set, so we can replace it with our own (or itself)
    if (insn != null)
        instructions.insert(insn, list);
    else {
        ASMAPI.log('FATAL', 'Failed to locate local variable resourcelocation1 (index 4) in net.minecraft.data.recipes.RecipeBuilder.{}! This is very bad.', method.name + method.desc);
    }

    //ASMAPI.log('DEBUG', '{}.{}\n{}', 'net.minecraft.data.recipes.RecipeBuilder', method.name.replaceAll('/', '.'), ASMAPI.methodNodeToString(method));
    return method;
}
