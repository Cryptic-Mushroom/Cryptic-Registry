// noinspection DuplicatedCode, ES6ConvertVarToLetConst

var Opcodes = Java.type('org.objectweb.asm.Opcodes');
var FieldInsnNode = Java.type('org.objectweb.asm.tree.FieldInsnNode');
var FieldNode = Java.type('org.objectweb.asm.tree.FieldNode');
var FrameNode = Java.type('org.objectweb.asm.tree.FrameNode');
var InsnList = Java.type('org.objectweb.asm.tree.InsnList');
var InsnNode = Java.type('org.objectweb.asm.tree.InsnNode');
var JumpInsnNode = Java.type('org.objectweb.asm.tree.JumpInsnNode');
var LabelNode = Java.type('org.objectweb.asm.tree.LabelNode');
var LocalVariableNode = Java.type('org.objectweb.asm.tree.LocalVariableNode');
var MethodNode = Java.type('org.objectweb.asm.tree.MethodNode');
var TypeInsnNode = Java.type('org.objectweb.asm.tree.TypeInsnNode');
var VarInsnNode = Java.type('org.objectweb.asm.tree.VarInsnNode');

var ASMAPI = Java.type('net.minecraftforge.coremod.api.ASMAPI');

// noinspection JSUnusedGlobalSymbols
/**
 * The TorchBlockCoreMod coremod hooks into the TorchBlock, WallTorchBlock, RedstoneTorchBlock, and
 * RedstoneWallTorchBlock classes and injects the ASM bytecode presented in the transformers returned by this method.
 *
 * @returns {{save: {transformer: (function(*): *), target: {names: (function(*): string[]), type: string}}}} The transformers to use for this coremod.
 */
function initializeCoreMod() {
    return {
        'save': {
            'target': {
                'type': 'CLASS',
                'names': function (_) {
                    return [
                        'net.minecraft.world.level.block.TorchBlock',
                        'net.minecraft.world.level.block.WallTorchBlock',
                        'net.minecraft.world.level.block.RedstoneTorchBlock',
                        'net.minecraft.world.level.block.RedstoneWallTorchBlock'
                    ];
                }
            },
            'transformer': flameParticle
        }
    };
}

/**
 * This function transforms the given class (defined in the coremod) to account custom implementations of the flame
 * particle. If the instanceof check succeeds, we attempt to call our own (cached) flame particle rather than getting
 * the original field from the class.
 *
 * @param clazz The class's bytecode that will be transformed by this coremod.
 * @returns {*} The transformed class.
 */
function flameParticle(clazz) {
    // replace all field calls
    for (var i = 0; i < clazz.methods.size(); i++) {
        var method = clazz.methods.get(i);
        for (var j = 0; j < method.instructions.size(); j++) {
            var insn = method.instructions.get(j);
            if (insn.getOpcode() === Opcodes.GETFIELD && insn.name.equals(ASMAPI.mapField('f_57488_')) && insn.desc.equals('Lnet/minecraft/core/particles/ParticleOptions;')) {
                ASMAPI.log('DEBUG', 'Replacing flameParticle field call in method "{}"', clazz.name + '.' + method.name + method.desc);
                method.instructions.set(insn, ASMAPI.buildMethodCall(clazz.name, 'cmreg$flameParticle', '()Lnet/minecraft/core/particles/ParticleOptions;', ASMAPI.MethodType.VIRTUAL));
            }
        }
    }

    // create a protected field with our cached flame particle
    clazz.fields.add(new FieldNode(Opcodes.ACC_PROTECTED, 'cmreg$flameParticle', 'Lnet/minecraft/core/particles/ParticleOptions;', null, null));

    // create our method
    var cmreg$flameParticle = new MethodNode(Opcodes.ACC_PRIVATE, 'cmreg$flameParticle', '()Lnet/minecraft/core/particles/ParticleOptions;', null, null);
    var list = new InsnList();
    var start = new LabelNode();
    list.add(start);

    // this instanceof TorchHasCustomFlameParticle
    var hasCustomFlameParticle = new LabelNode();
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new TypeInsnNode(Opcodes.INSTANCEOF, 'com/crypticmushroom/minecraft/registry/api/block/TorchHasCustomFlameParticle'));
    list.add(new JumpInsnNode(Opcodes.IFNE, hasCustomFlameParticle));
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new FieldInsnNode(Opcodes.GETFIELD, clazz.name, ASMAPI.mapField('f_57488_'), 'Lnet/minecraft/core/particles/ParticleOptions;'));
    list.add(new InsnNode(Opcodes.ARETURN));

    // if this.cmreg$flameParticle != null, skip to 'flameParticleInitialized' label
    var flameParticleInitialized = new LabelNode();
    list.add(hasCustomFlameParticle);
    list.add(new FrameNode(Opcodes.F_SAME, 0, null, 0, null));
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new FieldInsnNode(Opcodes.GETFIELD, clazz.name, 'cmreg$flameParticle', 'Lnet/minecraft/core/particles/ParticleOptions;'));
    list.add(new JumpInsnNode(Opcodes.IFNONNULL, flameParticleInitialized));

    // this.cmreg$flameParticle = ((TorchHasCustomFlameParticle) this).getFlameParticle();
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new TypeInsnNode(Opcodes.CHECKCAST, 'com/crypticmushroom/minecraft/registry/api/block/TorchHasCustomFlameParticle'));
    list.add(ASMAPI.buildMethodCall('com/crypticmushroom/minecraft/registry/api/block/TorchHasCustomFlameParticle', 'getFlameParticle', '()Lnet/minecraft/core/particles/ParticleOptions;', ASMAPI.MethodType.INTERFACE));
    list.add(new FieldInsnNode(Opcodes.PUTFIELD, clazz.name, 'cmreg$flameParticle', 'Lnet/minecraft/core/particles/ParticleOptions;'));

    // return this.cmreg$flameParticle
    list.add(flameParticleInitialized);
    list.add(new FrameNode(Opcodes.F_SAME, 0, null, 0, null));
    list.add(new VarInsnNode(Opcodes.ALOAD, 0));
    list.add(new FieldInsnNode(Opcodes.GETFIELD, clazz.name, 'cmreg$flameParticle', 'Lnet/minecraft/core/particles/ParticleOptions;'));
    list.add(new InsnNode(Opcodes.ARETURN));

    var end = new LabelNode();
    list.add(end);

    cmreg$flameParticle.instructions = list;
    cmreg$flameParticle.localVariables = [new LocalVariableNode('this', 'L' + clazz.name + ';', null, start, end, 0)];
    cmreg$flameParticle.maxStack = 2;
    cmreg$flameParticle.maxLocals = 1;

    clazz.methods.add(cmreg$flameParticle);

    //ASMAPI.log('DEBUG', '{}\n{}', clazz.name.replaceAll('/', '.'), ASMAPI.classNodeToString(clazz));
    return clazz;
}
