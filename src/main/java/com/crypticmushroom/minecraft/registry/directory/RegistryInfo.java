package com.crypticmushroom.minecraft.registry.directory;

import com.crypticmushroom.minecraft.registry.data.resource.CustomResourceRegister;
import com.crypticmushroom.minecraft.registry.util.PluralString;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraftforge.registries.IForgeRegistry;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;
import java.util.function.Supplier;

public abstract sealed class RegistryInfo<T, SELF extends RegistryInfo<T, SELF>> {
    public final PluralString name;
    private final ResourceKey<? extends Registry<T>> key;

    // internal registries
    protected Supplier<@Nullable Registry<T>> vanilla = () -> null;
    protected Supplier<@Nullable IForgeRegistry<T>> forge = () -> null;
    protected Function<T, @Nullable ResourceKey<T>> fallback = t -> null;

    public static <T> Static<T> normal(PluralString name, ResourceKey<? extends Registry<T>> key) {
        return new Static<>(name, key);
    }

    public static <T> Custom<T> staticImpl(PluralString name, Function<String, CustomResourceRegister<T, ?>> factory) {
        return new Custom<>(name, factory);
    }

    public static <T> Dynamic<T> data(PluralString name, ResourceKey<? extends Registry<T>> key) {
        return new Dynamic<>(name, key);
    }

    protected RegistryInfo(PluralString name, @Nullable ResourceKey<? extends Registry<T>> key) {
        this.name = name;
        this.key = key;

        RegistryDirectory.ALL.put(key, this);
    }

    public ResourceKey<? extends Registry<T>> getKey() {
        return this.key;
    }

    public @Nullable Registry<T> getVanillaRegistry() {
        return this.vanilla.get();
    }

    public @Nullable IForgeRegistry<T> getForgeRegistry() {
        return this.forge.get();
    }

    public @Nullable ResourceKey<T> getResourceKey(T value) {
        // get all registries
        @Nullable var forge = this.forge.get();
        @Nullable var vanilla = this.vanilla.get();

        // setup result variable
        @Nullable ResourceKey<T> result = null;

        // first - forge registry
        if (forge != null) {
            result = forge.getResourceKey(value).orElse(null);
        }

        // second - vanilla registry
        if (vanilla != null && result == null) {
            result = vanilla.getResourceKey(value).orElse(null);
        }

        // finally - fallback getter
        if (result == null) {
            result = this.fallback.apply(value);
        }

        // even if it is null, lets give it to the user anyway
        // they signed up for it with the @Nullable contract
        return result;
    }

    @SuppressWarnings("unchecked")
    SELF vanilla(Supplier<@Nullable Registry<T>> vanilla) {
        this.vanilla = vanilla;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    SELF forge(Supplier<@Nullable IForgeRegistry<T>> forge) {
        this.forge = forge;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    SELF fallback(Function<T, @Nullable ResourceKey<T>> fallback) {
        this.fallback = fallback;
        return (SELF) this;
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof RegistryInfo<?, ?> o && this.key != null && this.key.equals(o.key));
    }

    @Override
    public String toString() {
        return "%s[%s]".formatted(this.getClass().getName(), this.key);
    }

    public non-sealed static class Static<T> extends RegistryInfo<T, Static<T>> {
        protected Static(PluralString name, ResourceKey<? extends Registry<T>> key) {
            super(name, key);
        }
    }

    public non-sealed static class Dynamic<T> extends RegistryInfo<T, Dynamic<T>> {
        protected Dynamic(PluralString name, ResourceKey<? extends Registry<T>> key) {
            super(name, key);
        }

        /**
         * @deprecated It is likely that a data registries will not actually have any statically-referencable
         *     registries.
         */
        @Override
        @Deprecated
        public @Nullable Registry<T> getVanillaRegistry() {
            return super.getVanillaRegistry();
        }

        /**
         * @deprecated It is likely that a data registries will not actually have any statically-referencable
         *     registries.
         */
        @Override
        @Deprecated
        public @Nullable IForgeRegistry<T> getForgeRegistry() {
            return super.getForgeRegistry();
        }
    }

    public non-sealed static class Custom<T> extends RegistryInfo<T, Custom<T>> {
        private final Function<String, CustomResourceRegister<T, ?>> factory;

        protected Custom(PluralString name, Function<String, CustomResourceRegister<T, ?>> factory) {
            super(name, null);
            this.factory = factory;
        }

        public CustomResourceRegister<T, ?> makeRegister(String modId) {
            return this.factory.apply(modId);
        }

        /** @deprecated Static registries will never have a linked registry key. */
        @Override
        @Deprecated
        @Contract("-> null")
        public @Nullable ResourceKey<? extends Registry<T>> getKey() {
            return super.getKey();
        }

        /** @deprecated Static registries will never have any linked vanilla or forge registries. */
        @Override
        @Deprecated
        @Contract("-> null")
        public @Nullable Registry<T> getVanillaRegistry() {
            return super.getVanillaRegistry();
        }

        /** @deprecated Static registries will never have any linked vanilla or forge registries. */
        @Override
        @Deprecated
        @Contract("-> null")
        public @Nullable IForgeRegistry<T> getForgeRegistry() {
            return super.getForgeRegistry();
        }
    }
}
