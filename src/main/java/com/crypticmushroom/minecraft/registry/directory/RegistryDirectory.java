package com.crypticmushroom.minecraft.registry.directory;

import com.crypticmushroom.minecraft.registry.data.resource.CustomResourceRegister;
import com.crypticmushroom.minecraft.registry.util.PluralString;
import com.mojang.serialization.Codec;
import net.minecraft.commands.synchronization.ArgumentTypeInfo;
import net.minecraft.core.Registry;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.ChatType;
import net.minecraft.network.syncher.EntityDataSerializer;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.stats.StatType;
import net.minecraft.util.valueproviders.FloatProviderType;
import net.minecraft.util.valueproviders.IntProviderType;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.memory.MemoryModuleType;
import net.minecraft.world.entity.ai.sensing.SensorType;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.entity.animal.CatVariant;
import net.minecraft.world.entity.animal.FrogVariant;
import net.minecraft.world.entity.decoration.PaintingVariant;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.entity.npc.VillagerType;
import net.minecraft.world.entity.schedule.Activity;
import net.minecraft.world.entity.schedule.Schedule;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Instrument;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraft.world.item.armortrim.TrimMaterial;
import net.minecraft.world.item.armortrim.TrimPattern;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.biome.MultiNoiseBiomeSourceParameterList;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BannerPattern;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.chunk.ChunkStatus;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.dimension.LevelStem;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.gameevent.PositionSourceType;
import net.minecraft.world.level.levelgen.DensityFunction;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.SurfaceRules;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicateType;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.carver.WorldCarver;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.featuresize.FeatureSizeType;
import net.minecraft.world.level.levelgen.feature.foliageplacers.FoliagePlacerType;
import net.minecraft.world.level.levelgen.feature.rootplacers.RootPlacerType;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProviderType;
import net.minecraft.world.level.levelgen.feature.treedecorators.TreeDecoratorType;
import net.minecraft.world.level.levelgen.feature.trunkplacers.TrunkPlacerType;
import net.minecraft.world.level.levelgen.flat.FlatLevelGeneratorPreset;
import net.minecraft.world.level.levelgen.heightproviders.HeightProviderType;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifierType;
import net.minecraft.world.level.levelgen.presets.WorldPreset;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureSet;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceType;
import net.minecraft.world.level.levelgen.structure.placement.StructurePlacementType;
import net.minecraft.world.level.levelgen.structure.pools.StructurePoolElementType;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;
import net.minecraft.world.level.levelgen.structure.templatesystem.PosRuleTestType;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleTestType;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorList;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorType;
import net.minecraft.world.level.levelgen.synth.NormalNoise;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.storage.loot.entries.LootPoolEntryType;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraft.world.level.storage.loot.predicates.LootItemConditionType;
import net.minecraft.world.level.storage.loot.providers.nbt.LootNbtProviderType;
import net.minecraft.world.level.storage.loot.providers.number.LootNumberProviderType;
import net.minecraft.world.level.storage.loot.providers.score.LootScoreProviderType;
import net.minecraftforge.common.loot.IGlobalLootModifier;
import net.minecraftforge.common.world.BiomeModifier;
import net.minecraftforge.common.world.StructureModifier;
import net.minecraftforge.fluids.FluidType;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.holdersets.HolderSetType;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

import static com.crypticmushroom.minecraft.registry.directory.RegistryInfo.data;
import static com.crypticmushroom.minecraft.registry.directory.RegistryInfo.normal;
import static com.crypticmushroom.minecraft.registry.directory.RegistryInfo.staticImpl;

@SuppressWarnings({"Convert2MethodRef", "FunctionalExpressionCanBeFolded", "deprecation", "unused"})
public final class RegistryDirectory {
    static final Map<ResourceKey<? extends Registry<?>>, RegistryInfo<?, ?>> ALL = new HashMap<>();

    // FORGE - Game Objects
    public static final RegistryInfo.Static<Block> BLOCK = normal(PluralString.of("Block"), ForgeRegistries.Keys.BLOCKS).vanilla(() -> BuiltInRegistries.BLOCK).forge(() -> ForgeRegistries.BLOCKS);
    public static final RegistryInfo.Static<Fluid> FLUID = normal(PluralString.of("Fluid"), ForgeRegistries.Keys.FLUIDS).vanilla(() -> BuiltInRegistries.FLUID).forge(() -> ForgeRegistries.FLUIDS);
    public static final RegistryInfo.Static<Item> ITEM = normal(PluralString.of("Item"), ForgeRegistries.Keys.ITEMS).vanilla(() -> BuiltInRegistries.ITEM).forge(() -> ForgeRegistries.ITEMS);
    public static final RegistryInfo.Static<MobEffect> MOB_EFFECT = normal(PluralString.of("Mob Effect"), ForgeRegistries.Keys.MOB_EFFECTS).vanilla(() -> BuiltInRegistries.MOB_EFFECT).forge(() -> ForgeRegistries.MOB_EFFECTS);
    public static final RegistryInfo.Static<SoundEvent> SOUND_EVENT = normal(PluralString.of("Sound Event"), ForgeRegistries.Keys.SOUND_EVENTS).vanilla(() -> BuiltInRegistries.SOUND_EVENT).forge(() -> ForgeRegistries.SOUND_EVENTS);
    public static final RegistryInfo.Static<Potion> POTION = normal(PluralString.of("Potion"), ForgeRegistries.Keys.POTIONS).vanilla(() -> BuiltInRegistries.POTION).forge(() -> ForgeRegistries.POTIONS);
    public static final RegistryInfo.Static<Enchantment> ENCHANTMENT = normal(PluralString.of("Enchantment"), ForgeRegistries.Keys.ENCHANTMENTS).vanilla(() -> BuiltInRegistries.ENCHANTMENT).forge(() -> ForgeRegistries.ENCHANTMENTS);
    public static final RegistryInfo.Static<EntityType<?>> ENTITY_TYPE = normal(PluralString.of("Entity Type"), ForgeRegistries.Keys.ENTITY_TYPES).vanilla(() -> BuiltInRegistries.ENTITY_TYPE).forge(() -> ForgeRegistries.ENTITY_TYPES);
    public static final RegistryInfo.Static<BlockEntityType<?>> BLOCK_ENTITY_TYPE = normal(PluralString.of("Block Entity Type"), ForgeRegistries.Keys.BLOCK_ENTITY_TYPES).vanilla(() -> BuiltInRegistries.BLOCK_ENTITY_TYPE).forge(() -> ForgeRegistries.BLOCK_ENTITY_TYPES);
    public static final RegistryInfo.Static<ParticleType<?>> PARTICLE_TYPE = normal(PluralString.of("Particle Type"), ForgeRegistries.Keys.PARTICLE_TYPES).vanilla(() -> BuiltInRegistries.PARTICLE_TYPE).forge(() -> ForgeRegistries.PARTICLE_TYPES);
    public static final RegistryInfo.Static<MenuType<?>> MENU_TYPE = normal(PluralString.of("Menu Type"), ForgeRegistries.Keys.MENU_TYPES).vanilla(() -> BuiltInRegistries.MENU).forge(() -> ForgeRegistries.MENU_TYPES);
    public static final RegistryInfo.Static<PaintingVariant> PAINTING_VARIANT = normal(PluralString.of("Painting Variant"), ForgeRegistries.Keys.PAINTING_VARIANTS).vanilla(() -> BuiltInRegistries.PAINTING_VARIANT).forge(() -> ForgeRegistries.PAINTING_VARIANTS);
    public static final RegistryInfo.Static<RecipeType<?>> RECIPE_TYPE = normal(PluralString.of("Recipe Type"), ForgeRegistries.Keys.RECIPE_TYPES).vanilla(() -> BuiltInRegistries.RECIPE_TYPE).forge(() -> ForgeRegistries.RECIPE_TYPES);
    public static final RegistryInfo.Static<RecipeSerializer<?>> RECIPE_SERIALIZER = normal(PluralString.of("Recipe Serializer"), ForgeRegistries.Keys.RECIPE_SERIALIZERS).vanilla(() -> BuiltInRegistries.RECIPE_SERIALIZER).forge(() -> ForgeRegistries.RECIPE_SERIALIZERS);
    public static final RegistryInfo.Static<Attribute> ATTRIBUTE = normal(PluralString.of("Attribute"), ForgeRegistries.Keys.ATTRIBUTES).vanilla(() -> BuiltInRegistries.ATTRIBUTE).forge(() -> ForgeRegistries.ATTRIBUTES);
    public static final RegistryInfo.Static<StatType<?>> STAT_TYPE = normal(PluralString.of("Stat Type"), ForgeRegistries.Keys.STAT_TYPES).vanilla(() -> BuiltInRegistries.STAT_TYPE).forge(() -> ForgeRegistries.STAT_TYPES);
    public static final RegistryInfo.Static<ArgumentTypeInfo<?, ?>> COMMAND_ARGUMENT_TYPE = normal(PluralString.of("Command Argument Type"), ForgeRegistries.Keys.COMMAND_ARGUMENT_TYPES).vanilla(() -> BuiltInRegistries.COMMAND_ARGUMENT_TYPE).forge(() -> ForgeRegistries.COMMAND_ARGUMENT_TYPES);

    // FORGE - Villages
    public static final RegistryInfo.Static<VillagerProfession> VILLAGER_PROFESSION = normal(PluralString.of("Villager Profession"), ForgeRegistries.Keys.VILLAGER_PROFESSIONS).vanilla(() -> BuiltInRegistries.VILLAGER_PROFESSION).forge(() -> ForgeRegistries.VILLAGER_PROFESSIONS);
    public static final RegistryInfo.Static<PoiType> POI_TYPE = normal(PluralString.of("Point of Interest Type"), ForgeRegistries.Keys.POI_TYPES).vanilla(() -> BuiltInRegistries.POINT_OF_INTEREST_TYPE).forge(() -> ForgeRegistries.POI_TYPES);
    public static final RegistryInfo.Static<MemoryModuleType<?>> MEMORY_MODULE_TYPE = normal(PluralString.of("Memory Module Type"), ForgeRegistries.Keys.MEMORY_MODULE_TYPES).vanilla(() -> BuiltInRegistries.MEMORY_MODULE_TYPE).forge(() -> ForgeRegistries.MEMORY_MODULE_TYPES);
    public static final RegistryInfo.Static<SensorType<?>> SENSOR_TYPE = normal(PluralString.of("Sensor Type"), ForgeRegistries.Keys.SENSOR_TYPES).vanilla(() -> BuiltInRegistries.SENSOR_TYPE).forge(() -> ForgeRegistries.SENSOR_TYPES);
    public static final RegistryInfo.Static<Schedule> SCHEDULE = normal(PluralString.of("Schedule"), ForgeRegistries.Keys.SCHEDULES).vanilla(() -> BuiltInRegistries.SCHEDULE).forge(() -> ForgeRegistries.SCHEDULES);
    public static final RegistryInfo.Static<Activity> ACTIVITY = normal(PluralString.of("Activity", "Activities"), ForgeRegistries.Keys.ACTIVITIES).vanilla(() -> BuiltInRegistries.ACTIVITY).forge(() -> ForgeRegistries.ACTIVITIES);

    // FORGE - Worldgen
    public static final RegistryInfo.Static<WorldCarver<?>> WORLD_CARVER = normal(PluralString.of("World Carver"), ForgeRegistries.Keys.WORLD_CARVERS).vanilla(() -> BuiltInRegistries.CARVER).forge(() -> ForgeRegistries.WORLD_CARVERS);
    public static final RegistryInfo.Static<Feature<?>> FEATURES = normal(PluralString.of("Feature"), ForgeRegistries.Keys.FEATURES).vanilla(() -> BuiltInRegistries.FEATURE).forge(() -> ForgeRegistries.FEATURES);
    public static final RegistryInfo.Static<ChunkStatus> CHUNK_STATUS = normal(PluralString.of("Chunk Status", "Chunk Statuses"), ForgeRegistries.Keys.CHUNK_STATUS).vanilla(() -> BuiltInRegistries.CHUNK_STATUS).forge(() -> ForgeRegistries.CHUNK_STATUS);
    public static final RegistryInfo.Static<BlockStateProviderType<?>> BLOCKSTATE_PROVIDER_TYPE = normal(PluralString.of("Blockstate Provider Type"), ForgeRegistries.Keys.BLOCK_STATE_PROVIDER_TYPES).vanilla(() -> BuiltInRegistries.BLOCKSTATE_PROVIDER_TYPE).forge(() -> ForgeRegistries.BLOCK_STATE_PROVIDER_TYPES);
    public static final RegistryInfo.Static<FoliagePlacerType<?>> FOLIAGE_PLACER_TYPE = normal(PluralString.of("Foliage Placer Type"), ForgeRegistries.Keys.FOLIAGE_PLACER_TYPES).vanilla(() -> BuiltInRegistries.FOLIAGE_PLACER_TYPE).forge(() -> ForgeRegistries.FOLIAGE_PLACER_TYPES);
    public static final RegistryInfo.Static<TreeDecoratorType<?>> TREE_DECORATOR_TYPE = normal(PluralString.of("Tree Decorator Type"), ForgeRegistries.Keys.TREE_DECORATOR_TYPES).vanilla(() -> BuiltInRegistries.TREE_DECORATOR_TYPE).forge(() -> ForgeRegistries.TREE_DECORATOR_TYPES);

    // FORGE - Dynamic/Data driven
    public static final RegistryInfo.Dynamic<Biome> BIOME = data(PluralString.of("Biome"), ForgeRegistries.Keys.BIOMES).forge(() -> ForgeRegistries.BIOMES);

    // FORGE - Custom forge registries
    public static final RegistryInfo.Static<EntityDataSerializer<?>> ENTITY_DATA_SERIALIZER = normal(PluralString.of("Entity Data Serializer"), ForgeRegistries.Keys.ENTITY_DATA_SERIALIZERS).forge(() -> ForgeRegistries.ENTITY_DATA_SERIALIZERS.get());
    public static final RegistryInfo.Static<Codec<? extends IGlobalLootModifier>> GLOBAL_LOOT_MODIFIER_SERIALIZER = normal(PluralString.of("Global Loot Modifier Serializer"), ForgeRegistries.Keys.GLOBAL_LOOT_MODIFIER_SERIALIZERS).forge(() -> ForgeRegistries.GLOBAL_LOOT_MODIFIER_SERIALIZERS.get());
    public static final RegistryInfo.Static<Codec<? extends BiomeModifier>> BIOME_MODIFIER_SERIALIZER = normal(PluralString.of("Biome Modifier Serializer"), ForgeRegistries.Keys.BIOME_MODIFIER_SERIALIZERS).forge(() -> ForgeRegistries.BIOME_MODIFIER_SERIALIZERS.get());
    public static final RegistryInfo.Static<Codec<? extends StructureModifier>> STRUCTURE_MODIFIER_SERIALIZER = normal(PluralString.of("Structure Modifier Serializer"), ForgeRegistries.Keys.STRUCTURE_MODIFIER_SERIALIZERS).forge(() -> ForgeRegistries.STRUCTURE_MODIFIER_SERIALIZERS.get());
    public static final RegistryInfo.Static<FluidType> FLUID_TYPE = normal(PluralString.of("Fluid Type"), ForgeRegistries.Keys.FLUID_TYPES).forge(() -> ForgeRegistries.FLUID_TYPES.get());
    public static final RegistryInfo.Static<HolderSetType> HOLDER_SET_TYPE = normal(PluralString.of("Holder Set Type"), ForgeRegistries.Keys.HOLDER_SET_TYPES).forge(() -> ForgeRegistries.HOLDER_SET_TYPES.get());
    public static final RegistryInfo.Static<ItemDisplayContext> ITEM_DISPLAY_CONTEXT = normal(PluralString.of("Item Display Context"), ForgeRegistries.Keys.DISPLAY_CONTEXTS).forge(() -> ForgeRegistries.DISPLAY_CONTEXTS.get());

    // FORGE - Custom forge datapack registries
    public static final RegistryInfo.Dynamic<BiomeModifier> BIOME_MODIFIER = data(PluralString.of("Biome Modifier"), ForgeRegistries.Keys.BIOME_MODIFIERS);
    public static final RegistryInfo.Dynamic<StructureModifier> STRUCTURE_MODIFIER = data(PluralString.of("Structure Modifier"), ForgeRegistries.Keys.STRUCTURE_MODIFIERS);

    // VANILLA
    public static final RegistryInfo.Static<ResourceLocation> CUSTOM_STAT = normal(PluralString.of("Custom Stat"), Registries.CUSTOM_STAT).vanilla(() -> BuiltInRegistries.CUSTOM_STAT);
    public static final RegistryInfo.Static<RuleTestType<?>> RULE_TEST_TYPE = normal(PluralString.of("Rule Test Type"), Registries.RULE_TEST).vanilla(() -> BuiltInRegistries.RULE_TEST);
    public static final RegistryInfo.Static<PosRuleTestType<?>> POS_RULE_TEST = normal(PluralString.of("Position-based Rule Test Type"), Registries.POS_RULE_TEST).vanilla(() -> BuiltInRegistries.POS_RULE_TEST);
    public static final RegistryInfo.Static<GameEvent> GAME_EVENT = normal(PluralString.of("Game Event"), Registries.GAME_EVENT).vanilla(() -> BuiltInRegistries.GAME_EVENT);
    public static final RegistryInfo.Static<PositionSourceType<?>> POSITION_SOURCE_TYPE = normal(PluralString.of("Position Source Type"), Registries.POSITION_SOURCE_TYPE).vanilla(() -> BuiltInRegistries.POSITION_SOURCE_TYPE);
    public static final RegistryInfo.Static<VillagerType> VILLAGER_TYPE = normal(PluralString.of("Villager Type"), Registries.VILLAGER_TYPE).vanilla(() -> BuiltInRegistries.VILLAGER_TYPE);
    public static final RegistryInfo.Static<LootPoolEntryType> LOOT_POOL_ENTRY_TYPE = normal(PluralString.of("Loot Pool Entry Type"), Registries.LOOT_POOL_ENTRY_TYPE).vanilla(() -> BuiltInRegistries.LOOT_POOL_ENTRY_TYPE);
    public static final RegistryInfo.Static<LootItemFunctionType> LOOT_FUNCTION_TYPE = normal(PluralString.of("Loot Item Function Type"), Registries.LOOT_FUNCTION_TYPE).vanilla(() -> BuiltInRegistries.LOOT_FUNCTION_TYPE);
    public static final RegistryInfo.Static<LootItemConditionType> LOOT_CONDITION_TYPE = normal(PluralString.of("Loot Item Condition Type"), Registries.LOOT_CONDITION_TYPE).vanilla(() -> BuiltInRegistries.LOOT_CONDITION_TYPE);
    public static final RegistryInfo.Static<LootNumberProviderType> LOOT_NUMBER_PROVIDER_TYPE = normal(PluralString.of("Loot Number Provider Type"), Registries.LOOT_NUMBER_PROVIDER_TYPE).vanilla(() -> BuiltInRegistries.LOOT_NUMBER_PROVIDER_TYPE);
    public static final RegistryInfo.Static<LootNbtProviderType> LOOT_NBT_PROVIDER_TYPE = normal(PluralString.of("Loot NBT Provider Type"), Registries.LOOT_NBT_PROVIDER_TYPE).vanilla(() -> BuiltInRegistries.LOOT_NBT_PROVIDER_TYPE);
    public static final RegistryInfo.Static<LootScoreProviderType> LOOT_SCORE_PROVIDER_TYPE = normal(PluralString.of("Loot Score Provider Type"), Registries.LOOT_SCORE_PROVIDER_TYPE).vanilla(() -> BuiltInRegistries.LOOT_SCORE_PROVIDER_TYPE);
    public static final RegistryInfo.Dynamic<DimensionType> DIMENSION_TYPE = data(PluralString.of("Dimension Type"), Registries.DIMENSION_TYPE);
    public static final RegistryInfo.Static<Level> DIMENSION = normal(PluralString.of("Level"), Registries.DIMENSION);
    public static final RegistryInfo.Dynamic<LevelStem> LEVEL_STEM = data(PluralString.of("Level Stem"), Registries.LEVEL_STEM);

    public static final RegistryInfo.Static<FloatProviderType<?>> FLOAT_PROVIDER_TYPE = normal(PluralString.of("Float Provider Type"), Registries.FLOAT_PROVIDER_TYPE).vanilla(() -> BuiltInRegistries.FLOAT_PROVIDER_TYPE);
    public static final RegistryInfo.Static<IntProviderType<?>> INT_PROVIDER_TYPE = normal(PluralString.of("Integer Provider Type"), Registries.INT_PROVIDER_TYPE).vanilla(() -> BuiltInRegistries.INT_PROVIDER_TYPE);
    public static final RegistryInfo.Static<HeightProviderType<?>> HEIGHT_PROVIDER_TYPE = normal(PluralString.of("Height Provider Type"), Registries.HEIGHT_PROVIDER_TYPE).vanilla(() -> BuiltInRegistries.HEIGHT_PROVIDER_TYPE);
    public static final RegistryInfo.Static<BlockPredicateType<?>> BLOCK_PREDICATE_TYPE = normal(PluralString.of("Block Predicate Type"), Registries.BLOCK_PREDICATE_TYPE).vanilla(() -> BuiltInRegistries.BLOCK_PREDICATE_TYPE);

    public static final RegistryInfo.Dynamic<NoiseGeneratorSettings> NOISE_GENERATOR_SETTINGS = data(PluralString.of("Noise Generator Settings", false), Registries.NOISE_SETTINGS);
    public static final RegistryInfo.Dynamic<ConfiguredWorldCarver<?>> CONFIGURED_WORLD_CARVER = data(PluralString.of("Configured World Carver"), Registries.CONFIGURED_CARVER);
    public static final RegistryInfo.Dynamic<ConfiguredFeature<?, ?>> CONFIGURED_FEATURE = data(PluralString.of("Configured Feature"), Registries.CONFIGURED_FEATURE);
    public static final RegistryInfo.Dynamic<PlacedFeature> PLACED_FEATURE = data(PluralString.of("Placed Feature"), Registries.PLACED_FEATURE);
    public static final RegistryInfo.Dynamic<Structure> STRUCTURE = data(PluralString.of("Structure"), Registries.STRUCTURE);
    public static final RegistryInfo.Dynamic<StructureSet> STRUCTURE_SET = data(PluralString.of("Structure Set"), Registries.STRUCTURE_SET);
    public static final RegistryInfo.Dynamic<StructureProcessorList> STRUCTURE_PROCESSOR_LIST = data(PluralString.of("Structure Processor List"), Registries.PROCESSOR_LIST);
    public static final RegistryInfo.Dynamic<StructureTemplatePool> STRUCTURE_TEMPLATE_POOL = data(PluralString.of("Structure Template Pool"), Registries.TEMPLATE_POOL);
    public static final RegistryInfo.Dynamic<NormalNoise.NoiseParameters> NOISE = data(PluralString.of("Noise"), Registries.NOISE);
    public static final RegistryInfo.Dynamic<DensityFunction> DENSITY_FUNCTION = data(PluralString.of("Density Function"), Registries.DENSITY_FUNCTION);
    public static final RegistryInfo.Dynamic<WorldPreset> WORLD_PRESET = data(PluralString.of("World Preset"), Registries.WORLD_PRESET);
    public static final RegistryInfo.Dynamic<FlatLevelGeneratorPreset> FLAT_LEVEL_GENERATOR_PRESET = data(PluralString.of("Flat Level Generator Preset"), Registries.FLAT_LEVEL_GENERATOR_PRESET);

    public static final RegistryInfo.Static<StructurePlacementType<?>> STRUCTURE_PLACEMENT_TYPE = normal(PluralString.of("Structure Placement Type"), Registries.STRUCTURE_PLACEMENT).vanilla(() -> BuiltInRegistries.STRUCTURE_PLACEMENT);
    public static final RegistryInfo.Static<StructurePieceType> STRUCTURE_PIECE_TYPE = normal(PluralString.of("Structure Piece Type"), Registries.STRUCTURE_PIECE).vanilla(() -> BuiltInRegistries.STRUCTURE_PIECE);
    public static final RegistryInfo.Static<StructureType<?>> STRUCTURE_TYPE = normal(PluralString.of("Structure Type"), Registries.STRUCTURE_TYPE).vanilla(() -> BuiltInRegistries.STRUCTURE_TYPE);
    public static final RegistryInfo.Static<PlacementModifierType<?>> PLACEMENT_MODIFIER_TYPE = normal(PluralString.of("Placement Modifier Type"), Registries.PLACEMENT_MODIFIER_TYPE).vanilla(() -> BuiltInRegistries.PLACEMENT_MODIFIER_TYPE);

    public static final RegistryInfo.Static<TrunkPlacerType<?>> TRUNK_PLACER_TYPE = normal(PluralString.of("Trunk Placer Type"), Registries.TRUNK_PLACER_TYPE).vanilla(() -> BuiltInRegistries.TRUNK_PLACER_TYPE);
    public static final RegistryInfo.Static<RootPlacerType<?>> ROOT_PLACER_TYPE = normal(PluralString.of("Root Placer Type"), Registries.ROOT_PLACER_TYPE).vanilla(() -> BuiltInRegistries.ROOT_PLACER_TYPE);
    public static final RegistryInfo.Static<FeatureSizeType<?>> FEATURE_SIZE_TYPE = normal(PluralString.of("Feature Size Type"), Registries.FEATURE_SIZE_TYPE).vanilla(() -> BuiltInRegistries.FEATURE_SIZE_TYPE);
    public static final RegistryInfo.Static<Codec<? extends BiomeSource>> BIOME_SOURCE_SERIALIZER = normal(PluralString.of("Biome Source Serializer"), Registries.BIOME_SOURCE).vanilla(() -> BuiltInRegistries.BIOME_SOURCE);
    public static final RegistryInfo.Static<Codec<? extends ChunkGenerator>> CHUNK_GENERATOR = normal(PluralString.of("Chunk Generator Serializer"), Registries.CHUNK_GENERATOR).vanilla(() -> BuiltInRegistries.CHUNK_GENERATOR);
    public static final RegistryInfo.Static<Codec<? extends SurfaceRules.ConditionSource>> MATERIAL_CONDITION_SOURCE = normal(PluralString.of("Condition Source Serializer"), Registries.MATERIAL_CONDITION).vanilla(() -> BuiltInRegistries.MATERIAL_CONDITION);
    public static final RegistryInfo.Static<Codec<? extends SurfaceRules.RuleSource>> MATERIAL_RULE_SOURCE = normal(PluralString.of("Rule Source Serializer"), Registries.MATERIAL_RULE).vanilla(() -> BuiltInRegistries.MATERIAL_RULE);
    public static final RegistryInfo.Static<Codec<? extends DensityFunction>> DENSITY_FUNCTION_TYPE = normal(PluralString.of("Density Function Serializer"), Registries.DENSITY_FUNCTION_TYPE).vanilla(() -> BuiltInRegistries.DENSITY_FUNCTION_TYPE);
    public static final RegistryInfo.Static<StructureProcessorType<?>> STRUCTURE_PROCESSOR_TYPE = normal(PluralString.of("Structure Processor Type"), Registries.STRUCTURE_PROCESSOR).vanilla(() -> BuiltInRegistries.STRUCTURE_PROCESSOR);
    public static final RegistryInfo.Static<StructurePoolElementType<?>> STRUCTURE_POOL_ELEMENT_TYPE = normal(PluralString.of("Structure Pool Element Type"), Registries.STRUCTURE_POOL_ELEMENT).vanilla(() -> BuiltInRegistries.STRUCTURE_POOL_ELEMENT);

    public static final RegistryInfo.Dynamic<ChatType> CHAT_TYPE = data(PluralString.of("Chat Type"), Registries.CHAT_TYPE);
    public static final RegistryInfo.Static<CatVariant> CAT_VARIANT = normal(PluralString.of("Cat Variant"), Registries.CAT_VARIANT).vanilla(() -> BuiltInRegistries.CAT_VARIANT);
    public static final RegistryInfo.Static<FrogVariant> FROG_VARIANT = normal(PluralString.of("Frog Variant"), Registries.FROG_VARIANT).vanilla(() -> BuiltInRegistries.FROG_VARIANT);
    public static final RegistryInfo.Static<BannerPattern> BANNER_PATTERN = normal(PluralString.of("Banner Pattern"), Registries.BANNER_PATTERN).vanilla(() -> BuiltInRegistries.BANNER_PATTERN);
    public static final RegistryInfo.Static<Instrument> INSTRUMENT = normal(PluralString.of("Instrument"), Registries.INSTRUMENT).vanilla(() -> BuiltInRegistries.INSTRUMENT);

    // New in 1.19.4
    public static final RegistryInfo.Static<String> DECORATED_POT_PATTERN = normal(PluralString.of("Decorated Pot Pattern"), Registries.DECORATED_POT_PATTERNS).vanilla(() -> BuiltInRegistries.DECORATED_POT_PATTERNS);
    public static final RegistryInfo.Dynamic<DamageType> DAMAGE_TYPE = data(PluralString.of("Damage Type"), Registries.DAMAGE_TYPE);
    public static final RegistryInfo.Dynamic<TrimMaterial> TRIM_MATERIAL = data(PluralString.of("Armor Trim Material"), Registries.TRIM_MATERIAL);
    public static final RegistryInfo.Dynamic<TrimPattern> TRIM_PATTERN = data(PluralString.of("Armor Trim Pattern"), Registries.TRIM_PATTERN);
    public static final RegistryInfo.Dynamic<MultiNoiseBiomeSourceParameterList> MULTI_NOISE_BIOME_SOURCE_PARAMETER_LIST = data(PluralString.of("Multi Noise Biome Source Parameter List"), Registries.MULTI_NOISE_BIOME_SOURCE_PARAMETER_LIST);

    // Static registries (maintained by cmreg)
    public static final RegistryInfo.Custom<CreativeModeTab> CREATIVE_MODE_TAB = staticImpl(PluralString.of("Creative Mode Tab"), CustomResourceRegister.CreativeModeTabs::new);

    @SuppressWarnings("unchecked")
    public static <T> @Nullable RegistryInfo<T, ?> get(ResourceKey<? extends Registry<T>> registryKey) {
        return (RegistryInfo<T, ?>) ALL.get(registryKey);
    }
}
