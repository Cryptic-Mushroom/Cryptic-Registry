package com.crypticmushroom.minecraft.registry;

import org.jetbrains.annotations.ApiStatus.Internal;

/**
 * Contains various important pieces of information about the instance of Cryptic Registry.
 *
 * @author Jonathing
 * @author Shadew
 */
@Internal
public final class CrypticRegistryInfo {
    /**
     * The Mod ID of Cryptic Registry, which is fixed to {@code cryptic_registry}.
     */
    public static final String MOD_ID = "cryptic_registry";

    /**
     * The Mod Name of the Cryptic Registry, which is fixed to 'Cryptic Registry'.
     */
    public static final String NAME = "Cryptic Registry";
}
