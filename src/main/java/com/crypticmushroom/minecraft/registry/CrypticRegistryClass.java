package com.crypticmushroom.minecraft.registry;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <h1>Static Class-Based Registration</h1>
 * <p>
 * Because of Cryptic Registry's static nature, it is imperative that we keep track of vital states and values during
 * static initialization and the creation of registry objects. This class exists to solve that problem.
 * <p>
 * In short, this class holds a single string, which <strong>absolutely must be</strong> the relevant mod's mod ID. If
 * it isn't terrible things may or may not happen. From a class annotated with this annotation, creating
 * {@link net.minecraftforge.registries.DeferredRegister deferred} registers will now automatically be tied to the mod
 * ID held within the annotation. Additionally, when a class is added through one of the methods in
 * {@link CrypticRegistry} (and related methods that take a class), that class will now hold the mod ID as well. This
 * means that any {@link com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder registry object builders}
 * created will inherit the relevant mod ID as well.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CrypticRegistryClass {
    /**
     * This value should be the mod info of the relevant mod that this class will be annotated for.
     *
     * @return The mod's info.
     */
    String modId();

    String name();
}
