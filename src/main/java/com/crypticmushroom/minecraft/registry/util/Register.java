package com.crypticmushroom.minecraft.registry.util;

import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;

public interface Register<T> {
    ResourceKey<? extends Registry<T>> getRegistryKey();

    default ResourceLocation getRegistryName() {
        return this.getRegistryKey().location();
    }
}
