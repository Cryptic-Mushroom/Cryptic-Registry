package com.crypticmushroom.minecraft.registry.util;

import com.mojang.datafixers.util.Pair;

public final class PluralString extends Pair<String, String> {
    public static PluralString of(String s) {
        return of(s, true);
    }

    public static PluralString of(String s, boolean appendS) {
        return new PluralString(s, appendS ? s.concat("s") : s);
    }

    public static PluralString of(String singular, String plural) {
        return new PluralString(singular, plural);
    }

    private PluralString(String singular, String plural) {
        super(singular, plural);
    }

    public String singular() {
        return this.getFirst();
    }

    public String plural() {
        return this.getSecond();
    }

    @Override
    public String toString() {
        return "%s/%s".formatted(this.singular(), this.plural());
    }
}
