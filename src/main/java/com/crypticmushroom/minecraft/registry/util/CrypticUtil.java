package com.crypticmushroom.minecraft.registry.util;

import net.minecraft.resources.ResourceLocation;

public final class CrypticUtil {
    /**
     * Copy of {@link net.minecraft.Util#makeDescriptionId(String, ResourceLocation)} because I am traumatized from
     * early classloading.
     */
    public static String makeDescriptionId(String type, ResourceLocation id) {
        return type + "." + id.getNamespace() + "." + id.getPath().replace('/', '.');
    }

    private CrypticUtil() { }
}
