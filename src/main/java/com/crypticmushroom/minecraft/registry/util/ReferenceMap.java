package com.crypticmushroom.minecraft.registry.util;

import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceLocation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class ReferenceMap<K, V> extends HashMap<ResourceLocation, V> {
    private HashMap<Holder<K>, V> preliminaryMap = new HashMap<>();
    private boolean loaded = false;

    private void load() {
        this.preliminaryMap.forEach((k, v) -> super.put(k.unwrapKey().get().location(), v));
        this.preliminaryMap = null;

        this.loaded = true;
    }

    @Override
    public int size() {
        return this.loaded ? super.size() : this.preliminaryMap.size();
    }

    @Override
    public boolean isEmpty() {
        return this.loaded ? super.isEmpty() : this.preliminaryMap.isEmpty();
    }

    @Override
    public V get(Object key) {
        if (!this.loaded) this.load();

        return super.get(key);
    }

    public boolean containsKey(Holder<K> key) {
        if (!this.loaded) {
            return this.preliminaryMap.containsKey(key);
        } else {
            return key.kind() == Holder.Kind.REFERENCE && super.containsKey(key.unwrapKey().get().location());
        }
    }

    @Override
    public boolean containsKey(Object key) {
        if (!this.loaded) this.load();

        return super.containsKey(key);
    }

    public V put(Holder<K> key, V value) {
        if (this.loaded) {
            throw new IllegalStateException("Cannot add holders to a loaded reference map");
        }

        return this.preliminaryMap.put(key, value);
    }

    @Override
    public V put(ResourceLocation key, V value) {
        if (!this.loaded) this.load();

        return super.put(key, value);
    }

    @Override
    public void putAll(Map<? extends ResourceLocation, ? extends V> m) {
        if (!this.loaded) this.load();

        super.putAll(m);
    }

    @Override
    public V remove(Object key) {
        if (!this.loaded) this.load();

        return super.remove(key);
    }

    /**
     * @deprecated This method is identical to {@link HashMap#clear()}. If you want to reset the reference map, use
     *     {@link #reset()}.
     */
    @Deprecated
    @Override
    public void clear() {
        if (!this.loaded) this.load();

        super.clear();
    }

    public void reset() {
        super.clear();

        this.loaded = false;
        if (this.preliminaryMap == null) {
            this.preliminaryMap = new HashMap<>();
        }
    }

    @Override
    public boolean containsValue(Object value) {
        return this.loaded ? super.containsValue(value) : this.preliminaryMap.containsValue(value);
    }

    @Override
    public Set<ResourceLocation> keySet() {
        if (!this.loaded) this.load();

        return super.keySet();
    }

    @Override
    public Collection<V> values() {
        return this.loaded ? super.values() : this.preliminaryMap.values();
    }

    @Override
    public Set<Entry<ResourceLocation, V>> entrySet() {
        if (!this.loaded) this.load();

        return super.entrySet();
    }

    @Override
    public V getOrDefault(Object key, V defaultValue) {
        if (!this.loaded) this.load();

        return super.getOrDefault(key, defaultValue);
    }

    @Override
    public V putIfAbsent(ResourceLocation key, V value) {
        if (!this.loaded) this.load();

        return super.putIfAbsent(key, value);
    }

    @Override
    public boolean remove(Object key, Object value) {
        if (!this.loaded) this.load();

        return super.remove(key, value);
    }

    @Override
    public boolean replace(ResourceLocation key, V oldValue, V newValue) {
        if (!this.loaded) this.load();

        return super.replace(key, oldValue, newValue);
    }

    @Override
    public V replace(ResourceLocation key, V value) {
        if (!this.loaded) this.load();

        return super.replace(key, value);
    }

    @Override
    public V computeIfAbsent(ResourceLocation key, Function<? super ResourceLocation, ? extends V> mappingFunction) {
        if (!this.loaded) this.load();

        return super.computeIfAbsent(key, mappingFunction);
    }

    @Override
    public V computeIfPresent(ResourceLocation key, BiFunction<? super ResourceLocation, ? super V, ? extends V> remappingFunction) {
        if (!this.loaded) this.load();

        return super.computeIfPresent(key, remappingFunction);
    }

    @Override
    public V compute(ResourceLocation key, BiFunction<? super ResourceLocation, ? super V, ? extends V> remappingFunction) {
        if (!this.loaded) this.load();

        return super.compute(key, remappingFunction);
    }

    @Override
    public V merge(ResourceLocation key, V value, BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
        if (!this.loaded) this.load();

        return super.merge(key, value, remappingFunction);
    }

    @Override
    public void forEach(BiConsumer<? super ResourceLocation, ? super V> action) {
        if (!this.loaded) this.load();

        super.forEach(action);
    }

    @Override
    public void replaceAll(BiFunction<? super ResourceLocation, ? super V, ? extends V> function) {
        if (!this.loaded) this.load();

        super.replaceAll(function);
    }

    @Override
    public Object clone() {
        throw new IllegalStateException(new CloneNotSupportedException("Reference maps are not cloneable"));
    }
}
