package com.crypticmushroom.minecraft.registry.util;

public interface Type<T> {
    static <T> Type<T> of(Class<T> clazz) {
        return of();
    }

    static <T> Type<T> of() {
        return new Type<>() { };
    }
}
