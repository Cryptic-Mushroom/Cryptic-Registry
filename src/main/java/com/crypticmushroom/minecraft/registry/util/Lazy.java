package com.crypticmushroom.minecraft.registry.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public interface Lazy<T> extends net.minecraftforge.common.util.Lazy<T> {
    boolean isResolved();

    /**
     * Constructs a lazy-initialized object
     *
     * @param supplier The supplier for the value, to be called the first time the value is needed.
     */
    static <T> Lazy<T> of(@NotNull Supplier<T> supplier) {
        return new Fast<>(supplier);
    }

    /**
     * Constructs a thread-safe lazy-initialized object
     *
     * @param supplier The supplier for the value, to be called the first time the value is needed.
     */
    static <T> Lazy<T> concurrentOf(@NotNull Supplier<T> supplier) {
        return new Concurrent<>(supplier);
    }

    /**
     * Non-thread-safe implementation.
     *
     * @see net.minecraftforge.common.util.Lazy.Fast
     */
    class Fast<@Nullable T> implements Lazy<T> {
        private Supplier<@Nullable T> supplier;
        private @Nullable T instance;

        protected Fast(Supplier<@Nullable T> supplier) {
            this.supplier = supplier;
        }

        @Override
        public @Nullable T get() {
            if (this.supplier != null) {
                this.instance = this.supplier.get();
                this.supplier = null;
            }
            return this.instance;
        }

        @Override
        public boolean isResolved() {
            return this.supplier == null;
        }
    }

    /**
     * Thread-safe implementation.
     *
     * @see net.minecraftforge.common.util.Lazy.Concurrent
     */
    class Concurrent<@Nullable T> implements Lazy<T> {
        private volatile Object lock = new Object();
        private volatile Supplier<@Nullable T> supplier;
        private volatile @Nullable T instance;

        protected Concurrent(Supplier<@Nullable T> supplier) {
            this.supplier = supplier;
        }

        @Override
        public @Nullable T get() {
            // Copy the lock to a local variable to prevent NPEs if the lock field is set to null between the
            // null-check and the synchronization
            Object localLock = this.lock;
            if (this.supplier != null) {
                // localLock is not null here because supplier was non-null after we copied the lock and both of them
                // are volatile
                synchronized (localLock) {
                    if (this.supplier != null) {
                        this.instance = this.supplier.get();
                        this.supplier = null;
                        this.lock = null;
                    }
                }
            }
            return this.instance;
        }

        @Override
        public boolean isResolved() {
            return this.supplier == null;
        }
    }
}

