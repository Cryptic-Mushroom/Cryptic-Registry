package com.crypticmushroom.minecraft.registry.data.provider.model;

import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import com.crypticmushroom.minecraft.registry.data.provider.model.ICrypticModelProvider.RenderType;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.ButtonBlock;
import net.minecraft.world.level.block.CeilingHangingSignBlock;
import net.minecraft.world.level.block.CrossCollisionBlock;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.IronBarsBlock;
import net.minecraft.world.level.block.PressurePlateBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.WallBlock;
import net.minecraft.world.level.block.WallHangingSignBlock;
import net.minecraft.world.level.block.WallSignBlock;
import net.minecraft.world.level.block.WeightedPressurePlateBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.client.model.generators.BlockModelBuilder;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ConfiguredModel;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.client.model.generators.MultiPartBlockStateBuilder;
import net.minecraftforge.client.model.generators.VariantBlockStateBuilder;
import net.minecraftforge.data.event.GatherDataEvent;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * This abstract block state and model provider is, essentially, an extension to the normal {@link BlockStateProvider}
 * that provides additional features like proper usage of {@link net.minecraft.client.renderer.RenderType render types},
 * usage of {@link Supplier suppliers} over static objects, and a much more concise notation for dictating the objects
 * to generate. For a clear example and additional info, see the wiki.
 *
 * @author Jonathing
 */
public class CrypticBlockStateProvider<B extends CrypticBlockModelProvider, I extends CrypticItemModelProvider> extends BlockStateProvider implements ICrypticDataProvider {
    private final GatherDataEvent event;
    private final String modId;
    private final B blockModels;
    private final I itemModels;

    @SuppressWarnings("unchecked")
    public CrypticBlockStateProvider(String modId, GatherDataEvent event) {
        this(
            modId,
            event,
            (B) new CrypticBlockModelProvider(modId, event) {
                @Override
                protected void registerModels() { }
            },
            (I) new CrypticItemModelProvider(modId, event) {
                @Override
                protected void registerModels() { }
            }
        );
    }

    public CrypticBlockStateProvider(String modId, GatherDataEvent event, B blockModels, I itemModels) {
        super(event.getGenerator().getPackOutput(), modId, event.getExistingFileHelper());

        this.event = event;
        this.modId = modId;
        this.blockModels = blockModels;
        this.itemModels = itemModels;

        this.blockModels.dummy = true;
        this.itemModels.dummy = true;
    }

    @Override
    protected void registerStatesAndModels() {
        DataRegistry.applyBlockstateBuilders(this.modId, this);
    }

    @Override
    public String getName() {
        return ICrypticDataProvider.super.getName();
    }

    @Override
    public String getSimpleName() {
        return "Block States and Models";
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public boolean shouldRun() {
        return this.event.includeClient();
    }

    @Override
    public B models() {
        return this.blockModels;
    }

    @Override
    public I itemModels() {
        return this.itemModels;
    }


    /*
     * HELPER CLASSES
     */

    public record StandingAndWallStates(VariantBlockStateBuilder standing, VariantBlockStateBuilder wall) { }


    /*
     * BLOCK STATE GENERATION
     */

    /* WEIGHTED PRESSURE PLATES */

    public final VariantBlockStateBuilder weightedPressurePlateBlock(Supplier<? extends WeightedPressurePlateBlock> block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.weightedPressurePlateBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder weightedPressurePlateBlock(Supplier<? extends WeightedPressurePlateBlock> block, Block texture, RenderType renderType) {
        return this.weightedPressurePlateBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder weightedPressurePlateBlock(Supplier<? extends WeightedPressurePlateBlock> block, ResourceLocation texture, RenderType renderType) {
        return this.weightedPressurePlateBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder weightedPressurePlateBlock(Supplier<? extends WeightedPressurePlateBlock> block, Supplier<? extends Block> texture) {
        return this.weightedPressurePlateBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder weightedPressurePlateBlock(Supplier<? extends WeightedPressurePlateBlock> block, Block texture) {
        return this.weightedPressurePlateBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder weightedPressurePlateBlock(Supplier<? extends WeightedPressurePlateBlock> block, ResourceLocation texture) {
        var b = block.get();
        this.weightedPressurePlateBlock(b, texture);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder weightedPressurePlateBlock(WeightedPressurePlateBlock block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.weightedPressurePlateBlock(block, texture.get(), renderType);
    }

    public final VariantBlockStateBuilder weightedPressurePlateBlock(WeightedPressurePlateBlock block, Block texture, RenderType renderType) {
        this.weightedPressurePlateBlock(block, this.texture(texture), renderType);
        return this.getVariantBuilder(block);
    }

    public VariantBlockStateBuilder weightedPressurePlateBlock(WeightedPressurePlateBlock block, ResourceLocation texture, RenderType renderType) {
        var pressurePlate = this.models().pressurePlate(this.name(block), texture).renderType(renderType);
        var pressurePlateDown = this.models().pressurePlateDown(this.name(block) + "_down", texture).renderType(renderType);
        return this.weightedPressurePlateBlock(block, pressurePlate, pressurePlateDown);
    }

    public final VariantBlockStateBuilder weightedPressurePlateBlock(WeightedPressurePlateBlock block, Supplier<? extends Block> texture) {
        return this.weightedPressurePlateBlock(block, texture.get());
    }

    public final VariantBlockStateBuilder weightedPressurePlateBlock(WeightedPressurePlateBlock block, Block texture) {
        this.weightedPressurePlateBlock(block, this.texture(texture));
        return this.getVariantBuilder(block);
    }

    public VariantBlockStateBuilder weightedPressurePlateBlock(WeightedPressurePlateBlock block, ResourceLocation texture) {
        var pressurePlate = this.models().pressurePlate(this.name(block), texture);
        var pressurePlateDown = this.models().pressurePlateDown(this.name(block) + "_down", texture);
        return this.weightedPressurePlateBlock(block, pressurePlate, pressurePlateDown);
    }


    public final VariantBlockStateBuilder weightedPressurePlateBlock(Supplier<? extends WeightedPressurePlateBlock> block, ModelFile pressurePlate, ModelFile pressurePlateDown) {
        var b = block.get();
        this.weightedPressurePlateBlock(b, pressurePlate, pressurePlateDown);
        return this.getVariantBuilder(b);
    }

    public VariantBlockStateBuilder weightedPressurePlateBlock(WeightedPressurePlateBlock block, ModelFile pressurePlate, ModelFile pressurePlateDown) {
        var builder = this.getVariantBuilder(block);
        WeightedPressurePlateBlock.POWER.getPossibleValues().forEach(i -> builder.partialState().with(WeightedPressurePlateBlock.POWER, i).addModels(new ConfiguredModel(i > 0 ? pressurePlateDown : pressurePlate)));
        return builder;
    }


    /* HANGING SIGNS */

    public final StandingAndWallStates hangingSignBlock(Supplier<? extends CeilingHangingSignBlock> hangingSignBlock, Supplier<? extends WallHangingSignBlock> wallHangingSignBlock, Supplier<? extends Block> texture) {
        return this.hangingSignBlock(hangingSignBlock.get(), wallHangingSignBlock.get(), texture);
    }

    public final StandingAndWallStates hangingSignBlock(Supplier<? extends CeilingHangingSignBlock> hangingSignBlock, Supplier<? extends WallHangingSignBlock> wallHangingSignBlock, Block texture) {
        return this.hangingSignBlock(hangingSignBlock.get(), wallHangingSignBlock.get(), texture);
    }

    public final StandingAndWallStates hangingSignBlock(Supplier<? extends CeilingHangingSignBlock> hangingSignBlock, Supplier<? extends WallHangingSignBlock> wallHangingSignBlock, ResourceLocation texture) {
        return this.hangingSignBlock(hangingSignBlock.get(), wallHangingSignBlock.get(), texture);
    }

    public final StandingAndWallStates hangingSignBlock(CeilingHangingSignBlock hangingSignBlock, WallHangingSignBlock wallHangingSignBlock, Supplier<? extends Block> texture) {
        return this.hangingSignBlock(hangingSignBlock, wallHangingSignBlock, texture.get());
    }

    public final StandingAndWallStates hangingSignBlock(CeilingHangingSignBlock hangingSignBlock, WallHangingSignBlock wallHangingSignBlock, Block texture) {
        this.hangingSignBlock(hangingSignBlock, wallHangingSignBlock, this.texture(texture));
        return new StandingAndWallStates(this.getVariantBuilder(hangingSignBlock), this.getVariantBuilder(wallHangingSignBlock));
    }

    public StandingAndWallStates hangingSignBlock(CeilingHangingSignBlock signBlock, WallHangingSignBlock wallHangingSignBlock, ResourceLocation texture) {
        var sign = this.models().sign(this.name(signBlock), texture);
        return this.hangingSignBlock(signBlock, wallHangingSignBlock, sign);
    }


    public final StandingAndWallStates hangingSignBlock(Supplier<? extends CeilingHangingSignBlock> hangingSignBlock, Supplier<? extends WallHangingSignBlock> wallHangingSignBlock, ModelFile sign) {
        return this.hangingSignBlock(hangingSignBlock.get(), wallHangingSignBlock.get(), sign);
    }

    public StandingAndWallStates hangingSignBlock(CeilingHangingSignBlock signBlock, WallHangingSignBlock wallHangingSignBlock, ModelFile sign) {
        this.simpleBlock(signBlock, sign);
        this.simpleBlock(wallHangingSignBlock, sign);
        return new StandingAndWallStates(this.getVariantBuilder(signBlock), this.getVariantBuilder(wallHangingSignBlock));
    }


    /*
     * HELPER METHODS
     */

    /** @see ICrypticModelProvider#key(Supplier) */
    public ResourceLocation key(Supplier<? extends ItemLike> block) {
        return this.models().key(block);
    }

    /** @see ICrypticModelProvider#key(ItemLike) */
    public ResourceLocation key(ItemLike block) {
        return this.models().key(block);
    }

    /** @see ICrypticModelProvider#name(Supplier, String, String) */
    public String name(Supplier<? extends Block> block, @Nullable String append, String removeSuffix) {
        return this.models().name(block, append, removeSuffix);
    }

    /** @see ICrypticModelProvider#name(Supplier, String) */
    public String name(Supplier<? extends Block> block, @Nullable String append) {
        return this.models().name(block, append);
    }

    /** @see ICrypticModelProvider#name(Supplier) */
    public String name(Supplier<? extends Block> block) {
        return this.models().name(block);
    }

    /** @see ICrypticModelProvider#name(Supplier, String, String) */
    public String name(Block block, @Nullable String append, String removeSuffix) {
        return this.models().name(block, append, removeSuffix);
    }

    /** @see ICrypticModelProvider#name(Supplier, String) */
    public String name(Block block, @Nullable String append) {
        return this.models().name(block, append);
    }

    /** @see ICrypticModelProvider#name(ItemLike) */
    public String name(Block block) {
        return this.models().name(block);
    }

    /** @see ICrypticModelProvider#texture(Supplier, String, String) */
    public ResourceLocation texture(Supplier<? extends Block> block, @Nullable String append, String removeSuffix) {
        return this.models().texture(block, append, removeSuffix);
    }

    /** @see ICrypticModelProvider#texture(Supplier, String) */
    public ResourceLocation texture(Supplier<? extends Block> block, @Nullable String append) {
        return this.models().texture(block, append);
    }

    /** @see ICrypticModelProvider#texture(Supplier) */
    public ResourceLocation texture(Supplier<? extends Block> block) {
        return this.models().texture(block);
    }

    /** @see ICrypticModelProvider#texture(ItemLike, String, String) */
    public ResourceLocation texture(Block block, @Nullable String append, String removeSuffix) {
        return this.models().texture(block, append, removeSuffix);
    }

    /** @see ICrypticModelProvider#texture(ItemLike, String) */
    public ResourceLocation texture(Block block, @Nullable String append) {
        return this.models().texture(block, append);
    }

    /** @see ICrypticModelProvider#texture(ItemLike) */
    public ResourceLocation texture(Block block) {
        return this.models().texture(block);
    }

    /** @see ICrypticModelProvider#blockFolder(String, boolean) */
    public ResourceLocation blockFolder(String name, boolean mod) {
        return this.models().blockFolder(name, mod);
    }

    /** @see ICrypticModelProvider#blockFolder(ResourceLocation) */
    public ResourceLocation blockFolder(ResourceLocation name) {
        return this.models().blockFolder(name);
    }


    // The rest of this class, from this line on, is vanilla methods and implementations of BlockStateProvider.

    /*
     * VANILLA BLOCK STATE GENERATION
     */

    public final VariantBlockStateBuilder simpleBlock(Supplier<? extends Block> block, ModelFile model) {
        this.simpleBlock(block.get(), model);
        return this.getVariantBuilder(block.get());
    }

    @Override
    public void simpleBlock(Block block, ModelFile model) {
        super.simpleBlock(block, model);
    }


    public final VariantBlockStateBuilder simpleBlock(Supplier<? extends Block> block) {
        this.simpleBlock(block.get());
        return this.getVariantBuilder(block.get());
    }

    @Override
    public void simpleBlock(Block block) {
        super.simpleBlock(block);
    }


    public final VariantBlockStateBuilder simpleBlock(Supplier<? extends Block> block, Function<ModelFile, ConfiguredModel[]> expander) {
        this.simpleBlock(block.get(), expander);
        return this.getVariantBuilder(block.get());
    }

    public final VariantBlockStateBuilder simpleBlock(Supplier<? extends Block> block, ConfiguredModel... models) {
        this.simpleBlock(block.get(), models);
        return this.getVariantBuilder(block.get());
    }

    @Override
    public void simpleBlock(Block block, Function<ModelFile, ConfiguredModel[]> expander) {
        super.simpleBlock(block, expander);
    }

    @Override
    public void simpleBlock(Block block, ConfiguredModel... models) {
        super.simpleBlock(block, models);
    }


    @Override
    @Deprecated
    public void simpleBlockItem(Block block, ModelFile model) {
        super.simpleBlockItem(block, model);
    }

    @Override
    @Deprecated
    public void simpleBlockWithItem(Block block, ModelFile model) {
        super.simpleBlockWithItem(block, model);
    }


    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, RenderType renderType) {
        return this.axisBlock(block.get(), renderType);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block) {
        var b = block.get();
        this.axisBlock(b);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, RenderType renderType) {
        this.axisBlockWithRenderType(block, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void axisBlockWithRenderType(RotatedPillarBlock block, String renderType) {
        super.axisBlockWithRenderType(block, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void axisBlockWithRenderType(RotatedPillarBlock block, ResourceLocation renderType) {
        super.axisBlockWithRenderType(block, renderType);
    }

    @Override
    public void axisBlock(RotatedPillarBlock block) {
        super.axisBlock(block);
    }


    public final VariantBlockStateBuilder logBlock(Supplier<? extends RotatedPillarBlock> block, RenderType renderType) {
        return this.logBlock(block.get(), renderType);
    }

    public final VariantBlockStateBuilder logBlock(Supplier<? extends RotatedPillarBlock> block) {
        var b = block.get();
        this.logBlock(b);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder logBlock(RotatedPillarBlock block, RenderType renderType) {
        this.logBlockWithRenderType(block, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void logBlockWithRenderType(RotatedPillarBlock block, String renderType) {
        super.logBlockWithRenderType(block, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void logBlockWithRenderType(RotatedPillarBlock block, ResourceLocation renderType) {
        super.logBlockWithRenderType(block, renderType);
    }

    @Override
    public void logBlock(RotatedPillarBlock block) {
        super.logBlock(block);
    }


    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.axisBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Block texture, RenderType renderType) {
        return this.axisBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, ResourceLocation texture, RenderType renderType) {
        return this.axisBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Supplier<? extends Block> texture) {
        return this.axisBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Block texture) {
        return this.axisBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, ResourceLocation texture) {
        var b = block.get();
        this.axisBlock(b, texture);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.axisBlock(block, texture.get(), renderType);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Block texture, RenderType renderType) {
        return this.axisBlock(block, this.texture(texture), renderType);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, ResourceLocation texture, RenderType renderType) {
        this.axisBlockWithRenderType(block, texture, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void axisBlockWithRenderType(RotatedPillarBlock block, ResourceLocation texture, String renderType) {
        super.axisBlockWithRenderType(block, texture, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void axisBlockWithRenderType(RotatedPillarBlock block, ResourceLocation texture, ResourceLocation renderType) {
        super.axisBlockWithRenderType(block, texture, renderType);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Supplier<? extends Block> texture) {
        return this.axisBlock(block, texture.get());
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Block texture) {
        this.axisBlock(block, this.texture(texture));
        return this.getVariantBuilder(block);
    }

    @Override
    public void axisBlock(RotatedPillarBlock block, ResourceLocation texture) {
        super.axisBlock(block, texture);
    }


    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Supplier<? extends Block> side, Supplier<? extends Block> end, RenderType renderType) {
        return this.axisBlock(block.get(), side, end, renderType);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Supplier<? extends Block> side, Block end, RenderType renderType) {
        return this.axisBlock(block.get(), side, end, renderType);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Block side, Supplier<? extends Block> end, RenderType renderType) {
        return this.axisBlock(block.get(), side, end, renderType);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Block side, Block end, RenderType renderType) {
        return this.axisBlock(block.get(), side, end, renderType);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, ResourceLocation side, ResourceLocation end, RenderType renderType) {
        return this.axisBlock(block.get(), side, end, renderType);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Supplier<? extends Block> side, Supplier<? extends Block> end) {
        return this.axisBlock(block.get(), side, end);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Supplier<? extends Block> side, Block end) {
        return this.axisBlock(block.get(), side, end);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Block side, Supplier<? extends Block> end) {
        return this.axisBlock(block.get(), side, end);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, Block side, Block end) {
        return this.axisBlock(block.get(), side, end);
    }

    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, ResourceLocation side, ResourceLocation end) {
        var b = block.get();
        this.axisBlock(b, side, end);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Supplier<? extends Block> side, Supplier<? extends Block> end, RenderType renderType) {
        return this.axisBlock(block, side.get(), end.get(), renderType);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Supplier<? extends Block> side, Block end, RenderType renderType) {
        return this.axisBlock(block, side.get(), end, renderType);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Block side, Supplier<? extends Block> end, RenderType renderType) {
        return this.axisBlock(block, side, end.get(), renderType);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Block side, Block end, RenderType renderType) {
        return this.axisBlock(block, this.texture(side), this.texture(end), renderType);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, ResourceLocation side, ResourceLocation end, RenderType renderType) {
        this.axisBlockWithRenderType(block, side, end, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void axisBlockWithRenderType(RotatedPillarBlock block, ResourceLocation side, ResourceLocation end, String renderType) {
        super.axisBlockWithRenderType(block, side, end, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void axisBlockWithRenderType(RotatedPillarBlock block, ResourceLocation side, ResourceLocation end, ResourceLocation renderType) {
        super.axisBlockWithRenderType(block, side, end, renderType);
    }


    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Supplier<? extends Block> side, Supplier<? extends Block> end) {
        return this.axisBlock(block, side.get(), end.get());
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Supplier<? extends Block> side, Block end) {
        return this.axisBlock(block, side.get(), end);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Block side, Supplier<? extends Block> end) {
        return this.axisBlock(block, side, end.get());
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, Block side, Block end) {
        this.axisBlock(block, this.texture(side), this.texture(end));
        return this.getVariantBuilder(block);
    }

    @Override
    public void axisBlock(RotatedPillarBlock block, ResourceLocation side, ResourceLocation end) {
        super.axisBlock(block, side, end);
    }


    public final VariantBlockStateBuilder axisBlock(Supplier<? extends RotatedPillarBlock> block, ModelFile vertical, ModelFile horizontal) {
        var b = block.get();
        this.axisBlock(b, vertical, horizontal);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder axisBlock(RotatedPillarBlock block, BlockModelBuilder vertical, BlockModelBuilder horizontal) {
        this.axisBlock(block, (ModelFile) vertical, (ModelFile) horizontal);
        return this.getVariantBuilder(block);
    }

    public void axisBlock(RotatedPillarBlock block, ModelFile vertical, ModelFile horizontal) {
        super.axisBlock(block, vertical, horizontal);
    }


    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Supplier<? extends Block> side, Supplier<? extends Block> front, Supplier<? extends Block> top, RenderType renderType) {
        return this.horizontalBlock(block.get(), side, front, top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Supplier<? extends Block> side, Supplier<? extends Block> front, Block top, RenderType renderType) {
        return this.horizontalBlock(block.get(), side, front, top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Block side, Supplier<? extends Block> front, Supplier<? extends Block> top, RenderType renderType) {
        return this.horizontalBlock(block.get(), side, front, top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Supplier<? extends Block> side, Block front, Supplier<? extends Block> top, RenderType renderType) {
        return this.horizontalBlock(block.get(), side, front, top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Supplier<? extends Block> side, Block front, Block top, RenderType renderType) {
        return this.horizontalBlock(block.get(), side, front, top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Block side, Supplier<? extends Block> front, Block top, RenderType renderType) {
        return this.horizontalBlock(block.get(), side, front, top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Block side, Block front, Supplier<? extends Block> top, RenderType renderType) {
        return this.horizontalBlock(block.get(), side, front, top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Block side, Block front, Block top, RenderType renderType) {
        return this.horizontalBlock(block.get(), side, front, top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation front, ResourceLocation top, RenderType renderType) {
        return this.horizontalBlock(block.get(), side, front, top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Supplier<? extends Block> side, Supplier<? extends Block> front, Supplier<? extends Block> top) {
        return this.horizontalBlock(block.get(), side, front, top);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Supplier<? extends Block> side, Supplier<? extends Block> front, Block top) {
        return this.horizontalBlock(block.get(), side, front, top);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Block side, Supplier<? extends Block> front, Supplier<? extends Block> top) {
        return this.horizontalBlock(block.get(), side, front, top);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Supplier<? extends Block> side, Block front, Supplier<? extends Block> top) {
        return this.horizontalBlock(block.get(), side, front, top);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Supplier<? extends Block> side, Block front, Block top) {
        return this.horizontalBlock(block.get(), side, front, top);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Block side, Supplier<? extends Block> front, Block top) {
        return this.horizontalBlock(block.get(), side, front, top);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Block side, Block front, Supplier<? extends Block> top) {
        return this.horizontalBlock(block.get(), side, front, top);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Block side, Block front, Block top) {
        return this.horizontalBlock(block.get(), side, front, top);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation front, ResourceLocation top) {
        var b = block.get();
        this.horizontalBlock(b, side, front, top);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Supplier<? extends Block> side, Supplier<? extends Block> front, Supplier<? extends Block> top, RenderType renderType) {
        return this.horizontalBlock(block, side.get(), front.get(), top.get(), renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Supplier<? extends Block> side, Supplier<? extends Block> front, Block top, RenderType renderType) {
        return this.horizontalBlock(block, side.get(), front.get(), top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Block side, Supplier<? extends Block> front, Supplier<? extends Block> top, RenderType renderType) {
        return this.horizontalBlock(block, side, front.get(), top.get(), renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Supplier<? extends Block> side, Block front, Supplier<? extends Block> top, RenderType renderType) {
        return this.horizontalBlock(block, side.get(), front, top.get(), renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Supplier<? extends Block> side, Block front, Block top, RenderType renderType) {
        return this.horizontalBlock(block, side.get(), front, top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Block side, Supplier<? extends Block> front, Block top, RenderType renderType) {
        return this.horizontalBlock(block, side, front.get(), top, renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Block side, Block front, Supplier<? extends Block> top, RenderType renderType) {
        return this.horizontalBlock(block, side, front, top.get(), renderType);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Block side, Block front, Block top, RenderType renderType) {
        return this.horizontalBlock(block, this.texture(side), this.texture(front), this.texture(top), renderType);
    }

    public VariantBlockStateBuilder horizontalBlock(Block block, ResourceLocation side, ResourceLocation front, ResourceLocation top, RenderType renderType) {
        this.horizontalBlock(block, this.models().orientable(this.name(block), side, front, top).renderType(renderType));
        return this.getVariantBuilder(block);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Supplier<? extends Block> side, Supplier<? extends Block> front, Supplier<? extends Block> top) {
        return this.horizontalBlock(block, side.get(), front.get(), top.get());
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Supplier<? extends Block> side, Supplier<? extends Block> front, Block top) {
        return this.horizontalBlock(block, side.get(), front.get(), top);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Block side, Supplier<? extends Block> front, Supplier<? extends Block> top) {
        return this.horizontalBlock(block, side, front.get(), top.get());
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Supplier<? extends Block> side, Block front, Supplier<? extends Block> top) {
        return this.horizontalBlock(block, side.get(), front, top.get());
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Supplier<? extends Block> side, Block front, Block top) {
        return this.horizontalBlock(block, side.get(), front, top);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Block side, Supplier<? extends Block> front, Block top) {
        return this.horizontalBlock(block, side, front.get(), top);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Block side, Block front, Supplier<? extends Block> top) {
        return this.horizontalBlock(block, side, front, top.get());
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, Block side, Block front, Block top) {
        this.horizontalBlock(block, this.texture(side), this.texture(front), this.texture(top));
        return this.getVariantBuilder(block);
    }

    @Override
    public void horizontalBlock(Block block, ResourceLocation side, ResourceLocation front, ResourceLocation top) {
        super.horizontalBlock(block, side, front, top);
    }


    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Function<BlockState, ModelFile> modelFunc) {
        var b = block.get();
        this.horizontalBlock(b, modelFunc);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, ModelFile model) {
        var b = block.get();
        this.horizontalBlock(b, model);
        return this.getVariantBuilder(b);
    }

    @Override
    public void horizontalBlock(Block block, Function<BlockState, ModelFile> modelFunc) {
        super.horizontalBlock(block, modelFunc);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, BlockModelBuilder model) {
        this.horizontalBlock(block, (ModelFile) model);
        return this.getVariantBuilder(block);
    }

    @Override
    public void horizontalBlock(Block block, ModelFile model) {
        super.horizontalBlock(block, model);
    }


    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, Function<BlockState, ModelFile> modelFunc, int angleOffset) {
        var b = block.get();
        this.horizontalBlock(b, modelFunc, angleOffset);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder horizontalBlock(Supplier<? extends Block> block, ModelFile model, int angleOffset) {
        var b = block.get();
        this.horizontalBlock(b, model, angleOffset);
        return this.getVariantBuilder(b);
    }

    @Override
    public void horizontalBlock(Block block, Function<BlockState, ModelFile> modelFunc, int angleOffset) {
        super.horizontalBlock(block, modelFunc, angleOffset);
    }

    public final VariantBlockStateBuilder horizontalBlock(Block block, BlockModelBuilder model, int angleOffset) {
        this.horizontalBlock(block, (ModelFile) model, angleOffset);
        return this.getVariantBuilder(block);
    }

    @Override
    public void horizontalBlock(Block block, ModelFile model, int angleOffset) {
        super.horizontalBlock(block, model, angleOffset);
    }


    public final VariantBlockStateBuilder horizontalFaceBlock(Supplier<? extends Block> block, Function<BlockState, ModelFile> modelFunc) {
        var b = block.get();
        this.horizontalFaceBlock(b, modelFunc);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder horizontalFaceBlock(Supplier<? extends Block> block, ModelFile model) {
        var b = block.get();
        this.horizontalFaceBlock(b, model);
        return this.getVariantBuilder(b);
    }

    @Override
    public void horizontalFaceBlock(Block block, Function<BlockState, ModelFile> modelFunc) {
        super.horizontalFaceBlock(block, modelFunc);
    }

    public final VariantBlockStateBuilder horizontalFaceBlock(Block block, BlockModelBuilder model) {
        this.horizontalFaceBlock(block, (ModelFile) model);
        return this.getVariantBuilder(block);
    }

    @Override
    public void horizontalFaceBlock(Block block, ModelFile model) {
        super.horizontalFaceBlock(block, model);
    }


    public final VariantBlockStateBuilder horizontalFaceBlock(Supplier<? extends Block> block, Function<BlockState, ModelFile> modelFunc, int angleOffset) {
        var b = block.get();
        this.horizontalFaceBlock(b, modelFunc, angleOffset);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder horizontalFaceBlock(Supplier<? extends Block> block, ModelFile model, int angleOffset) {
        var b = block.get();
        this.horizontalFaceBlock(b, model, angleOffset);
        return this.getVariantBuilder(b);
    }

    @Override
    public void horizontalFaceBlock(Block block, Function<BlockState, ModelFile> modelFunc, int angleOffset) {
        super.horizontalFaceBlock(block, modelFunc, angleOffset);
    }

    public final VariantBlockStateBuilder horizontalFaceBlock(Block block, BlockModelBuilder model, int angleOffset) {
        this.horizontalFaceBlock(block, (ModelFile) model, angleOffset);
        return this.getVariantBuilder(block);
    }

    @Override
    public void horizontalFaceBlock(Block block, ModelFile model, int angleOffset) {
        super.horizontalFaceBlock(block, model, angleOffset);
    }


    public final VariantBlockStateBuilder directionalBlock(Supplier<? extends Block> block, Function<BlockState, ModelFile> modelFunc) {
        var b = block.get();
        this.directionalBlock(b, modelFunc);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder directionalBlock(Supplier<? extends Block> block, ModelFile model) {
        var b = block.get();
        this.directionalBlock(b, model);
        return this.getVariantBuilder(b);
    }

    @Override
    public void directionalBlock(Block block, Function<BlockState, ModelFile> modelFunc) {
        super.directionalBlock(block, modelFunc);
    }

    public final VariantBlockStateBuilder directionalBlock(Block block, BlockModelBuilder model) {
        this.directionalBlock(block, (ModelFile) model);
        return this.getVariantBuilder(block);
    }

    @Override
    public void directionalBlock(Block block, ModelFile model) {
        super.directionalBlock(block, model);
    }


    public final VariantBlockStateBuilder directionalBlock(Supplier<? extends Block> block, Function<BlockState, ModelFile> modelFunc, int angleOffset) {
        var b = block.get();
        this.directionalBlock(b, modelFunc, angleOffset);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder directionalBlock(Supplier<? extends Block> block, ModelFile model, int angleOffset) {
        var b = block.get();
        this.directionalBlock(b, model, angleOffset);
        return this.getVariantBuilder(b);
    }

    @Override
    public void directionalBlock(Block block, Function<BlockState, ModelFile> modelFunc, int angleOffset) {
        super.directionalBlock(block, modelFunc, angleOffset);
    }

    public final VariantBlockStateBuilder directionalBlock(Block block, BlockModelBuilder model, int angleOffset) {
        this.directionalBlock(block, (ModelFile) model, angleOffset);
        return this.getVariantBuilder(block);
    }

    @Override
    public void directionalBlock(Block block, ModelFile model, int angleOffset) {
        super.directionalBlock(block, model, angleOffset);
    }


    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.stairsBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Block texture, RenderType renderType) {
        return this.stairsBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, ResourceLocation texture, RenderType renderType) {
        return this.stairsBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Supplier<? extends Block> texture) {
        return this.stairsBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Block texture) {
        return this.stairsBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, ResourceLocation texture) {
        var b = block.get();
        this.stairsBlock(b, texture);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.stairsBlock(block, texture.get(), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Block texture, RenderType renderType) {
        return this.stairsBlock(block, this.texture(texture), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, ResourceLocation texture, RenderType renderType) {
        this.stairsBlockWithRenderType(block, texture, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void stairsBlockWithRenderType(StairBlock block, ResourceLocation texture, String renderType) {
        super.stairsBlockWithRenderType(block, texture, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void stairsBlockWithRenderType(StairBlock block, ResourceLocation texture, ResourceLocation renderType) {
        super.stairsBlockWithRenderType(block, texture, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Supplier<? extends Block> texture) {
        return this.stairsBlock(block, texture.get());
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Block texture) {
        this.stairsBlock(block, this.texture(texture));
        return this.getVariantBuilder(block);
    }

    @Override
    public void stairsBlock(StairBlock block, ResourceLocation texture) {
        super.stairsBlock(block, texture);
    }


    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Supplier<? extends Block> texture, RenderType renderType) {
        return this.stairsBlock(block.get(), name, texture, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Block texture, RenderType renderType) {
        return this.stairsBlock(block.get(), name, texture, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, ResourceLocation texture, RenderType renderType) {
        return this.stairsBlock(block.get(), name, texture, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Supplier<? extends Block> texture) {
        return this.stairsBlock(block.get(), name, texture);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Block texture) {
        return this.stairsBlock(block.get(), name, texture);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, ResourceLocation texture) {
        var b = block.get();
        this.stairsBlock(b, name, texture);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Supplier<? extends Block> texture, RenderType renderType) {
        return this.stairsBlock(block, name, texture.get(), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Block texture, RenderType renderType) {
        return this.stairsBlock(block, name, this.texture(texture), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, ResourceLocation texture, RenderType renderType) {
        this.stairsBlockWithRenderType(block, name, texture, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void stairsBlockWithRenderType(StairBlock block, String name, ResourceLocation texture, String renderType) {
        super.stairsBlockWithRenderType(block, name, texture, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void stairsBlockWithRenderType(StairBlock block, String name, ResourceLocation texture, ResourceLocation renderType) {
        super.stairsBlockWithRenderType(block, name, texture, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Supplier<? extends Block> texture) {
        return this.stairsBlock(block, name, texture.get());
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Block texture) {
        this.stairsBlock(block, name, this.texture(texture));
        return this.getVariantBuilder(block);
    }

    @Override
    public void stairsBlock(StairBlock block, String name, ResourceLocation texture) {
        super.stairsBlock(block, name, texture);
    }


    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block.get(), side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block.get(), side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Block side, Supplier<? extends Block> bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block.get(), side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Supplier<? extends Block> side, Block bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block.get(), side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Supplier<? extends Block> side, Block bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block.get(), side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Block side, Supplier<? extends Block> bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block.get(), side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Block side, Block bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block.get(), side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Block side, Block bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block.get(), side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        return this.stairsBlock(block.get(), side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block.get(), side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Block top) {
        return this.stairsBlock(block.get(), side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Block side, Supplier<? extends Block> bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block.get(), side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Supplier<? extends Block> side, Block bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block.get(), side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Supplier<? extends Block> side, Block bottom, Block top) {
        return this.stairsBlock(block.get(), side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Block side, Supplier<? extends Block> bottom, Block top) {
        return this.stairsBlock(block.get(), side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Block side, Block bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block.get(), side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, Block side, Block bottom, Block top) {
        return this.stairsBlock(block.get(), side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        var b = block.get();
        this.stairsBlock(b, side, bottom, top);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block, side.get(), bottom.get(), top.get(), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block, side.get(), bottom.get(), top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Block side, Supplier<? extends Block> bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block, side, bottom.get(), top.get(), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Supplier<? extends Block> side, Block bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block, side.get(), bottom, top.get(), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Supplier<? extends Block> side, Block bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block, side.get(), bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Block side, Supplier<? extends Block> bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block, side, bottom.get(), top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Block side, Block bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block, side, bottom, top.get(), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Block side, Block bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block, this.texture(side), this.texture(bottom), this.texture(top), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        this.stairsBlockWithRenderType(block, side, bottom, top, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void stairsBlockWithRenderType(StairBlock block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, String renderType) {
        super.stairsBlockWithRenderType(block, side, bottom, top, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void stairsBlockWithRenderType(StairBlock block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, ResourceLocation renderType) {
        super.stairsBlockWithRenderType(block, side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block, side.get(), bottom.get(), top.get());
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Block top) {
        return this.stairsBlock(block, side.get(), bottom.get(), top);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Block side, Supplier<? extends Block> bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block, side, bottom.get(), top.get());
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Supplier<? extends Block> side, Block bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block, side.get(), bottom, top.get());
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Supplier<? extends Block> side, Block bottom, Block top) {
        return this.stairsBlock(block, side.get(), bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Block side, Supplier<? extends Block> bottom, Block top) {
        return this.stairsBlock(block, side, bottom.get(), top);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Block side, Block bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block, side, bottom, top.get());
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, Block side, Block bottom, Block top) {
        this.stairsBlock(block, this.texture(side), this.texture(bottom), this.texture(top));
        return this.getVariantBuilder(block);
    }

    @Override
    public void stairsBlock(StairBlock block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        super.stairsBlock(block, side, bottom, top);
    }


    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block.get(), name, side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block.get(), name, side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Block side, Supplier<? extends Block> bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block.get(), name, side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Supplier<? extends Block> side, Block bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block.get(), name, side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Supplier<? extends Block> side, Block bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block.get(), name, side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Block side, Supplier<? extends Block> bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block.get(), name, side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Block side, Block bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block.get(), name, side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Block side, Block bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block.get(), name, side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        return this.stairsBlock(block.get(), name, side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block.get(), name, side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Block top) {
        return this.stairsBlock(block.get(), name, side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Block side, Supplier<? extends Block> bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block.get(), name, side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Supplier<? extends Block> side, Block bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block.get(), name, side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Supplier<? extends Block> side, Block bottom, Block top) {
        return this.stairsBlock(block.get(), name, side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Block side, Supplier<? extends Block> bottom, Block top) {
        return this.stairsBlock(block.get(), name, side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Block side, Block bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block.get(), name, side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, Block side, Block bottom, Block top) {
        return this.stairsBlock(block.get(), name, side, bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        var b = block.get();
        this.stairsBlock(b, name, side, bottom, top);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block, name, side.get(), bottom.get(), top.get(), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block, name, side.get(), bottom.get(), top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Block side, Supplier<? extends Block> bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block, name, side, bottom.get(), top.get(), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Supplier<? extends Block> side, Block bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block, name, side.get(), bottom, top.get(), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Supplier<? extends Block> side, Block bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block, name, side.get(), bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Block side, Supplier<? extends Block> bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block, name, side, bottom.get(), top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Block side, Block bottom, Supplier<? extends Block> top, RenderType renderType) {
        return this.stairsBlock(block, name, side, bottom, top.get(), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Block side, Block bottom, Block top, RenderType renderType) {
        return this.stairsBlock(block, name, this.texture(side), this.texture(bottom), this.texture(top), renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        this.stairsBlockWithRenderType(block, name, side, bottom, top, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void stairsBlockWithRenderType(StairBlock block, String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, String renderType) {
        super.stairsBlockWithRenderType(block, name, side, bottom, top, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void stairsBlockWithRenderType(StairBlock block, String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, ResourceLocation renderType) {
        super.stairsBlockWithRenderType(block, name, side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block, name, side.get(), bottom.get(), top.get());
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Supplier<? extends Block> side, Supplier<? extends Block> bottom, Block top) {
        return this.stairsBlock(block, name, side.get(), bottom.get(), top);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Block side, Supplier<? extends Block> bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block, name, side, bottom.get(), top.get());
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Supplier<? extends Block> side, Block bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block, name, side.get(), bottom, top.get());
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Supplier<? extends Block> side, Block bottom, Block top) {
        return this.stairsBlock(block, name, side.get(), bottom, top);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Block side, Supplier<? extends Block> bottom, Block top) {
        return this.stairsBlock(block, name, side, bottom.get(), top);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Block side, Block bottom, Supplier<? extends Block> top) {
        return this.stairsBlock(block, name, side, bottom, top.get());
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, String name, Block side, Block bottom, Block top) {
        this.stairsBlock(block, name, this.texture(side), this.texture(bottom), this.texture(top));
        return this.getVariantBuilder(block);
    }

    @Override
    public void stairsBlock(StairBlock block, String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        super.stairsBlock(block, name, side, bottom, top);
    }


    public final VariantBlockStateBuilder stairsBlock(Supplier<? extends StairBlock> block, ModelFile stairs, ModelFile stairsInner, ModelFile stairsOuter) {
        var b = block.get();
        this.stairsBlock(b, stairs, stairsInner, stairsOuter);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder stairsBlock(StairBlock block, BlockModelBuilder stairs, BlockModelBuilder stairsInner, BlockModelBuilder stairsOuter) {
        this.stairsBlock(block, (ModelFile) stairs, (ModelFile) stairsInner, (ModelFile) stairsOuter);
        return this.getVariantBuilder(block);
    }

    @Override
    public void stairsBlock(StairBlock block, ModelFile stairs, ModelFile stairsInner, ModelFile stairsOuter) {
        super.stairsBlock(block, stairs, stairsInner, stairsOuter);
    }


    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ModelFile doubleSlab, Supplier<? extends Block> texture, RenderType renderType) {
        return this.slabBlock(block.get(), doubleSlab, texture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ModelFile doubleSlab, Block texture, RenderType renderType) {
        return this.slabBlock(block.get(), doubleSlab, texture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Supplier<? extends Block> doubleSlab, Supplier<? extends Block> texture, RenderType renderType) {
        return this.slabBlock(block.get(), doubleSlab, texture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Supplier<? extends Block> doubleSlab, Block texture, RenderType renderType) {
        return this.slabBlock(block.get(), doubleSlab, texture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Block doubleSlab, Supplier<? extends Block> texture, RenderType renderType) {
        return this.slabBlock(block.get(), doubleSlab, texture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Block doubleSlab, Block texture, RenderType renderType) {
        return this.slabBlock(block.get(), doubleSlab, texture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ModelFile doubleSlab, ResourceLocation texture, RenderType renderType) {
        return this.slabBlock(block.get(), doubleSlab, texture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ResourceLocation doubleSlabModel, ResourceLocation texture, RenderType renderType) {
        return this.slabBlock(block.get(), doubleSlabModel, texture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ModelFile doubleSlab, Supplier<? extends Block> texture) {
        return this.slabBlock(block.get(), doubleSlab, texture);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ModelFile doubleSlab, Block texture) {
        return this.slabBlock(block.get(), doubleSlab, texture);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Supplier<? extends Block> doubleSlab, Supplier<? extends Block> texture) {
        return this.slabBlock(block.get(), doubleSlab, texture);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Supplier<? extends Block> doubleSlab, Block texture) {
        return this.slabBlock(block.get(), doubleSlab, texture);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Block doubleSlab, Supplier<? extends Block> texture) {
        return this.slabBlock(block.get(), doubleSlab, texture);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Block doubleSlab, Block texture) {
        return this.slabBlock(block.get(), doubleSlab, texture);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ModelFile doubleSlab, ResourceLocation texture) {
        return this.slabBlock(block.get(), doubleSlab, texture);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ResourceLocation doubleSlab, ResourceLocation texture) {
        var b = block.get();
        this.slabBlock(b, doubleSlab, texture);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, ModelFile doubleSlab, Supplier<? extends Block> texture, RenderType renderType) {
        return this.slabBlock(block, doubleSlab, texture.get(), renderType);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, ModelFile doubleSlab, Block texture, RenderType renderType) {
        return this.slabBlock(block, doubleSlab, this.texture(texture), this.texture(texture), this.texture(texture), renderType);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Supplier<? extends Block> doubleSlab, Supplier<? extends Block> texture, RenderType renderType) {
        return this.slabBlock(block, doubleSlab.get(), texture.get(), renderType);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Supplier<? extends Block> doubleSlab, Block texture, RenderType renderType) {
        return this.slabBlock(block, doubleSlab.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Block doubleSlab, Supplier<? extends Block> texture, RenderType renderType) {
        return this.slabBlock(block, doubleSlab, texture.get(), renderType);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Block doubleSlab, Block texture, RenderType renderType) {
        return this.slabBlock(block, this.texture(doubleSlab), this.texture(texture), this.texture(texture), this.texture(texture), renderType);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, ModelFile doubleSlab, ResourceLocation texture, RenderType renderType) {
        return this.slabBlock(block, doubleSlab, texture, texture, texture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, ResourceLocation doubleSlabModel, ResourceLocation texture, RenderType renderType) {
        return this.slabBlock(block, doubleSlabModel, texture, texture, texture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, ModelFile doubleSlab, Supplier<? extends Block> texture) {
        return this.slabBlock(block, doubleSlab, texture.get());
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, ModelFile doubleSlab, Block texture) {
        return this.slabBlock(block, doubleSlab, this.texture(texture), this.texture(texture), this.texture(texture));
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Supplier<? extends Block> doubleSlab, Supplier<? extends Block> texture) {
        return this.slabBlock(block, doubleSlab.get(), texture.get());
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Supplier<? extends Block> doubleSlab, Block texture) {
        return this.slabBlock(block, doubleSlab.get(), texture);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Block doubleSlab, Supplier<? extends Block> texture) {
        return this.slabBlock(block, doubleSlab, texture.get());
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Block doubleSlab, Block texture) {
        this.slabBlock(block, this.texture(doubleSlab), this.texture(texture), this.texture(texture), this.texture(texture));
        return this.getVariantBuilder(block);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, ModelFile doubleSlab, ResourceLocation texture) {
        return this.slabBlock(block, doubleSlab, texture, texture, texture);
    }

    @Override
    public void slabBlock(SlabBlock block, ResourceLocation doubleslab, ResourceLocation texture) {
        super.slabBlock(block, doubleslab, texture);
    }


    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Supplier<? extends Block> doubleSlabAndTexture, RenderType renderType) {
        return this.slabBlock(block.get(), doubleSlabAndTexture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Block doubleSlabAndTexture, RenderType renderType) {
        return this.slabBlock(block.get(), doubleSlabAndTexture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ResourceLocation doubleSlabModelAndTexture, RenderType renderType) {
        return this.slabBlock(block.get(), doubleSlabModelAndTexture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Supplier<? extends Block> doubleSlabAndTexture) {
        return this.slabBlock(block.get(), doubleSlabAndTexture);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, Block doubleSlabAndTexture) {
        return this.slabBlock(block.get(), doubleSlabAndTexture);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ResourceLocation doubleSlabModelAndTexture) {
        return this.slabBlock(block.get(), doubleSlabModelAndTexture);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Supplier<? extends Block> doubleSlabAndTexture, RenderType renderType) {
        return this.slabBlock(block, doubleSlabAndTexture.get(), renderType);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Block doubleSlabAndTexture, RenderType renderType) {
        return this.slabBlock(block, this.texture(doubleSlabAndTexture), renderType);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, ResourceLocation doubleSlabModelAndTexture, RenderType renderType) {
        return this.slabBlock(block, doubleSlabModelAndTexture, doubleSlabModelAndTexture, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Supplier<? extends Block> doubleSlabAndTexture) {
        return this.slabBlock(block, doubleSlabAndTexture.get());
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, Block doubleSlabAndTexture) {
        return this.slabBlock(block, this.texture(doubleSlabAndTexture));
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, ResourceLocation doubleSlabModelAndTexture) {
        this.slabBlock(block, doubleSlabModelAndTexture, doubleSlabModelAndTexture);
        return this.getVariantBuilder(block);
    }


    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ResourceLocation doubleSlabModel, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        return this.slabBlock(block, this.models().getExistingFile(doubleSlabModel), side, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ModelFile doubleSlab, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        return this.slabBlock(
            block,
            this.models().slab(this.name(block), side, bottom, top).renderType(renderType),
            this.models().slabTop(this.name(block) + "_top", side, bottom, top).renderType(renderType),
            doubleSlab
        );
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ResourceLocation doubleSlabModel, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        var b = block.get();
        this.slabBlock(block.get(), doubleSlabModel, side, bottom, top);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ModelFile doubleSlab, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.slabBlock(
            block,
            this.models().slab(this.name(block), side, bottom, top),
            this.models().slabTop(this.name(block) + "_top", side, bottom, top),
            doubleSlab
        );
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, ResourceLocation doubleSlabModel, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        return this.slabBlock(block, this.models().getExistingFile(doubleSlabModel), side, bottom, top, renderType);
    }

    public VariantBlockStateBuilder slabBlock(SlabBlock block, ModelFile doubleSlab, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        this.slabBlock(
            block,
            this.models().slab(this.name(block), side, bottom, top).renderType(renderType),
            this.models().slabTop(this.name(block) + "_top", side, bottom, top).renderType(renderType),
            doubleSlab
        );
        return this.getVariantBuilder(block);
    }

    @Override
    public void slabBlock(SlabBlock block, ResourceLocation doubleslab, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        super.slabBlock(block, doubleslab, side, bottom, top);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, ModelFile doubleSlab, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.slabBlock(block, doubleSlab, side, bottom, top, RenderType.DEFAULT);
    }


    public final VariantBlockStateBuilder slabBlock(Supplier<? extends SlabBlock> block, ModelFile bottom, ModelFile top, ModelFile doubleslab) {
        var b = block.get();
        this.slabBlock(b, bottom, top, doubleslab);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder slabBlock(SlabBlock block, BlockModelBuilder bottom, BlockModelBuilder top, BlockModelBuilder doubleslab) {
        this.slabBlock(block, (ModelFile) bottom, (ModelFile) top, (ModelFile) doubleslab);
        return this.getVariantBuilder(block);
    }

    @Override
    public void slabBlock(SlabBlock block, ModelFile bottom, ModelFile top, ModelFile doubleslab) {
        super.slabBlock(block, bottom, top, doubleslab);
    }


    public final VariantBlockStateBuilder buttonBlock(Supplier<? extends ButtonBlock> block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.buttonBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder buttonBlock(Supplier<? extends ButtonBlock> block, Block texture, RenderType renderType) {
        return this.buttonBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder buttonBlock(Supplier<? extends ButtonBlock> block, ResourceLocation texture, RenderType renderType) {
        return this.buttonBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder buttonBlock(Supplier<? extends ButtonBlock> block, Supplier<? extends Block> texture) {
        return this.buttonBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder buttonBlock(Supplier<? extends ButtonBlock> block, Block texture) {
        return this.buttonBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder buttonBlock(Supplier<? extends ButtonBlock> block, ResourceLocation texture) {
        var b = block.get();
        this.buttonBlock(b, texture);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder buttonBlock(ButtonBlock block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.buttonBlock(block, texture.get(), renderType);
    }

    public final VariantBlockStateBuilder buttonBlock(ButtonBlock block, Block texture, RenderType renderType) {
        return this.buttonBlock(block, this.texture(texture), renderType);
    }

    public VariantBlockStateBuilder buttonBlock(ButtonBlock block, ResourceLocation texture, RenderType renderType) {
        var button = this.models().button(this.name(block), texture).renderType(renderType);
        var buttonPressed = this.models().buttonPressed(this.name(block, "pressed"), texture).renderType(renderType);
        this.buttonBlock(block, button, buttonPressed);
        return this.getVariantBuilder(block);
    }

    public final VariantBlockStateBuilder buttonBlock(ButtonBlock block, Supplier<? extends Block> texture) {
        return this.buttonBlock(block, texture.get());
    }

    public final VariantBlockStateBuilder buttonBlock(ButtonBlock block, Block texture) {
        this.buttonBlock(block, this.texture(texture));
        return this.getVariantBuilder(block);
    }

    @Override
    public void buttonBlock(ButtonBlock block, ResourceLocation texture) {
        super.buttonBlock(block, texture);
    }


    public final VariantBlockStateBuilder buttonBlock(Supplier<? extends ButtonBlock> block, ModelFile button, ModelFile buttonPressed) {
        var b = block.get();
        this.buttonBlock(b, button, buttonPressed);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder buttonBlock(ButtonBlock block, BlockModelBuilder button, BlockModelBuilder buttonPressed) {
        this.buttonBlock(block, (ModelFile) button, (ModelFile) buttonPressed);
        return this.getVariantBuilder(block);
    }

    @Override
    public void buttonBlock(ButtonBlock block, ModelFile button, ModelFile buttonPressed) {
        super.buttonBlock(block, button, buttonPressed);
    }


    public final VariantBlockStateBuilder pressurePlateBlock(Supplier<? extends PressurePlateBlock> block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.pressurePlateBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder pressurePlateBlock(Supplier<? extends PressurePlateBlock> block, Block texture, RenderType renderType) {
        return this.pressurePlateBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder pressurePlateBlock(Supplier<? extends PressurePlateBlock> block, ResourceLocation texture, RenderType renderType) {
        return this.pressurePlateBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder pressurePlateBlock(Supplier<? extends PressurePlateBlock> block, Supplier<? extends Block> texture) {
        return this.pressurePlateBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder pressurePlateBlock(Supplier<? extends PressurePlateBlock> block, Block texture) {
        return this.pressurePlateBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder pressurePlateBlock(Supplier<? extends PressurePlateBlock> block, ResourceLocation texture) {
        var b = block.get();
        this.pressurePlateBlock(b, texture);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder pressurePlateBlock(PressurePlateBlock block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.pressurePlateBlock(block, texture.get(), renderType);
    }

    public final VariantBlockStateBuilder pressurePlateBlock(PressurePlateBlock block, Block texture, RenderType renderType) {
        this.pressurePlateBlock(block, this.texture(texture), renderType);
        return this.getVariantBuilder(block);
    }

    public VariantBlockStateBuilder pressurePlateBlock(PressurePlateBlock block, ResourceLocation texture, RenderType renderType) {
        var pressurePlate = this.models().pressurePlate(this.name(block), texture).renderType(renderType);
        var pressurePlateDown = this.models().pressurePlateDown(this.name(block) + "_down", texture).renderType(renderType);
        return this.pressurePlateBlock(block, pressurePlate, pressurePlateDown);
    }

    public final VariantBlockStateBuilder pressurePlateBlock(PressurePlateBlock block, Supplier<? extends Block> texture) {
        return this.pressurePlateBlock(block, texture.get());
    }

    public final VariantBlockStateBuilder pressurePlateBlock(PressurePlateBlock block, Block texture) {
        this.pressurePlateBlock(block, this.texture(texture));
        return this.getVariantBuilder(block);
    }

    @Override
    public void pressurePlateBlock(PressurePlateBlock block, ResourceLocation texture) {
        super.pressurePlateBlock(block, texture);
    }


    public final VariantBlockStateBuilder pressurePlateBlock(Supplier<? extends PressurePlateBlock> block, ModelFile pressurePlate, ModelFile pressurePlateDown) {
        var b = block.get();
        this.pressurePlateBlock(b, pressurePlate, pressurePlateDown);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder pressurePlateBlock(PressurePlateBlock block, BlockModelBuilder pressurePlate, BlockModelBuilder pressurePlateDown) {
        this.pressurePlateBlock(block, (ModelFile) pressurePlate, (ModelFile) pressurePlateDown);
        return this.getVariantBuilder(block);
    }

    @Override
    public void pressurePlateBlock(PressurePlateBlock block, ModelFile pressurePlate, ModelFile pressurePlateDown) {
        super.pressurePlateBlock(block, pressurePlate, pressurePlateDown);
    }


    public StandingAndWallStates signBlock(Supplier<? extends StandingSignBlock> signBlock, Supplier<? extends WallSignBlock> wallSignBlock, Supplier<? extends Block> texture) {
        return this.signBlock(signBlock.get(), wallSignBlock.get(), texture);
    }

    public StandingAndWallStates signBlock(Supplier<? extends StandingSignBlock> signBlock, Supplier<? extends WallSignBlock> wallSignBlock, Block texture) {
        return this.signBlock(signBlock.get(), wallSignBlock.get(), texture);
    }

    public StandingAndWallStates signBlock(Supplier<? extends StandingSignBlock> signBlock, Supplier<? extends WallSignBlock> wallSignBlock, ResourceLocation texture) {
        var signB = signBlock.get();
        var wallSignB = wallSignBlock.get();
        this.signBlock(signB, wallSignB, texture);
        return new StandingAndWallStates(this.getVariantBuilder(signB), this.getVariantBuilder(wallSignB));
    }

    public StandingAndWallStates signBlock(StandingSignBlock signBlock, WallSignBlock wallSignBlock, Supplier<? extends Block> texture) {
        return this.signBlock(signBlock, wallSignBlock, texture.get());
    }

    public StandingAndWallStates signBlock(StandingSignBlock signBlock, WallSignBlock wallSignBlock, Block texture) {
        this.signBlock(signBlock, wallSignBlock, this.texture(texture));
        return new StandingAndWallStates(this.getVariantBuilder(signBlock), this.getVariantBuilder(wallSignBlock));
    }

    @Override
    public void signBlock(StandingSignBlock signBlock, WallSignBlock wallSignBlock, ResourceLocation texture) {
        super.signBlock(signBlock, wallSignBlock, texture);
    }


    public StandingAndWallStates signBlock(Supplier<? extends StandingSignBlock> signBlock, Supplier<? extends WallSignBlock> wallSignBlock, ModelFile sign) {
        var signB = signBlock.get();
        var wallSignB = wallSignBlock.get();
        this.signBlock(signB, wallSignB, sign);
        return new StandingAndWallStates(this.getVariantBuilder(signB), this.getVariantBuilder(wallSignB));
    }

    public StandingAndWallStates signBlock(StandingSignBlock signBlock, WallSignBlock wallSignBlock, BlockModelBuilder sign) {
        this.signBlock(signBlock, wallSignBlock, (ModelFile) sign);
        return new StandingAndWallStates(this.getVariantBuilder(signBlock), this.getVariantBuilder(wallSignBlock));
    }

    @Override
    public void signBlock(StandingSignBlock signBlock, WallSignBlock wallSignBlock, ModelFile sign) {
        super.signBlock(signBlock, wallSignBlock, sign);
    }


    public final MultiPartBlockStateBuilder fourWayBlock(Supplier<? extends CrossCollisionBlock> block, ModelFile post, ModelFile side) {
        var b = block.get();
        this.fourWayBlock(b, post, side);
        return this.getMultipartBuilder(b);
    }

    public final MultiPartBlockStateBuilder fourWayBlock(CrossCollisionBlock block, BlockModelBuilder post, BlockModelBuilder side) {
        this.fourWayBlock(block, (ModelFile) post, (ModelFile) side);
        return this.getMultipartBuilder(block);
    }

    @Override
    public void fourWayBlock(CrossCollisionBlock block, ModelFile post, ModelFile side) {
        super.fourWayBlock(block, post, side);
    }


    public final void fourWayMultipart(MultiPartBlockStateBuilder builder, Function<MultiPartBlockStateBuilder, ModelFile> side) {
        super.fourWayMultipart(builder, side.apply(builder));
    }

    @Override
    public void fourWayMultipart(MultiPartBlockStateBuilder builder, ModelFile side) {
        super.fourWayMultipart(builder, side);
    }


    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.fenceBlock(block.get(), texture, renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, Block texture, RenderType renderType) {
        return this.fenceBlock(block.get(), texture, renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, ResourceLocation texture, RenderType renderType) {
        return this.fenceBlock(block.get(), texture, renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, Supplier<? extends Block> texture) {
        return this.fenceBlock(block.get(), texture);
    }

    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, Block texture) {
        return this.fenceBlock(block.get(), texture);
    }

    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, ResourceLocation texture) {
        var b = block.get();
        this.fenceBlock(b, texture);
        return this.getMultipartBuilder(b);
    }

    public final MultiPartBlockStateBuilder fenceBlock(FenceBlock block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.fenceBlock(block, texture.get(), renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(FenceBlock block, Block texture, RenderType renderType) {
        return this.fenceBlock(block, this.texture(texture), renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(FenceBlock block, ResourceLocation texture, RenderType renderType) {
        this.fenceBlockWithRenderType(block, texture, renderType);
        return this.getMultipartBuilder(block);
    }

    @Override
    @Deprecated
    public void fenceBlockWithRenderType(FenceBlock block, ResourceLocation texture, String renderType) {
        super.fenceBlockWithRenderType(block, texture, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void fenceBlockWithRenderType(FenceBlock block, ResourceLocation texture, ResourceLocation renderType) {
        super.fenceBlockWithRenderType(block, texture, renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(FenceBlock block, Supplier<? extends Block> texture) {
        return this.fenceBlock(block, texture.get());
    }

    public final MultiPartBlockStateBuilder fenceBlock(FenceBlock block, Block texture) {
        this.fenceBlock(block, this.texture(texture));
        return this.getMultipartBuilder(block);
    }

    @Override
    public void fenceBlock(FenceBlock block, ResourceLocation texture) {
        super.fenceBlock(block, texture);
    }


    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, String name, Supplier<? extends Block> texture, RenderType renderType) {
        return this.fenceBlock(block.get(), name, texture, renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, String name, Block texture, RenderType renderType) {
        return this.fenceBlock(block.get(), name, texture, renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, String name, ResourceLocation texture, RenderType renderType) {
        return this.fenceBlock(block.get(), name, texture, renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, String name, Supplier<? extends Block> texture) {
        return this.fenceBlock(block.get(), name, texture);
    }

    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, String name, Block texture) {
        return this.fenceBlock(block.get(), name, texture);
    }

    public final MultiPartBlockStateBuilder fenceBlock(Supplier<? extends FenceBlock> block, String name, ResourceLocation texture) {
        var b = block.get();
        this.fenceBlock(b, name, texture);
        return this.getMultipartBuilder(b);
    }

    public final MultiPartBlockStateBuilder fenceBlock(FenceBlock block, String name, Supplier<? extends Block> texture, RenderType renderType) {
        return this.fenceBlock(block, name, texture.get(), renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(FenceBlock block, String name, Block texture, RenderType renderType) {
        return this.fenceBlock(block, name, this.texture(texture), renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(FenceBlock block, String name, ResourceLocation texture, RenderType renderType) {
        this.fenceBlockWithRenderType(block, name, texture, renderType);
        return this.getMultipartBuilder(block);
    }

    @Override
    @Deprecated
    public void fenceBlockWithRenderType(FenceBlock block, String name, ResourceLocation texture, String renderType) {
        super.fenceBlockWithRenderType(block, name, texture, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void fenceBlockWithRenderType(FenceBlock block, String name, ResourceLocation texture, ResourceLocation renderType) {
        super.fenceBlockWithRenderType(block, name, texture, renderType);
    }

    public final MultiPartBlockStateBuilder fenceBlock(FenceBlock block, String name, Supplier<? extends Block> texture) {
        return this.fenceBlock(block, name, texture.get());
    }

    public final MultiPartBlockStateBuilder fenceBlock(FenceBlock block, String name, Block texture) {
        this.fenceBlock(block, name, this.texture(texture));
        return this.getMultipartBuilder(block);
    }

    @Override
    public void fenceBlock(FenceBlock block, String name, ResourceLocation texture) {
        super.fenceBlock(block, name, texture);
    }


    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.fenceGateBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, Block texture, RenderType renderType) {
        return this.fenceGateBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, ResourceLocation texture, RenderType renderType) {
        return this.fenceGateBlock(block.get(), texture, renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, Supplier<? extends Block> texture) {
        return this.fenceGateBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, Block texture) {
        return this.fenceGateBlock(block.get(), texture);
    }

    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, ResourceLocation texture) {
        var b = block.get();
        this.fenceGateBlock(b, texture);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder fenceGateBlock(FenceGateBlock block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.fenceGateBlock(block, texture.get(), renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(FenceGateBlock block, Block texture, RenderType renderType) {
        return this.fenceGateBlock(block, this.texture(texture), renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(FenceGateBlock block, ResourceLocation texture, RenderType renderType) {
        this.fenceGateBlockWithRenderType(block, texture, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void fenceGateBlockWithRenderType(FenceGateBlock block, ResourceLocation texture, String renderType) {
        super.fenceGateBlockWithRenderType(block, texture, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void fenceGateBlockWithRenderType(FenceGateBlock block, ResourceLocation texture, ResourceLocation renderType) {
        super.fenceGateBlockWithRenderType(block, texture, renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(FenceGateBlock block, Supplier<? extends Block> texture) {
        return this.fenceGateBlock(block, texture.get());
    }

    public final VariantBlockStateBuilder fenceGateBlock(FenceGateBlock block, Block texture) {
        this.fenceGateBlock(block, this.texture(texture));
        return this.getVariantBuilder(block);
    }

    @Override
    public void fenceGateBlock(FenceGateBlock block, ResourceLocation texture) {
        super.fenceGateBlock(block, texture);
    }


    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, String name, Supplier<? extends Block> texture, RenderType renderType) {
        return this.fenceGateBlock(block.get(), name, texture, renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, String name, Block texture, RenderType renderType) {
        return this.fenceGateBlock(block.get(), name, texture, renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, String name, ResourceLocation texture, RenderType renderType) {
        return this.fenceGateBlock(block.get(), name, texture, renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, String name, Supplier<? extends Block> texture) {
        return this.fenceGateBlock(block.get(), name, texture);
    }

    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, String name, Block texture) {
        return this.fenceGateBlock(block.get(), name, texture);
    }

    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, String name, ResourceLocation texture) {
        var b = block.get();
        this.fenceGateBlock(b, name, texture);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder fenceGateBlock(FenceGateBlock block, String name, Supplier<? extends Block> texture, RenderType renderType) {
        return this.fenceGateBlock(block, name, texture.get(), renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(FenceGateBlock block, String name, Block texture, RenderType renderType) {
        return this.fenceGateBlock(block, name, this.texture(texture), renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(FenceGateBlock block, String name, ResourceLocation texture, RenderType renderType) {
        this.fenceGateBlockWithRenderType(block, name, texture, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void fenceGateBlockWithRenderType(FenceGateBlock block, String name, ResourceLocation texture, String renderType) {
        super.fenceGateBlockWithRenderType(block, name, texture, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void fenceGateBlockWithRenderType(FenceGateBlock block, String name, ResourceLocation texture, ResourceLocation renderType) {
        super.fenceGateBlockWithRenderType(block, name, texture, renderType);
    }

    public final VariantBlockStateBuilder fenceGateBlock(FenceGateBlock block, String name, Supplier<? extends Block> texture) {
        return this.fenceGateBlock(block, name, texture.get());
    }

    public final VariantBlockStateBuilder fenceGateBlock(FenceGateBlock block, String name, Block texture) {
        this.fenceGateBlock(block, name, this.texture(texture));
        return this.getVariantBuilder(block);
    }

    @Override
    public void fenceGateBlock(FenceGateBlock block, String name, ResourceLocation texture) {
        super.fenceGateBlock(block, name, texture);
    }


    public final VariantBlockStateBuilder fenceGateBlock(Supplier<? extends FenceGateBlock> block, ModelFile gate, ModelFile gateOpen, ModelFile gateWall, ModelFile gateWallOpen) {
        var b = block.get();
        this.fenceGateBlock(b, gate, gateOpen, gateWall, gateWallOpen);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder fenceGateBlock(FenceGateBlock block, BlockModelBuilder gate, BlockModelBuilder gateOpen, BlockModelBuilder gateWall, BlockModelBuilder gateWallOpen) {
        this.fenceGateBlock(block, (ModelFile) gate, (ModelFile) gateOpen, (ModelFile) gateWall, (ModelFile) gateWallOpen);
        return this.getVariantBuilder(block);
    }

    @Override
    public void fenceGateBlock(FenceGateBlock block, ModelFile gate, ModelFile gateOpen, ModelFile gateWall, ModelFile gateWallOpen) {
        super.fenceGateBlock(block, gate, gateOpen, gateWall, gateWallOpen);
    }


    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.wallBlock(block.get(), texture, renderType);
    }

    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, Block texture, RenderType renderType) {
        return this.wallBlock(block.get(), texture, renderType);
    }

    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, ResourceLocation texture, RenderType renderType) {
        return this.wallBlock(block.get(), texture, renderType);
    }

    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, Supplier<? extends Block> texture) {
        return this.wallBlock(block.get(), texture);
    }

    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, Block texture) {
        return this.wallBlock(block.get(), texture);
    }

    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, ResourceLocation texture) {
        var b = block.get();
        this.wallBlock(b, texture);
        return this.getMultipartBuilder(b);
    }

    public final MultiPartBlockStateBuilder wallBlock(WallBlock block, Supplier<? extends Block> texture, RenderType renderType) {
        return this.wallBlock(block, texture.get(), renderType);
    }

    public final MultiPartBlockStateBuilder wallBlock(WallBlock block, Block texture, RenderType renderType) {
        return this.wallBlock(block, this.texture(texture), renderType);
    }

    public final MultiPartBlockStateBuilder wallBlock(WallBlock block, ResourceLocation texture, RenderType renderType) {
        this.wallBlockWithRenderType(block, texture, renderType);
        return this.getMultipartBuilder(block);
    }

    @Override
    @Deprecated
    public void wallBlockWithRenderType(WallBlock block, ResourceLocation texture, String renderType) {
        super.wallBlockWithRenderType(block, texture, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void wallBlockWithRenderType(WallBlock block, ResourceLocation texture, ResourceLocation renderType) {
        super.wallBlockWithRenderType(block, texture, renderType);
    }

    public final MultiPartBlockStateBuilder wallBlock(WallBlock block, Supplier<? extends Block> texture) {
        return this.wallBlock(block, texture.get());
    }

    public final MultiPartBlockStateBuilder wallBlock(WallBlock block, Block texture) {
        this.wallBlock(block, this.texture(texture));
        return this.getMultipartBuilder(block);
    }

    @Override
    public void wallBlock(WallBlock block, ResourceLocation texture) {
        super.wallBlock(block, texture);
    }


    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, String name, Supplier<? extends Block> texture, RenderType renderType) {
        return this.wallBlock(block.get(), name, texture, renderType);
    }


    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, String name, Block texture, RenderType renderType) {
        return this.wallBlock(block.get(), name, texture, renderType);
    }

    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, String name, ResourceLocation texture, RenderType renderType) {
        return this.wallBlock(block.get(), name, texture, renderType);
    }

    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, String name, Supplier<? extends Block> texture) {
        return this.wallBlock(block.get(), name, texture);
    }

    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, String name, Block texture) {
        return this.wallBlock(block.get(), name, texture);
    }

    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, String name, ResourceLocation texture) {
        var b = block.get();
        this.wallBlock(b, name, texture);
        return this.getMultipartBuilder(b);
    }

    public final MultiPartBlockStateBuilder wallBlock(WallBlock block, String name, Supplier<? extends Block> texture, RenderType renderType) {
        return this.wallBlock(block, name, texture.get(), renderType);
    }


    public final MultiPartBlockStateBuilder wallBlock(WallBlock block, String name, Block texture, RenderType renderType) {
        return this.wallBlock(block, name, this.texture(texture), renderType);
    }

    public final MultiPartBlockStateBuilder wallBlock(WallBlock block, String name, ResourceLocation texture, RenderType renderType) {
        this.wallBlockWithRenderType(block, name, texture, renderType);
        return this.getMultipartBuilder(block);
    }

    @Override
    @Deprecated
    public void wallBlockWithRenderType(WallBlock block, String name, ResourceLocation texture, String renderType) {
        super.wallBlockWithRenderType(block, name, texture, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void wallBlockWithRenderType(WallBlock block, String name, ResourceLocation texture, ResourceLocation renderType) {
        super.wallBlockWithRenderType(block, name, texture, renderType);
    }

    public final MultiPartBlockStateBuilder wallBlock(WallBlock block, String name, Supplier<? extends Block> texture) {
        return this.wallBlock(block, name, texture.get());
    }

    public final MultiPartBlockStateBuilder wallBlock(WallBlock block, String name, Block texture) {
        this.wallBlock(block, name, this.texture(texture));
        return this.getMultipartBuilder(block);
    }

    @Override
    public void wallBlock(WallBlock block, String name, ResourceLocation texture) {
        super.wallBlock(block, name, texture);
    }


    public final MultiPartBlockStateBuilder wallBlock(Supplier<? extends WallBlock> block, ModelFile post, ModelFile side, ModelFile sideTall) {
        var b = block.get();
        this.wallBlock(b, post, side, sideTall);
        return this.getMultipartBuilder(b);
    }

    public final MultiPartBlockStateBuilder wallBlock(WallBlock block, BlockModelBuilder post, BlockModelBuilder side, BlockModelBuilder sideTall) {
        this.wallBlock(block, (ModelFile) post, (ModelFile) side, (ModelFile) sideTall);
        return this.getMultipartBuilder(block);
    }

    @Override
    public void wallBlock(WallBlock block, ModelFile post, ModelFile side, ModelFile sideTall) {
        super.wallBlock(block, post, side, sideTall);
    }


    public final MultiPartBlockStateBuilder paneBlock(Supplier<? extends IronBarsBlock> block, ResourceLocation pane, ResourceLocation edge, RenderType renderType) {
        return this.paneBlock(block.get(), pane, edge, renderType);
    }

    public final MultiPartBlockStateBuilder paneBlock(Supplier<? extends IronBarsBlock> block, ResourceLocation pane, ResourceLocation edge) {
        var b = block.get();
        this.paneBlock(b, pane, edge);
        return this.getMultipartBuilder(b);
    }

    public final MultiPartBlockStateBuilder paneBlock(IronBarsBlock block, ResourceLocation pane, ResourceLocation edge, RenderType renderType) {
        this.paneBlockWithRenderType(block, pane, edge, renderType);
        return this.getMultipartBuilder(block);
    }

    @Override
    @Deprecated
    public void paneBlockWithRenderType(IronBarsBlock block, ResourceLocation pane, ResourceLocation edge, String renderType) {
        super.paneBlockWithRenderType(block, pane, edge, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void paneBlockWithRenderType(IronBarsBlock block, ResourceLocation pane, ResourceLocation edge, ResourceLocation renderType) {
        super.paneBlockWithRenderType(block, pane, edge, renderType);
    }

    @Override
    public void paneBlock(IronBarsBlock block, ResourceLocation pane, ResourceLocation edge) {
        super.paneBlock(block, pane, edge);
    }


    public final MultiPartBlockStateBuilder paneBlock(Supplier<? extends IronBarsBlock> block, String name, ResourceLocation pane, ResourceLocation edge, RenderType renderType) {
        return this.paneBlock(block.get(), name, pane, edge, renderType);
    }

    public final MultiPartBlockStateBuilder paneBlock(Supplier<? extends IronBarsBlock> block, String name, ResourceLocation pane, ResourceLocation edge) {
        var b = block.get();
        this.paneBlock(b, name, pane, edge);
        return this.getMultipartBuilder(b);
    }

    public final MultiPartBlockStateBuilder paneBlock(IronBarsBlock block, String name, ResourceLocation pane, ResourceLocation edge, RenderType renderType) {
        this.paneBlockWithRenderType(block, name, pane, edge, renderType);
        return this.getMultipartBuilder(block);
    }

    @Override
    @Deprecated
    public void paneBlockWithRenderType(IronBarsBlock block, String name, ResourceLocation pane, ResourceLocation edge, String renderType) {
        super.paneBlockWithRenderType(block, name, pane, edge, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void paneBlockWithRenderType(IronBarsBlock block, String name, ResourceLocation pane, ResourceLocation edge, ResourceLocation renderType) {
        super.paneBlockWithRenderType(block, name, pane, edge, renderType);
    }

    @Override
    public void paneBlock(IronBarsBlock block, String name, ResourceLocation pane, ResourceLocation edge) {
        super.paneBlock(block, name, pane, edge);
    }


    public final MultiPartBlockStateBuilder paneBlock(Supplier<? extends IronBarsBlock> block, ModelFile post, ModelFile side, ModelFile sideAlt, ModelFile noSide, ModelFile noSideAlt) {
        var b = block.get();
        this.paneBlock(b, post, side, sideAlt, noSide, noSideAlt);
        return this.getMultipartBuilder(b);
    }

    public final MultiPartBlockStateBuilder paneBlock(IronBarsBlock block, BlockModelBuilder post, BlockModelBuilder side, BlockModelBuilder sideAlt, BlockModelBuilder noSide, BlockModelBuilder noSideAlt) {
        this.paneBlock(block, (ModelFile) post, (ModelFile) side, (ModelFile) sideAlt, (ModelFile) noSide, (ModelFile) noSideAlt);
        return this.getMultipartBuilder(block);
    }

    @Override
    public void paneBlock(IronBarsBlock block, ModelFile post, ModelFile side, ModelFile sideAlt, ModelFile noSide, ModelFile noSideAlt) {
        super.paneBlock(block, post, side, sideAlt, noSide, noSideAlt);
    }


    public final VariantBlockStateBuilder doorBlock(Supplier<? extends DoorBlock> block, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        return this.doorBlock(block.get(), bottom, top, renderType);
    }

    public final VariantBlockStateBuilder doorBlock(Supplier<? extends DoorBlock> block, ResourceLocation bottom, ResourceLocation top) {
        var b = block.get();
        this.doorBlock(b, bottom, top);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder doorBlock(DoorBlock block, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        this.doorBlockWithRenderType(block, bottom, top, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void doorBlockWithRenderType(DoorBlock block, ResourceLocation bottom, ResourceLocation top, String renderType) {
        super.doorBlockWithRenderType(block, bottom, top, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void doorBlockWithRenderType(DoorBlock block, ResourceLocation bottom, ResourceLocation top, ResourceLocation renderType) {
        super.doorBlockWithRenderType(block, bottom, top, renderType);
    }

    @Override
    public void doorBlock(DoorBlock block, ResourceLocation bottom, ResourceLocation top) {
        super.doorBlock(block, bottom, top);
    }


    public final VariantBlockStateBuilder doorBlock(Supplier<? extends DoorBlock> block, String name, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        return this.doorBlock(block.get(), name, bottom, top, renderType);
    }

    public final VariantBlockStateBuilder doorBlock(Supplier<? extends DoorBlock> block, String name, ResourceLocation bottom, ResourceLocation top) {
        var b = block.get();
        this.doorBlock(b, name, bottom, top);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder doorBlock(DoorBlock block, String name, ResourceLocation bottom, ResourceLocation top, RenderType renderType) {
        this.doorBlockWithRenderType(block, name, bottom, top, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void doorBlockWithRenderType(DoorBlock block, String name, ResourceLocation bottom, ResourceLocation top, String renderType) {
        super.doorBlockWithRenderType(block, name, bottom, top, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void doorBlockWithRenderType(DoorBlock block, String name, ResourceLocation bottom, ResourceLocation top, ResourceLocation renderType) {
        super.doorBlockWithRenderType(block, name, bottom, top, renderType);
    }

    @Override
    public void doorBlock(DoorBlock block, String name, ResourceLocation bottom, ResourceLocation top) {
        super.doorBlock(block, name, bottom, top);
    }


    public final VariantBlockStateBuilder doorBlock(Supplier<? extends DoorBlock> block, ModelFile bottomLeft, ModelFile bottomLeftOpen, ModelFile bottomRight, ModelFile bottomRightOpen, ModelFile topLeft, ModelFile topLeftOpen, ModelFile topRight, ModelFile topRightOpen) {
        var b = block.get();
        this.doorBlock(b, bottomLeft, bottomLeftOpen, bottomRight, bottomRightOpen, topLeft, topLeftOpen, topRight, topRightOpen);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder doorBlock(DoorBlock block, BlockModelBuilder bottomLeft, BlockModelBuilder bottomLeftOpen, BlockModelBuilder bottomRight, BlockModelBuilder bottomRightOpen, BlockModelBuilder topLeft, BlockModelBuilder topLeftOpen, BlockModelBuilder topRight, ModelFile topRightOpen) {
        this.doorBlock(block, (ModelFile) bottomLeft, (ModelFile) bottomLeftOpen, (ModelFile) bottomRight, (ModelFile) bottomRightOpen, (ModelFile) topLeft, (ModelFile) topLeftOpen, (ModelFile) topRight, (ModelFile) topRightOpen);
        return this.getVariantBuilder(block);
    }

    @Override
    public void doorBlock(DoorBlock block, ModelFile bottomLeft, ModelFile bottomLeftOpen, ModelFile bottomRight, ModelFile bottomRightOpen, ModelFile topLeft, ModelFile topLeftOpen, ModelFile topRight, ModelFile topRightOpen) {
        super.doorBlock(block, bottomLeft, bottomLeftOpen, bottomRight, bottomRightOpen, topLeft, topLeftOpen, topRight, topRightOpen);
    }


    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, Supplier<? extends Block> texture, boolean orientable, RenderType renderType) {
        return this.trapdoorBlock(block.get(), texture, orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, Block texture, boolean orientable, RenderType renderType) {
        return this.trapdoorBlock(block.get(), texture, orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, ResourceLocation texture, boolean orientable, RenderType renderType) {
        return this.trapdoorBlock(block.get(), texture, orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, Supplier<? extends Block> texture, boolean orientable) {
        return this.trapdoorBlock(block.get(), texture, orientable);
    }

    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, Block texture, boolean orientable) {
        return this.trapdoorBlock(block.get(), texture, orientable);
    }

    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, ResourceLocation texture, boolean orientable) {
        var b = block.get();
        this.trapdoorBlock(b, texture, orientable);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder trapdoorBlock(TrapDoorBlock block, Supplier<? extends Block> texture, boolean orientable, RenderType renderType) {
        return this.trapdoorBlock(block, texture.get(), orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(TrapDoorBlock block, Block texture, boolean orientable, RenderType renderType) {
        return this.trapdoorBlock(block, this.texture(texture), orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(TrapDoorBlock block, ResourceLocation texture, boolean orientable, RenderType renderType) {
        this.trapdoorBlockWithRenderType(block, texture, orientable, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void trapdoorBlockWithRenderType(TrapDoorBlock block, ResourceLocation texture, boolean orientable, String renderType) {
        super.trapdoorBlockWithRenderType(block, texture, orientable, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void trapdoorBlockWithRenderType(TrapDoorBlock block, ResourceLocation texture, boolean orientable, ResourceLocation renderType) {
        super.trapdoorBlockWithRenderType(block, texture, orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(TrapDoorBlock block, Supplier<? extends Block> texture, boolean orientable) {
        return this.trapdoorBlock(block, texture.get(), orientable);
    }

    public final VariantBlockStateBuilder trapdoorBlock(TrapDoorBlock block, Block texture, boolean orientable) {
        this.trapdoorBlock(block, this.texture(texture), orientable);
        return this.getVariantBuilder(block);
    }

    @Override
    public void trapdoorBlock(TrapDoorBlock block, ResourceLocation texture, boolean orientable) {
        super.trapdoorBlock(block, texture, orientable);
    }


    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, String name, Supplier<? extends Block> texture, boolean orientable, RenderType renderType) {
        return this.trapdoorBlock(block.get(), name, texture, orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, String name, Block texture, boolean orientable, RenderType renderType) {
        return this.trapdoorBlock(block.get(), name, texture, orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, String name, ResourceLocation texture, boolean orientable, RenderType renderType) {
        return this.trapdoorBlock(block.get(), name, texture, orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, String name, Supplier<? extends Block> texture, boolean orientable) {
        return this.trapdoorBlock(block.get(), name, texture, orientable);
    }

    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, String name, Block texture, boolean orientable) {
        return this.trapdoorBlock(block.get(), name, texture, orientable);
    }

    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, String name, ResourceLocation texture, boolean orientable) {
        var b = block.get();
        this.trapdoorBlock(b, name, texture, orientable);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder trapdoorBlock(TrapDoorBlock block, String name, Supplier<? extends Block> texture, boolean orientable, RenderType renderType) {
        return this.trapdoorBlock(block, name, texture.get(), orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(TrapDoorBlock block, String name, Block texture, boolean orientable, RenderType renderType) {
        return this.trapdoorBlock(block, name, this.texture(texture), orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(TrapDoorBlock block, String name, ResourceLocation texture, boolean orientable, RenderType renderType) {
        this.trapdoorBlockWithRenderType(block, name, texture, orientable, renderType);
        return this.getVariantBuilder(block);
    }

    @Override
    @Deprecated
    public void trapdoorBlockWithRenderType(TrapDoorBlock block, String name, ResourceLocation texture, boolean orientable, String renderType) {
        super.trapdoorBlockWithRenderType(block, name, texture, orientable, renderType);
    }

    @Override
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public void trapdoorBlockWithRenderType(TrapDoorBlock block, String name, ResourceLocation texture, boolean orientable, ResourceLocation renderType) {
        super.trapdoorBlockWithRenderType(block, name, texture, orientable, renderType);
    }

    public final VariantBlockStateBuilder trapdoorBlock(TrapDoorBlock block, String name, Supplier<? extends Block> texture, boolean orientable) {
        return this.trapdoorBlock(block, name, texture.get(), orientable);
    }

    public final VariantBlockStateBuilder trapdoorBlock(TrapDoorBlock block, String name, Block texture, boolean orientable) {
        this.trapdoorBlock(block, name, this.texture(texture), orientable);
        return this.getVariantBuilder(block);
    }

    @Override
    public void trapdoorBlock(TrapDoorBlock block, String name, ResourceLocation texture, boolean orientable) {
        super.trapdoorBlock(block, name, texture, orientable);
    }


    public final VariantBlockStateBuilder trapdoorBlock(Supplier<? extends TrapDoorBlock> block, ModelFile bottom, ModelFile top, ModelFile open, boolean orientable) {
        var b = block.get();
        this.trapdoorBlock(b, bottom, top, open, orientable);
        return this.getVariantBuilder(b);
    }

    public final VariantBlockStateBuilder trapdoorBlock(TrapDoorBlock block, BlockModelBuilder bottom, BlockModelBuilder top, BlockModelBuilder open, boolean orientable) {
        this.trapdoorBlock(block, (ModelFile) bottom, (ModelFile) top, (ModelFile) open, orientable);
        return this.getVariantBuilder(block);
    }

    @Override
    public void trapdoorBlock(TrapDoorBlock block, ModelFile bottom, ModelFile top, ModelFile open, boolean orientable) {
        super.trapdoorBlock(block, bottom, top, open, orientable);
    }


    /*
     * VANILLA MODEL GENERATION
     */

    /**
     * @see CrypticBlockModelProvider#cubeAll(Block)
     * @deprecated Use {@link CrypticBlockModelProvider#cubeAll(Block) this.models().cubeAll(block)} instead.
     */
    @Override
    @Deprecated
    public BlockModelBuilder cubeAll(Block block) {
        return this.models().cubeAll(block);
    }
}
