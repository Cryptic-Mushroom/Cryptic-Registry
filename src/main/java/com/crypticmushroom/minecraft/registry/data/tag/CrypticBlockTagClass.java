package com.crypticmushroom.minecraft.registry.data.tag;

import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.TagRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;

/**
 * Block tags in Cryptic Registry have a special property where they can have linked item tags. This extension of the
 * base tag class exists to aid in that regard.
 *
 * @author Jonathing
 */
public abstract class CrypticBlockTagClass extends CrypticTagClass<Block> {
    /** @see CrypticTagClass#CrypticTagClass(String, TagRegistry) */
    public CrypticBlockTagClass(String modId) {
        super(modId, TagRegistry.get(RegistryDirectory.BLOCK));
    }

    /**
     * Creates a new block tag with the given ID and links the given item tag to it.
     *
     * @param id      The ID to give to the block tag.
     * @param itemTag The item tag to link to the block tag.
     * @return The newly created block tag.
     */
    protected TagKey<Block> tag(String id, TagKey<Item> itemTag) {
        TagKey<Block> blockTag = this.tag(id);
        DataRegistry.linkTags(this.modId, blockTag, itemTag);
        return blockTag;
    }
}
