package com.crypticmushroom.minecraft.registry.data.resource;

import com.crypticmushroom.minecraft.registry.util.Register;
import com.crypticmushroom.minecraft.registry.util.PluralString;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Lifecycle;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

public class DataResourceRegister<T> implements ResourceRegister<ResourceKey<T>>, RegistrySetBuilder.RegistryBootstrap<T>, Register<T> {
    public final RegistryInfo.Dynamic<T> registry;
    public final String modId;
    public final Lifecycle lifecycle;
    private final Set<ResourceKey<T>> normalKeys = new HashSet<>() {
        @Override
        public boolean add(ResourceKey<T> key) {
            if (!super.add(key)) {
                throw new RuntimeException("Cannot add duplicate entry: %s".formatted(key.toString()));
            }

            return true;
        }
    };
    private final Set<DataResourceKey<? extends T, T>> crypticKeys = new HashSet<>() {
        @Override
        public boolean add(DataResourceKey<? extends T, T> key) {
            if (!super.add(key)) {
                throw new RuntimeException("Cannot add duplicate entry: %s".formatted(key.toString()));
            }

            return true;
        }
    };
    private final Set<ResourceKey<T>> entries = Collections.unmodifiableSet(this.normalKeys);

    public DataResourceRegister(RegistryInfo.Dynamic<T> registry, String modId, Lifecycle lifecycle) {
        this.registry = registry;
        this.modId = modId;
        this.lifecycle = lifecycle;
    }


    /* BUILDER REGISTRATION */

    public <I extends T> DataResourceKey<I, T> register(String name, Function<BootstapContext<T>, I> function, Lifecycle lifecycle) {
        var key = DataResourceKey.create(this.registry.getKey(), this.makeResLoc(name), function, lifecycle);
        this.crypticKeys.add(key);
        this.normalKeys.add(key.cast());
        return key;
    }


    /* BOOTSTRAP REGISTRATION */

    public Holder<T> register(BootstapContext<T> bootstrap, ResourceKey<T> key, T value) {
        return bootstrap.register(key, value);
    }

    public ResourceKey<T> key(String path) {
        var key = ResourceKey.create(this.registry.getKey(), this.makeResLoc(path));
        this.normalKeys.add(key);
        return key;
    }


    /* EXTENSION */

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public PluralString getName() {
        return this.registry.name;
    }

    public Collection<ResourceKey<T>> getResources() {
        return this.entries;
    }

    @Override
    public ResourceKey<? extends Registry<T>> getRegistryKey() {
        return this.registry.getKey();
    }


    /* REGISTRATION */

    protected void bootstrap(BootstapContext<T> bootstrap) {
        this.crypticKeys.forEach(entry -> bootstrap.register(entry.cast(), entry.apply(bootstrap), entry.lifecycle));
    }

    @Override
    public final void run(BootstapContext<T> context) {
        this.bootstrap(context);
    }
}
