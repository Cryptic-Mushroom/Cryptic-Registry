package com.crypticmushroom.minecraft.registry.data.resource;

import com.crypticmushroom.minecraft.registry.util.PluralString;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.IEventBus;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class CustomResourceRegister<T, A> implements ResourceRegister<T> {
    private final String modId;
    private final PluralString name;
    public final RegistryInfo.Custom<T> registry;

    protected final Set<CustomResource<? extends T, A>> entries = new TreeSet<>();

    public CustomResourceRegister(String modId, PluralString name, RegistryInfo.Custom<T> registry) {
        this.modId = modId;
        this.name = name;
        this.registry = registry;
    }

    public abstract void register(IEventBus eventBus);

    public <E extends T> CustomResource<E, A> register(String name, Function<A, E> factory) {
        var resource = CustomResource.create(this.makeResLoc(name), factory);
        this.entries.add(resource);
        return resource;
    }

    @Override
    public Collection<T> getResources() {
        return this.entries.stream().map(CustomResource::get).collect(Collectors.toSet());
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public PluralString getName() {
        return this.name;
    }

    public static final class CreativeModeTabs extends CustomResourceRegister<CreativeModeTab, CreativeModeTabEvent.Register> {
        public CreativeModeTabs(String modId) {
            super(modId, RegistryDirectory.CREATIVE_MODE_TAB.name, RegistryDirectory.CREATIVE_MODE_TAB);
        }

        @Override
        public void register(IEventBus eventBus) {
            eventBus.addListener(this::onCreativeTabRegister);
        }

        private void onCreativeTabRegister(CreativeModeTabEvent.Register event) {
            this.entries.forEach(e -> e.apply(event));
        }
    }
}
