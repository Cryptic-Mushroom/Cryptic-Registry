package com.crypticmushroom.minecraft.registry.data.registry;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.data.tag.CrypticTagClass;
import com.crypticmushroom.minecraft.registry.util.PluralString;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.ApiStatus.Internal;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Due to the game-oriented runtime nature of tags, a custom data registry for tags is used in order to interact with
 * them during data generation. This also gives us the ability to iterate through tags and assign parents and children
 * through our own data structures without the need of relying on Minecraft or Forge.
 *
 * @param <T> The type of tag for the tag registry.
 * @author Jonathing
 * @implNote Due to the complex nature of tags, I would not recommend making and maintaining your own tag registry.
 *     If you would like one added, feel free to make an issue on GitLab.
 */
public sealed class TagRegistry<T> permits TagRegistry.Item {
    private static final List<TagRegistry<?>> ALL_NORMAL_EXCEPT_BLOCK_AND_ITEM = new LinkedList<>();
    private static final List<TagRegistry<?>> ALL_DATA = new LinkedList<>();
    private static final Map<RegistryInfo<?, ?>, TagRegistry<?>> ALL = new LinkedHashMap<>();

    public final PluralString name;
    public final RegistryInfo<T, ?> registryInfo;

    private final Map<String, Set<TagKey<T>>> tags = DataRegistry.create();
    private final Map<String, Map<TagKey<T>, Set<ResourceKey<T>>>> tagMap = DataRegistry.create();
    private final Map<String, Map<TagKey<T>, Set<TagKey<T>>>> tagOfTags = DataRegistry.create();
    private final Map<String, Map<TagKey<T>, Set<TagKey<T>>>> tagOfTagsReverse = DataRegistry.create();

    @Deprecated
    private TagRegistry(PluralString name, ResourceKey<? extends Registry<T>> registryKey) {
        this(name, RegistryDirectory.get(registryKey));
    }

    private TagRegistry(PluralString name, RegistryInfo<T, ?> registryInfo) {
        this.name = name;
        this.registryInfo = registryInfo;

        ALL.put(this.registryInfo, this);
        if (!(this.registryInfo.equals(RegistryDirectory.BLOCK) || this.registryInfo.equals(RegistryDirectory.ITEM))) {
            (registryInfo instanceof RegistryInfo.Dynamic ? ALL_DATA : ALL_NORMAL_EXCEPT_BLOCK_AND_ITEM).add(this);
        }
    }

    @Deprecated(forRemoval = true)
    public static <T> TagRegistry<T> get(ResourceKey<? extends Registry<T>> registryKey) {
        return get(RegistryDirectory.<T>get(registryKey));
    }

    @SuppressWarnings("unchecked")
    public static <T> TagRegistry<T> get(RegistryInfo<T, ?> registryInfo) {
        var tagRegistry = (TagRegistry<T>) ALL.get(registryInfo);
        if (tagRegistry != null) return tagRegistry;

        if (registryInfo.equals(RegistryDirectory.ITEM)) {
            return (TagRegistry<T>) new Item();
        }

        return new TagRegistry<>(registryInfo.name, registryInfo);
    }

    /**
     * Creates a new tag using the underlying registry key.
     *
     * @param modId The mod ID to create the new tag for.
     * @param name  The name for the tag.
     * @return The created tag.
     */
    public TagKey<T> createTag(String modId, String name) {
        return this.createTag(modId, new ResourceLocation(modId, name));
    }

    /**
     * Creates a new tag using the underlying registry key.
     *
     * @param modId The mod ID to create the new tag for.
     * @param id    The ID for the tag.
     * @return The created tag.
     *
     * @see #createTag(String, String)
     * @deprecated It is safer to use {@link #createTag(String, String)} to create tags for your mod. Only use this
     *     method if you have an edge case or if you know what you are doing.
     */
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public TagKey<T> createTag(String modId, ResourceLocation id) {
        TagKey<T> tag = TagKey.create(this.registryInfo.getKey(), id);
        this.tags.compute(modId, (k, v) -> {
            var set = v != null ? v : new TreeSet<TagKey<T>>(Comparator.comparing(TagKey::location));
            set.add(tag);
            return set;
        });
        return tag;
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public final void register(String modId, TagKey<T> tag, RegistryObject<? extends T>... object) {
        this.register(modId, tag, Arrays.stream(object).map(RegistryObject::getKey).toArray(ResourceKey[]::new));
    }

    /**
     * Registers the given suppliers of objects to the given tag.
     *
     * @param modId  The mod ID to register the objects to the tag for.
     * @param tag    The tag to register the objects to.
     * @param object The objects to register to the tag.
     */
    @SafeVarargs
    public final void register(String modId, TagKey<T> tag, ResourceKey<? extends T>... object) {
        this.tagMap.compute(modId, (id, m) -> {
            var map = m != null ? m : DataRegistry.<TagKey<T>, Set<ResourceKey<T>>>create(Comparator.comparing(TagKey::location));
            map.compute(tag, (k, v) -> {
                Set<ResourceKey<T>> set = Objects.requireNonNullElseGet(v, HashSet::new);
                for (ResourceKey<? extends T> key : object) {
                    //noinspection unchecked
                    if (!set.add((ResourceKey<T>) key)) {
                        var message = "Adding duplicate object to %s tag registry: %s".formatted(this.name.singular(), key.location().toString());
                        var exception = new IllegalStateException(message);
                        CrypticRegistry.LOGGER.error(exception);
                    }
                }
                return set;
            });
            return map;
        });
    }

    /**
     * Registers the given tags to the given parent tag (the first argument after the mod ID).
     *
     * @param modId   The mod ID to register the nested tags for.
     * @param tag     The parent tag.
     * @param toAddTo The tags to add to the parent tag.
     */
    @SafeVarargs
    public final void register(String modId, TagKey<T> tag, TagKey<T>... toAddTo) {
        this.tagOfTags.compute(modId, (id, m) -> {
            var map = m != null ? m : DataRegistry.<TagKey<T>, Set<TagKey<T>>>create(Comparator.comparing(TagKey::location));
            map.compute(tag, (k, v) -> {
                Set<TagKey<T>> set = Objects.requireNonNullElseGet(v, () -> new TreeSet<TagKey<T>>(Comparator.comparing(TagKey::location)));
                Collections.addAll(set, toAddTo);
                return set;
            });
            return map;
        });

        this.tagOfTagsReverse.compute(modId, (id, m) -> {
            var map = m != null ? m : DataRegistry.<TagKey<T>, Set<TagKey<T>>>create(Comparator.comparing(TagKey::location));
            for (TagKey<T> t : toAddTo) {
                map.compute(t, (k, v) -> {
                    Set<TagKey<T>> set = Objects.requireNonNullElseGet(v, () -> new TreeSet<TagKey<T>>(Comparator.comparing(TagKey::location)));
                    set.add(tag);
                    return set;
                });
            }
            return map;
        });
    }

    /**
     * For the given mod ID, iterates upon each registered tag.
     *
     * @param modId  The mod ID to get the tags for.
     * @param action The action to execute for each tag.
     */
    public void forTags(String modId, Consumer<? super TagKey<T>> action) {
        this.tags.getOrDefault(modId, ImmutableSet.of()).forEach(action);
    }

    /**
     * For the given mod ID, iterates through each tag and their set of objects.
     * <p>
     * Note that the {@link BiConsumer} requires a {@link TagKey} and the generic type because it will iterate through
     * the set of each tag.
     *
     * @param modId  The mod ID to get the tags for.
     * @param action The action to execute on each pair of tag and its set of objects.
     */
    public void forTagObjectEntries(String modId, BiConsumer<? super TagKey<T>, ResourceKey<T>> action) {
        this.tagMap.getOrDefault(modId, ImmutableMap.of()).forEach((k, v) -> {
            v.stream().sorted(Comparator.comparing(ResourceKey::location)).forEach(t -> action.accept(k, t));
        });
    }

    /**
     * For the given mod ID, iterates through each tag and their set of tags.
     * <p>
     * Note that the {@link BiConsumer} requires two {@link TagKey} arguments because it will iterate through the set of
     * each tag.
     *
     * @param modId  The mod ID to get the tags for.
     * @param action The action to execute on each pair of tag and its set of tags.
     */
    // NOTE: This method reverses the key and value in the biconsumer
    public void forTagOfTagEntries(String modId, BiConsumer<? super TagKey<T>, ? super TagKey<T>> action) {
        this.tagOfTags.getOrDefault(modId, ImmutableMap.of()).forEach((k, v) -> v.forEach(t -> action.accept(t, k)));
    }

    /**
     * Checks if a given tag contains the given object. It will search through nested tags as well.
     *
     * @param modId  The mod ID to check for if the object is contained in the tag.
     * @param tag    The tag to check.
     * @param object The object to search for.
     * @return The result of the check.
     *
     * @apiNote Note that this method of contains checking uses direct object comparison, and as such, you should
     *     already be keeping track of your {@link Supplier suppliers} or {@link RegistryObject registry objects} when
     *     they are registered into the tag registry.
     * @see #tagContains(String, TagKey, ResourceKey, boolean)
     */
    public boolean tagContains(String modId, TagKey<T> tag, ResourceKey<T> object) {
        return this.tagContains(modId, tag, object, true);
    }

    /**
     * Checks if a given tag contains the given object.
     *
     * @param modId     The mod ID to check for if the object is contained in the tag.
     * @param tag       The tag to check.
     * @param object    The object to search for.
     * @param recursive Whether or not to search through nested tags as well.
     * @return The result of the check.
     *
     * @apiNote Note that this method of contains checking uses direct object comparison, and as such, you should
     *     already be keeping track of your {@link Supplier suppliers} or {@link RegistryObject registry objects} when
     *     they are registered into the tag registry.
     */
    public boolean tagContains(String modId, TagKey<T> tag, ResourceKey<T> object, boolean recursive) {
        return recursive
               ? this.getEntrySet(modId, tag).contains(object)
               : this.tagMap.getOrDefault(modId, ImmutableMap.of()).getOrDefault(tag, ImmutableSet.of()).contains(object);
    }

    public boolean isEmpty() {
        return this.tags.isEmpty() && this.tagMap.isEmpty() && this.tagOfTags.isEmpty() && this.tagOfTagsReverse.isEmpty();
    }

    private Set<ResourceKey<T>> getEntrySet(String modId, TagKey<T> tag) {
        var set = new HashSet<>(this.tagMap.getOrDefault(modId, ImmutableMap.of()).getOrDefault(tag, ImmutableSet.of()));
        this.tagOfTagsReverse.getOrDefault(modId, ImmutableMap.of()).getOrDefault(tag, ImmutableSet.of()).forEach(t -> set.addAll(this.getEntrySet(modId, t)));
        return set;
    }

    /**
     * Applies an action for each registry <strong>except for the block and item registries</strong>. This method is
     * marked for internal usage only.
     *
     * @param action The action to execute on each tag registry.
     */
    @Internal
    public static void forEachNormalRegistry(Consumer<TagRegistry<?>> action) {
        ALL_NORMAL_EXCEPT_BLOCK_AND_ITEM.forEach(action);
    }

    @Internal
    public static void forEachDataRegistry(Consumer<TagRegistry<?>> action) {
        ALL_DATA.forEach(action);
    }

    public static final class Item extends TagRegistry<net.minecraft.world.item.Item> {
        private final Map<String, Set<ResourceLocation>> classes = DataRegistry.create();

        private Item() {
            super(RegistryDirectory.ITEM.name, RegistryDirectory.ITEM.getKey());
        }

        /**
         * Registers a {@link CrypticTagClass tag class} with the given mod ID.
         *
         * @param modId        The mod ID to register the item tag class for.
         * @param itemTagClass The item tag class to register.
         */
        @SuppressWarnings("unchecked")
        public void register(String modId, Class<? extends CrypticTagClass<net.minecraft.world.item.Item>> itemTagClass) {
            var set = new HashSet<ResourceLocation>();
            for (Field field : itemTagClass.getFields()) {
                TagKey<net.minecraft.world.item.Item> tag;
                try {
                    tag = Objects.requireNonNull((TagKey<net.minecraft.world.item.Item>) field.get(null));
                } catch (IllegalArgumentException | IllegalAccessException | ClassCastException |
                         SecurityException | NullPointerException e) {
                    continue;
                }

                set.add(tag.location());
            }
            this.classes.put(modId, set);
        }

        /**
         * Checks a given mod ID's item tag class if the given tag ID is present within.
         *
         * @param modId The mod ID whose tag class should be searched.
         * @param tagId The tag ID to search for.
         * @return The result of the check.
         */
        public boolean contains(String modId, ResourceLocation tagId) {
            return this.classes.getOrDefault(modId, Collections.emptySet()).contains(tagId);
        }
    }
}
