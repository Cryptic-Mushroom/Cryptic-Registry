package com.crypticmushroom.minecraft.registry.data.resource;

import com.crypticmushroom.minecraft.registry.util.Lazy;
import com.mojang.serialization.Lifecycle;
import net.minecraft.core.Registry;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;

public class DataResourceKey<T extends S, S> extends ResourceKey<T> implements Function<BootstapContext<S>, T>, Lazy<T> {
    private final Function<BootstapContext<S>, T> factory;
    public final Lifecycle lifecycle;

    private @Nullable T instance = null;

    @Override
    public T apply(BootstapContext<S> bootstrap) {
        var result = this.factory.apply(bootstrap);
        this.instance = result;
        return result;
    }

    /**
     * To accommodate for systems needing access to the object after it has been created.
     *
     * @return The object if it exists, or null of it has not been created.
     */
    @Override
    public final @Nullable T get() {
        return this.instance;
    }

    @Override
    public boolean isResolved() {
        return this.instance != null;
    }

    @SuppressWarnings("unchecked")
    public ResourceKey<S> cast() {
        return (ResourceKey<S>) this;
    }

    @SuppressWarnings("unchecked")
    public DataResourceKey<S, S> castSelf() {
        return (DataResourceKey<S, S>) this;
    }


    /* INTERNAL EXTENSION OF RESOURCE KEY */

    /** @see ResourceKey#create(ResourceKey, ResourceLocation) */
    @SuppressWarnings("unchecked")
    public static <T extends S, S> DataResourceKey<T, S> create(ResourceKey<? extends Registry<? super T>> registryKey, ResourceLocation location, Function<BootstapContext<S>, T> factory, Lifecycle lifecycle) {
        return (DataResourceKey<T, S>) ResourceKey.VALUES.computeIfAbsent(new ResourceKey.InternKey(registryKey.location(), location), (internKey) -> new DataResourceKey<>(internKey.registry(), internKey.location(), factory, lifecycle));
    }

    protected DataResourceKey(ResourceLocation registryName, ResourceLocation location, Function<BootstapContext<S>, T> factory, Lifecycle lifecycle) {
        super(registryName, location);
        this.factory = factory;
        this.lifecycle = lifecycle;
    }
}
