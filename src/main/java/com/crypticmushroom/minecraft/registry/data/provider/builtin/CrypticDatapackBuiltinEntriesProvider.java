package com.crypticmushroom.minecraft.registry.data.provider.builtin;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraftforge.common.data.DatapackBuiltinEntriesProvider;
import net.minecraftforge.data.event.GatherDataEvent;

import java.util.Collections;

public class CrypticDatapackBuiltinEntriesProvider extends DatapackBuiltinEntriesProvider implements ICrypticDataProvider {
    private final GatherDataEvent event;
    private final String modId;

    public CrypticDatapackBuiltinEntriesProvider(String modId, GatherDataEvent event) {
        this(modId, event, CrypticRegistry.getDataBuilder(modId));
    }

    public CrypticDatapackBuiltinEntriesProvider(String modId, GatherDataEvent event, RegistrySetBuilder datapackEntriesBuilder) {
        super(event.getGenerator().getPackOutput(), event.getLookupProvider(), datapackEntriesBuilder, Collections.singleton(modId));
        this.event = event;
        this.modId = modId;
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public String getName() {
        return ICrypticDataProvider.super.getName();
    }

    @Override
    public String getSimpleName() {
        return "Datapack Registries";
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public boolean shouldRun() {
        return this.event.includeServer();
    }
}
