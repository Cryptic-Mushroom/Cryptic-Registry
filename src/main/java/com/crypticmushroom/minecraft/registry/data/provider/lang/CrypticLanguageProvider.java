package com.crypticmushroom.minecraft.registry.data.provider.lang;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import net.minecraftforge.common.data.LanguageProvider;
import net.minecraftforge.data.event.GatherDataEvent;

/**
 * If you have been properly using the {@link RegistryObjectBuilder}, you may have noticed the mandatory usage of
 * {@link RegistryObjectBuilder.Named#localizedName(String)}. This is so that the builder can then save the localized
 * name to a {@link LocalizedNameRegistry}. When data generation is run, this is where the magic happens.
 *
 * @author Jonathing
 */
public class CrypticLanguageProvider extends LanguageProvider implements ICrypticDataProvider {
    private final GatherDataEvent event;
    private final String modId;
    private final String locale;

    /**
     * Creates a new language provider with the given locale, and ties it with the given event and mod ID. <strong>Note
     * that no matter what locale you use, all registered localized names will be dumped into this language
     * provider.</strong>
     *
     * @param modId  The mod ID to tie this provider to.
     * @param event  The event to tie this provider to.
     * @param locale The locale to use for this provider.
     */
    public CrypticLanguageProvider(String modId, GatherDataEvent event, String locale) {
        super(event.getGenerator().getPackOutput(), modId, locale);

        this.event = event;
        this.modId = modId;
        this.locale = locale;
    }

    @Override
    protected void addTranslations() {
        LocalizedNameRegistry.forEach(this.modId, this::add);
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public String getName() {
        return ICrypticDataProvider.super.getName();
    }

    @Override
    public String getSimpleName() {
        return "Localizations for " + this.locale;
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public boolean shouldRun() {
        return this.event.includeClient() || this.event.includeServer();
    }
}
