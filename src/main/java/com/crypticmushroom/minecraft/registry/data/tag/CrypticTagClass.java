package com.crypticmushroom.minecraft.registry.data.tag;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.TagRegistry;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;

import java.util.HashMap;
import java.util.Map;

/**
 * To aid in the creation and maintenance of tags, a barebones tag class provided by Cryptic Registry can be used. For
 * an extensive look on how to effectively use this class, see the wiki.
 *
 * @param <T> The type of object for the tags in this class.
 * @author Jonathing
 */
public abstract class CrypticTagClass<T> {
    protected final String modId;
    protected final TagRegistry<T> registry;

    private final AppenderHolder appenders = new AppenderHolder();

    /**
     * Creates a new tag class instance. <strong>For a stable implementation, only one instance should never need to
     * exist at any given point.</strong>
     *
     * @param modId    The mod ID to use.
     * @param registry The tag registry to use.
     */
    public CrypticTagClass(String modId, TagRegistry<T> registry) {
        this.modId = modId;
        this.registry = registry;
    }

    /**
     * Creates an appender for the given tag.
     *
     * @param tag The tag to create the appender for.
     * @return The newly created appender.
     *
     * @see CrypticTagAppender
     */
    protected CrypticTagAppender<T> appender(TagKey<T> tag) {
        return this.appenders.get(tag);
    }

    /**
     * Creates a new tag with the given ID.
     *
     * @param id The ID for the tag.
     * @return The newly-created tag.
     */
    protected TagKey<T> tag(String id) {
        return this.registry.createTag(this.modId, id);
    }

    /**
     * Creates a new tag with the given ID.
     *
     * @param id The ID for the tag.
     * @return The newly-created tag.
     *
     * @deprecated For the same reason that {@link TagRegistry#createTag(String, ResourceLocation)} is deprecated.
     */
    @Deprecated
    protected TagKey<T> tag(ResourceLocation id) {
        return this.registry.createTag(this.modId, id);
    }

    /**
     * This method is called by {@link CrypticRegistry} once the related registry class has been statically
     * initialized.
     */
    public void dataRegisterTags() {
        this.applyAppenders();
        this.appenders.save();
    }

    /**
     * This method is to be implemented by the subclass so that {@link #appender(TagKey) appenders} can be used to add
     * data to the newly created tags.
     */
    protected abstract void applyAppenders();

    private class AppenderHolder {
        private boolean saved = false;

        private Map<TagKey<T>, CrypticTagAppender<T>> appenders = new HashMap<>();

        CrypticTagAppender<T> get(TagKey<T> tag) {
            if (this.saved) {
                throw new IllegalStateException("Cannot create new appenders for a tag class that already has its tags data registered!");
            }

            // does some normal compute logic on the map
            // if you don't know already: it's checking if the appender already exists in the map
            // if it doesn't, we will make a new one and add it in
            return this.appenders.compute(tag, (t, a) -> a != null ? a : new CrypticTagAppender<>(CrypticTagClass.this.modId, tag, CrypticTagClass.this.registry));
        }

        void save() {
            this.appenders.forEach((tag, appender) -> appender.finish());
            this.saved = true;
            this.appenders = null;
        }
    }
}
