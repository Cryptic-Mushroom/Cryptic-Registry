package com.crypticmushroom.minecraft.registry.data.registry;

import com.crypticmushroom.minecraft.registry.data.resource.DataResourceKey;
import com.crypticmushroom.minecraft.registry.data.resource.CustomResource;
import com.google.common.collect.ImmutableMap;
import net.minecraft.stats.StatType;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.RegistryObject;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * A localized name registry is an effective {@link DataRegistry data registry} that contains a map of translation keys
 * and value for a given mod ID.
 *
 * @param <T> The type of object to generate the localized name for.
 */
@SuppressWarnings("StaticInitializerReferencesSubClass")
public class LocalizedNameRegistry<T, S extends Supplier<T>> {
    // ALL REGISTRIES
    private static final List<LocalizedNameRegistry<?, ?>> ALL = new LinkedList<>();

    // UNIVERSAL REGISTRY
    public static final RawLocalizedNameRegistry OTHER = new RawLocalizedNameRegistry();

    // SPECIFIC REGISTRIES
    public static final LocalizedNameRegistry.Normal<Block> BLOCK = normal(block -> block.get().getDescriptionId());
    public static final LocalizedNameRegistry.Normal<Item> ITEM = normal(item -> item.get().getDescriptionId());
    public static final LocalizedNameRegistry.Normal<Enchantment> ENCHANTMENT = normal(enchantment -> enchantment.get().getDescriptionId());
    public static final LocalizedNameRegistry.Normal<MobEffect> MOB_EFFECT = normal(effect -> effect.get().getDescriptionId());
    public static final LocalizedNameRegistry.Normal<EntityType<?>> ENTITY_TYPE = normal(entity -> entity.get().getDescriptionId());
    public static final LocalizedNameRegistry.Normal<Attribute> ATTRIBUTE = normal(attribute -> attribute.get().getDescriptionId());
    public static final LocalizedNameRegistry.Normal<StatType<?>> STAT_TYPE = normal(attribute -> attribute.get().getTranslationKey());

    private final Map<String, Map<S, String>> localizedNames = DataRegistry.create();
    private final Function<S, String> mapper;

    private static <T> LocalizedNameRegistry.Normal<T> normal(Function<RegistryObject<T>, String> mapper) {
        return new LocalizedNameRegistry.Normal<>(mapper);
    }

    private static <T> LocalizedNameRegistry.Data<T> data(Function<DataResourceKey<T, T>, String> mapper) {
        return new LocalizedNameRegistry.Data<>(mapper);
    }

    public LocalizedNameRegistry(Function<S, String> mapper) {
        this.mapper = mapper;
        ALL.add(this);
    }

    /**
     * Registers a given translation value to a given object for the given mod ID.
     *
     * @param modId       The mod ID to register the translation for.
     * @param key         The translation key.
     * @param translation The translation value to register.
     */
    public void register(String modId, S key, String translation) {
        this.localizedNames.compute(modId, (k, v) -> {
            var map = v != null ? v : DataRegistry.<S, String>create();
            map.put(key, translation);
            return map;
        });
    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public void register(String modId, Object registered, String localizedName) {
        this.register(modId, (S) registered, localizedName);
    }

    protected void forEachMapped(String modId, BiConsumer<? super String, ? super String> action) {
        this.localizedNames.getOrDefault(modId, ImmutableMap.of()).forEach((k, v) -> action.accept(this.mapper.apply(k), v));
    }

    public static void forEach(String modId, BiConsumer<? super String, ? super String> action) {
        ALL.forEach(registry -> registry.forEachMapped(modId, action));
    }

    public static class Normal<T> extends LocalizedNameRegistry<T, RegistryObject<T>> {
        public Normal(Function<RegistryObject<T>, String> mapper) {
            super(mapper);
        }
    }

    public static class Static<T> extends LocalizedNameRegistry<T, CustomResource<T, ?>> {
        public Static(Function<CustomResource<T, ?>, String> mapper) {
            super(mapper);
        }
    }

    public static class Data<T> extends LocalizedNameRegistry<T, DataResourceKey<T, T>> {
        public Data(Function<DataResourceKey<T, T>, String> mapper) {
            super(mapper);
        }
    }

    public static final class RawLocalizedNameRegistry extends LocalizedNameRegistry<String, Supplier<String>> {
        private final Map<String, Map<String, String>> localizedNames = DataRegistry.create();

        private RawLocalizedNameRegistry() {
            super(String::valueOf);
        }

        @Override
        public void register(String modId, Supplier<String> key, String translation) {
            this.register(modId, key.get(), translation);
        }

        public void register(String modId, String key, String translation) {
            this.localizedNames.compute(modId, (k, v) -> {
                var map = v != null ? v : DataRegistry.<String, String>create();
                map.put(key, translation);
                return map;
            });
        }

        public void registerReplace(String modId, String key, String translation) {
            this.localizedNames.compute(modId, (k, v) -> {
                var map = v != null ? v : DataRegistry.<String, String>create();
                map.compute(key, (s, t) -> translation);
                return map;
            });
        }

        @Override
        protected void forEachMapped(String modId, BiConsumer<? super String, ? super String> action) {
            this.localizedNames.getOrDefault(modId, ImmutableMap.of()).forEach(action);
        }
    }
}
