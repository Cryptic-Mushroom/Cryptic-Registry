package com.crypticmushroom.minecraft.registry.data.registry;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import com.crypticmushroom.minecraft.registry.data.provider.loot.CrypticBlockLootSubProvider;
import com.crypticmushroom.minecraft.registry.data.provider.loot.CrypticEntityLootSubProvider;
import com.crypticmushroom.minecraft.registry.data.provider.model.CrypticBlockStateProvider;
import com.crypticmushroom.minecraft.registry.data.provider.model.CrypticItemModelProvider;
import com.crypticmushroom.minecraft.registry.data.provider.particle.CrypticParticleDescriptionProvider;
import com.crypticmushroom.minecraft.registry.data.provider.sound.CrypticSoundsDefinitionsProvider;
import com.crypticmushroom.minecraft.registry.data.provider.tag.CrypticBlockTagsProvider;
import com.mojang.datafixers.util.Pair;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.data.loot.packs.VanillaBlockLoot;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.model.generators.IGeneratedBlockState;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.common.data.SoundDefinition;
import net.minecraftforge.common.util.MutableHashedLinkedMap;
import net.minecraftforge.registries.RegistryObject;
import org.apache.commons.lang3.function.TriFunction;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Spliterator;
import java.util.TreeMap;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The Cryptic Registry data registry contains various internal registries to store information that needs to be data
 * generated into static JSON objects.
 *
 * @author Jonathing
 * @apiNote This entire class is marked for internal usage only because the various registries it contains are
 *     handled by either {@link RegistryObjectBuilder builders} or the {@link ICrypticDataProvider data providers}.
 */
@Internal
public final class DataRegistry {
    // LINKED BLOCK AND ITEM TAGKEYS
    private static final Map<String, Map<TagKey<Block>, TagKey<Item>>> LINKED_TAGS = create();

    /**
     * This method will link an item tag to a given block tag for the given mod ID.
     *
     * @param modId    The mod ID to link the tags for.
     * @param blockTag The block tag.
     * @param itemTag  The item tag to link to the block tag.
     * @see CrypticBlockTagsProvider
     */
    public static void linkTags(String modId, TagKey<Block> blockTag, TagKey<Item> itemTag) {
        LINKED_TAGS.compute(modId, (k, v) -> {
            var map = v != null ? v : DataRegistry.<TagKey<Block>, TagKey<Item>>create();
            map.put(blockTag, itemTag);
            return map;
        });
    }

    @Internal
    public static void forLinkedTags(String modId, BiConsumer<? super TagKey<Block>, ? super TagKey<Item>> action) {
        LINKED_TAGS.compute(modId, (k, v) -> {
            var map = v != null ? v : DataRegistry.<TagKey<Block>, TagKey<Item>>create();
            map.forEach(action);
            return map;
        });
    }


    // EXPLOSION RESISTANT BLOCKS

    private static final Set<Supplier<? extends ItemLike>> EXPLOSION_RESISTANT = new HashSet<>() {
        private boolean resolved = false;

        private void resolve() {
            if (this.resolved) return;

            VanillaBlockLoot.EXPLOSION_RESISTANT.forEach(item -> this.add(() -> item));
            this.resolved = true;
        }

        @Override
        public Iterator<Supplier<? extends ItemLike>> iterator() {
            this.resolve();
            return super.iterator();
        }

        @Override
        public int size() {
            this.resolve();
            return super.size();
        }

        @Override
        public boolean contains(Object o) {
            this.resolve();
            return super.contains(o);
        }

        @Override
        public boolean remove(Object o) {
            this.resolve();
            return super.remove(o);
        }

        @Override
        public Spliterator<Supplier<? extends ItemLike>> spliterator() {
            this.resolve();
            return super.spliterator();
        }

        @Override
        public Object[] toArray() {
            this.resolve();
            return super.toArray();
        }

        @Override
        public <T> T[] toArray(T[] a) {
            this.resolve();
            return super.toArray(a);
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            this.resolve();
            return super.removeAll(c);
        }

        @Override
        public boolean containsAll(@NotNull Collection<?> c) {
            this.resolve();
            return super.containsAll(c);
        }

        @Override
        public boolean retainAll(@NotNull Collection<?> c) {
            this.resolve();
            return super.retainAll(c);
        }

        @Override
        public <T> T[] toArray(IntFunction<T[]> generator) {
            this.resolve();
            return super.toArray(generator);
        }

        @Override
        public boolean removeIf(Predicate<? super Supplier<? extends ItemLike>> filter) {
            this.resolve();
            return super.removeIf(filter);
        }

        @Override
        public Stream<Supplier<? extends ItemLike>> stream() {
            this.resolve();
            return super.stream();
        }

        @Override
        public Stream<Supplier<? extends ItemLike>> parallelStream() {
            this.resolve();
            return super.parallelStream();
        }

        @Override
        public void forEach(Consumer<? super Supplier<? extends ItemLike>> action) {
            this.resolve();
            super.forEach(action);
        }
    };

    public static void registerExplosionResistant(Supplier<? extends ItemLike> item) {
        EXPLOSION_RESISTANT.add(item);
    }

    public static Set<Item> getExplosionResistant() {
        return EXPLOSION_RESISTANT.stream().map(Supplier::get).map(ItemLike::asItem).collect(Collectors.toSet());
    }


    private static final Map<String, List<Pair<Supplier<? extends Item>, CreativeModeTabBuildOptions>>> CREATIVE_MODE_TAB_ITEMS = create();

    public static void registerCreativeModeTabBuildOptions(String modId, Supplier<? extends Item> item, Collection<CreativeModeTabBuildOptions> options) {
        CREATIVE_MODE_TAB_ITEMS.compute(modId, (k, v) -> {
            var list = v != null ? v : new LinkedList<Pair<Supplier<? extends Item>, CreativeModeTabBuildOptions>>();
            options.forEach(o -> list.add(Pair.of(item, o)));
            return list;
        });
    }

    public static void forCreativeModeTabBuildOptions(String modId, BiConsumer<? super Supplier<? extends Item>, ? super CreativeModeTabBuildOptions> action) {
        CREATIVE_MODE_TAB_ITEMS.compute(modId, (k, v) -> v != null ? v : new LinkedList<>()).forEach(p -> action.accept(p.getFirst(), p.getSecond()));
    }

    public record CreativeModeTabBuildOptions(Supplier<? extends CreativeModeTab> tab, OutputOptions conditions,
                                              CreativeModeTab.TabVisibility visibility) {
        public CreativeModeTabBuildOptions(Supplier<? extends CreativeModeTab> tab, OutputOptions conditions) {
            this(tab, conditions, CreativeModeTab.TabVisibility.PARENT_AND_SEARCH_TABS);
        }
    }

    @FunctionalInterface
    public interface OutputOptions extends TriFunction<CreativeModeTab.ItemDisplayParameters, MutableHashedLinkedMap<ItemStack, CreativeModeTab.TabVisibility>, Item, ItemStack> {
        @Override
        @Nullable ItemStack apply(CreativeModeTab.ItemDisplayParameters parameters, MutableHashedLinkedMap<ItemStack, CreativeModeTab.TabVisibility> entries, Item item);
    }


    // PARTICLE TEXTURES
    private static final Map<String, Map<Supplier<? extends ParticleType<?>>, List<ResourceLocation>>> PARTICLE_TEXTURES = create();

    /**
     * This method will register a list of textures (as {@link ResourceLocation resource locations}) to a given particle
     * ID for the given mod ID.
     *
     * @param modId    The mod ID to register the textures for.
     * @param particle The particle ID.
     * @param textures The list of textures to register to the particle ID.
     * @see CrypticParticleDescriptionProvider
     */
    public static void registerParticleTextures(String modId, Supplier<? extends ParticleType<?>> particle, List<ResourceLocation> textures) {
        PARTICLE_TEXTURES.compute(modId, (k, v) -> {
            var map = v != null ? v : DataRegistry.<Supplier<? extends ParticleType<?>>, List<ResourceLocation>>create();
            map.put(particle, textures);
            return map;
        });
    }

    @Internal
    public static void forParticleTextures(String modId, BiConsumer<? super ParticleType<?>, ? super List<ResourceLocation>> action) {
        PARTICLE_TEXTURES.compute(modId, (k, v) -> {
            var map = v != null ? v : DataRegistry.<Supplier<? extends ParticleType<?>>, List<ResourceLocation>>create();
            map.forEach((supplier, textures) -> action.accept(supplier.get(), textures));
            return map;
        });
    }


    // SOUND DEFINITIONS
    private static final Map<String, Map<Supplier<? extends SoundEvent>, Supplier<SoundDefinition>>> SOUND_DEFINITIONS = create();

    /**
     * This method will register a given sound definition to the given sound event for the given mod ID.
     *
     * @param modId      The mod ID to register the sound definition for.
     * @param event      The sound event.
     * @param definition The sound definition to register to the sound event.
     * @see CrypticSoundsDefinitionsProvider
     */
    public static void registerSoundDefinition(String modId, Supplier<? extends SoundEvent> event, Supplier<SoundDefinition> definition) {
        SOUND_DEFINITIONS.compute(modId, (k, v) -> {
            var map = v != null ? v : DataRegistry.<Supplier<? extends SoundEvent>, Supplier<SoundDefinition>>create();
            map.put(event, definition);
            return map;
        });
    }

    @Internal
    public static void forSoundDefinitions(String modId, BiConsumer<? super SoundEvent, ? super SoundDefinition> action) {
        SOUND_DEFINITIONS.compute(modId, (k, v) -> {
            var map = v != null ? v : DataRegistry.<Supplier<? extends SoundEvent>, Supplier<SoundDefinition>>create();
            map.forEach((event, definition) -> action.accept(event.get(), definition.get()));
            return map;
        });
    }


    // BLOCK STATES
    private static final Map<String, List<BlockStateBuilderAction<?, ?, ?, ?>>> BLOCKSTATE_BUILDERS = create();

    @SuppressWarnings("unchecked")
    public static <S extends RegistryObject<B>, B extends Block, P extends CrypticBlockStateProvider, R extends IGeneratedBlockState> void registerBlockstateBuilder(String modId, S block, BiFunction<P, S, R> builder) {
        registerBlockstateBuilder(modId, block, (Class<P>) CrypticBlockStateProvider.class, builder);
    }

    public static <S extends RegistryObject<B>, B extends Block, P extends CrypticBlockStateProvider, R extends IGeneratedBlockState> void registerBlockstateBuilder(String modId, S block, Class<P> providerType, BiFunction<P, S, R> builder) {
        BLOCKSTATE_BUILDERS.compute(modId, (k, v) -> {
            var set = v != null ? v : new ArrayList<BlockStateBuilderAction<?, ?, ?, ?>>();
            set.add(new BlockStateBuilderAction<>(block, builder, providerType));
            return set;
        });
    }

    public static void applyBlockstateBuilders(String modId, CrypticBlockStateProvider provider) {
        BLOCKSTATE_BUILDERS.compute(modId, (k, v) -> {
            var set = v != null ? v : new ArrayList<BlockStateBuilderAction<?, ?, ?, ?>>();
            set.forEach(builder -> builder.apply(provider));
            return set;
        });
    }

    private record BlockStateBuilderAction<S extends RegistryObject<B>, B extends Block, P extends CrypticBlockStateProvider, R extends IGeneratedBlockState>(
        S block,
        BiFunction<P, S, R> builderAction,
        Class<P> providerType) {

        public R apply(CrypticBlockStateProvider provider) {
            P casted;
            try {
                casted = this.providerType.cast(provider);
            } catch (ClassCastException e) {
                throw new RuntimeException("Failed to cast provider into the type specified by the saved builder action for block [%s]".formatted(this.block.getId()), e);
            }

            return this.builderAction.apply(casted, this.block);
        }
    }


    // BLOCK LOOT
    private static final Map<String, List<BlockLootBuilderAction<?, ?, ?>>> BLOCK_LOOT_BUILDERS = create();

    @SuppressWarnings("unchecked")
    public static <S extends RegistryObject<B>, B extends Block, P extends CrypticBlockLootSubProvider> void registerBlockLootBuilder(String modId, S block, BiConsumer<P, S> builder) {
        registerBlockLootBuilder(modId, block, (Class<P>) CrypticBlockLootSubProvider.class, builder);
    }

    public static <S extends RegistryObject<B>, B extends Block, P extends CrypticBlockLootSubProvider> void registerBlockLootBuilder(String modId, S block, Class<P> providerType, BiConsumer<P, S> builder) {
        BLOCK_LOOT_BUILDERS.compute(modId, (k, v) -> {
            var set = v != null ? v : new ArrayList<BlockLootBuilderAction<?, ?, ?>>();
            set.add(new BlockLootBuilderAction<>(block, builder, providerType));
            return set;
        });
    }

    public static void applyLootBuilders(String modId, CrypticBlockLootSubProvider provider) {
        BLOCK_LOOT_BUILDERS.compute(modId, (k, v) -> {
            var set = v != null ? v : new ArrayList<BlockLootBuilderAction<?, ?, ?>>();
            set.forEach(builder -> builder.accept(provider));
            return set;
        });
    }

    private record BlockLootBuilderAction<S extends RegistryObject<B>, B extends Block, P extends CrypticBlockLootSubProvider>(
        S block,
        BiConsumer<P, S> builderAction,
        Class<P> providerType) {

        public void accept(CrypticBlockLootSubProvider provider) {
            P casted;
            try {
                casted = this.providerType.cast(provider);
            } catch (ClassCastException e) {
                throw new RuntimeException("Failed to cast provider into the type specified by the saved builder action for block [%s]".formatted(this.block.getId()), e);
            }

            this.builderAction.accept(casted, this.block);
        }
    }


    // ENTITY LOOT
    private static final Map<String, List<EntityLootBuilderAction<?, ?, ?>>> ENTITY_LOOT_BUILDERS = create();

    @SuppressWarnings("unchecked")
    public static <S extends RegistryObject<E>, E extends EntityType<?>, P extends CrypticEntityLootSubProvider> void registerEntityLootBuilder(String modId, S entity, BiConsumer<P, S> builder) {
        registerEntityLootBuilder(modId, entity, (Class<P>) CrypticEntityLootSubProvider.class, builder);
    }

    public static <S extends RegistryObject<E>, E extends EntityType<?>, P extends CrypticEntityLootSubProvider> void registerEntityLootBuilder(String modId, S entity, Class<P> providerType, BiConsumer<P, S> builder) {
        ENTITY_LOOT_BUILDERS.compute(modId, (k, v) -> {
            var set = v != null ? v : new ArrayList<EntityLootBuilderAction<?, ?, ?>>();
            set.add(new EntityLootBuilderAction<>(entity, builder, providerType));
            return set;
        });
    }

    public static void applyLootBuilders(String modId, CrypticEntityLootSubProvider provider) {
        ENTITY_LOOT_BUILDERS.compute(modId, (k, v) -> {
            var set = v != null ? v : new ArrayList<EntityLootBuilderAction<?, ?, ?>>();
            set.forEach(builder -> builder.accept(provider));
            return set;
        });
    }

    private record EntityLootBuilderAction<S extends RegistryObject<E>, E extends EntityType<?>, P extends CrypticEntityLootSubProvider>(
        S entity,
        BiConsumer<P, S> builderAction,
        Class<P> providerType) {

        public void accept(CrypticEntityLootSubProvider provider) {
            P casted;
            try {
                casted = this.providerType.cast(provider);
            } catch (ClassCastException e) {
                throw new RuntimeException("Failed to cast provider into the type specified by the saved builder action for entity [%s]".formatted(this.entity.getId()), e);
            }

            this.builderAction.accept(casted, this.entity);
        }
    }


    // ITEM MODELS
    private static final Map<String, List<ItemModelBuilderAction<?, ?, ?, ?>>> ITEM_MODEL_BUILDERS = create();

    @SuppressWarnings("unchecked")
    public static <S extends RegistryObject<I>, I extends Item, P extends CrypticItemModelProvider, R extends ItemModelBuilder> void registerItemModelBuilder(String modId, S item, BiFunction<P, S, R> builder) {
        registerItemModelBuilder(modId, item, (Class<P>) CrypticItemModelProvider.class, builder);
    }

    public static <S extends RegistryObject<I>, I extends Item, P extends CrypticItemModelProvider, R extends ItemModelBuilder> void registerItemModelBuilder(String modId, S item, Class<P> providerType, BiFunction<P, S, R> builder) {
        ITEM_MODEL_BUILDERS.compute(modId, (k, v) -> {
            var set = v != null ? v : new ArrayList<ItemModelBuilderAction<?, ?, ?, ?>>();
            set.add(new ItemModelBuilderAction<>(item, builder, providerType));
            return set;
        });
    }

    public static void applyItemModelBuilders(String modId, CrypticItemModelProvider provider) {
        ITEM_MODEL_BUILDERS.compute(modId, (k, v) -> {
            var set = v != null ? v : new ArrayList<ItemModelBuilderAction<?, ?, ?, ?>>();
            set.forEach(builder -> builder.apply(provider));
            return set;
        });
    }

    private record ItemModelBuilderAction<S extends RegistryObject<I>, I extends Item, P extends CrypticItemModelProvider, R extends ItemModelBuilder>(
        S item,
        BiFunction<P, S, R> builderAction,
        Class<P> providerType)
        implements Function<P, R> {

        @Override
        public R apply(CrypticItemModelProvider provider) {
            P casted;
            try {
                casted = this.providerType.cast(provider);
            } catch (ClassCastException e) {
                throw new RuntimeException("Failed to cast provider into the type specified by the saved builder action for item [%s]".formatted(this.item.getId()), e);
            }

            return this.builderAction.apply(casted, this.item);
        }
    }


    /**
     * Creates a map for internal usage that does not allow the overriding of pre-existing keys.
     *
     * @param <K> The type of the key.
     * @param <V> The type of the value.
     * @return The newly-created map.
     */
    static <K, V> Map<K, V> create() {
        return new HashMap<>() {
            @Override
            public V put(K key, V value) {
                if (this.containsKey(key)) {
                    throw new IllegalArgumentException(String.format("Key %s already exists!", key));
                }

                return super.put(key, value);
            }
        };
    }

    static <K, V> Map<K, V> create(Comparator<K> comparator) {
        return new TreeMap<>(comparator) {
            @Override
            public V put(K key, V value) {
                if (this.containsKey(key)) {
                    throw new IllegalArgumentException(String.format("Key %s already exists!", key));
                }

                return super.put(key, value);
            }
        };
    }
}
