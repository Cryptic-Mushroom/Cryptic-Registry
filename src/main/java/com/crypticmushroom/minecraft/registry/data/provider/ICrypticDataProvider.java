package com.crypticmushroom.minecraft.registry.data.provider;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import net.minecraft.data.DataProvider;
import net.minecraftforge.data.event.GatherDataEvent;

public interface ICrypticDataProvider extends DataProvider {
    @Override
    @SuppressWarnings("override") // ForgeItemTagsProviderMixin force overwrite
    default String getName() {
        return String.format("%s - %s", this.getDataOwnerName(), this.getSimpleName());
    }

    String getModId();

    default String getModName() {
        return CrypticRegistry.getModInfo(this.getModId()).name();
    }

    default String getDataOwnerName() {
        return this.getModName();
    }

    String getSimpleName();

    GatherDataEvent getEvent();

    boolean shouldRun();

    default void addToGenerator() {
        this.getEvent().getGenerator().addProvider(this.shouldRun(), this);
    }
}
