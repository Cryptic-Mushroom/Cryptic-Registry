package com.crypticmushroom.minecraft.registry.data.provider.advancement;

import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.CriterionTriggerInstance;
import net.minecraft.advancements.DisplayInfo;
import net.minecraft.advancements.FrameType;
import net.minecraft.advancements.RequirementsStrategy;
import net.minecraft.data.advancements.AdvancementSubProvider;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.common.data.ForgeAdvancementProvider;
import net.minecraftforge.data.event.GatherDataEvent;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public class CrypticAdvancementProvider extends ForgeAdvancementProvider implements ICrypticDataProvider {
    private final GatherDataEvent event;
    private final String modId;

    public CrypticAdvancementProvider(String modId, GatherDataEvent event, AdvancementGenerator... subProviders) {
        super(event.getGenerator().getPackOutput(), event.getLookupProvider(), event.getExistingFileHelper(), List.of(subProviders));
        this.modId = modId;
        this.event = event;
    }

    @Deprecated
    public CrypticAdvancementProvider(String modId, GatherDataEvent event, AdvancementSubProvider... advancementTabs) {
        super(event.getGenerator().getPackOutput(), event.getLookupProvider(), event.getExistingFileHelper(), Arrays.stream(advancementTabs).map(tab -> (AdvancementGenerator) (registries, saver, existingFileHelper) -> tab.generate(registries, saver)).toList());
        this.modId = modId;
        this.event = event;
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public String getName() {
        return ICrypticDataProvider.super.getName();
    }

    @Override
    public String getSimpleName() {
        return "Advancements";
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public boolean shouldRun() {
        return this.event.includeServer();
    }


    public static class AdvancementBuilder extends Advancement.Builder {
        private final String modId;

        public AdvancementBuilder(String modId, @Nullable ResourceLocation parentId, @Nullable DisplayInfo display, AdvancementRewards rewards, Map<String, Criterion> criteria, String[][] requirements) {
            super(parentId, display, rewards, criteria, requirements);
            this.modId = modId;
        }

        public AdvancementBuilder(String modId) {
            super();
            this.modId = modId;
        }

        @Override
        public AdvancementBuilder parent(Advancement parent) {
            return (AdvancementBuilder) super.parent(parent);
        }

        @Override
        public AdvancementBuilder parent(ResourceLocation parentId) {
            return (AdvancementBuilder) super.parent(parentId);
        }

        public AdvancementBuilder display(ItemStack stack, String titleKey, String titleTranslation, String descriptionKey, String descriptionTranslation, @Nullable ResourceLocation background, FrameType frame, boolean showToast, boolean announceToChat, boolean hidden) {
            return this.display(stack, this.translatable(titleKey, titleTranslation), this.translatable(descriptionKey, descriptionTranslation), background, frame, showToast, announceToChat, hidden);
        }

        public AdvancementBuilder display(ItemLike item, String titleKey, String titleTranslation, String descriptionKey, String descriptionTranslation, @Nullable ResourceLocation background, FrameType frame, boolean showToast, boolean announceToChat, boolean hidden) {
            return this.display(item, this.translatable(titleKey, titleTranslation), this.translatable(descriptionKey, descriptionTranslation), background, frame, showToast, announceToChat, hidden);
        }

        public AdvancementBuilder display(Supplier<? extends ItemLike> item, String titleKey, String titleTranslation, String descriptionKey, String descriptionTranslation, @Nullable ResourceLocation background, FrameType frame, boolean showToast, boolean announceToChat, boolean hidden) {
            return this.display(item.get(), this.translatable(titleKey, titleTranslation), this.translatable(descriptionKey, descriptionTranslation), background, frame, showToast, announceToChat, hidden);
        }

        @Override
        public AdvancementBuilder display(ItemStack stack, Component title, Component description, @Nullable ResourceLocation background, FrameType frame, boolean showToast, boolean announceToChat, boolean hidden) {
            return (AdvancementBuilder) super.display(stack, title, description, background, frame, showToast, announceToChat, hidden);
        }

        @Override
        public AdvancementBuilder display(ItemLike item, Component title, Component description, @Nullable ResourceLocation background, FrameType frame, boolean showToast, boolean announceToChat, boolean hidden) {
            return (AdvancementBuilder) super.display(item, title, description, background, frame, showToast, announceToChat, hidden);
        }

        @Override
        public AdvancementBuilder display(DisplayInfo display) {
            return (AdvancementBuilder) super.display(display);
        }

        @Override
        public AdvancementBuilder rewards(AdvancementRewards.Builder rewardsBuilder) {
            return (AdvancementBuilder) super.rewards(rewardsBuilder);
        }

        @Override
        public AdvancementBuilder rewards(AdvancementRewards rewards) {
            return (AdvancementBuilder) super.rewards(rewards);
        }

        @Override
        public AdvancementBuilder addCriterion(String key, CriterionTriggerInstance criterion) {
            return (AdvancementBuilder) super.addCriterion(key, criterion);
        }

        @Override
        public AdvancementBuilder addCriterion(String key, Criterion criterion) {
            return (AdvancementBuilder) super.addCriterion(key, criterion);
        }

        @Override
        public AdvancementBuilder requirements(RequirementsStrategy strategy) {
            return (AdvancementBuilder) super.requirements(strategy);
        }

        @Override
        public AdvancementBuilder requirements(String[][] requirements) {
            return (AdvancementBuilder) super.requirements(requirements);
        }

        private MutableComponent translatable(String key, String translation) {
            LocalizedNameRegistry.OTHER.register(this.modId, key, translation);
            return Component.translatable(key);
        }
    }
}
