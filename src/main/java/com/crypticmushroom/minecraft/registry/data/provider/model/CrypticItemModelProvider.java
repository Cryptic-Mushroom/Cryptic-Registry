package com.crypticmushroom.minecraft.registry.data.provider.model;

import com.crypticmushroom.minecraft.registry.coremod.mixin.data.forge.accessor.ModelProviderAccessor;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.models.model.ModelTemplate;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.data.event.GatherDataEvent;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

/**
 * This abstract model provider is, essentially, an extension to the normal {@link ItemModelProvider} that provides
 * additional features like the usage of {@link Supplier suppliers} over static objects and a much more concise notation
 * for dictating the objects to generate. For a clear example and additional info, see the wiki.
 *
 * @author Jonathing
 */
public non-sealed class CrypticItemModelProvider extends ItemModelProvider implements ICrypticModelProvider<Item, ItemModelBuilder> {
    private final String modId;
    private final GatherDataEvent event;

    boolean dummy = false;

    {
        //noinspection unchecked
        ((ModelProviderAccessor<ItemModelBuilder>) this).cmreg$factory(resLoc -> new ItemModelBuilder(resLoc, this.existingFileHelper) {
            @Override
            public ItemModelBuilder renderType(ResourceLocation renderType) {
                try {
                    return renderType instanceof RenderType r ? this.renderType(r) : super.renderType(renderType);
                } catch (NullPointerException e) {
                    throw new IllegalArgumentException("Render type must not be null. To remove a render type, use ICrypticModelProvider.RenderType.NONE", e);
                }
            }

            public ItemModelBuilder renderType(RenderType renderType) {
                if (renderType == null || renderType == RenderType.NONE) {
                    this.renderType = null;
                    return this;
                }

                return renderType != RenderType.DEFAULT ? this.renderType(Objects.requireNonNull(renderType, "A RenderType object cannot have a null ID")) : this;
            }
        });
    }

    public CrypticItemModelProvider(String modId, GatherDataEvent event) {
        super(event.getGenerator().getPackOutput(), modId, event.getExistingFileHelper());

        this.modId = modId;
        this.event = event;
    }

    @Override
    protected void registerModels() {
        DataRegistry.applyItemModelBuilders(this.getModId(), this);
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public String getName() {
        return ICrypticModelProvider.super.getName();
    }

    @Override
    public String getSimpleName() {
        return "Item Models";
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public boolean shouldRun() {
        return this.event.includeClient();
    }

    @Override
    public String getFolder() {
        return this.folder;
    }

    @Override
    public CompletableFuture<?> run(CachedOutput cache) {
        return !this.dummy ? super.run(cache) : CompletableFuture.allOf();
    }


    /*
     * MODEL GENERATION
     */

    public final <B extends Block> ItemModelBuilder inherit(Supplier<B> block, Function<B, ResourceLocation> texture) {
        return this.inherit(block.get(), texture.apply(block.get()));
    }

    public final ItemModelBuilder inherit(Supplier<? extends Block> block, ResourceLocation texture) {
        return this.inherit(block.get(), texture);
    }

    public final ItemModelBuilder inherit(Supplier<? extends Block> block) {
        return this.inherit(block.get());
    }

    public final <B extends Block> ItemModelBuilder inherit(B block, Function<B, ResourceLocation> texture) {
        return this.inherit(block, texture.apply(block));
    }

    public ItemModelBuilder inherit(Block block, ResourceLocation texture) {
        return this.withExistingParent(this.name(block), texture);
    }

    public final ItemModelBuilder inherit(Block block) {
        return this.inherit(block, this.texture(block));
    }


    @SafeVarargs
    public final ItemModelBuilder generated(Supplier<? extends ItemLike> obj, Function<Supplier<? extends ItemLike>, ResourceLocation>... layers) {
        return this.generated(obj, Arrays.stream(layers).map(f -> f.apply(obj)).toArray(ResourceLocation[]::new));
    }

    public ItemModelBuilder generated(Supplier<? extends ItemLike> obj, ResourceLocation... layers) {
        return this.type(obj, this.mcLoc("generated"), layers);
    }

    public final ItemModelBuilder generated(Supplier<? extends ItemLike> obj) {
        return this.generated(obj, this::texture);
    }

    public final ItemModelBuilder generated(ItemLike item) {
        return this.generated(() -> item, this::texture);
    }


    @SuppressWarnings("unchecked")
    public ItemModelBuilder handheld(Supplier<? extends ItemLike> obj, Function<Supplier<? extends ItemLike>, ResourceLocation>... layers) {
        return this.handheld(obj.get(), Arrays.stream(layers).map(f -> f.apply(obj)).toArray(ResourceLocation[]::new));
    }

    public ItemModelBuilder handheld(Supplier<? extends ItemLike> obj, ResourceLocation... layers) {
        return this.handheld(obj.get(), layers);
    }

    public ItemModelBuilder handheld(Supplier<? extends ItemLike> obj) {
        return this.handheld(obj.get());
    }

    public ItemModelBuilder handheld(ItemLike obj, ResourceLocation... layers) {
        return this.type(obj, this.mcLoc("handheld"), layers);
    }

    public ItemModelBuilder handheld(ItemLike item) {
        return this.handheld(item, this.texture(item));
    }


    public ItemModelBuilder spawnEgg(Supplier<? extends SpawnEggItem> item) {
        return this.spawnEgg(item.get());
    }

    public ItemModelBuilder spawnEgg(SpawnEggItem item) {
        return this.withExistingParent(this.name(item), this.mcLoc("item/template_spawn_egg"));
    }


    public ItemModelBuilder type(Supplier<? extends ItemLike> obj, ResourceLocation type, UnaryOperator<ItemModelBuilder> builderOptions, ResourceLocation... layers) {
        return this.type(obj.get(), type, builderOptions, layers);
    }

    public ItemModelBuilder type(Supplier<? extends ItemLike> obj, ResourceLocation type, ResourceLocation... layers) {
        return this.type(obj.get(), type, layers);
    }

    public ItemModelBuilder type(ItemLike obj, ResourceLocation type, UnaryOperator<ItemModelBuilder> builderOptions, ResourceLocation... layers) {
        var builder = this.withExistingParent(this.name(obj), type);

        builderOptions.apply(builder);
        for (int i = 0; i < layers.length; i++) {
            builder = builder.texture(String.format("layer%d", i), layers[i]);
        }

        return builder;
    }

    public ItemModelBuilder type(ItemLike obj, ResourceLocation type, ResourceLocation... layers) {
        return this.type(obj, type, b -> b, layers);
    }


    /**
     * @see #generated(ItemLike)
     * @see ItemModelProvider#basicItem(Item)
     * @deprecated This implementation only accounts for generated models with a single layer. Use
     *     {@link #generated(ItemLike)}
     */
    @Override
    @Deprecated
    public ItemModelBuilder basicItem(Item item) {
        return super.basicItem(item);
    }

    /**
     * @see #generated(ItemLike)
     * @see ItemModelProvider#basicItem(ResourceLocation)
     * @deprecated This implementation only accounts for generated models with a single layer. Use *
     *     {@link #generated(ItemLike)}
     */
    @Override
    @Deprecated
    public ItemModelBuilder basicItem(ResourceLocation item) {
        return super.basicItem(item);
    }


    /*
     * HELPER METHODS
     */

    /* IMPLEMENTATION OF ICrypticModelProvider */

    /* MODEL GENERATION - USING TEMPLATES */

    @Override
    public final ItemModelBuilder fromTemplate(Supplier<? extends Item> item, ModelTemplate parent, ResourceLocation... texture) {
        return ICrypticModelProvider.super.fromTemplate(item, parent, texture);
    }

    @Override
    public final ItemModelBuilder fromTemplate(Item item, ModelTemplate parent, ResourceLocation... texture) {
        return ICrypticModelProvider.super.fromTemplate(item, parent, texture);
    }

    @Override
    public ItemModelBuilder fromTemplate(String name, ModelTemplate parent, ResourceLocation... texture) {
        return ICrypticModelProvider.super.fromTemplate(name, parent, texture);
    }


    /*
     * MODEL GENERATION - VANILLA
     */

    @Override
    public final ItemModelBuilder cube(Supplier<? extends Item> item, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return ICrypticModelProvider.super.cube(item, down, up, north, south, east, west);
    }

    @Override
    public final ItemModelBuilder cube(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.cube(item);
    }

    @Override
    public final ItemModelBuilder cube(Item item, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return ICrypticModelProvider.super.cube(item, down, up, north, south, east, west);
    }

    @Override
    public final ItemModelBuilder cube(Item item) {
        return ICrypticModelProvider.super.cube(item);
    }

    @Override
    public ItemModelBuilder cube(String name, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return super.cube(name, down, up, north, south, east, west);
    }


    @Override
    public final ItemModelBuilder singleTexture(Supplier<? extends Item> item, ResourceLocation parent) {
        return ICrypticModelProvider.super.singleTexture(item, parent);
    }

    @Override
    public final ItemModelBuilder singleTexture(Supplier<? extends Item> item, ModelTemplate parent) {
        return ICrypticModelProvider.super.singleTexture(item, parent);
    }

    @Override
    public final ItemModelBuilder singleTexture(Supplier<? extends Item> item, ResourceLocation parent, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(item, parent, texture);
    }

    @Override
    public final ItemModelBuilder singleTexture(Supplier<? extends Item> item, ModelTemplate parent, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(item, parent, texture);
    }

    @Override
    public final ItemModelBuilder singleTexture(Item item, ResourceLocation parent) {
        return ICrypticModelProvider.super.singleTexture(item, parent);
    }

    @Override
    public final ItemModelBuilder singleTexture(Item item, ModelTemplate parent) {
        return ICrypticModelProvider.super.singleTexture(item, parent);
    }

    @Override
    public final ItemModelBuilder singleTexture(Item item, ResourceLocation parent, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(item, parent, texture);
    }

    @Override
    public final ItemModelBuilder singleTexture(Item item, ModelTemplate parent, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(item, parent, texture);
    }

    @Override
    public ItemModelBuilder singleTexture(String name, ResourceLocation parent, ResourceLocation texture) {
        return super.singleTexture(name, parent, texture);
    }


    @Override
    public ItemModelBuilder singleTexture(Supplier<? extends Item> item, ResourceLocation parent, String textureKey) {
        return ICrypticModelProvider.super.singleTexture(item, parent, textureKey);
    }

    @Override
    public final ItemModelBuilder singleTexture(Supplier<? extends Item> item, ModelTemplate parent, String textureKey) {
        return ICrypticModelProvider.super.singleTexture(item, parent, textureKey);
    }

    @Override
    public final ItemModelBuilder singleTexture(Supplier<? extends Item> item, ResourceLocation parent, String textureKey, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(item, parent, textureKey, texture);
    }

    @Override
    public final ItemModelBuilder singleTexture(Supplier<? extends Item> item, ModelTemplate parent, String textureKey, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(item, parent, textureKey, texture);
    }

    @Override
    public final ItemModelBuilder singleTexture(Item item, ResourceLocation parent, String textureKey) {
        return ICrypticModelProvider.super.singleTexture(item, parent, textureKey);
    }

    @Override
    public final ItemModelBuilder singleTexture(Item item, ModelTemplate parent, String textureKey) {
        return ICrypticModelProvider.super.singleTexture(item, parent, textureKey);
    }

    @Override
    public final ItemModelBuilder singleTexture(Item item, ResourceLocation parent, String textureKey, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(item, parent, textureKey, texture);
    }

    @Override
    public final ItemModelBuilder singleTexture(Item item, ModelTemplate parent, String textureKey, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(item, parent, textureKey, texture);
    }

    @Override
    public ItemModelBuilder singleTexture(String name, ResourceLocation parent, String textureKey, ResourceLocation texture) {
        return super.singleTexture(name, parent, textureKey, texture);
    }


    @Override
    public final ItemModelBuilder cubeAll(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.cubeAll(item, texture);
    }

    @Override
    public final ItemModelBuilder cubeAll(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.cubeAll(item);
    }

    @Override
    public final ItemModelBuilder cubeAll(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.cubeAll(item, texture);
    }

    @Override
    public final ItemModelBuilder cubeAll(Item item) {
        return ICrypticModelProvider.super.cubeAll(item);
    }

    @Override
    public ItemModelBuilder cubeAll(String name, ResourceLocation texture) {
        return super.cubeAll(name, texture);
    }


    @Override
    public final ItemModelBuilder cubeTop(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeTop(item, side, top);
    }

    @Override
    public final ItemModelBuilder cubeTop(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.cubeTop(item);
    }

    @Override
    public final ItemModelBuilder cubeTop(Item item, ResourceLocation side, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeTop(item, side, top);
    }

    @Override
    public final ItemModelBuilder cubeTop(Item item) {
        return ICrypticModelProvider.super.cubeTop(item);
    }

    @Override
    public ItemModelBuilder cubeTop(String name, ResourceLocation side, ResourceLocation top) {
        return super.cubeTop(name, side, top);
    }


    @Override
    public final ItemModelBuilder cubeBottomTop(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeBottomTop(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder cubeBottomTop(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.cubeBottomTop(item);
    }

    @Override
    public final ItemModelBuilder cubeBottomTop(Item item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeBottomTop(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder cubeBottomTop(Item item) {
        return ICrypticModelProvider.super.cubeBottomTop(item);
    }

    @Override
    public ItemModelBuilder cubeBottomTop(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.cubeBottomTop(name, side, bottom, top);
    }


    @Override
    public final ItemModelBuilder cubeColumn(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation end) {
        return ICrypticModelProvider.super.cubeColumn(item, side, end);
    }

    @Override
    public final ItemModelBuilder cubeColumn(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.cubeColumn(item);
    }

    @Override
    public final ItemModelBuilder cubeColumn(Item item, ResourceLocation side, ResourceLocation end) {
        return ICrypticModelProvider.super.cubeColumn(item, side, end);
    }

    @Override
    public final ItemModelBuilder cubeColumn(Item item) {
        return ICrypticModelProvider.super.cubeColumn(item);
    }

    @Override
    public ItemModelBuilder cubeColumn(String name, ResourceLocation side, ResourceLocation end) {
        return super.cubeColumn(name, side, end);
    }


    @Override
    public final ItemModelBuilder cubeColumnHorizontal(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation end) {
        return ICrypticModelProvider.super.cubeColumnHorizontal(item, side, end);
    }

    @Override
    public final ItemModelBuilder cubeColumnHorizontal(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.cubeColumnHorizontal(item);
    }

    @Override
    public final ItemModelBuilder cubeColumnHorizontal(Item item, ResourceLocation side, ResourceLocation end) {
        return ICrypticModelProvider.super.cubeColumnHorizontal(item, side, end);
    }

    @Override
    public final ItemModelBuilder cubeColumnHorizontal(Item item) {
        return ICrypticModelProvider.super.cubeColumnHorizontal(item);
    }

    @Override
    public ItemModelBuilder cubeColumnHorizontal(String name, ResourceLocation side, ResourceLocation end) {
        return super.cubeColumnHorizontal(name, side, end);
    }


    @Override
    public final ItemModelBuilder orientableVertical(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation front) {
        return ICrypticModelProvider.super.orientableVertical(item, side, front);
    }

    @Override
    public final ItemModelBuilder orientableVertical(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.orientableVertical(item);
    }

    @Override
    public final ItemModelBuilder orientableVertical(Item item, ResourceLocation side, ResourceLocation front) {
        return ICrypticModelProvider.super.orientableVertical(item, side, front);
    }

    @Override
    public final ItemModelBuilder orientableVertical(Item item) {
        return ICrypticModelProvider.super.orientableVertical(item);
    }

    @Override
    public ItemModelBuilder orientableVertical(String name, ResourceLocation side, ResourceLocation front) {
        return super.orientableVertical(name, side, front);
    }


    @Override
    public final ItemModelBuilder orientableWithBottom(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation front, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.orientableWithBottom(item, side, front, bottom, top);
    }

    @Override
    public final ItemModelBuilder orientableWithBottom(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.orientableWithBottom(item);
    }

    @Override
    public final ItemModelBuilder orientableWithBottom(Item item, ResourceLocation side, ResourceLocation front, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.orientableWithBottom(item, side, front, bottom, top);
    }

    @Override
    public final ItemModelBuilder orientableWithBottom(Item item) {
        return ICrypticModelProvider.super.orientableWithBottom(item);
    }

    @Override
    public ItemModelBuilder orientableWithBottom(String name, ResourceLocation side, ResourceLocation front, ResourceLocation bottom, ResourceLocation top) {
        return super.orientableWithBottom(name, side, front, bottom, top);
    }


    @Override
    public final ItemModelBuilder orientable(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation front, ResourceLocation top) {
        return ICrypticModelProvider.super.orientable(item, side, front, top);
    }

    @Override
    public final ItemModelBuilder orientable(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.orientable(item);
    }

    @Override
    public final ItemModelBuilder orientable(Item item, ResourceLocation side, ResourceLocation front, ResourceLocation top) {
        return ICrypticModelProvider.super.orientable(item, side, front, top);
    }

    @Override
    public final ItemModelBuilder orientable(Item item) {
        return ICrypticModelProvider.super.orientable(item);
    }

    @Override
    public ItemModelBuilder orientable(String name, ResourceLocation side, ResourceLocation front, ResourceLocation top) {
        return super.orientable(name, side, front, top);
    }


    @Override
    public final ItemModelBuilder crop(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.crop(item, texture);
    }

    @Override
    public final ItemModelBuilder crop(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.crop(item);
    }

    @Override
    public final ItemModelBuilder crop(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.crop(item, texture);
    }

    @Override
    public final ItemModelBuilder crop(Item item) {
        return ICrypticModelProvider.super.crop(item);
    }

    @Override
    public ItemModelBuilder crop(String name, ResourceLocation crop) {
        return super.crop(name, crop).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder cross(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.cross(item, texture);
    }

    @Override
    public final ItemModelBuilder cross(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.cross(item);
    }

    @Override
    public final ItemModelBuilder cross(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.cross(item, texture);
    }

    @Override
    public final ItemModelBuilder cross(Item item) {
        return ICrypticModelProvider.super.cross(item);
    }

    @Override
    public ItemModelBuilder cross(String name, ResourceLocation cross) {
        return super.cross(name, cross).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder stairs(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairs(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder stairs(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.stairs(item);
    }

    @Override
    public final ItemModelBuilder stairs(Item item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairs(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder stairs(Item item) {
        return ICrypticModelProvider.super.stairs(item);
    }

    @Override
    public ItemModelBuilder stairs(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.stairs(name, side, bottom, top);
    }


    @Override
    public final ItemModelBuilder stairsOuter(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairsOuter(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder stairsOuter(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.stairsOuter(item);
    }

    @Override
    public final ItemModelBuilder stairsOuter(Item item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairsOuter(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder stairsOuter(Item item) {
        return ICrypticModelProvider.super.stairsOuter(item);
    }

    @Override
    public ItemModelBuilder stairsOuter(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.stairsOuter(name, side, bottom, top);
    }


    @Override
    public final ItemModelBuilder stairsInner(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairsInner(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder stairsInner(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.stairsInner(item);
    }

    @Override
    public final ItemModelBuilder stairsInner(Item item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairsInner(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder stairsInner(Item item) {
        return ICrypticModelProvider.super.stairsInner(item);
    }

    @Override
    public ItemModelBuilder stairsInner(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.stairsInner(name, side, bottom, top);
    }


    @Override
    public final ItemModelBuilder slab(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.slab(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder slab(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.slab(item);
    }

    @Override
    public final ItemModelBuilder slab(Item item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.slab(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder slab(Item item) {
        return ICrypticModelProvider.super.slab(item);
    }

    @Override
    public ItemModelBuilder slab(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.slab(name, side, bottom, top);
    }


    @Override
    public final ItemModelBuilder slabTop(Supplier<? extends Item> item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.slabTop(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder slabTop(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.slabTop(item);
    }

    @Override
    public final ItemModelBuilder slabTop(Item item, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.slabTop(item, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder slabTop(Item item) {
        return ICrypticModelProvider.super.slabTop(item);
    }

    @Override
    public ItemModelBuilder slabTop(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.slabTop(name, side, bottom, top);
    }


    @Override
    public final ItemModelBuilder button(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.button(item, texture);
    }

    @Override
    public final ItemModelBuilder button(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.button(item);
    }

    @Override
    public final ItemModelBuilder button(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.button(item, texture);
    }

    @Override
    public final ItemModelBuilder button(Item item) {
        return ICrypticModelProvider.super.button(item);
    }

    @Override
    public ItemModelBuilder button(String name, ResourceLocation texture) {
        return super.button(name, texture);
    }


    @Override
    public final ItemModelBuilder buttonPressed(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.buttonPressed(item, texture);
    }

    @Override
    public final ItemModelBuilder buttonPressed(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.buttonPressed(item);
    }

    @Override
    public final ItemModelBuilder buttonPressed(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.buttonPressed(item, texture);
    }

    @Override
    public final ItemModelBuilder buttonPressed(Item item) {
        return ICrypticModelProvider.super.buttonPressed(item);
    }

    @Override
    public ItemModelBuilder buttonPressed(String name, ResourceLocation texture) {
        return super.buttonPressed(name, texture);
    }


    @Override
    public final ItemModelBuilder buttonInventory(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.buttonInventory(item, texture);
    }

    @Override
    public final ItemModelBuilder buttonInventory(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.buttonInventory(item);
    }

    @Override
    public final ItemModelBuilder buttonInventory(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.buttonInventory(item, texture);
    }

    @Override
    public final ItemModelBuilder buttonInventory(Item item) {
        return ICrypticModelProvider.super.buttonInventory(item);
    }

    @Override
    public ItemModelBuilder buttonInventory(String name, ResourceLocation texture) {
        return super.buttonInventory(name, texture);
    }


    @Override
    public final ItemModelBuilder pressurePlate(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.pressurePlate(item, texture);
    }

    @Override
    public final ItemModelBuilder pressurePlate(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.pressurePlate(item);
    }

    @Override
    public final ItemModelBuilder pressurePlate(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.pressurePlate(item, texture);
    }

    @Override
    public final ItemModelBuilder pressurePlate(Item item) {
        return ICrypticModelProvider.super.pressurePlate(item);
    }

    @Override
    public ItemModelBuilder pressurePlate(String name, ResourceLocation texture) {
        return super.pressurePlate(name, texture);
    }


    @Override
    public final ItemModelBuilder pressurePlateDown(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.pressurePlateDown(item, texture);
    }

    @Override
    public final ItemModelBuilder pressurePlateDown(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.pressurePlateDown(item);
    }

    @Override
    public final ItemModelBuilder pressurePlateDown(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.pressurePlateDown(item, texture);
    }

    @Override
    public final ItemModelBuilder pressurePlateDown(Item item) {
        return ICrypticModelProvider.super.pressurePlateDown(item);
    }

    @Override
    public ItemModelBuilder pressurePlateDown(String name, ResourceLocation texture) {
        return super.pressurePlateDown(name, texture);
    }


    @Override
    public final ItemModelBuilder sign(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.sign(item, texture);
    }

    @Override
    public final ItemModelBuilder sign(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.sign(item);
    }

    @Override
    public final ItemModelBuilder sign(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.sign(item, texture);
    }

    @Override
    public final ItemModelBuilder sign(Item item) {
        return ICrypticModelProvider.super.sign(item);
    }

    @Override
    public ItemModelBuilder sign(String name, ResourceLocation texture) {
        return super.sign(name, texture);
    }


    @Override
    public final ItemModelBuilder fencePost(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fencePost(item, texture);
    }

    @Override
    public final ItemModelBuilder fencePost(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.fencePost(item);
    }

    @Override
    public final ItemModelBuilder fencePost(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fencePost(item, texture);
    }

    @Override
    public final ItemModelBuilder fencePost(Item item) {
        return ICrypticModelProvider.super.fencePost(item);
    }

    @Override
    public ItemModelBuilder fencePost(String name, ResourceLocation texture) {
        return super.fencePost(name, texture);
    }


    @Override
    public final ItemModelBuilder fenceSide(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceSide(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceSide(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.fenceSide(item);
    }

    @Override
    public final ItemModelBuilder fenceSide(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceSide(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceSide(Item item) {
        return ICrypticModelProvider.super.fenceSide(item);
    }

    @Override
    public ItemModelBuilder fenceSide(String name, ResourceLocation texture) {
        return super.fenceSide(name, texture);
    }


    @Override
    public final ItemModelBuilder fenceInventory(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceInventory(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceInventory(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.fenceInventory(item);
    }

    @Override
    public final ItemModelBuilder fenceInventory(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceInventory(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceInventory(Item item) {
        return ICrypticModelProvider.super.fenceInventory(item);
    }

    @Override
    public ItemModelBuilder fenceInventory(String name, ResourceLocation texture) {
        return super.fenceInventory(name, texture);
    }


    @Override
    public final ItemModelBuilder fenceGate(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGate(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceGate(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.fenceGate(item);
    }

    @Override
    public final ItemModelBuilder fenceGate(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGate(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceGate(Item item) {
        return ICrypticModelProvider.super.fenceGate(item);
    }

    @Override
    public ItemModelBuilder fenceGate(String name, ResourceLocation texture) {
        return super.fenceGate(name, texture);
    }


    @Override
    public final ItemModelBuilder fenceGateOpen(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateOpen(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceGateOpen(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.fenceGateOpen(item);
    }

    @Override
    public final ItemModelBuilder fenceGateOpen(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateOpen(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceGateOpen(Item item) {
        return ICrypticModelProvider.super.fenceGateOpen(item);
    }

    @Override
    public ItemModelBuilder fenceGateOpen(String name, ResourceLocation texture) {
        return super.fenceGateOpen(name, texture);
    }


    @Override
    public final ItemModelBuilder fenceGateWall(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateWall(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceGateWall(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.fenceGateWall(item);
    }

    @Override
    public final ItemModelBuilder fenceGateWall(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateWall(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceGateWall(Item item) {
        return ICrypticModelProvider.super.fenceGateWall(item);
    }

    @Override
    public ItemModelBuilder fenceGateWall(String name, ResourceLocation texture) {
        return super.fenceGateWall(name, texture);
    }


    @Override
    public final ItemModelBuilder fenceGateWallOpen(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateWallOpen(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceGateWallOpen(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.fenceGateWallOpen(item);
    }

    @Override
    public final ItemModelBuilder fenceGateWallOpen(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateWallOpen(item, texture);
    }

    @Override
    public final ItemModelBuilder fenceGateWallOpen(Item item) {
        return ICrypticModelProvider.super.fenceGateWallOpen(item);
    }

    @Override
    public ItemModelBuilder fenceGateWallOpen(String name, ResourceLocation texture) {
        return super.fenceGateWallOpen(name, texture);
    }


    @Override
    public final ItemModelBuilder wallPost(Supplier<? extends Item> item, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallPost(item, wall);
    }

    @Override
    public final ItemModelBuilder wallPost(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.wallPost(item);
    }

    @Override
    public final ItemModelBuilder wallPost(Item item, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallPost(item, wall);
    }

    @Override
    public final ItemModelBuilder wallPost(Item item) {
        return ICrypticModelProvider.super.wallPost(item);
    }

    @Override
    public ItemModelBuilder wallPost(String name, ResourceLocation wall) {
        return super.wallPost(name, wall);
    }


    @Override
    public final ItemModelBuilder wallSide(Supplier<? extends Item> item, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallSide(item, wall);
    }

    @Override
    public final ItemModelBuilder wallSide(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.wallSide(item);
    }

    @Override
    public final ItemModelBuilder wallSide(Item item, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallSide(item, wall);
    }

    @Override
    public final ItemModelBuilder wallSide(Item item) {
        return ICrypticModelProvider.super.wallSide(item);
    }

    @Override
    public ItemModelBuilder wallSide(String name, ResourceLocation wall) {
        return super.wallSide(name, wall);
    }


    @Override
    public final ItemModelBuilder wallSideTall(Supplier<? extends Item> item, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallSideTall(item, wall);
    }

    @Override
    public final ItemModelBuilder wallSideTall(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.wallSideTall(item);
    }

    @Override
    public final ItemModelBuilder wallSideTall(Item item, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallSideTall(item, wall);
    }

    @Override
    public final ItemModelBuilder wallSideTall(Item item) {
        return ICrypticModelProvider.super.wallSideTall(item);
    }

    @Override
    public ItemModelBuilder wallSideTall(String name, ResourceLocation wall) {
        return super.wallSideTall(name, wall);
    }


    @Override
    public final ItemModelBuilder wallInventory(Supplier<? extends Item> item, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallInventory(item, wall);
    }

    @Override
    public final ItemModelBuilder wallInventory(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.wallInventory(item);
    }

    @Override
    public final ItemModelBuilder wallInventory(Item item, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallInventory(item, wall);
    }

    @Override
    public final ItemModelBuilder wallInventory(Item item) {
        return ICrypticModelProvider.super.wallInventory(item);
    }

    @Override
    public ItemModelBuilder wallInventory(String name, ResourceLocation wall) {
        return super.wallInventory(name, wall);
    }


    @Override
    public final ItemModelBuilder panePost(Supplier<? extends Item> item, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.panePost(item, pane, edge);
    }

    @Override
    public final ItemModelBuilder panePost(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.panePost(item);
    }

    @Override
    public final ItemModelBuilder panePost(Item item, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.panePost(item, pane, edge);
    }

    @Override
    public final ItemModelBuilder panePost(Item item) {
        return ICrypticModelProvider.super.panePost(item);
    }

    @Override
    public ItemModelBuilder panePost(String name, ResourceLocation pane, ResourceLocation edge) {
        return super.panePost(name, pane, edge).renderType(RenderType.CUTOUT_MIPPED);
    }


    @Override
    public final ItemModelBuilder paneSide(Supplier<? extends Item> item, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.paneSide(item, pane, edge);
    }

    @Override
    public final ItemModelBuilder paneSide(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.paneSide(item);
    }

    @Override
    public final ItemModelBuilder paneSide(Item item, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.paneSide(item, pane, edge);
    }

    @Override
    public final ItemModelBuilder paneSide(Item item) {
        return ICrypticModelProvider.super.paneSide(item);
    }

    @Override
    public ItemModelBuilder paneSide(String name, ResourceLocation pane, ResourceLocation edge) {
        return super.paneSide(name, pane, edge).renderType(RenderType.CUTOUT_MIPPED);
    }


    @Override
    public final ItemModelBuilder paneSideAlt(Supplier<? extends Item> item, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.paneSideAlt(item, pane, edge);
    }

    @Override
    public final ItemModelBuilder paneSideAlt(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.paneSideAlt(item);
    }

    @Override
    public final ItemModelBuilder paneSideAlt(Item item, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.paneSideAlt(item, pane, edge);
    }

    @Override
    public final ItemModelBuilder paneSideAlt(Item item) {
        return ICrypticModelProvider.super.paneSideAlt(item);
    }

    @Override
    public ItemModelBuilder paneSideAlt(String name, ResourceLocation pane, ResourceLocation edge) {
        return super.paneSideAlt(name, pane, edge).renderType(RenderType.CUTOUT_MIPPED);
    }


    @Override
    public final ItemModelBuilder paneNoSide(Supplier<? extends Item> item, ResourceLocation pane) {
        return ICrypticModelProvider.super.paneNoSide(item, pane);
    }

    @Override
    public final ItemModelBuilder paneNoSide(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.paneNoSide(item);
    }

    @Override
    public final ItemModelBuilder paneNoSide(Item item, ResourceLocation pane) {
        return ICrypticModelProvider.super.paneNoSide(item, pane);
    }

    @Override
    public final ItemModelBuilder paneNoSide(Item item) {
        return ICrypticModelProvider.super.paneNoSide(item);
    }

    @Override
    public ItemModelBuilder paneNoSide(String name, ResourceLocation pane) {
        return super.paneNoSide(name, pane).renderType(RenderType.CUTOUT_MIPPED);
    }


    @Override
    public final ItemModelBuilder paneNoSideAlt(Supplier<? extends Item> item, ResourceLocation pane) {
        return ICrypticModelProvider.super.paneNoSideAlt(item, pane);
    }

    @Override
    public final ItemModelBuilder paneNoSideAlt(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.paneNoSideAlt(item);
    }

    @Override
    public final ItemModelBuilder paneNoSideAlt(Item item, ResourceLocation pane) {
        return ICrypticModelProvider.super.paneNoSideAlt(item, pane);
    }

    @Override
    public final ItemModelBuilder paneNoSideAlt(Item item) {
        return ICrypticModelProvider.super.paneNoSideAlt(item);
    }

    @Override
    public ItemModelBuilder paneNoSideAlt(String name, ResourceLocation pane) {
        return super.paneNoSideAlt(name, pane).renderType(RenderType.CUTOUT_MIPPED);
    }


    @Override
    public final ItemModelBuilder doorBottomLeft(Supplier<? extends Item> item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomLeft(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorBottomLeft(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.doorBottomLeft(item);
    }

    @Override
    public final ItemModelBuilder doorBottomLeft(Item item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomLeft(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorBottomLeft(Item item) {
        return ICrypticModelProvider.super.doorBottomLeft(item);
    }

    @Override
    public ItemModelBuilder doorBottomLeft(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorBottomLeft(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder doorBottomLeftOpen(Supplier<? extends Item> item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomLeftOpen(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorBottomLeftOpen(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.doorBottomLeftOpen(item);
    }

    @Override
    public final ItemModelBuilder doorBottomLeftOpen(Item item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomLeftOpen(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorBottomLeftOpen(Item item) {
        return ICrypticModelProvider.super.doorBottomLeftOpen(item);
    }

    @Override
    public ItemModelBuilder doorBottomLeftOpen(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorBottomLeftOpen(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder doorBottomRight(Supplier<? extends Item> item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomRight(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorBottomRight(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.doorBottomRight(item);
    }

    @Override
    public final ItemModelBuilder doorBottomRight(Item item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomRight(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorBottomRight(Item item) {
        return ICrypticModelProvider.super.doorBottomRight(item);
    }

    @Override
    public ItemModelBuilder doorBottomRight(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorBottomRight(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder doorBottomRightOpen(Supplier<? extends Item> item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomRightOpen(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorBottomRightOpen(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.doorBottomRightOpen(item);
    }

    @Override
    public final ItemModelBuilder doorBottomRightOpen(Item item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomRightOpen(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorBottomRightOpen(Item item) {
        return ICrypticModelProvider.super.doorBottomRightOpen(item);
    }

    @Override
    public ItemModelBuilder doorBottomRightOpen(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorBottomRightOpen(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder doorTopLeft(Supplier<? extends Item> item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopLeft(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorTopLeft(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.doorTopLeft(item);
    }

    @Override
    public final ItemModelBuilder doorTopLeft(Item item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopLeft(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorTopLeft(Item item) {
        return ICrypticModelProvider.super.doorTopLeft(item);
    }

    @Override
    public ItemModelBuilder doorTopLeft(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorTopLeft(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder doorTopLeftOpen(Supplier<? extends Item> item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopLeftOpen(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorTopLeftOpen(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.doorTopLeftOpen(item);
    }

    @Override
    public final ItemModelBuilder doorTopLeftOpen(Item item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopLeftOpen(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorTopLeftOpen(Item item) {
        return ICrypticModelProvider.super.doorTopLeftOpen(item);
    }

    @Override
    public ItemModelBuilder doorTopLeftOpen(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorTopLeftOpen(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder doorTopRight(Supplier<? extends Item> item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopRight(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorTopRight(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.doorTopRight(item);
    }

    @Override
    public final ItemModelBuilder doorTopRight(Item item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopRight(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorTopRight(Item item) {
        return ICrypticModelProvider.super.doorTopRight(item);
    }

    @Override
    public ItemModelBuilder doorTopRight(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorTopRight(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder doorTopRightOpen(Supplier<? extends Item> item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopRightOpen(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorTopRightOpen(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.doorTopRightOpen(item);
    }

    @Override
    public final ItemModelBuilder doorTopRightOpen(Item item, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopRightOpen(item, bottom, top);
    }

    @Override
    public final ItemModelBuilder doorTopRightOpen(Item item) {
        return ICrypticModelProvider.super.doorTopRightOpen(item);
    }

    @Override
    public ItemModelBuilder doorTopRightOpen(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorTopRightOpen(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder trapdoorBottom(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorBottom(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorBottom(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.trapdoorBottom(item);
    }

    @Override
    public final ItemModelBuilder trapdoorBottom(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorBottom(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorBottom(Item item) {
        return ICrypticModelProvider.super.trapdoorBottom(item);
    }

    @Override
    public ItemModelBuilder trapdoorBottom(String name, ResourceLocation texture) {
        return super.trapdoorBottom(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder trapdoorTop(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorTop(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorTop(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.trapdoorTop(item);
    }

    @Override
    public final ItemModelBuilder trapdoorTop(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorTop(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorTop(Item item) {
        return ICrypticModelProvider.super.trapdoorTop(item);
    }

    @Override
    public ItemModelBuilder trapdoorTop(String name, ResourceLocation texture) {
        return super.trapdoorTop(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder trapdoorOpen(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOpen(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorOpen(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.trapdoorOpen(item);
    }

    @Override
    public final ItemModelBuilder trapdoorOpen(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOpen(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorOpen(Item item) {
        return ICrypticModelProvider.super.trapdoorOpen(item);
    }

    @Override
    public ItemModelBuilder trapdoorOpen(String name, ResourceLocation texture) {
        return super.trapdoorOpen(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder trapdoorOrientableBottom(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableBottom(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorOrientableBottom(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.trapdoorOrientableBottom(item);
    }

    @Override
    public final ItemModelBuilder trapdoorOrientableBottom(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableBottom(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorOrientableBottom(Item item) {
        return ICrypticModelProvider.super.trapdoorOrientableBottom(item);
    }

    @Override
    public ItemModelBuilder trapdoorOrientableBottom(String name, ResourceLocation texture) {
        return super.trapdoorOrientableBottom(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder trapdoorOrientableTop(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableTop(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorOrientableTop(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.trapdoorOrientableTop(item);
    }

    @Override
    public final ItemModelBuilder trapdoorOrientableTop(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableTop(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorOrientableTop(Item item) {
        return ICrypticModelProvider.super.trapdoorOrientableTop(item);
    }

    @Override
    public ItemModelBuilder trapdoorOrientableTop(String name, ResourceLocation texture) {
        return super.trapdoorOrientableTop(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder trapdoorOrientableOpen(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableOpen(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorOrientableOpen(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.trapdoorOrientableOpen(item);
    }

    @Override
    public final ItemModelBuilder trapdoorOrientableOpen(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableOpen(item, texture);
    }

    @Override
    public final ItemModelBuilder trapdoorOrientableOpen(Item item) {
        return ICrypticModelProvider.super.trapdoorOrientableOpen(item);
    }

    @Override
    public ItemModelBuilder trapdoorOrientableOpen(String name, ResourceLocation texture) {
        return super.trapdoorOrientableOpen(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder torch(Supplier<? extends Item> item, ResourceLocation torch) {
        return ICrypticModelProvider.super.torch(item, torch);
    }

    @Override
    public final ItemModelBuilder torch(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.torch(item);
    }

    @Override
    public final ItemModelBuilder torch(Item item, ResourceLocation torch) {
        return ICrypticModelProvider.super.torch(item, torch);
    }

    @Override
    public final ItemModelBuilder torch(Item item) {
        return ICrypticModelProvider.super.torch(item);
    }

    @Override
    public ItemModelBuilder torch(String name, ResourceLocation torch) {
        return super.torch(name, torch).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder torchWall(Supplier<? extends Item> item, ResourceLocation torch) {
        return ICrypticModelProvider.super.torchWall(item, torch);
    }

    @Override
    public final ItemModelBuilder torchWall(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.torchWall(item);
    }

    @Override
    public final ItemModelBuilder torchWall(Item item, ResourceLocation torch) {
        return ICrypticModelProvider.super.torchWall(item, torch);
    }

    @Override
    public final ItemModelBuilder torchWall(Item item) {
        return ICrypticModelProvider.super.torchWall(item);
    }

    @Override
    public ItemModelBuilder torchWall(String name, ResourceLocation torch) {
        return super.torchWall(name, torch).renderType(RenderType.CUTOUT);
    }


    @Override
    public final ItemModelBuilder carpet(Supplier<? extends Item> item, ResourceLocation wool) {
        return ICrypticModelProvider.super.carpet(item, wool);
    }

    @Override
    public final ItemModelBuilder carpet(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.carpet(item);
    }

    @Override
    public final ItemModelBuilder carpet(Item item, ResourceLocation wool) {
        return ICrypticModelProvider.super.carpet(item, wool);
    }

    @Override
    public final ItemModelBuilder carpet(Item item) {
        return ICrypticModelProvider.super.carpet(item);
    }

    @Override
    public ItemModelBuilder carpet(String name, ResourceLocation wool) {
        return super.carpet(name, wool);
    }


    /*
     * MODEL GENERATION - ADDITIONAL VANILLA
     */

    @Override
    public final ItemModelBuilder leaves(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.leaves(item, texture);
    }

    @Override
    public final ItemModelBuilder leaves(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.leaves(item);
    }

    @Override
    public final ItemModelBuilder leaves(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.leaves(item, texture);
    }

    @Override
    public final ItemModelBuilder leaves(Item item) {
        return ICrypticModelProvider.super.leaves(item);
    }

    @Override
    public ItemModelBuilder leaves(String name, ResourceLocation texture) {
        return ICrypticModelProvider.super.leaves(name, texture);
    }


    @Override
    public final ItemModelBuilder cubeMirroredAll(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.cubeMirroredAll(item);
    }

    @Override
    public final ItemModelBuilder cubeMirroredAll(Supplier<? extends Item> item, ResourceLocation texture) {
        return ICrypticModelProvider.super.cubeMirroredAll(item, texture);
    }

    @Override
    public final ItemModelBuilder cubeMirroredAll(Item item, ResourceLocation texture) {
        return ICrypticModelProvider.super.cubeMirroredAll(item, texture);
    }

    @Override
    public final ItemModelBuilder cubeMirroredAll(Item item) {
        return ICrypticModelProvider.super.cubeMirroredAll(item);
    }

    @Override
    public ItemModelBuilder cubeMirroredAll(String name, ResourceLocation texture) {
        return ICrypticModelProvider.super.cubeMirroredAll(name, texture);
    }


    @Override
    public final ItemModelBuilder cubeMirrored(Supplier<? extends Item> item, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return ICrypticModelProvider.super.cubeMirrored(item, down, up, north, south, east, west);
    }

    @Override
    public final ItemModelBuilder cubeMirrored(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.cubeMirrored(item);
    }

    @Override
    public final ItemModelBuilder cubeMirrored(Item item, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return ICrypticModelProvider.super.cubeMirrored(item, down, up, north, south, east, west);
    }

    @Override
    public final ItemModelBuilder cubeMirrored(Item item) {
        return ICrypticModelProvider.super.cubeMirrored(item);
    }

    @Override
    public ItemModelBuilder cubeMirrored(String name, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return ICrypticModelProvider.super.cubeMirrored(name, down, up, north, south, east, west);
    }


    @Override
    public final ItemModelBuilder cubeFrontSided(Supplier<? extends Item> item, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeFrontSided(item, front, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder cubeFrontSided(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.cubeFrontSided(item);
    }

    @Override
    public final ItemModelBuilder cubeFrontSided(Item item, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeFrontSided(item, front, side, bottom, top);
    }

    @Override
    public final ItemModelBuilder cubeFrontSided(Item item) {
        return ICrypticModelProvider.super.cubeFrontSided(item);
    }

    @Override
    public ItemModelBuilder cubeFrontSided(String name, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeFrontSided(name, front, side, bottom, top);
    }


    @Override
    public final ItemModelBuilder cubeFrontBackSided(Supplier<? extends Item> item, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, ResourceLocation back) {
        return ICrypticModelProvider.super.cubeFrontBackSided(item, front, side, bottom, top, back);
    }

    @Override
    public final ItemModelBuilder cubeFrontBackSided(Supplier<? extends Item> item) {
        return ICrypticModelProvider.super.cubeFrontBackSided(item);
    }

    @Override
    public final ItemModelBuilder cubeFrontBackSided(Item item, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, ResourceLocation back) {
        return ICrypticModelProvider.super.cubeFrontBackSided(item, front, side, bottom, top, back);
    }

    @Override
    public final ItemModelBuilder cubeFrontBackSided(Item item) {
        return ICrypticModelProvider.super.cubeFrontBackSided(item);
    }

    @Override
    public ItemModelBuilder cubeFrontBackSided(String name, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, ResourceLocation back) {
        return ICrypticModelProvider.super.cubeFrontBackSided(name, front, side, bottom, top, back);
    }
}
