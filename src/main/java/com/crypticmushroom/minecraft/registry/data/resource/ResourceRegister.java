package com.crypticmushroom.minecraft.registry.data.resource;

import com.crypticmushroom.minecraft.registry.util.PluralString;
import net.minecraft.resources.ResourceLocation;

import java.util.Collection;

public interface ResourceRegister<T> {
    Collection<T> getResources();


    /* IDENTIFICATION */

    PluralString getName();


    /* RESOURCE LOCATION */

    String getModId();

    /**
     * @return A resource location containing the {@link #getModId()} mod ID} and the given path.
     */
    default ResourceLocation makeResLoc(String path) {
        return new ResourceLocation(this.makeResLocString(path));
    }

    /**
     * @return A resource location containing the {@link #getModId()} mod ID} and the given path, in the form of a
     *     {@link String}.
     */
    default String makeResLocString(String path) {
        return path.indexOf(':') >= 0 ? path : this.getModId() + ":" + path;
    }
}
