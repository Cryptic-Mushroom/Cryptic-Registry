package com.crypticmushroom.minecraft.registry.data.resource;

import com.crypticmushroom.minecraft.registry.util.Lazy;
import net.minecraft.resources.ResourceLocation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;

public class CustomResource<T, A> implements Function<A, T>, Lazy<T>, Comparable<CustomResource<T, A>> {
    private final ResourceLocation id;
    private final Function<A, T> factory;

    private @Nullable T instance = null;

    @Override
    public T apply(A argument) {
        var result = this.factory.apply(argument);
        this.instance = result;
        return result;
    }

    /**
     * To accommodate for systems needing access to the object after it has been created.
     *
     * @return The object if it exists, or null of it has not been created.
     */
    @Override
    public final @Nullable T get() {
        return this.instance;
    }

    public ResourceLocation getId() {
        return this.id;
    }

    @Override
    public boolean isResolved() {
        return this.instance != null;
    }

    @Override
    public int compareTo(@NotNull CustomResource<T, A> o) {
        return this.id.compareTo(o.id);
    }


    /* CREATION */

    public static <T, A> CustomResource<T, A> create(ResourceLocation id, Function<A, T> factory) {
        return new CustomResource<>(id, factory);
    }

    protected CustomResource(ResourceLocation id, Function<A, T> factory) {
        this.id = id;
        this.factory = factory;
    }
}
