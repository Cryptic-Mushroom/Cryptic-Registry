package com.crypticmushroom.minecraft.registry.data.provider.metadata;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import net.minecraft.SharedConstants;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraftforge.data.event.GatherDataEvent;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class CrypticPackMetadataGenerator extends PackMetadataGenerator implements ICrypticDataProvider {
    private final String modId;
    private final GatherDataEvent event;

    public CrypticPackMetadataGenerator(String modId, GatherDataEvent event) {
        super(event.getGenerator().getPackOutput());
        this.modId = modId;
        this.event = event;
    }

    @Override
    public String getName() {
        return ICrypticDataProvider.super.getName();
    }

    @Override
    public String getSimpleName() {
        return "Pack Metadata";
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public boolean shouldRun() {
        return true;
    }

    public void addToGenerator() {
        this.event.getGenerator().addProvider(true, this);
    }

    @Override
    public CompletableFuture<?> run(CachedOutput output) {
        var packTypes = Map.of(
            PackType.CLIENT_RESOURCES, SharedConstants.getCurrentVersion().getPackVersion(PackType.CLIENT_RESOURCES),
            PackType.SERVER_DATA, SharedConstants.getCurrentVersion().getPackVersion(PackType.SERVER_DATA)
        );

        LocalizedNameRegistry.OTHER.register(this.modId, "pack.%s.mod.description".formatted(this.modId), CrypticRegistry.getModInfo(this.modId).name() + " resources and data");

        this.add(
            PackMetadataSection.TYPE,
            new PackMetadataSection(
                Component.translatable("pack.%s.mod.description".formatted(this.modId)),
                SharedConstants.getCurrentVersion().getPackVersion(PackType.CLIENT_RESOURCES),
                packTypes
            )
        );

        return super.run(output);
    }
}
