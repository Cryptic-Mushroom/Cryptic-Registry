package com.crypticmushroom.minecraft.registry.data.provider.particle;

import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import net.minecraftforge.common.data.ParticleDescriptionProvider;
import net.minecraftforge.data.event.GatherDataEvent;

/**
 * The cryptic particle provider uses the {@link DataRegistry} for storing particle textures to output them in data
 * generation.
 *
 * @author Jonathing
 */
public class CrypticParticleDescriptionProvider extends ParticleDescriptionProvider implements ICrypticDataProvider {
    private final String modId;
    private final GatherDataEvent event;

    public CrypticParticleDescriptionProvider(String modId, GatherDataEvent event) {
        super(event.getGenerator().getPackOutput(), event.getExistingFileHelper());

        this.modId = modId;
        this.event = event;
    }

    @Override
    protected void addDescriptions() {
        DataRegistry.forParticleTextures(this.modId, this::spriteSet);
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public String getName() {
        return ICrypticDataProvider.super.getName();
    }

    @Override
    public String getSimpleName() {
        return "Particle Textures";
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public boolean shouldRun() {
        return this.event.includeClient();
    }
}
