package com.crypticmushroom.minecraft.registry.data.provider.tag;

import com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft.accessor.TagsProviderAccessor;
import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import com.crypticmushroom.minecraft.registry.data.registry.TagRegistry;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;

/**
 * This interface exists as the base contract for all tag providers created by Cryptic Registry.
 *
 * @param <T> The type for the tags to be generated.
 * @author Jonathing
 */
public interface ICrypticTagsProvider<T> extends ICrypticDataProvider {
    @SuppressWarnings("unchecked")
    private TagsProvider<T> self() {
        try {
            return (TagsProvider<T>) this;
        } catch (ClassCastException e) {
            throw new IllegalCallerException("ICrypticTagProvider can only be applied onto a subtype of TagsProvider", e);
        }
    }

    @SuppressWarnings("unchecked")
    private TagsProviderAccessor<T> selfAccess() {
        return (TagsProviderAccessor<T>) this.self();
    }

    @Override
    default String getSimpleName() {
        var tagRegistry = this.getTagRegistry();
        var namespace = tagRegistry.registryInfo.getKey().location().getNamespace();
        return "%s%s Tags".formatted(!namespace.equals("minecraft") ? "%s - ".formatted(namespace) : "", tagRegistry.name.singular());
    }

    @Override
    default boolean shouldRun() {
        return this.getEvent().includeServer() && !this.getTagRegistry().isEmpty();
    }

    /**
     * Since tags are mostly a runtime-only game object, the tag provider must use a {@link TagRegistry tag registry} to
     * function.
     *
     * @return The tag registry to use for this tag provider.
     */
    TagRegistry<T> getTagRegistry();

    /**
     * Adds all registered tags in this provider's {@link #getTagRegistry() tag registry} to the provider.
     *
     * @implNote This method <strong>must be used</strong> in the implementation class's
     *     {@link TagsProvider#addTags()} method.
     */
    @SuppressWarnings("JavadocReference")
    default void addRegisteredTags(HolderLookup.Provider provider) {
        this.getTagRegistry().forTags(this.getModId(), this::add);
        this.getTagRegistry().forTagObjectEntries(this.getModId(), this::add);
        this.getTagRegistry().forTagOfTagEntries(this.getModId(), this::add);
    }

    /**
     * This method must add the given tag to the provider.
     *
     * @param tag The tag to add.
     */
    default void add(TagKey<T> tag) {
        this.self().tag(tag);
    }

    /**
     * This method must add the given child tag to the given parent tag.
     *
     * @param parent The parent tag.
     * @param child  The child tag to add to the parent tag.
     */
    default void add(TagKey<T> parent, TagKey<T> child) {
        this.self().tag(parent).addTag(child);
    }

    /**
     * This method must add the given object to the given tag.
     *
     * @param tag    The tag.
     * @param object The object to add to the tag.
     */
    default void add(TagKey<T> tag, ResourceKey<T> object) {
        this.self().tag(tag).add(object);
    }
}
