package com.crypticmushroom.minecraft.registry.data.tag;

import com.crypticmushroom.minecraft.registry.data.registry.TagRegistry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraftforge.registries.RegistryObject;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Tag appenders exist as a way to add data to tags for Cryptic Registry.
 *
 * @param <T> The type of the tag for the tag appender.
 * @author Jonathing
 */
public final class CrypticTagAppender<T> {
    private final String modId;
    private final TagKey<T> tag;
    private final TagRegistry<T> registry;

    private final Set<TagKey<T>> tagsToAdd = new HashSet<>();
    private final Set<TagKey<T>> tagsToAddReverse = new HashSet<>();
    private final Set<ResourceKey<T>> objectsToAdd = new HashSet<>();

    /**
     * Creates a new tag appender with the given tag and tag registry for the given mod ID.
     *
     * @param modId    The mod ID to create the appender for.
     * @param tag      The tag to create the appender for.
     * @param registry The tag registry to use with the appender.
     */
    public CrypticTagAppender(String modId, TagKey<T> tag, TagRegistry<T> registry) {
        this.modId = modId;
        this.tag = tag;
        this.registry = registry;
    }

    @SuppressWarnings("unchecked")
    void finish() {
        this.registry.register(this.modId, this.tag, this.tagsToAdd.toArray(TagKey[]::new));
        this.tagsToAddReverse.forEach(tag -> this.registry.register(this.modId, tag, this.tag));
        this.registry.register(this.modId, this.tag, this.objectsToAdd.toArray(ResourceKey[]::new));
    }

    /**
     * Adds tags that this appender's tag should be the child of.
     *
     * @param tag The tags to add.
     * @return This appender.
     */
    @SafeVarargs
    public final CrypticTagAppender<T> childOf(TagKey<T>... tag) {
        this.tagsToAdd.addAll(Arrays.asList(tag));
        return this;
    }

    /**
     * Adds tags that are to be the children of this appender's tag.
     *
     * @param tag The tags to add.
     * @return This appender.
     */
    @SafeVarargs
    public final CrypticTagAppender<T> add(TagKey<T>... tag) {
        this.tagsToAddReverse.addAll(Arrays.asList(tag));
        return this;
    }

    /**
     * Adds suppliers of objects to this appender's tag.
     *
     * @param object The objects to add.
     * @return This appender.
     */
    @SafeVarargs
    public final CrypticTagAppender<T> add(Supplier<? extends T>... object) {
        this.objectsToAdd.addAll(Arrays.stream(object).map(s -> this.registry.registryInfo.getResourceKey(s.get())).toList());
        return this;
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public final CrypticTagAppender<T> add(RegistryObject<? extends T>... object) {
        this.objectsToAdd.addAll(Arrays.stream(object).map(s -> (ResourceKey<T>) s.getKey()).toList());
        return this;
    }

    /**
     * Adds the resource keys of objects to this appender's tag.
     *
     * @param object The objects to add.
     * @return This appender.
     */
    @SafeVarargs
    @SuppressWarnings("unchecked")
    public final CrypticTagAppender<T> add(ResourceKey<? extends T>... object) {
        this.objectsToAdd.addAll(Arrays.asList((ResourceKey<T>[]) object));
        return this;
    }
}
