package com.crypticmushroom.minecraft.registry.data.provider.model;

import com.crypticmushroom.minecraft.registry.coremod.mixin.data.forge.accessor.ModelProviderAccessor;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.models.model.ModelTemplate;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.model.generators.BlockModelBuilder;
import net.minecraftforge.client.model.generators.BlockModelProvider;
import net.minecraftforge.data.event.GatherDataEvent;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public abstract non-sealed class CrypticBlockModelProvider extends BlockModelProvider implements ICrypticModelProvider<Block, BlockModelBuilder> {
    private final String modId;
    private final GatherDataEvent event;

    boolean dummy = false;

    {
        //noinspection unchecked
        ((ModelProviderAccessor<BlockModelBuilder>) this).cmreg$factory(resLoc -> new BlockModelBuilder(resLoc, this.existingFileHelper) {
            @Override
            public BlockModelBuilder renderType(ResourceLocation renderType) {
                try {
                    return renderType instanceof RenderType r ? this.renderType(r) : super.renderType(renderType);
                } catch (NullPointerException e) {
                    throw new IllegalArgumentException("Render type must not be null. To remove a render type, use ICrypticModelProvider.RenderType.NONE", e);
                }
            }

            public BlockModelBuilder renderType(RenderType renderType) {
                if (renderType == null || renderType == RenderType.NONE) {
                    this.renderType = null;
                    return this;
                }

                return renderType != RenderType.DEFAULT ? super.renderType(Objects.requireNonNull(renderType, "A RenderType object cannot have a null ID")) : this;
            }
        });
    }

    public CrypticBlockModelProvider(String modId, GatherDataEvent event) {
        super(event.getGenerator().getPackOutput(), modId, event.getExistingFileHelper());

        this.modId = modId;
        this.event = event;
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public String getName() {
        return ICrypticModelProvider.super.getName();
    }

    @Override
    public String getSimpleName() {
        return "Block Models";
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public boolean shouldRun() {
        return this.event.includeClient();
    }

    @Override
    public String getFolder() {
        return this.folder;
    }

    @Override
    public final CompletableFuture<?> run(CachedOutput cache) {
        return !this.dummy ? super.run(cache) : CompletableFuture.allOf();
    }


    /* IMPLEMENTATION OF ICRYPTICMODELPROVIDER */

    /* MODEL GENERATION - USING TEMPLATES */

    @Override
    public final BlockModelBuilder fromTemplate(Supplier<? extends Block> block, ModelTemplate parent, ResourceLocation... texture) {
        return ICrypticModelProvider.super.fromTemplate(block, parent, texture);
    }

    @Override
    public final BlockModelBuilder fromTemplate(Block block, ModelTemplate parent, ResourceLocation... texture) {
        return ICrypticModelProvider.super.fromTemplate(block, parent, texture);
    }

    @Override
    public BlockModelBuilder fromTemplate(String name, ModelTemplate parent, ResourceLocation... texture) {
        return ICrypticModelProvider.super.fromTemplate(name, parent, texture);
    }


    /*
     * MODEL GENERATION - VANILLA
     */

    @Override
    public final BlockModelBuilder cube(Supplier<? extends Block> block, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return ICrypticModelProvider.super.cube(block, down, up, north, south, east, west);
    }

    @Override
    public final BlockModelBuilder cube(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.cube(block);
    }

    @Override
    public final BlockModelBuilder cube(Block block, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return ICrypticModelProvider.super.cube(block, down, up, north, south, east, west);
    }

    @Override
    public final BlockModelBuilder cube(Block block) {
        return ICrypticModelProvider.super.cube(block);
    }

    @Override
    public BlockModelBuilder cube(String name, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return super.cube(name, down, up, north, south, east, west);
    }


    @Override
    public final BlockModelBuilder singleTexture(Supplier<? extends Block> block, ResourceLocation parent) {
        return ICrypticModelProvider.super.singleTexture(block, parent);
    }

    @Override
    public final BlockModelBuilder singleTexture(Supplier<? extends Block> block, ModelTemplate parent) {
        return ICrypticModelProvider.super.singleTexture(block, parent);
    }

    @Override
    public final BlockModelBuilder singleTexture(Supplier<? extends Block> block, ResourceLocation parent, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(block, parent, texture);
    }

    @Override
    public final BlockModelBuilder singleTexture(Supplier<? extends Block> block, ModelTemplate parent, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(block, parent, texture);
    }

    @Override
    public final BlockModelBuilder singleTexture(Block block, ResourceLocation parent) {
        return ICrypticModelProvider.super.singleTexture(block, parent);
    }

    @Override
    public final BlockModelBuilder singleTexture(Block block, ModelTemplate parent) {
        return ICrypticModelProvider.super.singleTexture(block, parent);
    }

    @Override
    public final BlockModelBuilder singleTexture(Block block, ResourceLocation parent, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(block, parent, texture);
    }

    @Override
    public final BlockModelBuilder singleTexture(Block block, ModelTemplate parent, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(block, parent, texture);
    }

    @Override
    public BlockModelBuilder singleTexture(String name, ResourceLocation parent, ResourceLocation texture) {
        return super.singleTexture(name, parent, texture);
    }


    @Override
    public BlockModelBuilder singleTexture(Supplier<? extends Block> block, ResourceLocation parent, String textureKey) {
        return ICrypticModelProvider.super.singleTexture(block, parent, textureKey);
    }

    @Override
    public final BlockModelBuilder singleTexture(Supplier<? extends Block> block, ModelTemplate parent, String textureKey) {
        return ICrypticModelProvider.super.singleTexture(block, parent, textureKey);
    }

    @Override
    public final BlockModelBuilder singleTexture(Supplier<? extends Block> block, ResourceLocation parent, String textureKey, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(block, parent, textureKey, texture);
    }

    @Override
    public final BlockModelBuilder singleTexture(Supplier<? extends Block> block, ModelTemplate parent, String textureKey, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(block, parent, textureKey, texture);
    }

    @Override
    public final BlockModelBuilder singleTexture(Block block, ResourceLocation parent, String textureKey) {
        return ICrypticModelProvider.super.singleTexture(block, parent, textureKey);
    }

    @Override
    public final BlockModelBuilder singleTexture(Block block, ModelTemplate parent, String textureKey) {
        return ICrypticModelProvider.super.singleTexture(block, parent, textureKey);
    }

    @Override
    public final BlockModelBuilder singleTexture(Block block, ResourceLocation parent, String textureKey, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(block, parent, textureKey, texture);
    }

    @Override
    public final BlockModelBuilder singleTexture(Block block, ModelTemplate parent, String textureKey, ResourceLocation texture) {
        return ICrypticModelProvider.super.singleTexture(block, parent, textureKey, texture);
    }

    @Override
    public BlockModelBuilder singleTexture(String name, ResourceLocation parent, String textureKey, ResourceLocation texture) {
        return super.singleTexture(name, parent, textureKey, texture);
    }


    @Override
    public final BlockModelBuilder cubeAll(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.cubeAll(block, texture);
    }

    @Override
    public final BlockModelBuilder cubeAll(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.cubeAll(block);
    }

    @Override
    public final BlockModelBuilder cubeAll(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.cubeAll(block, texture);
    }

    @Override
    public final BlockModelBuilder cubeAll(Block block) {
        return ICrypticModelProvider.super.cubeAll(block);
    }

    @Override
    public BlockModelBuilder cubeAll(String name, ResourceLocation texture) {
        return super.cubeAll(name, texture);
    }


    @Override
    public final BlockModelBuilder cubeTop(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeTop(block, side, top);
    }

    @Override
    public final BlockModelBuilder cubeTop(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.cubeTop(block);
    }

    @Override
    public final BlockModelBuilder cubeTop(Block block, ResourceLocation side, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeTop(block, side, top);
    }

    @Override
    public final BlockModelBuilder cubeTop(Block block) {
        return ICrypticModelProvider.super.cubeTop(block);
    }

    @Override
    public BlockModelBuilder cubeTop(String name, ResourceLocation side, ResourceLocation top) {
        return super.cubeTop(name, side, top);
    }


    @Override
    public final BlockModelBuilder cubeBottomTop(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeBottomTop(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder cubeBottomTop(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.cubeBottomTop(block);
    }

    @Override
    public final BlockModelBuilder cubeBottomTop(Block block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeBottomTop(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder cubeBottomTop(Block block) {
        return ICrypticModelProvider.super.cubeBottomTop(block);
    }

    @Override
    public BlockModelBuilder cubeBottomTop(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.cubeBottomTop(name, side, bottom, top);
    }


    @Override
    public final BlockModelBuilder cubeColumn(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation end) {
        return ICrypticModelProvider.super.cubeColumn(block, side, end);
    }

    @Override
    public final BlockModelBuilder cubeColumn(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.cubeColumn(block);
    }

    @Override
    public final BlockModelBuilder cubeColumn(Block block, ResourceLocation side, ResourceLocation end) {
        return ICrypticModelProvider.super.cubeColumn(block, side, end);
    }

    @Override
    public final BlockModelBuilder cubeColumn(Block block) {
        return ICrypticModelProvider.super.cubeColumn(block);
    }

    @Override
    public BlockModelBuilder cubeColumn(String name, ResourceLocation side, ResourceLocation end) {
        return super.cubeColumn(name, side, end);
    }


    @Override
    public final BlockModelBuilder cubeColumnHorizontal(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation end) {
        return ICrypticModelProvider.super.cubeColumnHorizontal(block, side, end);
    }

    @Override
    public final BlockModelBuilder cubeColumnHorizontal(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.cubeColumnHorizontal(block);
    }

    @Override
    public final BlockModelBuilder cubeColumnHorizontal(Block block, ResourceLocation side, ResourceLocation end) {
        return ICrypticModelProvider.super.cubeColumnHorizontal(block, side, end);
    }

    @Override
    public final BlockModelBuilder cubeColumnHorizontal(Block block) {
        return ICrypticModelProvider.super.cubeColumnHorizontal(block);
    }

    @Override
    public BlockModelBuilder cubeColumnHorizontal(String name, ResourceLocation side, ResourceLocation end) {
        return super.cubeColumnHorizontal(name, side, end);
    }


    @Override
    public final BlockModelBuilder orientableVertical(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation front) {
        return ICrypticModelProvider.super.orientableVertical(block, side, front);
    }

    @Override
    public final BlockModelBuilder orientableVertical(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.orientableVertical(block);
    }

    @Override
    public final BlockModelBuilder orientableVertical(Block block, ResourceLocation side, ResourceLocation front) {
        return ICrypticModelProvider.super.orientableVertical(block, side, front);
    }

    @Override
    public final BlockModelBuilder orientableVertical(Block block) {
        return ICrypticModelProvider.super.orientableVertical(block);
    }

    @Override
    public BlockModelBuilder orientableVertical(String name, ResourceLocation side, ResourceLocation front) {
        return super.orientableVertical(name, side, front);
    }


    @Override
    public final BlockModelBuilder orientableWithBottom(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation front, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.orientableWithBottom(block, side, front, bottom, top);
    }

    @Override
    public final BlockModelBuilder orientableWithBottom(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.orientableWithBottom(block);
    }

    @Override
    public final BlockModelBuilder orientableWithBottom(Block block, ResourceLocation side, ResourceLocation front, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.orientableWithBottom(block, side, front, bottom, top);
    }

    @Override
    public final BlockModelBuilder orientableWithBottom(Block block) {
        return ICrypticModelProvider.super.orientableWithBottom(block);
    }

    @Override
    public BlockModelBuilder orientableWithBottom(String name, ResourceLocation side, ResourceLocation front, ResourceLocation bottom, ResourceLocation top) {
        return super.orientableWithBottom(name, side, front, bottom, top);
    }


    @Override
    public final BlockModelBuilder orientable(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation front, ResourceLocation top) {
        return ICrypticModelProvider.super.orientable(block, side, front, top);
    }

    @Override
    public final BlockModelBuilder orientable(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.orientable(block);
    }

    @Override
    public final BlockModelBuilder orientable(Block block, ResourceLocation side, ResourceLocation front, ResourceLocation top) {
        return ICrypticModelProvider.super.orientable(block, side, front, top);
    }

    @Override
    public final BlockModelBuilder orientable(Block block) {
        return ICrypticModelProvider.super.orientable(block);
    }

    @Override
    public BlockModelBuilder orientable(String name, ResourceLocation side, ResourceLocation front, ResourceLocation top) {
        return super.orientable(name, side, front, top);
    }


    @Override
    public final BlockModelBuilder crop(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.crop(block, texture);
    }

    @Override
    public final BlockModelBuilder crop(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.crop(block);
    }

    @Override
    public final BlockModelBuilder crop(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.crop(block, texture);
    }

    @Override
    public final BlockModelBuilder crop(Block block) {
        return ICrypticModelProvider.super.crop(block);
    }

    @Override
    public BlockModelBuilder crop(String name, ResourceLocation crop) {
        return super.crop(name, crop).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder cross(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.cross(block, texture);
    }

    @Override
    public final BlockModelBuilder cross(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.cross(block);
    }

    @Override
    public final BlockModelBuilder cross(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.cross(block, texture);
    }

    @Override
    public final BlockModelBuilder cross(Block block) {
        return ICrypticModelProvider.super.cross(block);
    }

    @Override
    public BlockModelBuilder cross(String name, ResourceLocation cross) {
        return super.cross(name, cross).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder stairs(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairs(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder stairs(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.stairs(block);
    }

    @Override
    public final BlockModelBuilder stairs(Block block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairs(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder stairs(Block block) {
        return ICrypticModelProvider.super.stairs(block);
    }

    @Override
    public BlockModelBuilder stairs(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.stairs(name, side, bottom, top);
    }


    @Override
    public final BlockModelBuilder stairsOuter(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairsOuter(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder stairsOuter(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.stairsOuter(block);
    }

    @Override
    public final BlockModelBuilder stairsOuter(Block block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairsOuter(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder stairsOuter(Block block) {
        return ICrypticModelProvider.super.stairsOuter(block);
    }

    @Override
    public BlockModelBuilder stairsOuter(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.stairsOuter(name, side, bottom, top);
    }


    @Override
    public final BlockModelBuilder stairsInner(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairsInner(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder stairsInner(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.stairsInner(block);
    }

    @Override
    public final BlockModelBuilder stairsInner(Block block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.stairsInner(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder stairsInner(Block block) {
        return ICrypticModelProvider.super.stairsInner(block);
    }

    @Override
    public BlockModelBuilder stairsInner(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.stairsInner(name, side, bottom, top);
    }


    @Override
    public final BlockModelBuilder slab(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.slab(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder slab(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.slab(block);
    }

    @Override
    public final BlockModelBuilder slab(Block block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.slab(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder slab(Block block) {
        return ICrypticModelProvider.super.slab(block);
    }

    @Override
    public BlockModelBuilder slab(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.slab(name, side, bottom, top);
    }


    @Override
    public final BlockModelBuilder slabTop(Supplier<? extends Block> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.slabTop(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder slabTop(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.slabTop(block);
    }

    @Override
    public final BlockModelBuilder slabTop(Block block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.slabTop(block, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder slabTop(Block block) {
        return ICrypticModelProvider.super.slabTop(block);
    }

    @Override
    public BlockModelBuilder slabTop(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return super.slabTop(name, side, bottom, top);
    }


    @Override
    public final BlockModelBuilder button(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.button(block, texture);
    }

    @Override
    public final BlockModelBuilder button(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.button(block);
    }

    @Override
    public final BlockModelBuilder button(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.button(block, texture);
    }

    @Override
    public final BlockModelBuilder button(Block block) {
        return ICrypticModelProvider.super.button(block);
    }

    @Override
    public BlockModelBuilder button(String name, ResourceLocation texture) {
        return super.button(name, texture);
    }


    @Override
    public final BlockModelBuilder buttonPressed(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.buttonPressed(block, texture);
    }

    @Override
    public final BlockModelBuilder buttonPressed(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.buttonPressed(block);
    }

    @Override
    public final BlockModelBuilder buttonPressed(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.buttonPressed(block, texture);
    }

    @Override
    public final BlockModelBuilder buttonPressed(Block block) {
        return ICrypticModelProvider.super.buttonPressed(block);
    }

    @Override
    public BlockModelBuilder buttonPressed(String name, ResourceLocation texture) {
        return super.buttonPressed(name, texture);
    }


    @Override
    public final BlockModelBuilder buttonInventory(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.buttonInventory(block, texture);
    }

    @Override
    public final BlockModelBuilder buttonInventory(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.buttonInventory(block);
    }

    @Override
    public final BlockModelBuilder buttonInventory(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.buttonInventory(block, texture);
    }

    @Override
    public final BlockModelBuilder buttonInventory(Block block) {
        return ICrypticModelProvider.super.buttonInventory(block);
    }

    @Override
    public BlockModelBuilder buttonInventory(String name, ResourceLocation texture) {
        return super.buttonInventory(name, texture);
    }


    @Override
    public final BlockModelBuilder pressurePlate(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.pressurePlate(block, texture);
    }

    @Override
    public final BlockModelBuilder pressurePlate(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.pressurePlate(block);
    }

    @Override
    public final BlockModelBuilder pressurePlate(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.pressurePlate(block, texture);
    }

    @Override
    public final BlockModelBuilder pressurePlate(Block block) {
        return ICrypticModelProvider.super.pressurePlate(block);
    }

    @Override
    public BlockModelBuilder pressurePlate(String name, ResourceLocation texture) {
        return super.pressurePlate(name, texture);
    }


    @Override
    public final BlockModelBuilder pressurePlateDown(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.pressurePlateDown(block, texture);
    }

    @Override
    public final BlockModelBuilder pressurePlateDown(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.pressurePlateDown(block);
    }

    @Override
    public final BlockModelBuilder pressurePlateDown(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.pressurePlateDown(block, texture);
    }

    @Override
    public final BlockModelBuilder pressurePlateDown(Block block) {
        return ICrypticModelProvider.super.pressurePlateDown(block);
    }

    @Override
    public BlockModelBuilder pressurePlateDown(String name, ResourceLocation texture) {
        return super.pressurePlateDown(name, texture);
    }


    @Override
    public final BlockModelBuilder sign(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.sign(block, texture);
    }

    @Override
    public final BlockModelBuilder sign(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.sign(block);
    }

    @Override
    public final BlockModelBuilder sign(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.sign(block, texture);
    }

    @Override
    public final BlockModelBuilder sign(Block block) {
        return ICrypticModelProvider.super.sign(block);
    }

    @Override
    public BlockModelBuilder sign(String name, ResourceLocation texture) {
        return super.sign(name, texture);
    }


    @Override
    public final BlockModelBuilder fencePost(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fencePost(block, texture);
    }

    @Override
    public final BlockModelBuilder fencePost(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.fencePost(block);
    }

    @Override
    public final BlockModelBuilder fencePost(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fencePost(block, texture);
    }

    @Override
    public final BlockModelBuilder fencePost(Block block) {
        return ICrypticModelProvider.super.fencePost(block);
    }

    @Override
    public BlockModelBuilder fencePost(String name, ResourceLocation texture) {
        return super.fencePost(name, texture);
    }


    @Override
    public final BlockModelBuilder fenceSide(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceSide(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceSide(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.fenceSide(block);
    }

    @Override
    public final BlockModelBuilder fenceSide(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceSide(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceSide(Block block) {
        return ICrypticModelProvider.super.fenceSide(block);
    }

    @Override
    public BlockModelBuilder fenceSide(String name, ResourceLocation texture) {
        return super.fenceSide(name, texture);
    }


    @Override
    public final BlockModelBuilder fenceInventory(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceInventory(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceInventory(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.fenceInventory(block);
    }

    @Override
    public final BlockModelBuilder fenceInventory(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceInventory(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceInventory(Block block) {
        return ICrypticModelProvider.super.fenceInventory(block);
    }

    @Override
    public BlockModelBuilder fenceInventory(String name, ResourceLocation texture) {
        return super.fenceInventory(name, texture);
    }


    @Override
    public final BlockModelBuilder fenceGate(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGate(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceGate(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.fenceGate(block);
    }

    @Override
    public final BlockModelBuilder fenceGate(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGate(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceGate(Block block) {
        return ICrypticModelProvider.super.fenceGate(block);
    }

    @Override
    public BlockModelBuilder fenceGate(String name, ResourceLocation texture) {
        return super.fenceGate(name, texture);
    }


    @Override
    public final BlockModelBuilder fenceGateOpen(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateOpen(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceGateOpen(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.fenceGateOpen(block);
    }

    @Override
    public final BlockModelBuilder fenceGateOpen(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateOpen(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceGateOpen(Block block) {
        return ICrypticModelProvider.super.fenceGateOpen(block);
    }

    @Override
    public BlockModelBuilder fenceGateOpen(String name, ResourceLocation texture) {
        return super.fenceGateOpen(name, texture);
    }


    @Override
    public final BlockModelBuilder fenceGateWall(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateWall(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceGateWall(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.fenceGateWall(block);
    }

    @Override
    public final BlockModelBuilder fenceGateWall(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateWall(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceGateWall(Block block) {
        return ICrypticModelProvider.super.fenceGateWall(block);
    }

    @Override
    public BlockModelBuilder fenceGateWall(String name, ResourceLocation texture) {
        return super.fenceGateWall(name, texture);
    }


    @Override
    public final BlockModelBuilder fenceGateWallOpen(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateWallOpen(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceGateWallOpen(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.fenceGateWallOpen(block);
    }

    @Override
    public final BlockModelBuilder fenceGateWallOpen(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.fenceGateWallOpen(block, texture);
    }

    @Override
    public final BlockModelBuilder fenceGateWallOpen(Block block) {
        return ICrypticModelProvider.super.fenceGateWallOpen(block);
    }

    @Override
    public BlockModelBuilder fenceGateWallOpen(String name, ResourceLocation texture) {
        return super.fenceGateWallOpen(name, texture);
    }


    @Override
    public final BlockModelBuilder wallPost(Supplier<? extends Block> block, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallPost(block, wall);
    }

    @Override
    public final BlockModelBuilder wallPost(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.wallPost(block);
    }

    @Override
    public final BlockModelBuilder wallPost(Block block, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallPost(block, wall);
    }

    @Override
    public final BlockModelBuilder wallPost(Block block) {
        return ICrypticModelProvider.super.wallPost(block);
    }

    @Override
    public BlockModelBuilder wallPost(String name, ResourceLocation wall) {
        return super.wallPost(name, wall);
    }


    @Override
    public final BlockModelBuilder wallSide(Supplier<? extends Block> block, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallSide(block, wall);
    }

    @Override
    public final BlockModelBuilder wallSide(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.wallSide(block);
    }

    @Override
    public final BlockModelBuilder wallSide(Block block, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallSide(block, wall);
    }

    @Override
    public final BlockModelBuilder wallSide(Block block) {
        return ICrypticModelProvider.super.wallSide(block);
    }

    @Override
    public BlockModelBuilder wallSide(String name, ResourceLocation wall) {
        return super.wallSide(name, wall);
    }


    @Override
    public final BlockModelBuilder wallSideTall(Supplier<? extends Block> block, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallSideTall(block, wall);
    }

    @Override
    public final BlockModelBuilder wallSideTall(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.wallSideTall(block);
    }

    @Override
    public final BlockModelBuilder wallSideTall(Block block, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallSideTall(block, wall);
    }

    @Override
    public final BlockModelBuilder wallSideTall(Block block) {
        return ICrypticModelProvider.super.wallSideTall(block);
    }

    @Override
    public BlockModelBuilder wallSideTall(String name, ResourceLocation wall) {
        return super.wallSideTall(name, wall);
    }


    @Override
    public final BlockModelBuilder wallInventory(Supplier<? extends Block> block, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallInventory(block, wall);
    }

    @Override
    public final BlockModelBuilder wallInventory(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.wallInventory(block);
    }

    @Override
    public final BlockModelBuilder wallInventory(Block block, ResourceLocation wall) {
        return ICrypticModelProvider.super.wallInventory(block, wall);
    }

    @Override
    public final BlockModelBuilder wallInventory(Block block) {
        return ICrypticModelProvider.super.wallInventory(block);
    }

    @Override
    public BlockModelBuilder wallInventory(String name, ResourceLocation wall) {
        return super.wallInventory(name, wall);
    }


    @Override
    public final BlockModelBuilder panePost(Supplier<? extends Block> block, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.panePost(block, pane, edge);
    }

    @Override
    public final BlockModelBuilder panePost(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.panePost(block);
    }

    @Override
    public final BlockModelBuilder panePost(Block block, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.panePost(block, pane, edge);
    }

    @Override
    public final BlockModelBuilder panePost(Block block) {
        return ICrypticModelProvider.super.panePost(block);
    }

    @Override
    public BlockModelBuilder panePost(String name, ResourceLocation pane, ResourceLocation edge) {
        return super.panePost(name, pane, edge).renderType(RenderType.CUTOUT_MIPPED);
    }


    @Override
    public final BlockModelBuilder paneSide(Supplier<? extends Block> block, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.paneSide(block, pane, edge);
    }

    @Override
    public final BlockModelBuilder paneSide(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.paneSide(block);
    }

    @Override
    public final BlockModelBuilder paneSide(Block block, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.paneSide(block, pane, edge);
    }

    @Override
    public final BlockModelBuilder paneSide(Block block) {
        return ICrypticModelProvider.super.paneSide(block);
    }

    @Override
    public BlockModelBuilder paneSide(String name, ResourceLocation pane, ResourceLocation edge) {
        return super.paneSide(name, pane, edge).renderType(RenderType.CUTOUT_MIPPED);
    }


    @Override
    public final BlockModelBuilder paneSideAlt(Supplier<? extends Block> block, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.paneSideAlt(block, pane, edge);
    }

    @Override
    public final BlockModelBuilder paneSideAlt(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.paneSideAlt(block);
    }

    @Override
    public final BlockModelBuilder paneSideAlt(Block block, ResourceLocation pane, ResourceLocation edge) {
        return ICrypticModelProvider.super.paneSideAlt(block, pane, edge);
    }

    @Override
    public final BlockModelBuilder paneSideAlt(Block block) {
        return ICrypticModelProvider.super.paneSideAlt(block);
    }

    @Override
    public BlockModelBuilder paneSideAlt(String name, ResourceLocation pane, ResourceLocation edge) {
        return super.paneSideAlt(name, pane, edge).renderType(RenderType.CUTOUT_MIPPED);
    }


    @Override
    public final BlockModelBuilder paneNoSide(Supplier<? extends Block> block, ResourceLocation pane) {
        return ICrypticModelProvider.super.paneNoSide(block, pane);
    }

    @Override
    public final BlockModelBuilder paneNoSide(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.paneNoSide(block);
    }

    @Override
    public final BlockModelBuilder paneNoSide(Block block, ResourceLocation pane) {
        return ICrypticModelProvider.super.paneNoSide(block, pane);
    }

    @Override
    public final BlockModelBuilder paneNoSide(Block block) {
        return ICrypticModelProvider.super.paneNoSide(block);
    }

    @Override
    public BlockModelBuilder paneNoSide(String name, ResourceLocation pane) {
        return super.paneNoSide(name, pane).renderType(RenderType.CUTOUT_MIPPED);
    }


    @Override
    public final BlockModelBuilder paneNoSideAlt(Supplier<? extends Block> block, ResourceLocation pane) {
        return ICrypticModelProvider.super.paneNoSideAlt(block, pane);
    }

    @Override
    public final BlockModelBuilder paneNoSideAlt(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.paneNoSideAlt(block);
    }

    @Override
    public final BlockModelBuilder paneNoSideAlt(Block block, ResourceLocation pane) {
        return ICrypticModelProvider.super.paneNoSideAlt(block, pane);
    }

    @Override
    public final BlockModelBuilder paneNoSideAlt(Block block) {
        return ICrypticModelProvider.super.paneNoSideAlt(block);
    }

    @Override
    public BlockModelBuilder paneNoSideAlt(String name, ResourceLocation pane) {
        return super.paneNoSideAlt(name, pane).renderType(RenderType.CUTOUT_MIPPED);
    }


    @Override
    public final BlockModelBuilder doorBottomLeft(Supplier<? extends Block> block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomLeft(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorBottomLeft(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.doorBottomLeft(block);
    }

    @Override
    public final BlockModelBuilder doorBottomLeft(Block block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomLeft(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorBottomLeft(Block block) {
        return ICrypticModelProvider.super.doorBottomLeft(block);
    }

    @Override
    public BlockModelBuilder doorBottomLeft(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorBottomLeft(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder doorBottomLeftOpen(Supplier<? extends Block> block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomLeftOpen(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorBottomLeftOpen(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.doorBottomLeftOpen(block);
    }

    @Override
    public final BlockModelBuilder doorBottomLeftOpen(Block block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomLeftOpen(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorBottomLeftOpen(Block block) {
        return ICrypticModelProvider.super.doorBottomLeftOpen(block);
    }

    @Override
    public BlockModelBuilder doorBottomLeftOpen(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorBottomLeftOpen(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder doorBottomRight(Supplier<? extends Block> block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomRight(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorBottomRight(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.doorBottomRight(block);
    }

    @Override
    public final BlockModelBuilder doorBottomRight(Block block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomRight(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorBottomRight(Block block) {
        return ICrypticModelProvider.super.doorBottomRight(block);
    }

    @Override
    public BlockModelBuilder doorBottomRight(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorBottomRight(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder doorBottomRightOpen(Supplier<? extends Block> block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomRightOpen(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorBottomRightOpen(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.doorBottomRightOpen(block);
    }

    @Override
    public final BlockModelBuilder doorBottomRightOpen(Block block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorBottomRightOpen(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorBottomRightOpen(Block block) {
        return ICrypticModelProvider.super.doorBottomRightOpen(block);
    }

    @Override
    public BlockModelBuilder doorBottomRightOpen(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorBottomRightOpen(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder doorTopLeft(Supplier<? extends Block> block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopLeft(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorTopLeft(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.doorTopLeft(block);
    }

    @Override
    public final BlockModelBuilder doorTopLeft(Block block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopLeft(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorTopLeft(Block block) {
        return ICrypticModelProvider.super.doorTopLeft(block);
    }

    @Override
    public BlockModelBuilder doorTopLeft(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorTopLeft(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder doorTopLeftOpen(Supplier<? extends Block> block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopLeftOpen(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorTopLeftOpen(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.doorTopLeftOpen(block);
    }

    @Override
    public final BlockModelBuilder doorTopLeftOpen(Block block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopLeftOpen(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorTopLeftOpen(Block block) {
        return ICrypticModelProvider.super.doorTopLeftOpen(block);
    }

    @Override
    public BlockModelBuilder doorTopLeftOpen(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorTopLeftOpen(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder doorTopRight(Supplier<? extends Block> block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopRight(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorTopRight(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.doorTopRight(block);
    }

    @Override
    public final BlockModelBuilder doorTopRight(Block block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopRight(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorTopRight(Block block) {
        return ICrypticModelProvider.super.doorTopRight(block);
    }

    @Override
    public BlockModelBuilder doorTopRight(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorTopRight(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder doorTopRightOpen(Supplier<? extends Block> block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopRightOpen(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorTopRightOpen(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.doorTopRightOpen(block);
    }

    @Override
    public final BlockModelBuilder doorTopRightOpen(Block block, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.doorTopRightOpen(block, bottom, top);
    }

    @Override
    public final BlockModelBuilder doorTopRightOpen(Block block) {
        return ICrypticModelProvider.super.doorTopRightOpen(block);
    }

    @Override
    public BlockModelBuilder doorTopRightOpen(String name, ResourceLocation bottom, ResourceLocation top) {
        return super.doorTopRightOpen(name, bottom, top).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder trapdoorBottom(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorBottom(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorBottom(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.trapdoorBottom(block);
    }

    @Override
    public final BlockModelBuilder trapdoorBottom(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorBottom(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorBottom(Block block) {
        return ICrypticModelProvider.super.trapdoorBottom(block);
    }

    @Override
    public BlockModelBuilder trapdoorBottom(String name, ResourceLocation texture) {
        return super.trapdoorBottom(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder trapdoorTop(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorTop(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorTop(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.trapdoorTop(block);
    }

    @Override
    public final BlockModelBuilder trapdoorTop(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorTop(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorTop(Block block) {
        return ICrypticModelProvider.super.trapdoorTop(block);
    }

    @Override
    public BlockModelBuilder trapdoorTop(String name, ResourceLocation texture) {
        return super.trapdoorTop(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder trapdoorOpen(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOpen(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorOpen(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.trapdoorOpen(block);
    }

    @Override
    public final BlockModelBuilder trapdoorOpen(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOpen(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorOpen(Block block) {
        return ICrypticModelProvider.super.trapdoorOpen(block);
    }

    @Override
    public BlockModelBuilder trapdoorOpen(String name, ResourceLocation texture) {
        return super.trapdoorOpen(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder trapdoorOrientableBottom(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableBottom(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorOrientableBottom(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.trapdoorOrientableBottom(block);
    }

    @Override
    public final BlockModelBuilder trapdoorOrientableBottom(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableBottom(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorOrientableBottom(Block block) {
        return ICrypticModelProvider.super.trapdoorOrientableBottom(block);
    }

    @Override
    public BlockModelBuilder trapdoorOrientableBottom(String name, ResourceLocation texture) {
        return super.trapdoorOrientableBottom(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder trapdoorOrientableTop(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableTop(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorOrientableTop(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.trapdoorOrientableTop(block);
    }

    @Override
    public final BlockModelBuilder trapdoorOrientableTop(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableTop(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorOrientableTop(Block block) {
        return ICrypticModelProvider.super.trapdoorOrientableTop(block);
    }

    @Override
    public BlockModelBuilder trapdoorOrientableTop(String name, ResourceLocation texture) {
        return super.trapdoorOrientableTop(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder trapdoorOrientableOpen(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableOpen(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorOrientableOpen(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.trapdoorOrientableOpen(block);
    }

    @Override
    public final BlockModelBuilder trapdoorOrientableOpen(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.trapdoorOrientableOpen(block, texture);
    }

    @Override
    public final BlockModelBuilder trapdoorOrientableOpen(Block block) {
        return ICrypticModelProvider.super.trapdoorOrientableOpen(block);
    }

    @Override
    public BlockModelBuilder trapdoorOrientableOpen(String name, ResourceLocation texture) {
        return super.trapdoorOrientableOpen(name, texture).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder torch(Supplier<? extends Block> block, ResourceLocation torch) {
        return ICrypticModelProvider.super.torch(block, torch);
    }

    @Override
    public final BlockModelBuilder torch(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.torch(block);
    }

    @Override
    public final BlockModelBuilder torch(Block block, ResourceLocation torch) {
        return ICrypticModelProvider.super.torch(block, torch);
    }

    @Override
    public final BlockModelBuilder torch(Block block) {
        return ICrypticModelProvider.super.torch(block);
    }

    @Override
    public BlockModelBuilder torch(String name, ResourceLocation torch) {
        return super.torch(name, torch).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder torchWall(Supplier<? extends Block> block, ResourceLocation torch) {
        return ICrypticModelProvider.super.torchWall(block, torch);
    }

    @Override
    public final BlockModelBuilder torchWall(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.torchWall(block);
    }

    @Override
    public final BlockModelBuilder torchWall(Block block, ResourceLocation torch) {
        return ICrypticModelProvider.super.torchWall(block, torch);
    }

    @Override
    public final BlockModelBuilder torchWall(Block block) {
        return ICrypticModelProvider.super.torchWall(block);
    }

    @Override
    public BlockModelBuilder torchWall(String name, ResourceLocation torch) {
        return super.torchWall(name, torch).renderType(RenderType.CUTOUT);
    }


    @Override
    public final BlockModelBuilder carpet(Supplier<? extends Block> block, ResourceLocation wool) {
        return ICrypticModelProvider.super.carpet(block, wool);
    }

    @Override
    public final BlockModelBuilder carpet(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.carpet(block);
    }

    @Override
    public final BlockModelBuilder carpet(Block block, ResourceLocation wool) {
        return ICrypticModelProvider.super.carpet(block, wool);
    }

    @Override
    public final BlockModelBuilder carpet(Block block) {
        return ICrypticModelProvider.super.carpet(block);
    }

    @Override
    public BlockModelBuilder carpet(String name, ResourceLocation wool) {
        return super.carpet(name, wool);
    }


    /*
     * MODEL GENERATION - ADDITIONAL VANILLA
     */

    @Override
    public final BlockModelBuilder leaves(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.leaves(block, texture);
    }

    @Override
    public final BlockModelBuilder leaves(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.leaves(block);
    }

    @Override
    public final BlockModelBuilder leaves(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.leaves(block, texture);
    }

    @Override
    public final BlockModelBuilder leaves(Block block) {
        return ICrypticModelProvider.super.leaves(block);
    }

    @Override
    public BlockModelBuilder leaves(String name, ResourceLocation texture) {
        return ICrypticModelProvider.super.leaves(name, texture);
    }


    @Override
    public final BlockModelBuilder cubeMirroredAll(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.cubeMirroredAll(block);
    }

    @Override
    public final BlockModelBuilder cubeMirroredAll(Supplier<? extends Block> block, ResourceLocation texture) {
        return ICrypticModelProvider.super.cubeMirroredAll(block, texture);
    }

    @Override
    public final BlockModelBuilder cubeMirroredAll(Block block, ResourceLocation texture) {
        return ICrypticModelProvider.super.cubeMirroredAll(block, texture);
    }

    @Override
    public final BlockModelBuilder cubeMirroredAll(Block block) {
        return ICrypticModelProvider.super.cubeMirroredAll(block);
    }

    @Override
    public BlockModelBuilder cubeMirroredAll(String name, ResourceLocation texture) {
        return ICrypticModelProvider.super.cubeMirroredAll(name, texture);
    }


    @Override
    public final BlockModelBuilder cubeMirrored(Supplier<? extends Block> block, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return ICrypticModelProvider.super.cubeMirrored(block, down, up, north, south, east, west);
    }

    @Override
    public final BlockModelBuilder cubeMirrored(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.cubeMirrored(block);
    }

    @Override
    public final BlockModelBuilder cubeMirrored(Block block, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return ICrypticModelProvider.super.cubeMirrored(block, down, up, north, south, east, west);
    }

    @Override
    public final BlockModelBuilder cubeMirrored(Block block) {
        return ICrypticModelProvider.super.cubeMirrored(block);
    }

    @Override
    public BlockModelBuilder cubeMirrored(String name, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return ICrypticModelProvider.super.cubeMirrored(name, down, up, north, south, east, west);
    }


    @Override
    public final BlockModelBuilder cubeFrontSided(Supplier<? extends Block> block, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeFrontSided(block, front, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder cubeFrontSided(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.cubeFrontSided(block);
    }

    @Override
    public final BlockModelBuilder cubeFrontSided(Block block, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeFrontSided(block, front, side, bottom, top);
    }

    @Override
    public final BlockModelBuilder cubeFrontSided(Block block) {
        return ICrypticModelProvider.super.cubeFrontSided(block);
    }

    @Override
    public BlockModelBuilder cubeFrontSided(String name, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return ICrypticModelProvider.super.cubeFrontSided(name, front, side, bottom, top);
    }


    @Override
    public final BlockModelBuilder cubeFrontBackSided(Supplier<? extends Block> block, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, ResourceLocation back) {
        return ICrypticModelProvider.super.cubeFrontBackSided(block, front, side, bottom, top, back);
    }

    @Override
    public final BlockModelBuilder cubeFrontBackSided(Supplier<? extends Block> block) {
        return ICrypticModelProvider.super.cubeFrontBackSided(block);
    }

    @Override
    public final BlockModelBuilder cubeFrontBackSided(Block block, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, ResourceLocation back) {
        return ICrypticModelProvider.super.cubeFrontBackSided(block, front, side, bottom, top, back);
    }

    @Override
    public final BlockModelBuilder cubeFrontBackSided(Block block) {
        return ICrypticModelProvider.super.cubeFrontBackSided(block);
    }

    @Override
    public BlockModelBuilder cubeFrontBackSided(String name, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, ResourceLocation back) {
        return ICrypticModelProvider.super.cubeFrontBackSided(name, front, side, bottom, top, back);
    }
}
