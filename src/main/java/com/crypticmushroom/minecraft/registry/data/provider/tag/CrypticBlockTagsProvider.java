package com.crypticmushroom.minecraft.registry.data.provider.tag;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.TagRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import net.minecraft.core.HolderLookup;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.data.BlockTagsProvider;
import net.minecraftforge.data.event.GatherDataEvent;

/**
 * This provider exists as the block tag implementation of a {@link ICrypticTagsProvider} using an
 * {@link BlockTagsProvider} as the superclass. The easiest way to generate your mod's tags (given you are using the
 * builders properly) is with {@link CrypticRegistry.DataProviders#addTagProviders()}.
 *
 * @author Jonathing
 */
public class CrypticBlockTagsProvider extends BlockTagsProvider implements ICrypticIntrinsicHolderTagsProvider<Block> {
    private final String modId;
    private final GatherDataEvent event;

    public CrypticBlockTagsProvider(String modId, GatherDataEvent event) {
        super(event.getGenerator().getPackOutput(), event.getLookupProvider(), modId, event.getExistingFileHelper());

        this.modId = modId;
        this.event = event;
    }

    @Override
    public TagRegistry<Block> getTagRegistry() {
        return TagRegistry.get(RegistryDirectory.BLOCK);
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public String getName() {
        return ICrypticIntrinsicHolderTagsProvider.super.getName();
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    protected void addTags(HolderLookup.Provider provider) {
        this.addRegisteredTags(provider);
    }
}
