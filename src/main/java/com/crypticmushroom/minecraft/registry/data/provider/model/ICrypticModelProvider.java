package com.crypticmushroom.minecraft.registry.data.provider.model;

import com.crypticmushroom.minecraft.registry.CrypticRegistryInfo;
import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import com.google.common.annotations.Beta;
import net.minecraft.data.models.model.ModelTemplate;
import net.minecraft.data.models.model.ModelTemplates;
import net.minecraft.data.models.model.TextureSlot;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.StringUtil;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.model.generators.BlockModelBuilder;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ModelBuilder;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.client.model.generators.ModelProvider;
import net.minecraftforge.registries.ForgeRegistries;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;
import java.util.function.Supplier;

public sealed interface ICrypticModelProvider<T extends ItemLike, B extends ModelBuilder<B>> extends ICrypticDataProvider permits CrypticBlockModelProvider, CrypticItemModelProvider {
    /* BASE IMPLEMENTATION */

    String getFolder();

    default String getFolder(ItemLike item) {
        return item instanceof Block ? ModelProvider.BLOCK_FOLDER : ModelProvider.ITEM_FOLDER;
    }

    B getBuilder(String path);

    ResourceLocation modLoc(String name);

    ResourceLocation mcLoc(String name);

    ModelFile.ExistingModelFile getExistingFile(ResourceLocation path);


    /* MODEL GENERATION - USING TEMPLATES */

    default B withExistingParent(String name, String parent) {
        return this.withExistingParent(name, this.mcLoc(parent));
    }

    default B withExistingParent(String name, ResourceLocation parent) {
        return this.getBuilder(name).parent(this.getExistingFile(parent));
    }


    @Beta
    default B fromTemplate(Supplier<? extends T> block, ModelTemplate parent, ResourceLocation... texture) {
        return this.fromTemplate(block.get(), parent, texture);
    }

    @Beta
    default B fromTemplate(T block, ModelTemplate parent, ResourceLocation... texture) {
        return this.fromTemplate(this.name(block), parent, texture);
    }

    @Beta
    default B fromTemplate(String name, ModelTemplate parent, ResourceLocation... texture) {
        var model = this.withExistingParent(
            name,
            parent.model.orElseThrow(() -> new IllegalArgumentException("The provided model template must have an ID!"))
        );

        var slots = parent.requiredSlots;
        if (slots.size() != texture.length) {
            throw new IllegalArgumentException("The amount of textures given doesn't match the amount of slots on the model template!");
        }

        int i = 0;
        for (TextureSlot slot : slots) {
            model.texture(slot.getId(), texture[i]);
            i++;
        }

        return model;
    }


    /*
     * MODEL GENERATION - VANILLA
     */

    default B cube(Supplier<? extends T> block, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return this.cube(block.get(), down, up, north, south, east, west);
    }

    default B cube(Supplier<? extends T> block) {
        return this.cube(block.get());
    }

    default B cube(T block, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return this.cube(this.name(block), down, up, north, south, east, west);
    }

    default B cube(T block) {
        return this.cube(
            this.name(block),
            this.texture(block, "down"),
            this.texture(block, "up"),
            this.texture(block, "north"),
            this.texture(block, "south"),
            this.texture(block, "east"),
            this.texture(block, "west")
        );
    }

    B cube(String name, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west);


    default B singleTexture(Supplier<? extends T> block, ResourceLocation parent) {
        return this.singleTexture(block.get(), parent);
    }

    default B singleTexture(Supplier<? extends T> block, ModelTemplate parent) {
        return this.singleTexture(block.get(), parent);
    }

    default B singleTexture(Supplier<? extends T> block, ResourceLocation parent, ResourceLocation texture) {
        return this.singleTexture(block.get(), parent, texture);
    }

    default B singleTexture(Supplier<? extends T> block, ModelTemplate parent, ResourceLocation texture) {
        return this.singleTexture(block.get(), parent, texture);
    }

    default B singleTexture(T block, ResourceLocation parent) {
        return this.singleTexture(this.name(block), parent, this.texture(block));
    }

    default B singleTexture(T block, ModelTemplate parent) {
        return this.singleTexture(
            this.name(block),
            parent.model.orElseThrow(() -> new IllegalArgumentException("The provided model template must have an ID!")),
            this.texture(block)
        );
    }

    default B singleTexture(T block, ResourceLocation parent, ResourceLocation texture) {
        return this.singleTexture(this.name(block), parent, texture);
    }

    default B singleTexture(T block, ModelTemplate parent, ResourceLocation texture) {
        return this.singleTexture(
            this.name(block),
            parent.model.orElseThrow(() -> new IllegalArgumentException("The provided model template must have an ID!")),
            texture
        );
    }

    B singleTexture(String name, ResourceLocation parent, ResourceLocation texture);

    default B singleTexture(Supplier<? extends T> block, ResourceLocation parent, String textureKey) {
        return this.singleTexture(block.get(), parent, textureKey);
    }

    default B singleTexture(Supplier<? extends T> block, ModelTemplate parent, String textureKey) {
        return this.singleTexture(block.get(), parent, textureKey);
    }

    default B singleTexture(Supplier<? extends T> block, ResourceLocation parent, String textureKey, ResourceLocation texture) {
        return this.singleTexture(block.get(), parent, textureKey, texture);
    }

    default B singleTexture(Supplier<? extends T> block, ModelTemplate parent, String textureKey, ResourceLocation texture) {
        return this.singleTexture(block.get(), parent, textureKey, texture);
    }

    default B singleTexture(T block, ResourceLocation parent, String textureKey) {
        return this.singleTexture(this.name(block), parent, textureKey, this.texture(block));
    }

    default B singleTexture(T block, ModelTemplate parent, String textureKey) {
        return this.singleTexture(
            this.name(block),
            parent.model.orElseThrow(() -> new IllegalArgumentException("The provided model template must have an ID!")),
            textureKey,
            this.texture(block)
        );
    }

    default B singleTexture(T block, ResourceLocation parent, String textureKey, ResourceLocation texture) {
        return this.singleTexture(this.name(block), parent, textureKey, texture);
    }

    default B singleTexture(T block, ModelTemplate parent, String textureKey, ResourceLocation texture) {
        return this.singleTexture(
            this.name(block),
            parent.model.orElseThrow(() -> new IllegalArgumentException("The provided model template must have an ID!")),
            textureKey,
            texture
        );
    }

    B singleTexture(String name, ResourceLocation parent, String textureKey, ResourceLocation texture);


    default B cubeAll(Supplier<? extends T> block, ResourceLocation texture) {
        return this.cubeAll(block.get(), texture);
    }

    default B cubeAll(Supplier<? extends T> block) {
        return this.cubeAll(block.get());
    }

    default B cubeAll(T block, ResourceLocation texture) {
        return this.cubeAll(this.name(block), texture);
    }

    default B cubeAll(T block) {
        return this.cubeAll(this.name(block), this.texture(block));
    }

    B cubeAll(String name, ResourceLocation texture);


    default B cubeTop(Supplier<? extends T> block, ResourceLocation side, ResourceLocation top) {
        return this.cubeTop(block.get(), side, top);
    }

    default B cubeTop(Supplier<? extends T> block) {
        return this.cubeTop(block.get());
    }

    default B cubeTop(T block, ResourceLocation side, ResourceLocation top) {
        return this.cubeTop(this.name(block), side, top);
    }

    default B cubeTop(T block) {
        return this.cubeTop(
            this.name(block),
            this.texture(block, "side"),
            this.texture(block, "top")
        );
    }

    B cubeTop(String name, ResourceLocation side, ResourceLocation top);


    default B cubeBottomTop(Supplier<? extends T> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.cubeBottomTop(block.get(), side, bottom, top);
    }

    default B cubeBottomTop(Supplier<? extends T> block) {
        return this.cubeBottomTop(block.get());
    }

    default B cubeBottomTop(T block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.cubeBottomTop(this.name(block), side, bottom, top);
    }

    default B cubeBottomTop(T block) {
        return this.cubeBottomTop(
            this.name(block),
            this.texture(block),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B cubeBottomTop(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top);


    default B cubeColumn(Supplier<? extends T> block, ResourceLocation side, ResourceLocation end) {
        return this.cubeColumn(block.get(), side, end);
    }

    default B cubeColumn(Supplier<? extends T> block) {
        return this.cubeColumn(block.get());
    }

    default B cubeColumn(T block, ResourceLocation side, ResourceLocation end) {
        return this.cubeColumn(this.name(block), side, end);
    }

    default B cubeColumn(T block) {
        return this.cubeColumn(
            this.name(block),
            this.texture(block, "side"),
            this.texture(block, "end")
        );
    }

    B cubeColumn(String name, ResourceLocation side, ResourceLocation end);


    default B cubeColumnHorizontal(Supplier<? extends T> block, ResourceLocation side, ResourceLocation end) {
        return this.cubeColumnHorizontal(block.get(), side, end);
    }

    default B cubeColumnHorizontal(Supplier<? extends T> block) {
        return this.cubeColumnHorizontal(block.get());
    }

    default B cubeColumnHorizontal(T block, ResourceLocation side, ResourceLocation end) {
        return this.cubeColumnHorizontal(this.name(block), side, end);
    }

    default B cubeColumnHorizontal(T block) {
        return this.cubeColumnHorizontal(
            this.name(block),
            this.texture(block, "side"),
            this.texture(block, "end")
        );
    }

    B cubeColumnHorizontal(String name, ResourceLocation side, ResourceLocation end);


    default B orientableVertical(Supplier<? extends T> block, ResourceLocation side, ResourceLocation front) {
        return this.orientableVertical(block.get(), side, front);
    }

    default B orientableVertical(Supplier<? extends T> block) {
        return this.orientableVertical(block.get());
    }

    default B orientableVertical(T block, ResourceLocation side, ResourceLocation front) {
        return this.orientableVertical(this.name(block), side, front);
    }

    default B orientableVertical(T block) {
        return this.orientableVertical(
            this.name(block),
            this.texture(block, "side"),
            this.texture(block, "front")
        );
    }

    B orientableVertical(String name, ResourceLocation side, ResourceLocation front);


    default B orientableWithBottom(Supplier<? extends T> block, ResourceLocation side, ResourceLocation front, ResourceLocation bottom, ResourceLocation top) {
        return this.orientableWithBottom(block.get(), side, front, bottom, top);
    }

    default B orientableWithBottom(Supplier<? extends T> block) {
        return this.orientableWithBottom(block.get());
    }

    default B orientableWithBottom(T block, ResourceLocation side, ResourceLocation front, ResourceLocation bottom, ResourceLocation top) {
        return this.orientableWithBottom(this.name(block), side, front, bottom, top);
    }

    default B orientableWithBottom(T block) {
        return this.orientableWithBottom(
            this.name(block),
            this.texture(block, "side"),
            this.texture(block, "front"),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B orientableWithBottom(String name, ResourceLocation side, ResourceLocation front, ResourceLocation bottom, ResourceLocation top);


    default B orientable(Supplier<? extends T> block, ResourceLocation side, ResourceLocation front, ResourceLocation top) {
        return this.orientable(block.get(), side, front, top);
    }

    default B orientable(Supplier<? extends T> block) {
        return this.orientable(block.get());
    }

    default B orientable(T block, ResourceLocation side, ResourceLocation front, ResourceLocation top) {
        return this.orientable(this.name(block), side, front, top);
    }

    default B orientable(T block) {
        return this.orientable(
            this.name(block),
            this.texture(block, "side"),
            this.texture(block, "front"),
            this.texture(block, "top")
        );
    }

    B orientable(String name, ResourceLocation side, ResourceLocation front, ResourceLocation top);


    default B crop(Supplier<? extends T> block, ResourceLocation texture) {
        return this.crop(block.get(), texture);
    }

    default B crop(Supplier<? extends T> block) {
        return this.crop(block.get());
    }

    default B crop(T block, ResourceLocation texture) {
        return this.crop(this.name(block), texture);
    }

    default B crop(T block) {
        return this.crop(this.name(block), this.texture(block));
    }

    B crop(String name, ResourceLocation texture);


    default B cross(Supplier<? extends T> block, ResourceLocation texture) {
        return this.cross(block.get(), texture);
    }

    default B cross(Supplier<? extends T> block) {
        return this.cross(block.get());
    }

    default B cross(T block, ResourceLocation texture) {
        return this.cross(this.name(block), texture);
    }

    default B cross(T block) {
        return this.cross(this.name(block), this.texture(block));
    }

    B cross(String name, ResourceLocation texture);


    default B stairs(Supplier<? extends T> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.stairs(block.get(), side, bottom, top);
    }

    default B stairs(Supplier<? extends T> block) {
        return this.stairs(block.get());
    }

    default B stairs(T block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.stairs(this.name(block), side, bottom, top);
    }

    default B stairs(T block) {
        return this.stairs(
            this.name(block),
            this.texture(block, "side"),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B stairs(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top);


    default B stairsOuter(Supplier<? extends T> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.stairsOuter(block.get(), side, bottom, top);
    }

    default B stairsOuter(Supplier<? extends T> block) {
        return this.stairsOuter(block.get());
    }

    default B stairsOuter(T block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.stairsOuter(this.name(block), side, bottom, top);
    }

    default B stairsOuter(T block) {
        return this.stairsOuter(
            this.name(block),
            this.texture(block, "side"),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B stairsOuter(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top);


    default B stairsInner(Supplier<? extends T> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.stairsInner(block.get(), side, bottom, top);
    }

    default B stairsInner(Supplier<? extends T> block) {
        return this.stairsInner(block.get());
    }

    default B stairsInner(T block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.stairsInner(this.name(block), side, bottom, top);
    }

    default B stairsInner(T block) {
        return this.stairsInner(
            this.name(block),
            this.texture(block, "side"),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B stairsInner(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top);


    default B slab(Supplier<? extends T> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.slab(block.get(), side, bottom, top);
    }

    default B slab(Supplier<? extends T> block) {
        return this.slab(block.get());
    }

    default B slab(T block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.slab(this.name(block), side, bottom, top);
    }

    default B slab(T block) {
        return this.slab(
            this.name(block),
            this.texture(block, "side"),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B slab(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top);


    default B slabTop(Supplier<? extends T> block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.slabTop(block.get(), side, bottom, top);
    }

    default B slabTop(Supplier<? extends T> block) {
        return this.slabTop(block.get());
    }

    default B slabTop(T block, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.slabTop(this.name(block), side, bottom, top);
    }

    default B slabTop(T block) {
        return this.slabTop(
            this.name(block),
            this.texture(block, "side"),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B slabTop(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top);


    default B button(Supplier<? extends T> block, ResourceLocation texture) {
        return this.button(block.get(), texture);
    }

    default B button(Supplier<? extends T> block) {
        return this.button(block.get());
    }

    default B button(T block, ResourceLocation texture) {
        return this.button(this.name(block), texture);
    }

    default B button(T block) {
        return this.button(this.name(block), this.texture(block));
    }

    B button(String name, ResourceLocation texture);


    default B buttonPressed(Supplier<? extends T> block, ResourceLocation texture) {
        return this.buttonPressed(block.get(), texture);
    }

    default B buttonPressed(Supplier<? extends T> block) {
        return this.buttonPressed(block.get());
    }

    default B buttonPressed(T block, ResourceLocation texture) {
        return this.buttonPressed(this.name(block), texture);
    }

    default B buttonPressed(T block) {
        return this.buttonPressed(this.name(block), this.texture(block));
    }

    B buttonPressed(String name, ResourceLocation texture);


    default B buttonInventory(Supplier<? extends T> block, ResourceLocation texture) {
        return this.buttonInventory(block.get(), texture);
    }

    default B buttonInventory(Supplier<? extends T> block) {
        return this.buttonInventory(block.get());
    }

    default B buttonInventory(T block, ResourceLocation texture) {
        return this.buttonInventory(this.name(block), texture);
    }

    default B buttonInventory(T block) {
        return this.buttonInventory(this.name(block), this.texture(block));
    }

    B buttonInventory(String name, ResourceLocation texture);


    default B pressurePlate(Supplier<? extends T> block, ResourceLocation texture) {
        return this.pressurePlate(block.get(), texture);
    }

    default B pressurePlate(Supplier<? extends T> block) {
        return this.pressurePlate(block.get());
    }

    default B pressurePlate(T block, ResourceLocation texture) {
        return this.pressurePlate(this.name(block), texture);
    }

    default B pressurePlate(T block) {
        return this.pressurePlate(this.name(block), this.texture(block));
    }

    B pressurePlate(String name, ResourceLocation texture);


    default B pressurePlateDown(Supplier<? extends T> block, ResourceLocation texture) {
        return this.pressurePlateDown(block.get(), texture);
    }

    default B pressurePlateDown(Supplier<? extends T> block) {
        return this.pressurePlateDown(block.get());
    }

    default B pressurePlateDown(T block, ResourceLocation texture) {
        return this.pressurePlateDown(this.name(block), texture);
    }

    default B pressurePlateDown(T block) {
        return this.pressurePlateDown(this.name(block), this.texture(block));
    }

    B pressurePlateDown(String name, ResourceLocation texture);


    default B sign(Supplier<? extends T> block, ResourceLocation texture) {
        return this.sign(block.get(), texture);
    }

    default B sign(Supplier<? extends T> block) {
        return this.sign(block.get());
    }

    default B sign(T block, ResourceLocation texture) {
        return this.sign(this.name(block), texture);
    }

    default B sign(T block) {
        return this.sign(this.name(block), this.texture(block));
    }

    B sign(String name, ResourceLocation texture);


    default B fencePost(Supplier<? extends T> block, ResourceLocation texture) {
        return this.fencePost(block.get(), texture);
    }

    default B fencePost(Supplier<? extends T> block) {
        return this.fencePost(block.get());
    }

    default B fencePost(T block, ResourceLocation texture) {
        return this.fencePost(this.name(block), texture);
    }

    default B fencePost(T block) {
        return this.fencePost(this.name(block), this.texture(block));
    }

    B fencePost(String name, ResourceLocation texture);


    default B fenceSide(Supplier<? extends T> block, ResourceLocation texture) {
        return this.fenceSide(block.get(), texture);
    }

    default B fenceSide(Supplier<? extends T> block) {
        return this.fenceSide(block.get());
    }

    default B fenceSide(T block, ResourceLocation texture) {
        return this.fenceSide(this.name(block), texture);
    }

    default B fenceSide(T block) {
        return this.fenceSide(this.name(block), this.texture(block));
    }

    B fenceSide(String name, ResourceLocation texture);


    default B fenceInventory(Supplier<? extends T> block, ResourceLocation texture) {
        return this.fenceInventory(block.get(), texture);
    }

    default B fenceInventory(Supplier<? extends T> block) {
        return this.fenceInventory(block.get());
    }

    default B fenceInventory(T block, ResourceLocation texture) {
        return this.fenceInventory(this.name(block), texture);
    }

    default B fenceInventory(T block) {
        return this.fenceInventory(this.name(block), this.texture(block));
    }

    B fenceInventory(String name, ResourceLocation texture);


    default B fenceGate(Supplier<? extends T> block, ResourceLocation texture) {
        return this.fenceGate(block.get(), texture);
    }

    default B fenceGate(Supplier<? extends T> block) {
        return this.fenceGate(block.get());
    }

    default B fenceGate(T block, ResourceLocation texture) {
        return this.fenceGate(this.name(block), texture);
    }

    default B fenceGate(T block) {
        return this.fenceGate(this.name(block), this.texture(block));
    }

    B fenceGate(String name, ResourceLocation texture);


    default B fenceGateOpen(Supplier<? extends T> block, ResourceLocation texture) {
        return this.fenceGateOpen(block.get(), texture);
    }

    default B fenceGateOpen(Supplier<? extends T> block) {
        return this.fenceGateOpen(block.get());
    }

    default B fenceGateOpen(T block, ResourceLocation texture) {
        return this.fenceGateOpen(this.name(block), texture);
    }

    default B fenceGateOpen(T block) {
        return this.fenceGateOpen(this.name(block), this.texture(block));
    }

    B fenceGateOpen(String name, ResourceLocation texture);


    default B fenceGateWall(Supplier<? extends T> block, ResourceLocation texture) {
        return this.fenceGateWall(block.get(), texture);
    }

    default B fenceGateWall(Supplier<? extends T> block) {
        return this.fenceGateWall(block.get());
    }

    default B fenceGateWall(T block, ResourceLocation texture) {
        return this.fenceGateWall(this.name(block), texture);
    }

    default B fenceGateWall(T block) {
        return this.fenceGateWall(this.name(block), this.texture(block));
    }

    B fenceGateWall(String name, ResourceLocation texture);


    default B fenceGateWallOpen(Supplier<? extends T> block, ResourceLocation texture) {
        return this.fenceGateWallOpen(block.get(), texture);
    }

    default B fenceGateWallOpen(Supplier<? extends T> block) {
        return this.fenceGateWallOpen(block.get());
    }

    default B fenceGateWallOpen(T block, ResourceLocation texture) {
        return this.fenceGateWallOpen(this.name(block), texture);
    }

    default B fenceGateWallOpen(T block) {
        return this.fenceGateWallOpen(this.name(block), this.texture(block));
    }

    B fenceGateWallOpen(String name, ResourceLocation texture);


    default B wallPost(Supplier<? extends T> block, ResourceLocation wall) {
        return this.wallPost(block.get(), wall);
    }

    default B wallPost(Supplier<? extends T> block) {
        return this.wallPost(block.get());
    }

    default B wallPost(T block, ResourceLocation wall) {
        return this.wallPost(this.name(block), wall);
    }

    default B wallPost(T block) {
        return this.wallPost(this.name(block), this.texture(block));
    }

    B wallPost(String name, ResourceLocation wall);


    default B wallSide(Supplier<? extends T> block, ResourceLocation wall) {
        return this.wallSide(block.get(), wall);
    }

    default B wallSide(Supplier<? extends T> block) {
        return this.wallSide(block.get());
    }

    default B wallSide(T block, ResourceLocation wall) {
        return this.wallSide(this.name(block), wall);
    }

    default B wallSide(T block) {
        return this.wallSide(this.name(block), this.texture(block));
    }

    B wallSide(String name, ResourceLocation wall);


    default B wallSideTall(Supplier<? extends T> block, ResourceLocation wall) {
        return this.wallSideTall(block.get(), wall);
    }

    default B wallSideTall(Supplier<? extends T> block) {
        return this.wallSideTall(block.get());
    }

    default B wallSideTall(T block, ResourceLocation wall) {
        return this.wallSideTall(this.name(block), wall);
    }

    default B wallSideTall(T block) {
        return this.wallSideTall(this.name(block), this.texture(block));
    }

    B wallSideTall(String name, ResourceLocation wall);


    default B wallInventory(Supplier<? extends T> block, ResourceLocation wall) {
        return this.wallInventory(block.get(), wall);
    }

    default B wallInventory(Supplier<? extends T> block) {
        return this.wallInventory(block.get());
    }

    default B wallInventory(T block, ResourceLocation wall) {
        return this.wallInventory(this.name(block), wall);
    }

    default B wallInventory(T block) {
        return this.wallInventory(this.name(block), this.texture(block));
    }

    B wallInventory(String name, ResourceLocation wall);


    default B panePost(Supplier<? extends T> block, ResourceLocation pane, ResourceLocation edge) {
        return this.panePost(block.get(), pane, edge);
    }

    default B panePost(Supplier<? extends T> block) {
        return this.panePost(block.get());
    }

    default B panePost(T block, ResourceLocation pane, ResourceLocation edge) {
        return this.panePost(this.name(block), pane, edge);
    }

    default B panePost(T block) {
        return this.panePost(
            this.name(block),
            this.texture(block),
            this.texture(block, "edge")
        );
    }

    B panePost(String name, ResourceLocation pane, ResourceLocation edge);


    default B paneSide(Supplier<? extends T> block, ResourceLocation pane, ResourceLocation edge) {
        return this.paneSide(block.get(), pane, edge);
    }

    default B paneSide(Supplier<? extends T> block) {
        return this.paneSide(block.get());
    }

    default B paneSide(T block, ResourceLocation pane, ResourceLocation edge) {
        return this.paneSide(this.name(block), pane, edge);
    }

    default B paneSide(T block) {
        return this.paneSide(
            this.name(block),
            this.texture(block),
            this.texture(block, "edge")
        );
    }

    B paneSide(String name, ResourceLocation pane, ResourceLocation edge);


    default B paneSideAlt(Supplier<? extends T> block, ResourceLocation pane, ResourceLocation edge) {
        return this.paneSideAlt(block.get(), pane, edge);
    }

    default B paneSideAlt(Supplier<? extends T> block) {
        return this.paneSideAlt(block.get());
    }

    default B paneSideAlt(T block, ResourceLocation pane, ResourceLocation edge) {
        return this.paneSideAlt(this.name(block), pane, edge);
    }

    default B paneSideAlt(T block) {
        return this.paneSideAlt(
            this.name(block),
            this.texture(block),
            this.texture(block, "edge")
        );
    }

    B paneSideAlt(String name, ResourceLocation pane, ResourceLocation edge);


    default B paneNoSide(Supplier<? extends T> block, ResourceLocation pane) {
        return this.paneNoSide(block.get(), pane);
    }

    default B paneNoSide(Supplier<? extends T> block) {
        return this.paneNoSide(block.get());
    }

    default B paneNoSide(T block, ResourceLocation pane) {
        return this.paneNoSide(this.name(block), pane);
    }

    default B paneNoSide(T block) {
        return this.paneNoSide(this.name(block), this.texture(block));
    }

    B paneNoSide(String name, ResourceLocation pane);


    default B paneNoSideAlt(Supplier<? extends T> block, ResourceLocation pane) {
        return this.paneNoSideAlt(block.get(), pane);
    }

    default B paneNoSideAlt(Supplier<? extends T> block) {
        return this.paneNoSideAlt(block.get());
    }

    default B paneNoSideAlt(T block, ResourceLocation pane) {
        return this.paneNoSideAlt(this.name(block), pane);
    }

    default B paneNoSideAlt(T block) {
        return this.paneNoSideAlt(this.name(block), this.texture(block));
    }

    B paneNoSideAlt(String name, ResourceLocation pane);


    default B doorBottomLeft(Supplier<? extends T> block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorBottomLeft(block.get(), bottom, top);
    }

    default B doorBottomLeft(Supplier<? extends T> block) {
        return this.doorBottomLeft(block.get());
    }

    default B doorBottomLeft(T block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorBottomLeft(this.name(block), bottom, top);
    }

    default B doorBottomLeft(T block) {
        return this.doorBottomLeft(
            this.name(block),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B doorBottomLeft(String name, ResourceLocation bottom, ResourceLocation top);


    default B doorBottomLeftOpen(Supplier<? extends T> block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorBottomLeftOpen(block.get(), bottom, top);
    }

    default B doorBottomLeftOpen(Supplier<? extends T> block) {
        return this.doorBottomLeftOpen(block.get());
    }

    default B doorBottomLeftOpen(T block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorBottomLeftOpen(this.name(block), bottom, top);
    }

    default B doorBottomLeftOpen(T block) {
        return this.doorBottomLeftOpen(
            this.name(block),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B doorBottomLeftOpen(String name, ResourceLocation bottom, ResourceLocation top);


    default B doorBottomRight(Supplier<? extends T> block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorBottomRight(block.get(), bottom, top);
    }

    default B doorBottomRight(Supplier<? extends T> block) {
        return this.doorBottomRight(block.get());
    }

    default B doorBottomRight(T block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorBottomRight(this.name(block), bottom, top);
    }

    default B doorBottomRight(T block) {
        return this.doorBottomRight(
            this.name(block),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B doorBottomRight(String name, ResourceLocation bottom, ResourceLocation top);


    default B doorBottomRightOpen(Supplier<? extends T> block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorBottomRightOpen(block.get(), bottom, top);
    }

    default B doorBottomRightOpen(Supplier<? extends T> block) {
        return this.doorBottomRightOpen(block.get());
    }

    default B doorBottomRightOpen(T block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorBottomRightOpen(this.name(block), bottom, top);
    }

    default B doorBottomRightOpen(T block) {
        return this.doorBottomRightOpen(
            this.name(block),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B doorBottomRightOpen(String name, ResourceLocation bottom, ResourceLocation top);


    default B doorTopLeft(Supplier<? extends T> block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorTopLeft(block.get(), bottom, top);
    }

    default B doorTopLeft(Supplier<? extends T> block) {
        return this.doorTopLeft(block.get());
    }

    default B doorTopLeft(T block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorTopLeft(this.name(block), bottom, top);
    }

    default B doorTopLeft(T block) {
        return this.doorTopLeft(
            this.name(block),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B doorTopLeft(String name, ResourceLocation bottom, ResourceLocation top);


    default B doorTopLeftOpen(Supplier<? extends T> block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorTopLeftOpen(block.get(), bottom, top);
    }

    default B doorTopLeftOpen(Supplier<? extends T> block) {
        return this.doorTopLeftOpen(block.get());
    }

    default B doorTopLeftOpen(T block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorTopLeftOpen(this.name(block), bottom, top);
    }

    default B doorTopLeftOpen(T block) {
        return this.doorTopLeftOpen(
            this.name(block),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B doorTopLeftOpen(String name, ResourceLocation bottom, ResourceLocation top);


    default B doorTopRight(Supplier<? extends T> block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorTopRight(block.get(), bottom, top);
    }

    default B doorTopRight(Supplier<? extends T> block) {
        return this.doorTopRight(block.get());
    }

    default B doorTopRight(T block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorTopRight(this.name(block), bottom, top);
    }

    default B doorTopRight(T block) {
        return this.doorTopRight(
            this.name(block),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B doorTopRight(String name, ResourceLocation bottom, ResourceLocation top);


    default B doorTopRightOpen(Supplier<? extends T> block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorTopRightOpen(block.get(), bottom, top);
    }

    default B doorTopRightOpen(Supplier<? extends T> block) {
        return this.doorTopRightOpen(block.get());
    }

    default B doorTopRightOpen(T block, ResourceLocation bottom, ResourceLocation top) {
        return this.doorTopRightOpen(this.name(block), bottom, top);
    }

    default B doorTopRightOpen(T block) {
        return this.doorTopRightOpen(
            this.name(block),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    B doorTopRightOpen(String name, ResourceLocation bottom, ResourceLocation top);


    default B trapdoorBottom(Supplier<? extends T> block, ResourceLocation texture) {
        return this.trapdoorBottom(block.get(), texture);
    }

    default B trapdoorBottom(Supplier<? extends T> block) {
        return this.trapdoorBottom(block.get());
    }

    default B trapdoorBottom(T block, ResourceLocation texture) {
        return this.trapdoorBottom(this.name(block), texture);
    }

    default B trapdoorBottom(T block) {
        return this.trapdoorBottom(this.name(block), this.texture(block));
    }

    B trapdoorBottom(String name, ResourceLocation texture);


    default B trapdoorTop(Supplier<? extends T> block, ResourceLocation texture) {
        return this.trapdoorTop(block.get(), texture);
    }

    default B trapdoorTop(Supplier<? extends T> block) {
        return this.trapdoorTop(block.get());
    }

    default B trapdoorTop(T block, ResourceLocation texture) {
        return this.trapdoorTop(this.name(block), texture);
    }

    default B trapdoorTop(T block) {
        return this.trapdoorTop(this.name(block), this.texture((block)));
    }

    B trapdoorTop(String name, ResourceLocation texture);


    default B trapdoorOpen(Supplier<? extends T> block, ResourceLocation texture) {
        return this.trapdoorOpen(block.get(), texture);
    }

    default B trapdoorOpen(Supplier<? extends T> block) {
        return this.trapdoorOpen(block.get());
    }

    default B trapdoorOpen(T block, ResourceLocation texture) {
        return this.trapdoorOpen(this.name(block), texture);
    }

    default B trapdoorOpen(T block) {
        return this.trapdoorOpen(this.name(block), this.texture(block));
    }

    B trapdoorOpen(String name, ResourceLocation texture);


    default B trapdoorOrientableBottom(Supplier<? extends T> block, ResourceLocation texture) {
        return this.trapdoorOrientableBottom(block.get(), texture);
    }

    default B trapdoorOrientableBottom(Supplier<? extends T> block) {
        return this.trapdoorOrientableBottom(block.get());
    }

    default B trapdoorOrientableBottom(T block, ResourceLocation texture) {
        return this.trapdoorOrientableBottom(this.name(block), texture);
    }

    default B trapdoorOrientableBottom(T block) {
        return this.trapdoorOrientableBottom(this.name(block), this.texture(block));
    }

    B trapdoorOrientableBottom(String name, ResourceLocation texture);


    default B trapdoorOrientableTop(Supplier<? extends T> block, ResourceLocation texture) {
        return this.trapdoorOrientableTop(block.get(), texture);
    }

    default B trapdoorOrientableTop(Supplier<? extends T> block) {
        return this.trapdoorOrientableTop(block.get());
    }

    default B trapdoorOrientableTop(T block, ResourceLocation texture) {
        return this.trapdoorOrientableTop(this.name(block), texture);
    }

    default B trapdoorOrientableTop(T block) {
        return this.trapdoorOrientableTop(this.name(block), this.texture(block));
    }

    B trapdoorOrientableTop(String name, ResourceLocation texture);


    default B trapdoorOrientableOpen(Supplier<? extends T> block, ResourceLocation texture) {
        return this.trapdoorOrientableOpen(block.get(), texture);
    }

    default B trapdoorOrientableOpen(Supplier<? extends T> block) {
        return this.trapdoorOrientableOpen(block.get());
    }

    default B trapdoorOrientableOpen(T block, ResourceLocation texture) {
        return this.trapdoorOrientableOpen(this.name(block), texture);
    }

    default B trapdoorOrientableOpen(T block) {
        return this.trapdoorOrientableOpen(this.name(block), this.texture(block));
    }

    B trapdoorOrientableOpen(String name, ResourceLocation texture);


    default B torch(Supplier<? extends T> block, ResourceLocation torch) {
        return this.torch(block.get(), torch);
    }

    default B torch(Supplier<? extends T> block) {
        return this.torch(block.get());
    }

    default B torch(T block, ResourceLocation torch) {
        return this.torch(this.name(block), torch);
    }

    default B torch(T block) {
        return this.torch(this.name(block), this.texture(block));
    }

    B torch(String name, ResourceLocation torch);


    default B torchWall(Supplier<? extends T> block, ResourceLocation torch) {
        return this.torchWall(block.get(), torch);
    }

    default B torchWall(Supplier<? extends T> block) {
        return this.torchWall(block.get());
    }

    default B torchWall(T block, ResourceLocation torch) {
        return this.torchWall(this.name(block), torch);
    }

    default B torchWall(T block) {
        var id = this.texture(block);
        return this.torchWall(block, new ResourceLocation(id.getNamespace(), id.getPath().replace("_wall", "")));
    }

    B torchWall(String name, ResourceLocation torch);


    default B carpet(Supplier<? extends T> block, ResourceLocation wool) {
        return this.carpet(block.get(), wool);
    }

    default B carpet(Supplier<? extends T> block) {
        return this.carpet(block.get());
    }

    default B carpet(T block, ResourceLocation wool) {
        return this.carpet(this.name(block), wool);
    }

    default B carpet(T block) {
        return this.carpet(this.name(block), this.texture(block));
    }

    B carpet(String name, ResourceLocation wool);


    /*
     * MODEL GENERATION - ADDITIONAL VANILLA
     */

    default B leaves(Supplier<? extends T> block, ResourceLocation texture) {
        return this.leaves(block.get(), texture);
    }

    default B leaves(Supplier<? extends T> block) {
        return this.leaves(block.get());
    }

    default B leaves(T block, ResourceLocation texture) {
        return this.leaves(this.name(block), texture);
    }

    default B leaves(T block) {
        return this.leaves(this.name(block), this.texture(block));
    }

    default B leaves(String name, ResourceLocation texture) {
        return this.fromTemplate(name, ModelTemplates.LEAVES, texture).renderType(RenderType.CUTOUT_MIPPED);
    }


    default B cubeMirroredAll(Supplier<? extends T> block) {
        return this.cubeMirroredAll(block.get());
    }

    default B cubeMirroredAll(Supplier<? extends T> block, ResourceLocation texture) {
        return this.cubeMirroredAll(block.get(), texture);
    }

    default B cubeMirroredAll(T block, ResourceLocation texture) {
        return this.cubeMirroredAll(this.name(block), texture);
    }

    default B cubeMirroredAll(T block) {
        return this.cubeMirroredAll(this.name(block), this.texture(block));
    }

    default B cubeMirroredAll(String name, ResourceLocation texture) {
        return this.fromTemplate(name, ModelTemplates.CUBE_MIRRORED_ALL, texture);
    }


    @Deprecated
    default B cubeMirrored(Supplier<? extends T> block, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return this.cubeMirrored(block.get(), down, up, north, south, east, west);
    }

    @Deprecated
    default B cubeMirrored(Supplier<? extends T> block) {
        return this.cubeMirrored(block.get());
    }

    @Deprecated
    default B cubeMirrored(T block, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return this.cubeMirrored(this.name(block), down, up, north, south, east, west);
    }

    @Deprecated
    default B cubeMirrored(T block) {
        return this.cubeMirrored(
            this.name(block),
            this.texture(block, "down"),
            this.texture(block, "up"),
            this.texture(block, "north"),
            this.texture(block, "south"),
            this.texture(block, "east"),
            this.texture(block, "west")
        );
    }

    @Deprecated
    default B cubeMirrored(String name, ResourceLocation down, ResourceLocation up, ResourceLocation north, ResourceLocation south, ResourceLocation east, ResourceLocation west) {
        return this.withExistingParent(name, this.blockFolder("cube_mirrored", false))
                   .texture("down", down)
                   .texture("up", up)
                   .texture("north", north)
                   .texture("south", south)
                   .texture("east", east)
                   .texture("west", west);
    }


    /*
     * MODEL GENERATION - CRYPTIC REGISTRY ADDITIONS
     */


    default B cubeFrontSided(Supplier<? extends T> block, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.cubeFrontSided(block.get(), front, side, bottom, top);
    }

    default B cubeFrontSided(Supplier<? extends T> block) {
        return this.cubeFrontSided(block.get());
    }

    default B cubeFrontSided(T block, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.cubeFrontSided(this.name(block), front, side, bottom, top);
    }

    default B cubeFrontSided(T block) {
        return this.cubeFrontSided(
            this.name(block),
            this.texture(block, "front"),
            this.texture(block, "side"),
            this.texture(block, "bottom"),
            this.texture(block, "top")
        );
    }

    /**
     * Generates a {@link BlockModelBuilder} of a full block with a texture on the north side, texture on horizontal
     * sides, and separate textures on top and bottom.
     * <pre>
     * {
     *     "parent": "block/cube",
     *     "textures": {
     *         "down": "{bottom}",
     *         "up": "{top}"
     *         "north": "{front}",
     *         "south": "{side}",
     *         "east": "{side}",
     *         "west": "{side}"
     *     }
     * }
     * </pre>
     *
     * @param name   The name to use in the model.
     * @param front  The front (north) texture to use in the model.
     * @param side   The side (east, south, west) texture to use in the model.
     * @param top    The top (up) texture to use in the model.
     * @param bottom The bottom (down) texture to use in the model.
     * @return A {@code block/cube} model file for the data generator.
     */
    default B cubeFrontSided(String name, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top) {
        return this.cube(name, bottom, top, front, side, side, side).texture("particle", side);
    }


    default B cubeFrontBackSided(Supplier<? extends T> block, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, ResourceLocation back) {
        return this.cubeFrontBackSided(block.get(), front, side, bottom, top, back);
    }

    default B cubeFrontBackSided(Supplier<? extends T> block) {
        return this.cubeFrontBackSided(block.get());
    }

    default B cubeFrontBackSided(T block, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, ResourceLocation back) {
        return this.cubeFrontBackSided(this.name(block), front, side, bottom, top, back);
    }

    default B cubeFrontBackSided(T block) {
        return this.cubeFrontBackSided(
            this.name(block),
            this.texture(block, "front"),
            this.texture(block, "side"),
            this.texture(block, "bottom"),
            this.texture(block, "top"),
            this.texture(block, "back")
        );
    }

    /**
     * Generates a {@link BlockModelBuilder} of a full block with a texture on the north side, a texture on the south
     * side, a shared texture on east and west side, and separate textures on top and bottom.
     * <pre>
     * {
     *     "parent": "block/cube",
     *     "textures": {
     *         "down": "{bottom}",
     *         "up": "{top}",
     *         "north": "{front}",
     *         "south": "{back}",
     *         "east": "{side}",
     *         "west": "{side}"
     *     }
     * }
     * </pre>
     *
     * @param name   The name to use in the model.
     * @param front  The front (north) texture to use in the model.
     * @param back   The back (south) texture to use in the model.
     * @param side   The side (east, west) texture to use in the model.
     * @param top    The top (up) texture to use in the model.
     * @param bottom The bottom (down) texture to use in the model.
     * @return A {@code block/cube} model file for the data generator.
     */
    default B cubeFrontBackSided(String name, ResourceLocation front, ResourceLocation side, ResourceLocation bottom, ResourceLocation top, ResourceLocation back) {
        return this.cube(name, bottom, top, front, back, side, side).texture("particle", side);
    }


    /*
     * HELPER METHODS
     */

    default ResourceLocation key(Supplier<? extends ItemLike> itemLike) {
        return this.key(itemLike.get());
    }

    default ResourceLocation key(ItemLike itemLike) {
        return itemLike instanceof Block block
               ? ForgeRegistries.BLOCKS.getKey(block)
               : ForgeRegistries.ITEMS.getKey(itemLike.asItem());
    }

    /**
     * Gets the name of a {@link Block} for use in data generation, removed a given suffix, and appends a given
     * {@link String}.
     *
     * @param block        The block to get the name of.
     * @param append       The string to append to the end of the name.
     * @param removeSuffix The suffix to remove from the original block name.
     * @return The name of the {@link Block} to be used.
     *
     * @see #name(ItemLike, String)
     * @see #name(ItemLike)
     */
    default String name(Supplier<? extends ItemLike> block, @Nullable String append, String removeSuffix) {
        return this.name(block.get(), append, removeSuffix);
    }

    /**
     * Gets the name of a {@link Block} for use in data generation and appends a given {@link String} to the end of it.
     *
     * @param block  The block to get the name of.
     * @param append The string to append to the end of the name.
     * @return The name of the {@link Block} to be used.
     *
     * @see #name(ItemLike)
     */
    default String name(Supplier<? extends ItemLike> block, @Nullable String append) {
        return this.name(block.get(), append);
    }

    /**
     * Gets the name of a {@link Block} for use in data generation.
     *
     * @param block The block to get the name of.
     * @return The name of the {@link Block} to be used.
     *
     * @see #name(ItemLike)
     */
    default String name(Supplier<? extends ItemLike> block) {
        return this.name(block.get());
    }

    /**
     * Gets the name of a {@link Block} for use in data generation, removed a given suffix, and appends a given
     * {@link String}.
     *
     * @param block        The block to get the name of.
     * @param append       The string to append to the end of the name.
     * @param removeSuffix The suffix to remove from the original block name.
     * @return The name of the {@link Block} to be used.
     *
     * @see #name(ItemLike, String)
     * @see #name(ItemLike)
     */
    default String name(ItemLike block, @Nullable String append, String removeSuffix) {
        String name = this.name(block);

        removeSuffix = "_" + removeSuffix;
        if (name.endsWith(removeSuffix)) {
            name = name.substring(0, name.length() - removeSuffix.length());
        }

        return append(name, append);
    }

    /**
     * Gets the name of a {@link Block} for use in data generation and appends a given {@link String} to the end of it.
     *
     * @param block  The block to get the name of.
     * @param append The string to append to the end of the name.
     * @return The name of the {@link Block} to be used.
     *
     * @see #name(ItemLike)
     */
    default String name(ItemLike block, @Nullable String append) {
        return append(this.name(block), append);
    }

    /**
     * Gets the name of a {@link Block} for use in data generation.
     *
     * @param block The block to get the name of.
     * @return The name of the {@link Block} to be used.
     *
     * @see BlockStateProvider#name(ItemLike)
     */
    @SuppressWarnings("JavadocReference")
    default String name(ItemLike block) {
        return this.key(block).getPath();
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Supplier} of a block to use as a texture, removes a given suffix,
     * and appends a given {@link String}.
     *
     * @param block        The block to get the {@link ResourceLocation} of.
     * @param append       The string to append to the end of the new {@link ResourceLocation}.
     * @param removeSuffix The suffix to remove from the original {@link ResourceLocation} of the block.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see #texture(ItemLike, String, String)
     */
    default ResourceLocation texture(Supplier<? extends ItemLike> block, @Nullable String append, String removeSuffix) {
        return this.texture(block.get(), append, removeSuffix);
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Supplier} of a block to use as a texture and appends a given
     * {@link String} to the end of it.
     *
     * @param block  The block to get the {@link ResourceLocation} of.
     * @param append The string to append to the end of the new {@link ResourceLocation}.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see #texture(ItemLike, String)
     */
    default ResourceLocation texture(Supplier<? extends ItemLike> block, @Nullable String append) {
        return this.texture(block.get(), append);
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Supplier} of a block to use as a texture.
     *
     * @param block The block to get the {@link ResourceLocation} of.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see BlockStateProvider#blockTexture(Block)
     */
    default ResourceLocation texture(Supplier<? extends ItemLike> block) {
        return this.texture(block.get());
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Block} to use as a texture, removes a given suffix, and appends a
     * given {@link String}.
     *
     * @param block        The block to get the {@link ResourceLocation} of.
     * @param append       The string to append to the end of the new {@link ResourceLocation}.
     * @param removeSuffix The suffix to remove from the original {@link ResourceLocation} of the block.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see #texture(ItemLike, String)
     * @see BlockStateProvider#blockTexture(Block)
     */
    default ResourceLocation texture(ItemLike block, @Nullable String append, String removeSuffix) {
        String texture = this.texture(block).toString();

        removeSuffix = "_" + removeSuffix;
        if (texture.endsWith(removeSuffix)) {
            texture = texture.substring(0, texture.length() - removeSuffix.length());
        }

        return new ResourceLocation(append(texture, append));
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Block} to use as a texture and appends a given {@link String} to
     * the end of it.
     *
     * @param block  The block to get the {@link ResourceLocation} of.
     * @param append The string to append to the end of the new {@link ResourceLocation}.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see #texture(ItemLike)
     */
    default ResourceLocation texture(ItemLike block, @Nullable String append) {
        return new ResourceLocation(append(this.texture(block).toString(), append));
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Block} to use as a texture.
     *
     * @param block The block to get the {@link ResourceLocation} of.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see BlockStateProvider#blockTexture(Block)
     */
    default ResourceLocation texture(ItemLike block) {
        var name = this.key(block);
        return new ResourceLocation(name.getNamespace(), this.getFolder(block) + "/" + name.getPath());
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Supplier} of a block to use as a texture, removes a given suffix,
     * and appends a given {@link String}.
     *
     * @param block        The block to get the {@link ResourceLocation} of.
     * @param append       The string to append to the end of the new {@link ResourceLocation}.
     * @param removeSuffix The suffix to remove from the original {@link ResourceLocation} of the block.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see #texture(ItemLike, String, String)
     */
    default ResourceLocation itemTexture(Supplier<? extends ItemLike> block, @Nullable String append, String removeSuffix) {
        return this.itemTexture(block.get(), append, removeSuffix);
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Supplier} of a block to use as a texture and appends a given
     * {@link String} to the end of it.
     *
     * @param block  The block to get the {@link ResourceLocation} of.
     * @param append The string to append to the end of the new {@link ResourceLocation}.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see #texture(ItemLike, String)
     */
    default ResourceLocation itemTexture(Supplier<? extends ItemLike> block, @Nullable String append) {
        return this.itemTexture(block.get(), append);
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Supplier} of a block to use as a texture.
     *
     * @param block The block to get the {@link ResourceLocation} of.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see BlockStateProvider#blockTexture(Block)
     */
    default ResourceLocation itemTexture(Supplier<? extends ItemLike> block) {
        return this.itemTexture(block.get());
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Block} to use as a texture, removes a given suffix, and appends a
     * given {@link String}.
     *
     * @param block        The block to get the {@link ResourceLocation} of.
     * @param append       The string to append to the end of the new {@link ResourceLocation}.
     * @param removeSuffix The suffix to remove from the original {@link ResourceLocation} of the block.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see #texture(ItemLike, String)
     * @see BlockStateProvider#blockTexture(Block)
     */
    default ResourceLocation itemTexture(ItemLike block, @Nullable String append, String removeSuffix) {
        String texture = this.itemTexture(block).toString();

        removeSuffix = "_" + removeSuffix;
        if (texture.endsWith(removeSuffix)) {
            texture = texture.substring(0, texture.length() - removeSuffix.length());
        }

        return new ResourceLocation(append(texture, append));
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Block} to use as a texture and appends a given {@link String} to
     * the end of it.
     *
     * @param block  The block to get the {@link ResourceLocation} of.
     * @param append The string to append to the end of the new {@link ResourceLocation}.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see #texture(ItemLike)
     */
    default ResourceLocation itemTexture(ItemLike block, @Nullable String append) {
        return new ResourceLocation(append(this.itemTexture(block).toString(), append));
    }

    /**
     * Gets the {@link ResourceLocation} for a {@link Block} to use as a texture.
     *
     * @param block The block to get the {@link ResourceLocation} of.
     * @return The new {@link ResourceLocation} to be used as a texture reference.
     *
     * @see BlockStateProvider#blockTexture(Block)
     */
    default ResourceLocation itemTexture(ItemLike block) {
        var name = this.key(block);
        return new ResourceLocation(name.getNamespace(), ModelProvider.ITEM_FOLDER + "/" + name.getPath());
    }

    default ResourceLocation folder(String name, boolean mod) {
        Function<String, ResourceLocation> locator = mod ? this::modLoc : this::mcLoc;
        return locator.apply(String.format("%s/%s", this.getFolder(), name));
    }

    default ResourceLocation folder(ResourceLocation name) {
        return new ResourceLocation(name.getNamespace(), String.format("%s/%s", this.getFolder(), name.getPath()));
    }

    default ResourceLocation blockFolder(String name, boolean mod) {
        Function<String, ResourceLocation> locator = mod ? this::modLoc : this::mcLoc;
        return locator.apply(String.format("%s/%s", ModelProvider.BLOCK_FOLDER, name));
    }

    default ResourceLocation blockFolder(ResourceLocation name) {
        return new ResourceLocation(name.getNamespace(), String.format("%s/%s", ModelProvider.BLOCK_FOLDER, name.getPath()));
    }

    default ResourceLocation itemFolder(String name, boolean mod) {
        Function<String, ResourceLocation> locator = mod ? this::modLoc : this::mcLoc;
        return locator.apply(String.format("%s/%s", ModelProvider.ITEM_FOLDER, name));
    }

    default ResourceLocation itemFolder(ResourceLocation name) {
        return new ResourceLocation(name.getNamespace(), String.format("%s/%s", ModelProvider.ITEM_FOLDER, name.getPath()));
    }

    static String append(String first, @Nullable String second) {
        return second != null && !second.isEmpty() ? String.format("%s_%s", first, second) : first;
    }

    final class RenderType extends ResourceLocation {
        public static final RenderType NONE = new RenderType(CrypticRegistryInfo.MOD_ID, "none");
        public static final RenderType DEFAULT = new RenderType(CrypticRegistryInfo.MOD_ID, "default");

        public static final RenderType SOLID = of("solid");
        public static final RenderType CUTOUT = of("cutout");
        public static final RenderType CUTOUT_MIPPED = of("cutout_mipped");
        public static final RenderType CUTOUT_MIPPED_ALL = of("cutout_mipped_all");
        public static final RenderType TRANSLUCENT = of("translucent");
        public static final RenderType TRIPWIRE = of("tripwire");

        private RenderType(String id) {
            super(id);
            validateId(this.getNamespace(), this.getPath());
        }

        private RenderType(String namespace, String path) {
            super(namespace, path);
        }

        private static void validateId(String namespace, String path) {
            if (namespace.equals(CrypticRegistryInfo.MOD_ID) && (path.equals("none") || path.equals("default"))) {
                throw new IllegalArgumentException("ResourceLocation '%s:%s' is reserved by Cryptic Registry and cannot be used to create custom RenderType objects.".formatted(namespace, path));
            }
        }

        private static RenderType of(String namespace, String path) {
            validateId(namespace, path);
            return new RenderType(namespace, path);
        }

        @Deprecated
        public static RenderType of(String id) {
            if (StringUtil.isNullOrEmpty(id)) {
                throw new IllegalArgumentException("RenderType objects cannot have a null or empty ID");
            }

            return new RenderType(id);
        }

        public static RenderType of(ResourceLocation id) {
            if (id == null || StringUtil.isNullOrEmpty(id.getNamespace()) || StringUtil.isNullOrEmpty(id.getPath())) {
                throw new IllegalArgumentException("RenderType objects cannot have a null or empty ID");
            }

            return new RenderType(id.getNamespace(), id.getPath());
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof RenderType o)) return false;

            if (o == DEFAULT) return this == DEFAULT;
            if (o == NONE) return this == NONE;

            return super.equals(o);
        }
    }
}
