package com.crypticmushroom.minecraft.registry.data.provider.sound;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.builder.minecraft.SoundEventBuilder;
import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import net.minecraftforge.common.data.SoundDefinitionsProvider;
import net.minecraftforge.data.event.GatherDataEvent;

/**
 * The cryptic sounds provider ties with the {@link DataRegistry} for storing sound definitions created by a
 * {@link SoundEventBuilder} and dumps them here. The easiest way of using this provider is with
 * {@link CrypticRegistry.DataProviders#addSoundDefinitionsProvider()}.
 *
 * @author Jonathing
 */
public class CrypticSoundsDefinitionsProvider extends SoundDefinitionsProvider implements ICrypticDataProvider {
    private final String modId;
    private final GatherDataEvent event;

    public CrypticSoundsDefinitionsProvider(String modId, GatherDataEvent event) {
        super(event.getGenerator().getPackOutput(), modId, event.getExistingFileHelper());

        this.modId = modId;
        this.event = event;
    }

    @Override
    public void registerSounds() {
        DataRegistry.forSoundDefinitions(this.modId, this::add);
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public String getName() {
        return ICrypticDataProvider.super.getName();
    }

    @Override
    public String getSimpleName() {
        return "Sound Definitions";
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public boolean shouldRun() {
        return this.event.includeServer();
    }
}
