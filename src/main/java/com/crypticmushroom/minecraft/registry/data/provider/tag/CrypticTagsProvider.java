package com.crypticmushroom.minecraft.registry.data.provider.tag;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.TagRegistry;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraftforge.data.event.GatherDataEvent;

import java.util.concurrent.CompletableFuture;

/**
 * This provider exists as the default implementation of a {@link ICrypticTagsProvider} using a {@link TagsProvider} as
 * the superclass. The easiest way to generate your mod's tags (given you are using the builders properly) is with
 * {@link CrypticRegistry.DataProviders#addTagProviders()}.
 *
 * @param <T> The type of the tag to be generated.
 * @author Jonathing
 */
public class CrypticTagsProvider<T> extends TagsProvider<T> implements ICrypticTagsProvider<T> {
    private final String modId;
    private final GatherDataEvent event;
    private final TagRegistry<T> tagRegistry;

    public CrypticTagsProvider(String modId, GatherDataEvent event, TagRegistry<T> tagRegistry) {
        super(event.getGenerator().getPackOutput(), tagRegistry.registryInfo.getKey(), event.getLookupProvider(), modId, event.getExistingFileHelper());

        this.modId = modId;
        this.event = event;
        this.tagRegistry = tagRegistry;
    }

    public CrypticTagsProvider(String modId, GatherDataEvent event, TagRegistry<T> tagRegistry, CompletableFuture<HolderLookup.Provider> lookupProvider) {
        super(event.getGenerator().getPackOutput(), tagRegistry.registryInfo.getKey(), lookupProvider, modId, event.getExistingFileHelper());

        this.modId = modId;
        this.event = event;
        this.tagRegistry = tagRegistry;
    }

    @Override
    protected void addTags(HolderLookup.Provider provider) {
        this.addRegisteredTags(provider);
    }

    @Override
    public TagRegistry<T> getTagRegistry() {
        return this.tagRegistry;
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public String getName() {
        return ICrypticTagsProvider.super.getName();
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    /**
     * @param tag The tag to add to.
     * @return A tag appender for the data provider.
     *
     * @see TagRegistry#register(String, TagKey, ResourceKey[])
     * @deprecated Do not manually register tags in this class! Instead, use one of the
     *     {@link TagRegistry tag registries}
     */
    @Deprecated
    @Override
    public TagAppender<T> tag(TagKey<T> tag) {
        return super.tag(tag);
    }
}
