package com.crypticmushroom.minecraft.registry.data.provider.loot;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import net.minecraft.advancements.critereon.DamageSourcePredicate;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.EntityTypePredicate;
import net.minecraft.data.loot.EntityLootSubProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.animal.FrogVariant;
import net.minecraft.world.flag.FeatureFlagSet;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.predicates.DamageSourceCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.registries.RegistryObject;

import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class CrypticEntityLootSubProvider extends EntityLootSubProvider {
    public static final EntityPredicate.Builder ENTITY_ON_FIRE = EntityLootSubProvider.ENTITY_ON_FIRE;

    private final String modId;

    public CrypticEntityLootSubProvider(String modId) {
        this(modId, FeatureFlags.REGISTRY.allFlags());
    }

    public CrypticEntityLootSubProvider(String modId, FeatureFlagSet enabledFeatures) {
        super(enabledFeatures);
        this.modId = modId;
    }

    public CrypticEntityLootSubProvider(String modId, FeatureFlagSet allowed, FeatureFlagSet required) {
        super(allowed, required);
        this.modId = modId;
    }

    @Override
    public void generate() {
        DataRegistry.applyLootBuilders(this.modId, this);
    }


    /* IMPLEMENTATION */

    @Override
    protected Stream<EntityType<?>> getKnownEntityTypes() {
        return Objects.requireNonNull(CrypticRegistry.getRegister(this.modId, RegistryDirectory.ENTITY_TYPE), "CrypticBlockLootSubProvider cannot run without a block register created with Cryptic Registry").getEntries().stream().map(RegistryObject::get);
    }


    /* GENERIC LOOT TABLES */

    public void add(Supplier<? extends EntityType<?>> entityType, LootTable.Builder builder) {
        this.add(entityType.get(), builder);
    }

    public void add(Supplier<? extends EntityType<?>> entityType, ResourceLocation lootTableLocation, LootTable.Builder builder) {
        this.add(entityType.get(), lootTableLocation, builder);
    }

    @Override
    public void add(EntityType<?> entityType, LootTable.Builder builder) {
        super.add(entityType, builder);
    }

    @Override
    public void add(EntityType<?> entityType, ResourceLocation lootTableLocation, LootTable.Builder builder) {
        super.add(entityType, lootTableLocation, builder);
    }

    @Override
    @Deprecated
    public boolean canHaveLootTable(EntityType<?> entityType) {
        return super.canHaveLootTable(entityType);
    }


    /* VANILLA LOOT TABLES */

    public LootTable.Builder createSheepTable(Supplier<? extends ItemLike> wool) {
        return createSheepTable(wool.get());
    }

    public static LootTable.Builder createSheepTable(ItemLike woolItem) {
        return EntityLootSubProvider.createSheepTable(woolItem);
    }

    @Override
    public LootItemCondition.Builder killedByFrog() {
        return super.killedByFrog();
    }

    @Override
    public LootItemCondition.Builder killedByFrogVariant(FrogVariant frogVariant) {
        return super.killedByFrogVariant(frogVariant);
    }

    public LootItemCondition.Builder killedByFrogVariant(Supplier<? extends FrogVariant> frogVariant) {
        return this.killedByFrogVariant(frogVariant.get());
    }


    /* CRYPTIC REGISTRY ADDITIONS */

    public LootItemCondition.Builder killedByEntityType(Supplier<? extends EntityType<?>> entityType) {
        return this.killedByEntityType(entityType.get());
    }

    public LootItemCondition.Builder killedByEntityType(EntityType<?> entityType) {
        return DamageSourceCondition.hasDamageSource(DamageSourcePredicate.Builder.damageType().source(EntityPredicate.Builder.entity().of(entityType)));
    }

    public LootItemCondition.Builder killedByEntityType(TagKey<EntityType<?>> entityType) {
        return DamageSourceCondition.hasDamageSource(DamageSourcePredicate.Builder.damageType().source(EntityPredicate.Builder.entity().of(entityType)));
    }

    public LootItemCondition.Builder killedByEntityType(EntityTypePredicate entityType) {
        return DamageSourceCondition.hasDamageSource(DamageSourcePredicate.Builder.damageType().source(EntityPredicate.Builder.entity().entityType(entityType)));
    }
}
