package com.crypticmushroom.minecraft.registry.data.tag;

import com.crypticmushroom.minecraft.registry.data.registry.TagRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import net.minecraft.world.item.Item;

public abstract class CrypticItemTagClass extends CrypticTagClass<Item> {
    /** @see CrypticTagClass#CrypticTagClass(String, TagRegistry) */
    public CrypticItemTagClass(String modId) {
        super(modId, TagRegistry.get(RegistryDirectory.ITEM));
    }

    @Override
    public void dataRegisterTags() {
        super.dataRegisterTags();
        ((TagRegistry.Item) this.registry).register(this.modId, this.getClass());
    }
}
