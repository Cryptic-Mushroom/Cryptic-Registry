package com.crypticmushroom.minecraft.registry.data.provider.loot;

import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.data.event.GatherDataEvent;

import java.util.List;
import java.util.Set;

public class CrypticLootTableProvider extends LootTableProvider implements ICrypticDataProvider {
    private final String modId;
    private final GatherDataEvent event;

    public CrypticLootTableProvider(String modId, GatherDataEvent event, Set<ResourceLocation> requiredTables, List<SubProviderEntry> subProviders) {
        super(event.getGenerator().getPackOutput(), requiredTables, subProviders);
        this.modId = modId;
        this.event = event;
    }

    @Override
    public String getName() {
        return ICrypticDataProvider.super.getName();
    }

    @Override
    public String getSimpleName() {
        return "Loot Tables";
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public boolean shouldRun() {
        return this.event.includeServer();
    }
}
