package com.crypticmushroom.minecraft.registry.data.provider.tag;

import net.minecraft.data.tags.IntrinsicHolderTagsProvider;
import net.minecraft.tags.TagKey;

public interface ICrypticIntrinsicHolderTagsProvider<T> extends ICrypticTagsProvider<T> {
    @SuppressWarnings("unchecked")
    private IntrinsicHolderTagsProvider<T> self() {
        try {
            return (IntrinsicHolderTagsProvider<T>) this;
        } catch (ClassCastException e) {
            throw new IllegalCallerException("ICrypticIntrinsicHolderTagProvider can only be applied onto a subtype of IntrinsicHolderTagsProvider", e);
        }
    }

    /**
     * This method must add the given object to the given tag.
     *
     * @param tag    The tag.
     * @param object The object to add to the tag.
     */
    default void add(TagKey<T> tag, T object) {
        this.self().tag(tag).add(object);
    }
}
