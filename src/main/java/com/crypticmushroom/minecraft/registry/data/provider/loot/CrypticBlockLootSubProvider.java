package com.crypticmushroom.minecraft.registry.data.provider.loot;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import net.minecraft.advancements.critereon.BlockPredicate;
import net.minecraft.advancements.critereon.LocationPredicate;
import net.minecraft.advancements.critereon.StatePropertiesPredicate;
import net.minecraft.core.BlockPos;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.data.loot.packs.VanillaBlockLoot;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.flag.FeatureFlagSet;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.AbstractBannerBlock;
import net.minecraft.world.level.block.AbstractCandleBlock;
import net.minecraft.world.level.block.BeehiveBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CaveVines;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.DoublePlantBlock;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.PinkPetalsBlock;
import net.minecraft.world.level.block.ShulkerBoxBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StemBlock;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.block.state.properties.Property;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.LootPoolEntryContainer;
import net.minecraft.world.level.storage.loot.functions.ApplyBonusCount;
import net.minecraft.world.level.storage.loot.functions.FunctionUserBuilder;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.predicates.BonusLevelTableCondition;
import net.minecraft.world.level.storage.loot.predicates.ConditionUserBuilder;
import net.minecraft.world.level.storage.loot.predicates.LocationCheck;
import net.minecraft.world.level.storage.loot.predicates.LootItemBlockStatePropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemRandomChanceCondition;
import net.minecraft.world.level.storage.loot.providers.number.BinomialDistributionGenerator;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.NumberProvider;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import net.minecraftforge.registries.RegistryObject;

import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// TODO add methods that call to super for suppliers
public class CrypticBlockLootSubProvider extends BlockLootSubProvider {
    public static final LootItemCondition.Builder HAS_SILK_TOUCH = BlockLootSubProvider.HAS_SILK_TOUCH;
    public static final LootItemCondition.Builder HAS_NO_SILK_TOUCH = BlockLootSubProvider.HAS_NO_SILK_TOUCH;
    public static final LootItemCondition.Builder HAS_SHEARS = BlockLootSubProvider.HAS_SHEARS;
    public static final LootItemCondition.Builder HAS_SHEARS_OR_SILK_TOUCH = BlockLootSubProvider.HAS_SHEARS_OR_SILK_TOUCH;
    public static final LootItemCondition.Builder HAS_NO_SHEARS_OR_SILK_TOUCH = BlockLootSubProvider.HAS_NO_SHEARS_OR_SILK_TOUCH;
    public static final float[] NORMAL_LEAVES_SAPLING_CHANCES = BlockLootSubProvider.NORMAL_LEAVES_SAPLING_CHANCES;
    public static final float[] JUNGLE_LEAVES_SAPLING_CHANGES = VanillaBlockLoot.JUNGLE_LEAVES_SAPLING_CHANGES;
    public static final float[] NORMAL_LEAVES_STICK_CHANCES = BlockLootSubProvider.NORMAL_LEAVES_STICK_CHANCES;

    private final String modId;

    public CrypticBlockLootSubProvider(String modId) {
        this(modId, FeatureFlags.REGISTRY.allFlags());
    }

    public CrypticBlockLootSubProvider(String modId, FeatureFlagSet enabledFeatures) {
        super(DataRegistry.getExplosionResistant(), enabledFeatures);
        this.modId = modId;
    }

    @Deprecated
    public CrypticBlockLootSubProvider(String modId, Set<Item> explosionResistant) {
        this(modId, explosionResistant, FeatureFlags.REGISTRY.allFlags());
    }

    @Deprecated
    public CrypticBlockLootSubProvider(String modId, Set<Item> explosionResistant, FeatureFlagSet enabledFeatures) {
        super(Stream.concat(DataRegistry.getExplosionResistant().stream(), explosionResistant.stream()).collect(Collectors.toSet()), enabledFeatures);
        this.modId = modId;
    }

    @Override
    public void generate() {
        DataRegistry.applyLootBuilders(this.modId, this);
    }


    /* IMPLEMENTATION */

    @Override
    protected Iterable<Block> getKnownBlocks() {
        return () -> Objects.requireNonNull(CrypticRegistry.getRegister(this.modId, RegistryDirectory.BLOCK), "CrypticBlockLootSubProvider cannot run without a block register created with Cryptic Registry").getEntries().stream().map(RegistryObject::get).iterator();
    }

    protected static float[] numToFloatArray(Number... numbers) {
        float[] floats = new float[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            floats[i] = numbers[i].floatValue();
        }
        return floats;
    }


    /* VANILLA LOOT TABLES */

    public void addNetherVinesDropTable(Supplier<? extends Block> vines, Supplier<? extends Block> plant) {
        this.addNetherVinesDropTable(vines.get(), plant.get());
    }

    @Override
    public void addNetherVinesDropTable(Block vines, Block plant) {
        super.addNetherVinesDropTable(vines, plant);
    }

    public void dropPottedContents(Supplier<? extends FlowerPotBlock> flowerPot) {
        this.dropPottedContents(flowerPot.get());
    }

    public void dropPottedContents(FlowerPotBlock flowerPot) {
        super.dropPottedContents(flowerPot);
    }

    @Override
    public void dropPottedContents(Block flowerPot) {
        super.dropPottedContents(flowerPot);
    }

    public void otherWhenSilkTouch(Supplier<? extends Block> block, Supplier<? extends Block> drop) {
        this.otherWhenSilkTouch(block.get(), drop.get());
    }

    @Override
    public void otherWhenSilkTouch(Block block, Block other) {
        super.otherWhenSilkTouch(block, other);
    }

    public void dropOther(Supplier<? extends Block> block, Supplier<? extends ItemLike> drop) {
        this.dropOther(block.get(), drop.get());
    }

    @Override
    public void dropOther(Block block, ItemLike item) {
        super.dropOther(block, item);
    }

    public void dropWhenSilkTouch(Supplier<? extends Block> block) {
        this.dropWhenSilkTouch(block.get());
    }

    @Override
    public void dropWhenSilkTouch(Block block) {
        super.dropWhenSilkTouch(block);
    }

    public void dropSelf(Supplier<? extends Block> block) {
        this.dropSelf(block.get());
    }

    @Override
    public void dropSelf(Block block) {
        super.dropSelf(block);
    }

    public void dropNothing(Supplier<? extends Block> block) {
        this.add(block, CrypticBlockLootSubProvider::noDrop);
    }

    public <B extends Block> void add(Supplier<B> block, Function<Supplier<B>, LootTable.Builder> builder) {
        this.add(block.get(), builder.apply(block));
    }

    public void add(Supplier<? extends Block> block, Supplier<LootTable.Builder> builder) {
        this.add(block, b -> builder.get());
    }

    @Override
    public void add(Block block, Function<Block, LootTable.Builder> factory) {
        super.add(block, factory);
    }

    public void add(Supplier<? extends Block> block, LootTable.Builder builder) {
        this.add(block.get(), builder);
    }

    @Override
    public void add(Block block, LootTable.Builder builder) {
        super.add(block, builder);
    }


    /* VANILLA LOOT TABLE BUILDERS */

    public <T extends FunctionUserBuilder<T>> T applyExplosionDecay(Supplier<? extends ItemLike> item, FunctionUserBuilder<T> functionBuilder) {
        return this.applyExplosionDecay(item.get(), functionBuilder);
    }

    @Override
    public <T extends FunctionUserBuilder<T>> T applyExplosionDecay(ItemLike item, FunctionUserBuilder<T> functionBuilder) {
        return super.applyExplosionDecay(item, functionBuilder);
    }

    public <T extends ConditionUserBuilder<T>> T applyExplosionCondition(Supplier<? extends ItemLike> item, ConditionUserBuilder<T> conditionBuilder) {
        return this.applyExplosionCondition(item.get(), conditionBuilder);
    }

    @Override
    public <T extends ConditionUserBuilder<T>> T applyExplosionCondition(ItemLike item, ConditionUserBuilder<T> conditionBuilder) {
        return super.applyExplosionCondition(item, conditionBuilder);
    }

    /** @see BlockLootSubProvider#createSingleItemTable(ItemLike) */
    public LootTable.Builder createSingleItemTable(Supplier<? extends ItemLike> item) {
        return this.createSingleItemTable(item.get());
    }

    @Override
    public LootTable.Builder createSingleItemTable(ItemLike item) {
        return super.createSingleItemTable(item);
    }

    /**
     * @see BlockLootSubProvider#createSelfDropDispatchTable(Block, LootItemCondition.Builder,
     *     LootPoolEntryContainer.Builder)
     */
    public LootTable.Builder createSelfDropDispatchTable(Supplier<? extends Block> block, LootItemCondition.Builder conditionBuilder, LootPoolEntryContainer.Builder<?> alternativeBuilder) {
        return createSelfDropDispatchTable(block.get(), conditionBuilder, alternativeBuilder);
    }

    public static LootTable.Builder createSelfDropDispatchTable(Block block, LootItemCondition.Builder conditionBuilder, LootPoolEntryContainer.Builder<?> alternativeBuilder) {
        return BlockLootSubProvider.createSelfDropDispatchTable(block, conditionBuilder, alternativeBuilder);
    }

    public LootTable.Builder createSilkTouchDispatchTable(Supplier<? extends Block> block, LootPoolEntryContainer.Builder<?> builder) {
        return createSilkTouchDispatchTable(block.get(), builder);
    }

    public static LootTable.Builder createSilkTouchDispatchTable(Block block, LootPoolEntryContainer.Builder<?> builder) {
        return BlockLootSubProvider.createSilkTouchDispatchTable(block, builder);
    }

    public LootTable.Builder createShearsDispatchTable(Supplier<? extends Block> block, LootPoolEntryContainer.Builder<?> builder) {
        return createShearsDispatchTable(block.get(), builder);
    }

    public static LootTable.Builder createShearsDispatchTable(Block block, LootPoolEntryContainer.Builder<?> builder) {
        return BlockLootSubProvider.createShearsDispatchTable(block, builder);
    }

    public LootTable.Builder createSilkTouchOrShearsDispatchTable(Supplier<? extends Block> block, LootPoolEntryContainer.Builder<?> builder) {
        return createSilkTouchOrShearsDispatchTable(block.get(), builder);
    }

    public static LootTable.Builder createSilkTouchOrShearsDispatchTable(Block block, LootPoolEntryContainer.Builder<?> builder) {
        return BlockLootSubProvider.createSilkTouchOrShearsDispatchTable(block, builder);
    }

    public LootTable.Builder createSingleItemTableWithSilkTouch(Supplier<? extends Block> block, Supplier<? extends ItemLike> item) {
        return this.createSingleItemTableWithSilkTouch(block.get(), item.get());
    }

    public LootTable.Builder createSingleItemTableWithSilkTouch(Supplier<? extends Block> block, ItemLike item) {
        return this.createSingleItemTableWithSilkTouch(block.get(), item);
    }

    @Override
    public LootTable.Builder createSingleItemTableWithSilkTouch(Block block, ItemLike item) {
        return super.createSingleItemTableWithSilkTouch(block, item);
    }

    public LootTable.Builder createSingleItemTable(Supplier<? extends ItemLike> item, NumberProvider count) {
        return this.createSingleItemTable(item.get(), count);
    }

    @Override
    public LootTable.Builder createSingleItemTable(ItemLike item, NumberProvider count) {
        return super.createSingleItemTable(item, count);
    }

    public LootTable.Builder createSingleItemTableWithSilkTouch(Supplier<? extends Block> block, Supplier<? extends ItemLike> item, NumberProvider count) {
        return this.createSingleItemTableWithSilkTouch(block.get(), item.get(), count);
    }

    public LootTable.Builder createSingleItemTableWithSilkTouch(Supplier<? extends Block> block, ItemLike item, NumberProvider count) {
        return this.createSingleItemTableWithSilkTouch(block.get(), item, count);
    }

    @Override
    public LootTable.Builder createSingleItemTableWithSilkTouch(Block block, ItemLike item, NumberProvider count) {
        return super.createSingleItemTableWithSilkTouch(block, item, count);
    }

    public LootTable.Builder createSilkTouchOnlyTable(Supplier<? extends ItemLike> item) {
        return createSilkTouchOnlyTable(item.get());
    }

    public static LootTable.Builder createSilkTouchOnlyTable(ItemLike item) {
        return BlockLootSubProvider.createSilkTouchOnlyTable(item);
    }

    public LootTable.Builder createPotFlowerItemTable(Supplier<? extends ItemLike> item) {
        return this.createPotFlowerItemTable(item.get());
    }

    @Override
    public LootTable.Builder createPotFlowerItemTable(ItemLike item) {
        return super.createPotFlowerItemTable(item);
    }

    @Override
    @Deprecated
    public LootTable.Builder createSlabItemTable(Block block) {
        return super.createSlabItemTable(block);
    }

    public LootTable.Builder createSlabItemTable(SlabBlock block) {
        return super.createSlabItemTable(block);
    }

    public LootTable.Builder createSlabItemTable(Supplier<? extends SlabBlock> block) {
        return this.createSlabItemTable(block.get());
    }

    public <T extends Comparable<T> & StringRepresentable> LootTable.Builder createSinglePropConditionTable(Supplier<? extends Block> block, Property<T> property, T value) {
        return this.createSinglePropConditionTable(block.get(), property, value);
    }

    @Override
    public <T extends Comparable<T> & StringRepresentable> LootTable.Builder createSinglePropConditionTable(Block block, Property<T> property, T value) {
        return super.createSinglePropConditionTable(block, property, value);
    }

    public <B extends Block & EntityBlock> LootTable.Builder createNameableBlockEntityTable(Supplier<B> block) {
        return this.createNameableBlockEntityTable(block.get());
    }

    @Override
    public LootTable.Builder createNameableBlockEntityTable(Block block) {
        return super.createNameableBlockEntityTable(block);
    }

    @Deprecated
    public LootTable.Builder createShulkerBoxDrop(Supplier<? extends ShulkerBoxBlock> block) {
        return this.createShulkerBoxDrop(block.get());
    }

    @Override
    @Deprecated
    public LootTable.Builder createShulkerBoxDrop(Block block) {
        return super.createShulkerBoxDrop(block);
    }

    @Deprecated
    public LootTable.Builder createCopperOreDrops(Supplier<? extends Block> block) {
        return this.createCopperOreDrops(block.get());
    }

    @Override
    @Deprecated
    public LootTable.Builder createCopperOreDrops(Block block) {
        return super.createCopperOreDrops(block);
    }

    @Deprecated
    public LootTable.Builder createLapisOreDrops(Supplier<? extends Block> block) {
        return this.createLapisOreDrops(block.get());
    }

    @Override
    @Deprecated
    public LootTable.Builder createLapisOreDrops(Block block) {
        return super.createLapisOreDrops(block);
    }

    @Deprecated
    public LootTable.Builder createRedstoneOreDrops(Supplier<? extends Block> block) {
        return this.createRedstoneOreDrops(block.get());
    }

    @Override
    @Deprecated
    public LootTable.Builder createRedstoneOreDrops(Block block) {
        return super.createRedstoneOreDrops(block);
    }

    @Override
    @Deprecated
    public LootTable.Builder createBannerDrop(Block block) {
        return super.createBannerDrop(block);
    }

    public LootTable.Builder createBannerDrop(AbstractBannerBlock block) {
        return super.createBannerDrop(block);
    }

    public LootTable.Builder createBannerDrop(Supplier<? extends AbstractBannerBlock> block) {
        return this.createBannerDrop(block.get());
    }

    @Deprecated
    public static LootTable.Builder createBeeNestDrop(Block block) {
        return BlockLootSubProvider.createBeeNestDrop(block);
    }

    @Deprecated
    public LootTable.Builder createBeeNestDrop(BeehiveBlock block) {
        return BlockLootSubProvider.createBeeNestDrop(block);
    }

    @Deprecated
    public LootTable.Builder createBeeNestDrop(Supplier<? extends BeehiveBlock> block) {
        return BlockLootSubProvider.createBeeNestDrop(block.get());
    }

    @Deprecated
    public static LootTable.Builder createBeeHiveDrop(Block block) {
        return BlockLootSubProvider.createBeeHiveDrop(block);
    }

    @Deprecated
    public LootTable.Builder createBeeHiveDrop(BeehiveBlock block) {
        return BlockLootSubProvider.createBeeHiveDrop(block);
    }

    @Deprecated
    public LootTable.Builder createBeeHiveDrop(Supplier<? extends Block> block) {
        return BlockLootSubProvider.createBeeHiveDrop(block.get());
    }

    @Deprecated
    public static LootTable.Builder createCaveVinesDrop(Block block) {
        return BlockLootSubProvider.createCaveVinesDrop(block);
    }

    @Deprecated
    public <B extends Block & CaveVines> LootTable.Builder createCaveVinesDrop(Supplier<B> block) {
        return BlockLootSubProvider.createCaveVinesDrop(block.get());
    }

    public LootTable.Builder createOreDrop(Supplier<? extends Block> block, Supplier<? extends ItemLike> item) {
        return this.createOreDrop(block.get(), item.get());
    }

    public LootTable.Builder createOreDrop(Supplier<? extends Block> block, ItemLike item) {
        return this.createOreDrop(block.get(), item);
    }

    public LootTable.Builder createOreDrop(Block block, Supplier<? extends ItemLike> item) {
        return this.createOreDrop(block, item.get());
    }

    public LootTable.Builder createOreDrop(Block block, ItemLike item) {
        return createSilkTouchDispatchTable(
            block,
            this.applyExplosionDecay(
                block,
                LootItem.lootTableItem(item)
                    .apply(ApplyBonusCount.addOreBonusCount(Enchantments.BLOCK_FORTUNE))
            )
        );
    }

    public LootTable.Builder createMushroomBlockDrop(Supplier<? extends Block> block, Supplier<? extends ItemLike> item) {
        return this.createMushroomBlockDrop(block.get(), item.get());
    }

    public LootTable.Builder createMushroomBlockDrop(Supplier<? extends Block> block, ItemLike item) {
        return this.createMushroomBlockDrop(block.get(), item);
    }

    @Override
    public LootTable.Builder createMushroomBlockDrop(Block block, ItemLike item) {
        return super.createMushroomBlockDrop(block, item);
    }

    @Deprecated
    public LootTable.Builder createGrassDrops(Supplier<? extends Block> block) {
        return this.createGrassDrops(block.get());
    }

    @Override
    @Deprecated
    public LootTable.Builder createGrassDrops(Block block) {
        return super.createGrassDrops(block);
    }

    public LootTable.Builder createStemDrops(Supplier<? extends StemBlock> block, Supplier<? extends ItemLike> item) {
        return this.createStemDrops(block.get(), item.get());
    }

    public LootTable.Builder createStemDrops(Supplier<? extends StemBlock> block, ItemLike item) {
        return this.createStemDrops(block.get(), item);
    }

    public LootTable.Builder createStemDrops(StemBlock block, Supplier<? extends ItemLike> item) {
        return this.createStemDrops(block, item.get());
    }

    public LootTable.Builder createStemDrops(StemBlock block, ItemLike item) {
        return LootTable.lootTable().withPool(this.applyExplosionDecay(block, LootPool.lootPool().setRolls(ConstantValue.exactly(1.0F)).add(LootItem.lootTableItem(item).apply(StemBlock.AGE.getPossibleValues(), (p_249795_) -> {
            return SetItemCountFunction.setCount(BinomialDistributionGenerator.binomial(3, (float) (p_249795_ + 1) / 15.0F)).when(LootItemBlockStatePropertyCondition.hasBlockStateProperties(block).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(StemBlock.AGE, p_249795_)));
        }))));
    }

    public LootTable.Builder createAttachedStemDrops(Supplier<? extends Block> block, Supplier<? extends ItemLike> item) {
        return this.createAttachedStemDrops(block.get(), item.get());
    }

    public LootTable.Builder createAttachedStemDrops(Supplier<? extends Block> block, ItemLike item) {
        return this.createAttachedStemDrops(block.get(), item);
    }

    public LootTable.Builder createAttachedStemDrops(Block block, Supplier<? extends ItemLike> item) {
        return this.createAttachedStemDrops(block, item.get());
    }

    public LootTable.Builder createAttachedStemDrops(Block block, ItemLike item) {
        return LootTable.lootTable().withPool(this.applyExplosionDecay(block, LootPool.lootPool().setRolls(ConstantValue.exactly(1.0F)).add(LootItem.lootTableItem(item).apply(SetItemCountFunction.setCount(BinomialDistributionGenerator.binomial(3, 0.53333336F))))));
    }

    public LootTable.Builder createShearsOnlyDrop(Supplier<? extends ItemLike> item) {
        return createShearsOnlyDrop(item.get());
    }

    public static LootTable.Builder createShearsOnlyDrop(ItemLike item) {
        return BlockLootSubProvider.createShearsOnlyDrop(item);
    }

    public LootTable.Builder createMultifaceBlockDrops(Supplier<? extends Block> block, LootItemCondition.Builder builder) {
        return this.createMultifaceBlockDrops(block.get(), builder);
    }

    @Override
    public LootTable.Builder createMultifaceBlockDrops(Block block, LootItemCondition.Builder builder) {
        return super.createMultifaceBlockDrops(block, builder);
    }

    public LootTable.Builder createLeavesDrops(Supplier<? extends Block> leavesBlock, Supplier<? extends Block> saplingBlock, Number... chances) {
        return this.createLeavesDrops(leavesBlock.get(), saplingBlock.get(), numToFloatArray(chances));
    }

    public LootTable.Builder createLeavesDrops(Supplier<? extends Block> leavesBlock, Block saplingBlock, Number... chances) {
        return this.createLeavesDrops(leavesBlock.get(), saplingBlock, numToFloatArray(chances));
    }

    public LootTable.Builder createLeavesDrops(Block leavesBlock, Supplier<? extends Block> saplingBlock, Number... chances) {
        return this.createLeavesDrops(leavesBlock, saplingBlock.get(), numToFloatArray(chances));
    }

    @Override
    public LootTable.Builder createLeavesDrops(Block leavesBlock, Block saplingBlock, float... chances) {
        return super.createLeavesDrops(leavesBlock, saplingBlock, chances);
    }

    public LootTable.Builder createOakLeavesDrops(Supplier<? extends Block> oakLeavesBlock, Supplier<? extends Block> saplingBlock, Number... chances) {
        return this.createOakLeavesDrops(oakLeavesBlock.get(), saplingBlock.get(), numToFloatArray(chances));
    }

    public LootTable.Builder createOakLeavesDrops(Supplier<? extends Block> oakLeavesBlock, Block saplingBlock, Number... chances) {
        return this.createOakLeavesDrops(oakLeavesBlock.get(), saplingBlock, numToFloatArray(chances));
    }

    public LootTable.Builder createOakLeavesDrops(Block oakLeavesBlock, Supplier<? extends Block> saplingBlock, Number... chances) {
        return this.createOakLeavesDrops(oakLeavesBlock, saplingBlock.get(), numToFloatArray(chances));
    }

    @Override
    public LootTable.Builder createOakLeavesDrops(Block oakLeavesBlock, Block saplingBlock, float... chances) {
        return super.createOakLeavesDrops(oakLeavesBlock, saplingBlock, chances);
    }

    @Deprecated
    public LootTable.Builder createMangroveLeavesDrops(Supplier<? extends Block> block) {
        return this.createMangroveLeavesDrops(block.get());
    }

    @Override
    @Deprecated
    public LootTable.Builder createMangroveLeavesDrops(Block block) {
        return super.createMangroveLeavesDrops(block);
    }

    public LootTable.Builder createCropDrops(Supplier<? extends Block> cropBlock, Supplier<? extends ItemLike> grownCropItem, LootItemCondition.Builder dropGrownCropCondition) {
        return this.createCropDrops(cropBlock.get(), grownCropItem.get(), dropGrownCropCondition);
    }

    public LootTable.Builder createCropDrops(Supplier<? extends Block> cropBlock, Supplier<? extends ItemLike> grownCropItem, LootItemCondition.Builder dropGrownCropCondition, NumberProvider fullyGrownCount) {
        return this.createCropDrops(cropBlock.get(), grownCropItem.get(), dropGrownCropCondition, fullyGrownCount);
    }

    public LootTable.Builder createCropDrops(Block cropBlock, Supplier<? extends ItemLike> grownCropItem, LootItemCondition.Builder dropGrownCropCondition) {
        return this.createCropDrops(cropBlock, grownCropItem.get(), dropGrownCropCondition);
    }

    public LootTable.Builder createCropDrops(Block cropBlock, Supplier<? extends ItemLike> grownCropItem, LootItemCondition.Builder dropGrownCropCondition, NumberProvider fullyGrownCount) {
        return this.createCropDrops(cropBlock, grownCropItem.get(), dropGrownCropCondition, fullyGrownCount);
    }

    public LootTable.Builder createCropDrops(Supplier<? extends Block> cropBlock, ItemLike grownCropItem, LootItemCondition.Builder dropGrownCropCondition) {
        return this.createCropDrops(cropBlock.get(), grownCropItem, dropGrownCropCondition);
    }

    public LootTable.Builder createCropDrops(Supplier<? extends Block> cropBlock, ItemLike grownCropItem, LootItemCondition.Builder dropGrownCropCondition, NumberProvider fullyGrownCount) {
        return this.createCropDrops(cropBlock.get(), grownCropItem, dropGrownCropCondition, fullyGrownCount);
    }

    public LootTable.Builder createCropDrops(Block cropBlock, ItemLike grownCropItem, LootItemCondition.Builder dropGrownCropCondition) {
        return this.createCropDrops(cropBlock, grownCropItem, grownCropItem, dropGrownCropCondition);
    }

    public LootTable.Builder createCropDrops(Block cropBlock, ItemLike grownCropItem, LootItemCondition.Builder dropGrownCropCondition, NumberProvider fullyGrownCount) {
        return this.createCropDrops(cropBlock, grownCropItem, grownCropItem, dropGrownCropCondition, fullyGrownCount);
    }

    public LootTable.Builder createCropDrops(Supplier<? extends Block> cropBlock, Supplier<? extends ItemLike> grownCropItem, Supplier<? extends ItemLike> seedsItem, LootItemCondition.Builder dropGrownCropCondition) {
        return this.createCropDrops(cropBlock.get(), grownCropItem.get(), seedsItem.get(), dropGrownCropCondition);
    }

    public LootTable.Builder createCropDrops(Supplier<? extends Block> cropBlock, Supplier<? extends ItemLike> grownCropItem, Supplier<? extends ItemLike> seedsItem, LootItemCondition.Builder dropGrownCropCondition, NumberProvider fullyGrownCount) {
        return this.createCropDrops(cropBlock.get(), grownCropItem.get(), seedsItem.get(), dropGrownCropCondition, fullyGrownCount);
    }

    public LootTable.Builder createCropDrops(Block cropBlock, Supplier<? extends ItemLike> grownCropItem, Supplier<? extends ItemLike> seedsItem, LootItemCondition.Builder dropGrownCropCondition) {
        return this.createCropDrops(cropBlock, grownCropItem.get(), seedsItem.get(), dropGrownCropCondition);
    }

    public LootTable.Builder createCropDrops(Block cropBlock, Supplier<? extends ItemLike> grownCropItem, Supplier<? extends ItemLike> seedsItem, LootItemCondition.Builder dropGrownCropCondition, NumberProvider fullyGrownCount) {
        return this.createCropDrops(cropBlock, grownCropItem.get(), seedsItem.get(), dropGrownCropCondition, fullyGrownCount);
    }

    public LootTable.Builder createCropDrops(Supplier<? extends Block> cropBlock, ItemLike grownCropItem, Supplier<? extends ItemLike> seedsItem, LootItemCondition.Builder dropGrownCropCondition) {
        return this.createCropDrops(cropBlock.get(), grownCropItem, seedsItem.get(), dropGrownCropCondition);
    }

    public LootTable.Builder createCropDrops(Supplier<? extends Block> cropBlock, ItemLike grownCropItem, Supplier<? extends ItemLike> seedsItem, LootItemCondition.Builder dropGrownCropCondition, NumberProvider fullyGrownCount) {
        return this.createCropDrops(cropBlock.get(), grownCropItem, seedsItem.get(), dropGrownCropCondition, fullyGrownCount);
    }

    public LootTable.Builder createCropDrops(Supplier<? extends Block> cropBlock, Supplier<? extends ItemLike> grownCropItem, ItemLike seedsItem, LootItemCondition.Builder dropGrownCropCondition) {
        return this.createCropDrops(cropBlock.get(), grownCropItem.get(), seedsItem, dropGrownCropCondition);
    }

    public LootTable.Builder createCropDrops(Supplier<? extends Block> cropBlock, Supplier<? extends ItemLike> grownCropItem, ItemLike seedsItem, LootItemCondition.Builder dropGrownCropCondition, NumberProvider fullyGrownCount) {
        return this.createCropDrops(cropBlock.get(), grownCropItem.get(), seedsItem, dropGrownCropCondition, fullyGrownCount);
    }

    public LootTable.Builder createCropDrops(Block cropBlock, ItemLike grownCropItem, Supplier<? extends ItemLike> seedsItem, LootItemCondition.Builder dropGrownCropCondition) {
        return this.createCropDrops(cropBlock, grownCropItem, seedsItem.get(), dropGrownCropCondition);
    }

    public LootTable.Builder createCropDrops(Block cropBlock, ItemLike grownCropItem, Supplier<? extends ItemLike> seedsItem, LootItemCondition.Builder dropGrownCropCondition, NumberProvider fullyGrownCount) {
        return this.createCropDrops(cropBlock, grownCropItem, seedsItem.get(), dropGrownCropCondition, fullyGrownCount);
    }

    public LootTable.Builder createCropDrops(Block cropBlock, Supplier<? extends ItemLike> grownCropItem, ItemLike seedsItem, LootItemCondition.Builder dropGrownCropCondition) {
        return this.createCropDrops(cropBlock, grownCropItem.get(), seedsItem, dropGrownCropCondition);
    }

    public LootTable.Builder createCropDrops(Block cropBlock, Supplier<? extends ItemLike> grownCropItem, ItemLike seedsItem, LootItemCondition.Builder dropGrownCropCondition, NumberProvider fullyGrownCount) {
        return this.createCropDrops(cropBlock, grownCropItem.get(), seedsItem, dropGrownCropCondition, fullyGrownCount);
    }

    public LootTable.Builder createCropDrops(Block cropBlock, ItemLike grownCropItem, ItemLike seedsItem, LootItemCondition.Builder dropGrownCropCondition) {
        return this.applyExplosionDecay(
            cropBlock,
            LootTable.lootTable()
                .withPool(
                    LootPool.lootPool()
                        .add(LootItem.lootTableItem(grownCropItem)
                                 .when(dropGrownCropCondition)
                                 .otherwise(LootItem.lootTableItem(seedsItem))
                        )
                )
                .withPool(
                    LootPool.lootPool()
                        .when(dropGrownCropCondition)
                        .add(LootItem.lootTableItem(seedsItem)
                                 .apply(ApplyBonusCount.addBonusBinomialDistributionCount(Enchantments.BLOCK_FORTUNE, 0.5714286F, 3))
                        )
                )
        );
    }

    public LootTable.Builder createCropDrops(Block cropBlock, ItemLike grownCropItem, ItemLike seedsItem, LootItemCondition.Builder dropGrownCropCondition, NumberProvider fullyGrownCount) {
        return this.applyExplosionDecay(
            cropBlock,
            LootTable.lootTable()
                .withPool(
                    LootPool.lootPool()
                        .add(LootItem.lootTableItem(grownCropItem)
                                 .when(dropGrownCropCondition)
                                 .apply(SetItemCountFunction.setCount(fullyGrownCount))
                                 .otherwise(LootItem.lootTableItem(seedsItem))
                        )
                )
                .withPool(
                    LootPool.lootPool()
                        .when(dropGrownCropCondition)
                        .add(LootItem.lootTableItem(seedsItem)
                                 .apply(ApplyBonusCount.addBonusBinomialDistributionCount(Enchantments.BLOCK_FORTUNE, 0.5714286F, 3))
                        )
                )
        );
    }

    public LootTable.Builder createDoublePlantShearsDrop(Supplier<? extends ItemLike> sheared) {
        return this.createDoublePlantShearsDrop(sheared.get());
    }

    public LootTable.Builder createDoublePlantShearsDrop(ItemLike sheared) {
        return LootTable.lootTable().withPool(
            LootPool.lootPool()
                .when(HAS_SHEARS)
                .add(LootItem.lootTableItem(sheared).apply(SetItemCountFunction.setCount(ConstantValue.exactly(2.0F))))
        );
    }

    @Deprecated
    public LootTable.Builder createDoublePlantWithSeedDrops(Supplier<? extends DoublePlantBlock> block, Supplier<? extends ItemLike> sheared) {
        return this.createDoublePlantWithSeedDrops(block.get(), sheared.get());
    }

    @Deprecated
    public LootTable.Builder createDoublePlantWithSeedDrops(Supplier<? extends DoublePlantBlock> block, ItemLike sheared) {
        return this.createDoublePlantWithSeedDrops(block.get(), sheared);
    }

    @Deprecated
    public LootTable.Builder createDoublePlantWithSeedDrops(DoublePlantBlock block, Supplier<? extends ItemLike> sheared) {
        return this.createDoublePlantWithSeedDrops(block, sheared.get());
    }

    @Deprecated
    public LootTable.Builder createDoublePlantWithSeedDrops(DoublePlantBlock block, ItemLike sheared) {
        LootPoolEntryContainer.Builder<?> builder =
            LootItem
                .lootTableItem(sheared)
                .apply(SetItemCountFunction.setCount(ConstantValue.exactly(2.0F)))
                .when(HAS_SHEARS)
                .otherwise(
                    this.applyExplosionCondition(block, LootItem.lootTableItem(Items.WHEAT_SEEDS))
                        .when(LootItemRandomChanceCondition.randomChance(0.125F))
                );
        return LootTable
                   .lootTable()
                   .withPool(
                       LootPool
                           .lootPool()
                           .add(builder)
                           .when(
                               LootItemBlockStatePropertyCondition
                                   .hasBlockStateProperties(block)
                                   .setProperties(
                                       StatePropertiesPredicate.Builder
                                           .properties()
                                           .hasProperty(DoublePlantBlock.HALF, DoubleBlockHalf.LOWER))
                           )
                           .when(
                               LocationCheck.checkLocation(
                                   LocationPredicate.Builder
                                       .location()
                                       .setBlock(
                                           BlockPredicate.Builder
                                               .block()
                                               .of(block)
                                               .setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(DoublePlantBlock.HALF, DoubleBlockHalf.UPPER).build())
                                               .build()
                                       ),
                                   new BlockPos(0, 1, 0)
                               )
                           )
                   )
                   .withPool(
                       LootPool
                           .lootPool()
                           .add(builder)
                           .when(
                               LootItemBlockStatePropertyCondition
                                   .hasBlockStateProperties(block)
                                   .setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(DoublePlantBlock.HALF, DoubleBlockHalf.UPPER))
                           )
                           .when(
                               LocationCheck
                                   .checkLocation(
                                       LocationPredicate.Builder
                                           .location()
                                           .setBlock(
                                               BlockPredicate.Builder
                                                   .block().of(block)
                                                   .setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(DoublePlantBlock.HALF, DoubleBlockHalf.LOWER).build())
                                                   .build()
                                           ),
                                       new BlockPos(0, -1, 0)
                                   )
                           )
                   );
    }

    @Override
    @Deprecated
    public LootTable.Builder createDoublePlantWithSeedDrops(Block block, Block sheared) {
        return super.createDoublePlantWithSeedDrops(block, sheared);
    }

    public LootTable.Builder createCandleDrops(Supplier<? extends AbstractCandleBlock> candleBlock) {
        return this.createCandleDrops(candleBlock.get());
    }

    public LootTable.Builder createCandleDrops(AbstractCandleBlock candleBlock) {
        return super.createCandleDrops(candleBlock);
    }

    @Override
    @Deprecated
    public LootTable.Builder createCandleDrops(Block candleBlock) {
        return super.createCandleDrops(candleBlock);
    }

    @Deprecated
    public LootTable.Builder createPetalsDrops(Supplier<? extends PinkPetalsBlock> petalBlock) {
        return this.createPetalsDrops(petalBlock.get());
    }

    @Deprecated
    public LootTable.Builder createPetalsDrops(PinkPetalsBlock petalBlock) {
        return super.createPetalsDrops(petalBlock);
    }

    @Override
    @Deprecated
    public LootTable.Builder createPetalsDrops(Block petalBlock) {
        return super.createPetalsDrops(petalBlock);
    }

    @Deprecated
    public LootTable.Builder createCandleCakeDrops(Supplier<? extends ItemLike> candleCakeBlock) {
        return this.createCandleCakeDrops(candleCakeBlock.get());
    }

    @Deprecated
    public LootTable.Builder createCandleCakeDrops(ItemLike candleCakeBlock) {
        return LootTable.lootTable().withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1.0F)).add(LootItem.lootTableItem(candleCakeBlock)));
    }

    @Deprecated
    public static LootTable.Builder createCandleCakeDrops(Block candleCakeBlock) {
        return BlockLootSubProvider.createCandleCakeDrops(candleCakeBlock);
    }

    public LootTable.Builder createDoorTable(Supplier<? extends DoorBlock> doorBlock) {
        return super.createDoorTable(doorBlock.get());
    }

    public LootTable.Builder createDoorTable(DoorBlock doorBlock) {
        return super.createDoorTable(doorBlock);
    }

    @Override
    @Deprecated
    public LootTable.Builder createDoorTable(Block doorBlock) {
        return super.createDoorTable(doorBlock);
    }


    /* CRYPTIC REGISTRY ADDITIONS */

    /**
     * Creates a loot table for an ore that is designed to drop multiple of its result, similar to copper, redstone,
     * etc.
     *
     * @param block The ore block to make the loot table for.
     * @param item  The resulting item to drop from the ore block.
     * @param min   The minimum amount of the resulting item.
     * @param max   The maximum amount of the resulting item.
     * @return The loot table builder.
     *
     * @see BlockLootSubProvider#createCopperOreDrops(Block)
     * @see BlockLootSubProvider#createLapisOreDrops(Block)
     * @see BlockLootSubProvider#createRedstoneOreDrops(Block)
     */
    public LootTable.Builder createOreDrop(Supplier<? extends Block> block, Supplier<? extends ItemLike> item, Number min, Number max) {
        return this.createOreDrop(block.get(), item.get(), min, max);
    }

    /**
     * Creates a loot table for an ore that is designed to drop multiple of its result, similar to copper, redstone,
     * etc.
     *
     * @param block The ore block to make the loot table for.
     * @param item  The resulting item to drop from the ore block.
     * @param min   The minimum amount of the resulting item.
     * @param max   The maximum amount of the resulting item.
     * @return The loot table builder.
     *
     * @see BlockLootSubProvider#createCopperOreDrops(Block)
     * @see BlockLootSubProvider#createLapisOreDrops(Block)
     * @see BlockLootSubProvider#createRedstoneOreDrops(Block)
     */
    public LootTable.Builder createOreDrop(Block block, Supplier<? extends ItemLike> item, Number min, Number max) {
        return this.createOreDrop(block, item.get(), min, max);
    }

    /**
     * Creates a loot table for an ore that is designed to drop multiple of its result, similar to copper, redstone,
     * etc.
     *
     * @param block The ore block to make the loot table for.
     * @param item  The resulting item to drop from the ore block.
     * @param min   The minimum amount of the resulting item.
     * @param max   The maximum amount of the resulting item.
     * @return The loot table builder.
     *
     * @see BlockLootSubProvider#createCopperOreDrops(Block)
     * @see BlockLootSubProvider#createLapisOreDrops(Block)
     * @see BlockLootSubProvider#createRedstoneOreDrops(Block)
     */
    public LootTable.Builder createOreDrop(Supplier<? extends Block> block, ItemLike item, Number min, Number max) {
        return this.createOreDrop(block.get(), item, min, max);
    }

    /**
     * Creates a loot table for an ore that is designed to drop multiple of its result, similar to copper, redstone,
     * etc.
     *
     * @param block The ore block to make the loot table for.
     * @param item  The resulting item to drop from the ore block.
     * @param min   The minimum amount of the resulting item.
     * @param max   The maximum amount of the resulting item.
     * @return The loot table builder.
     *
     * @see BlockLootSubProvider#createCopperOreDrops(Block)
     * @see BlockLootSubProvider#createLapisOreDrops(Block)
     * @see BlockLootSubProvider#createRedstoneOreDrops(Block)
     */
    public LootTable.Builder createOreDrop(Block block, ItemLike item, Number min, Number max) {
        return createSilkTouchDispatchTable(
            block,
            this.applyExplosionDecay(
                block,
                LootItem
                    .lootTableItem(item)
                    .apply(SetItemCountFunction.setCount(UniformGenerator.between(min.floatValue(), max.floatValue())))
                    .apply(ApplyBonusCount.addOreBonusCount(Enchantments.BLOCK_FORTUNE))
            )
        );
    }

    /**
     * Creates a simple loot table that gives the block chances to drop the item drop. Similar to
     * {@link #createLeavesDrops(Block, Supplier, Number...)} but without the other leaf-specific bloat.
     *
     * @param block   The block to create the drop for.
     * @param drop    The item to drop.
     * @param chances The chances to give to the block for dropping the item.
     * @return The loot table builder.
     */
    public LootTable.Builder createDropWithChances(Supplier<? extends Block> block, Supplier<? extends ItemLike> drop, Number... chances) {
        return this.createDropWithChances(block.get(), drop.get(), chances);
    }

    /**
     * Creates a simple loot table that gives the block chances to drop the item drop. Similar to
     * {@link #createLeavesDrops(Block, Supplier, Number...)} but without the other leaf-specific bloat.
     *
     * @param block   The block to create the drop for.
     * @param drop    The item to drop.
     * @param chances The chances to give to the block for dropping the item.
     * @return The loot table builder.
     */
    public LootTable.Builder createDropWithChances(Supplier<? extends Block> block, ItemLike drop, Number... chances) {
        return this.createDropWithChances(block.get(), drop, chances);
    }

    /**
     * Creates a simple loot table that gives the block chances to drop the item drop. Similar to
     * {@link #createLeavesDrops(Block, Supplier, Number...)} but without the other leaf-specific bloat.
     *
     * @param block   The block to create the drop for.
     * @param drop    The item to drop.
     * @param chances The chances to give to the block for dropping the item.
     * @return The loot table builder.
     */
    public LootTable.Builder createDropWithChances(Block block, Supplier<? extends ItemLike> drop, Number... chances) {
        return this.createDropWithChances(block, drop.get(), chances);
    }

    /**
     * Creates a simple loot table that gives the block chances to drop the item drop. Similar to
     * {@link #createLeavesDrops(Block, Supplier, Number...)} but without the other leaf-specific bloat.
     *
     * @param block   The block to create the drop for.
     * @param drop    The item to drop.
     * @param chances The chances to give to the block for dropping the item.
     * @return The loot table builder.
     */
    public LootTable.Builder createDropWithChances(Block block, ItemLike drop, Number... chances) {
        return createSilkTouchOrShearsDispatchTable(
            block,
            this.applyExplosionCondition(block, LootItem.lootTableItem(drop))
                .when(BonusLevelTableCondition.bonusLevelFlatChance(Enchantments.BLOCK_FORTUNE, numToFloatArray(chances)))
        );
    }

    /** @deprecated Use {@link #createDropWithChances(Supplier, Supplier, Number...)} */
    @Deprecated(forRemoval = true)
    public LootTable.Builder droppingWithChances(Supplier<? extends Block> block, Supplier<? extends ItemLike> drop, float... chances) {
        Number[] numbers = new Number[chances.length];
        for (int i = 0; i < chances.length; i++) {
            numbers[i] = chances[i];
        }

        return this.createDropWithChances(block, drop, numbers);
    }

    /** @deprecated Use {@link #noDrop()} */
    @Deprecated(forRemoval = true)
    public LootTable.Builder droppingNothing() {
        return LootTable.lootTable().withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(0.0F)));
    }

    /** @deprecated Use {@link #createSingleItemTableWithSilkTouch(Supplier, ItemLike, NumberProvider)} */
    @Deprecated(forRemoval = true)
    public LootTable.Builder droppingSilkTouchOrRanged(Supplier<? extends Block> block, Supplier<? extends ItemLike> item, int min, int max) {
        return createSilkTouchDispatchTable(
            block.get(),
            this.applyExplosionCondition(block.get(), LootItem.lootTableItem(item.get()))
                .apply(SetItemCountFunction.setCount(UniformGenerator.between(min, max)))
                .apply(ApplyBonusCount.addUniformBonusCount(Enchantments.BLOCK_FORTUNE, 1))
        );
    }
}
