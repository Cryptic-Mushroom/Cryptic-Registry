package com.crypticmushroom.minecraft.registry.data.provider.recipe;

import com.crypticmushroom.minecraft.registry.coremod.hook.impl.CrypticIngredient;
import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import com.google.common.collect.Maps;
import com.mojang.datafixers.util.Pair;
import net.minecraft.advancements.critereon.EnterBlockTrigger;
import net.minecraft.advancements.critereon.InventoryChangeTrigger;
import net.minecraft.advancements.critereon.ItemPredicate;
import net.minecraft.advancements.critereon.MinMaxBounds;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.LegacyUpgradeRecipeBuilder;
import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.ShapelessRecipeBuilder;
import net.minecraft.data.recipes.SimpleCookingRecipeBuilder;
import net.minecraft.data.recipes.SingleItemRecipeBuilder;
import net.minecraft.data.recipes.SmithingTransformRecipeBuilder;
import net.minecraft.data.recipes.SmithingTrimRecipeBuilder;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.AbstractCookingRecipe;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.crafting.CompoundIngredient;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.commons.lang3.tuple.Triple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

public abstract class CrypticRecipeProvider extends RecipeProvider implements ICrypticDataProvider {
    private final String modId;
    private final GatherDataEvent event;

    protected CrypticRecipeWriter writer;
    protected final Stonecutter stonecutter = new Stonecutter();

    public CrypticRecipeProvider(String modId, GatherDataEvent event) {
        super(event.getGenerator().getPackOutput());
        this.modId = modId;
        this.event = event;
    }

    protected abstract void buildRecipes();

    @Override
    protected final void buildRecipes(Consumer<FinishedRecipe> writer) {
        this.writer = new CrypticRecipeWriter(writer);
        this.buildRecipes();
        this.stonecutter.buildRecipes();
        this.writer = null;
    }

    @Override
    public final CompletableFuture<?> run(CachedOutput output) {
        return super.run(output);
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public String getName() {
        return ICrypticDataProvider.super.getName();
    }

    @Override
    public String getSimpleName() {
        return "Recipes";
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    public boolean shouldRun() {
        return this.event.includeServer();
    }

    public class CrypticRecipeWriter implements Consumer<FinishedRecipe> {
        private final Consumer<FinishedRecipe> writer;

        protected CrypticRecipeWriter(Consumer<FinishedRecipe> writer) {
            this.writer = writer;
        }

        @Override
        public void accept(FinishedRecipe recipe) {
            this.writer.accept(recipe);
        }

        @Override
        public Consumer<FinishedRecipe> andThen(Consumer<? super FinishedRecipe> after) {
            return this.writer.andThen(after);
        }

        public String getModId() {
            return CrypticRecipeProvider.this.getModId();
        }
    }


    /* CONFIGURATION */

    protected Ingredient getDefaultSandIngredient() {
        return ingredient(ItemTags.SAND, Tags.Items.SAND);
    }

    protected Ingredient getDefaultGravelIngredient() {
        return ingredient(Tags.Items.GRAVEL);
    }

    protected Ingredient getDefaultDiamondIngredient() {
        return ingredient(Tags.Items.GEMS_DIAMOND);
    }

    protected Ingredient getDefaultNetherBricksIngredient() {
        return ingredient(Tags.Items.INGOTS_NETHER_BRICK);
    }

    protected Ingredient getDefaultStickIngredient() {
        return ingredient(Tags.Items.RODS_WOODEN);
    }

    protected Ingredient getDefaultPlanksIngredient() {
        return ingredient(ItemTags.PLANKS);
    }

    protected int getDefaultSmeltingTime() {
        return 200;
    }

    protected int getDefaultCampfireCookingTime() {
        return this.getDefaultSmeltingTime() * 3;
    }

    protected int getDefaultSmokingTime() {
        return this.getDefaultSmeltingTime() / 2;
    }

    protected int getDefaultBlastingTime() {
        return this.getDefaultSmeltingTime() / 2;
    }

    protected boolean isNetherBrickFence(ItemLike item) {
        var stack = item.asItem().getDefaultInstance();
        return stack.is(Tags.Items.FENCES_NETHER_BRICK);
    }

    protected boolean isWoodenFence(ItemLike item) {
        var stack = item.asItem().getDefaultInstance();
        return stack.is(ItemTags.WOODEN_FENCES) || stack.is(Tags.Items.FENCES_WOODEN);
    }


    /* GENERIC RECIPES */

    protected final void shapeless(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        this.shapeless(category, result.get(), ingredient, count);
    }

    protected void shapeless(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        this.shapelessBuilder(category, result, ingredient, count)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient));
    }

    protected final RecipeBuilder shapelessBuilder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        return this.shapelessBuilder(category, result.get(), ingredient, count);
    }

    protected RecipeBuilder shapelessBuilder(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        return ShapelessRecipeBuilder.shapeless(category, result, count).requires(ingredient);
    }

    protected final void shapeless(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count, int ingredientCount) {
        this.shapeless(category, result.get(), ingredient, count, ingredientCount);
    }

    protected void shapeless(RecipeCategory category, ItemLike result, Ingredient ingredient, int count, int ingredientCount) {
        this.shapelessBuilder(category, result, ingredient, count, ingredientCount)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient));
    }

    protected final RecipeBuilder shapelessBuilder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count, int ingredientCount) {
        return this.shapelessBuilder(category, result.get(), ingredient, count, ingredientCount);
    }

    protected RecipeBuilder shapelessBuilder(RecipeCategory category, ItemLike result, Ingredient ingredient, int count, int ingredientCount) {
        return ShapelessRecipeBuilder.shapeless(category, result, count)
                   .requires(ingredient, ingredientCount);
    }

    protected final void generic1x2(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        this.generic1x2(category, result.get(), ingredient, count);
    }

    protected void generic1x2(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        this.generic1x2Builder(category, result, ingredient, count)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient));
    }

    protected final RecipeBuilder generic1x2Builder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        return this.generic1x2Builder(category, result.get(), ingredient, count);
    }

    protected RecipeBuilder generic1x2Builder(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        return ShapedRecipeBuilder.shaped(category, result, count)
                   .define('#', ingredient)
                   .pattern("#")
                   .pattern("#");
    }

    protected final void generic1x2(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredientTop, Ingredient ingredientBottom, int count) {
        this.generic1x2(category, result.get(), ingredientTop, ingredientBottom, count);
    }

    protected void generic1x2(RecipeCategory category, ItemLike result, Ingredient ingredientTop, Ingredient ingredientBottom, int count) {
        this.generic1x2Builder(category, result, ingredientTop, ingredientBottom, count)
            .unlockedBy(getHasName(ingredientTop), has(ingredientTop))
            .unlockedBy(getHasName(ingredientBottom), has(ingredientBottom))
            .save(this.writer, getConversionRecipeName(result, ingredientTop, ingredientBottom));
    }

    protected final RecipeBuilder generic1x2Builder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredientTop, Ingredient ingredientBottom, int count) {
        return this.generic1x2Builder(category, result.get(), ingredientTop, ingredientBottom, count);
    }

    protected RecipeBuilder generic1x2Builder(RecipeCategory category, ItemLike result, Ingredient ingredientTop, Ingredient ingredientBottom, int count) {
        return ShapedRecipeBuilder.shaped(category, result, count)
                   .define('#', ingredientTop)
                   .define('/', ingredientBottom)
                   .pattern("#")
                   .pattern("/");
    }

    protected final void generic1x3(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        this.generic1x3(category, result.get(), ingredient, count);
    }

    protected void generic1x3(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        this.generic1x3Builder(category, result, ingredient, count)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient));
    }

    protected final RecipeBuilder generic1x3Builder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        return this.generic1x3Builder(category, result.get(), ingredient, count);
    }

    protected RecipeBuilder generic1x3Builder(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        return ShapedRecipeBuilder.shaped(category, result, count)
                   .define('#', ingredient)
                   .pattern("#")
                   .pattern("#")
                   .pattern("#");
    }

    protected final void generic2x2(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        this.generic2x2(category, result.get(), ingredient, count);
    }

    protected void generic2x2(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        this.generic2x2Builder(category, result, ingredient, count)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient));
    }

    protected final RecipeBuilder generic2x2Builder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        return this.generic2x2Builder(category, result.get(), ingredient, count);
    }

    protected RecipeBuilder generic2x2Builder(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        return ShapedRecipeBuilder.shaped(category, result, count)
                   .define('#', ingredient)
                   .pattern("##")
                   .pattern("##");
    }

    protected final void generic2x3(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        this.generic2x3(category, result.get(), ingredient, count);
    }

    protected void generic2x3(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        this.generic2x3Builder(category, result, ingredient, count)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient));
    }

    protected final RecipeBuilder generic2x3Builder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        return this.generic2x3Builder(category, result.get(), ingredient, count);
    }

    protected RecipeBuilder generic2x3Builder(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        return ShapedRecipeBuilder.shaped(category, result, count)
                   .define('#', ingredient)
                   .pattern("##")
                   .pattern("##")
                   .pattern("##");
    }

    protected final void generic3x1(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        this.generic3x1(category, result.get(), ingredient, count);
    }

    protected void generic3x1(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        this.generic3x1Builder(category, result, ingredient, count)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient));
    }

    protected final RecipeBuilder generic3x1Builder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        return this.generic3x1Builder(category, result.get(), ingredient, count);
    }

    protected RecipeBuilder generic3x1Builder(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        return ShapedRecipeBuilder.shaped(category, result, count)
                   .define('#', ingredient)
                   .pattern("###");
    }

    protected final void generic3x2(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        this.generic3x2(category, result.get(), ingredient, count);
    }

    protected void generic3x2(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        this.generic3x2Builder(category, result, ingredient, count)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient));
    }

    protected final RecipeBuilder generic3x2Builder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        return this.generic3x2Builder(category, result.get(), ingredient, count);
    }

    protected RecipeBuilder generic3x2Builder(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        return ShapedRecipeBuilder.shaped(category, result, count)
                   .define('#', ingredient)
                   .pattern("###")
                   .pattern("###");
    }

    protected final void generic3x3(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        this.generic3x3(category, result.get(), ingredient, count);
    }

    protected void generic3x3(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        this.generic3x3Builder(category, result, ingredient, count)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient));
    }

    protected final RecipeBuilder generic3x3Builder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, int count) {
        return this.generic3x3Builder(category, result.get(), ingredient, count);
    }

    protected RecipeBuilder generic3x3Builder(RecipeCategory category, ItemLike result, Ingredient ingredient, int count) {
        return ShapedRecipeBuilder.shaped(category, result, count)
                   .define('#', ingredient)
                   .pattern("###")
                   .pattern("###")
                   .pattern("###");
    }


    /* ARMOR RECIPES */

    protected final void helmet(Supplier<? extends ItemLike> result, Ingredient material) {
        this.helmet(result.get(), material);
    }

    protected void helmet(ItemLike result, Ingredient material) {
        this.helmetBuilder(result, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder helmetBuilder(Supplier<? extends ItemLike> result, Ingredient material) {
        return this.helmetBuilder(result.get(), material);
    }

    protected RecipeBuilder helmetBuilder(ItemLike result, Ingredient material) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, result, 1)
                   .define('#', material)
                   .pattern("###")
                   .pattern("# #");
    }

    protected final void chestplate(Supplier<? extends ItemLike> result, Ingredient material) {
        this.chestplate(result.get(), material);
    }

    protected void chestplate(ItemLike result, Ingredient material) {
        this.chestplateBuilder(result, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder chestplateBuilder(Supplier<? extends ItemLike> result, Ingredient material) {
        return this.chestplateBuilder(result.get(), material);
    }

    protected RecipeBuilder chestplateBuilder(ItemLike result, Ingredient material) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, result, 1)
                   .define('#', material)
                   .pattern("# #")
                   .pattern("###")
                   .pattern("###");
    }

    protected final void leggings(Supplier<? extends ItemLike> result, Ingredient material) {
        this.leggings(result.get(), material);
    }

    protected void leggings(ItemLike result, Ingredient material) {
        this.leggingsBuilder(result, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder leggingsBuilder(Supplier<? extends ItemLike> result, Ingredient material) {
        return this.leggingsBuilder(result.get(), material);
    }

    protected RecipeBuilder leggingsBuilder(ItemLike result, Ingredient material) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, result, 1)
                   .define('#', material)
                   .pattern("###")
                   .pattern("# #")
                   .pattern("# #");
    }

    protected final void boots(Supplier<? extends ItemLike> result, Ingredient material) {
        this.boots(result.get(), material);
    }

    protected void boots(ItemLike result, Ingredient material) {
        this.bootsBuilder(result, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder bootsBuilder(Supplier<? extends ItemLike> result, Ingredient material) {
        return this.bootsBuilder(result.get(), material);
    }

    protected RecipeBuilder bootsBuilder(ItemLike result, Ingredient material) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, result, 1)
                   .define('#', material)
                   .pattern("# #")
                   .pattern("# #");
    }


    /* TOOL RECIPES */

    protected final void sword(Supplier<? extends ItemLike> result, Ingredient material) {
        this.sword(result.get(), material);
    }

    protected final void sword(ItemLike result, Ingredient material) {
        this.sword(result, material, this.getDefaultStickIngredient());
    }

    protected final RecipeBuilder swordBuilder(Supplier<? extends ItemLike> result, Ingredient material) {
        return this.swordBuilder(result.get(), material);
    }

    protected final RecipeBuilder swordBuilder(ItemLike result, Ingredient material) {
        return this.swordBuilder(result, material, this.getDefaultStickIngredient());
    }

    protected final void sword(Supplier<? extends ItemLike> result, Ingredient material, Ingredient stick) {
        this.sword(result.get(), material, stick);
    }

    protected void sword(ItemLike result, Ingredient material, Ingredient stick) {
        this.swordBuilder(result, material, stick)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder swordBuilder(Supplier<? extends ItemLike> result, Ingredient material, Ingredient stick) {
        return this.swordBuilder(result.get(), material, stick);
    }

    protected RecipeBuilder swordBuilder(ItemLike result, Ingredient material, Ingredient stick) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, result, 1)
                   .define('#', material)
                   .define('/', stick)
                   .pattern("#")
                   .pattern("#")
                   .pattern("/");
    }

    protected final void shovel(Supplier<? extends ItemLike> result, Ingredient material) {
        this.shovel(result.get(), material);
    }

    protected final void shovel(ItemLike result, Ingredient material) {
        this.shovel(result, material, this.getDefaultStickIngredient());
    }

    protected final RecipeBuilder shovelBuilder(Supplier<? extends ItemLike> result, Ingredient material) {
        return this.shovelBuilder(result.get(), material);
    }

    protected final RecipeBuilder shovelBuilder(ItemLike result, Ingredient material) {
        return this.shovelBuilder(result, material, this.getDefaultStickIngredient());
    }

    protected final void shovel(Supplier<? extends ItemLike> result, Ingredient material, Ingredient stick) {
        this.shovel(result.get(), material, stick);
    }

    protected void shovel(ItemLike result, Ingredient material, Ingredient stick) {
        this.shovelBuilder(result, material, stick)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder shovelBuilder(Supplier<? extends ItemLike> result, Ingredient material, Ingredient stick) {
        return this.shovelBuilder(result.get(), material, stick);
    }

    protected RecipeBuilder shovelBuilder(ItemLike result, Ingredient material, Ingredient stick) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, result, 1)
                   .define('#', material)
                   .define('/', stick)
                   .pattern("#")
                   .pattern("/")
                   .pattern("/");
    }

    protected final void axe(Supplier<? extends ItemLike> result, Ingredient material) {
        this.axe(result.get(), material);
    }

    protected final void axe(ItemLike result, Ingredient material) {
        this.axe(result, material, this.getDefaultStickIngredient());
    }

    protected final RecipeBuilder axeBuilder(Supplier<? extends ItemLike> result, Ingredient material) {
        return this.axeBuilder(result.get(), material);
    }

    protected final RecipeBuilder axeBuilder(ItemLike result, Ingredient material) {
        return this.axeBuilder(result, material, this.getDefaultStickIngredient());
    }

    protected final void axe(Supplier<? extends ItemLike> result, Ingredient material, Ingredient stick) {
        this.axe(result.get(), material, stick);
    }

    protected void axe(ItemLike result, Ingredient material, Ingredient stick) {
        this.axeBuilder(result, material, stick)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder axeBuilder(Supplier<? extends ItemLike> result, Ingredient material, Ingredient stick) {
        return this.axeBuilder(result.get(), material, stick);
    }

    protected RecipeBuilder axeBuilder(ItemLike result, Ingredient material, Ingredient stick) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, result, 1)
                   .define('#', material)
                   .define('/', stick)
                   .pattern("##")
                   .pattern("#/")
                   .pattern(" /");
    }

    protected final void pickaxe(Supplier<? extends ItemLike> result, Ingredient material) {
        this.pickaxe(result.get(), material);
    }

    protected final void pickaxe(ItemLike result, Ingredient material) {
        this.pickaxe(result, material, this.getDefaultStickIngredient());
    }

    protected final RecipeBuilder pickaxeBuilder(Supplier<? extends ItemLike> result, Ingredient material) {
        return this.pickaxeBuilder(result.get(), material);
    }

    protected final RecipeBuilder pickaxeBuilder(ItemLike result, Ingredient material) {
        return this.pickaxeBuilder(result, material, this.getDefaultStickIngredient());
    }

    protected final void pickaxe(Supplier<? extends ItemLike> result, Ingredient material, Ingredient stick) {
        this.pickaxe(result.get(), material, stick);
    }

    protected void pickaxe(ItemLike result, Ingredient material, Ingredient stick) {
        this.pickaxeBuilder(result, material, stick)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder pickaxeBuilder(Supplier<? extends ItemLike> result, Ingredient material, Ingredient stick) {
        return this.pickaxeBuilder(result.get(), material, stick);
    }

    protected RecipeBuilder pickaxeBuilder(ItemLike result, Ingredient material, Ingredient stick) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, result, 1)
                   .define('#', material)
                   .define('/', stick)
                   .pattern("###")
                   .pattern(" / ")
                   .pattern(" / ");
    }

    protected final void hoe(Supplier<? extends ItemLike> result, Ingredient material) {
        this.hoe(result.get(), material);
    }

    protected final void hoe(ItemLike result, Ingredient material) {
        this.hoe(result, material, this.getDefaultStickIngredient());
    }

    protected final RecipeBuilder hoeBuilder(Supplier<? extends ItemLike> result, Ingredient material) {
        return this.hoeBuilder(result.get(), material);
    }

    protected final RecipeBuilder hoeBuilder(ItemLike result, Ingredient material) {
        return this.hoeBuilder(result, material, this.getDefaultStickIngredient());
    }

    protected final void hoe(Supplier<? extends ItemLike> result, Ingredient material, Ingredient stick) {
        this.hoe(result.get(), material, stick);
    }

    protected void hoe(ItemLike result, Ingredient material, Ingredient stick) {
        this.hoeBuilder(result, material, stick)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder hoeBuilder(Supplier<? extends ItemLike> result, Ingredient material, Ingredient stick) {
        return this.hoeBuilder(result.get(), material, stick);
    }

    protected RecipeBuilder hoeBuilder(ItemLike result, Ingredient material, Ingredient stick) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, result, 1)
                   .define('#', material)
                   .define('/', stick)
                   .pattern("##")
                   .pattern(" /")
                   .pattern(" /");
    }


    /* VANILLA RECIPES */

    @Deprecated(forRemoval = true)
    protected void legacyNetheriteSmithing(RecipeCategory category, Supplier<? extends ItemLike> resultItem, Ingredient ingredientItem) {
        this.legacyNetheriteSmithing(category, resultItem.get(), ingredientItem);
    }

    @Deprecated(forRemoval = true)
    protected void legacyNetheriteSmithing(RecipeCategory category, ItemLike resultItem, Ingredient ingredientItem) {
        this.legacyNetheriteSmithingBuilder(category, resultItem, ingredientItem)
            .unlocks("has_netherite_ingot", has(Tags.Items.INGOTS_NETHERITE))
            .save(this.writer, getItemName(resultItem) + "_smithing");
    }

    @Deprecated(forRemoval = true)
    protected LegacyUpgradeRecipeBuilder legacyNetheriteSmithingBuilder(RecipeCategory category, Supplier<? extends ItemLike> resultItem, Ingredient ingredientItem) {
        return this.legacyNetheriteSmithingBuilder(category, resultItem.get(), ingredientItem);
    }

    @Deprecated(forRemoval = true)
    protected LegacyUpgradeRecipeBuilder legacyNetheriteSmithingBuilder(RecipeCategory category, ItemLike reultItem, Ingredient ingredientItem) {
        return LegacyUpgradeRecipeBuilder.smithing(
            ingredientItem,
            Ingredient.of(Tags.Items.INGOTS_NETHERITE),
            category,
            reultItem.asItem()
        );
    }

    protected void netheriteSmithing(RecipeCategory category, Supplier<? extends ItemLike> resultItem, Ingredient ingredientItem) {
        this.netheriteSmithing(category, resultItem.get(), ingredientItem);
    }

    protected void netheriteSmithing(RecipeCategory category, ItemLike resultItem, Ingredient ingredientItem) {
        this.netheriteSmithingBuilder(category, resultItem, ingredientItem)
            .unlocks("has_netherite_ingot", has(Tags.Items.INGOTS_NETHERITE))
            .save(this.writer, getItemName(resultItem) + "_smithing");
    }

    protected SmithingTransformRecipeBuilder netheriteSmithingBuilder(RecipeCategory category, Supplier<? extends ItemLike> resultItem, Ingredient ingredientItem) {
        return this.netheriteSmithingBuilder(category, resultItem.get(), ingredientItem);
    }

    protected SmithingTransformRecipeBuilder netheriteSmithingBuilder(RecipeCategory category, ItemLike resultItem, Ingredient ingredientItem) {
        return SmithingTransformRecipeBuilder.smithing(
            Ingredient.of(Items.NETHERITE_UPGRADE_SMITHING_TEMPLATE),
            ingredientItem,
            Ingredient.of(Tags.Items.INGOTS_NETHERITE),
            category,
            resultItem.asItem()
        );
    }

    protected void trimSmithing(Ingredient template) {
        this.trimSmithingBuilder(template)
            .unlocks("has_smithing_trim_template", has(template))
            .save(this.writer, getName(template) + "_smithing_trim");
    }

    protected SmithingTrimRecipeBuilder trimSmithingBuilder(Ingredient template) {
        return SmithingTrimRecipeBuilder.smithingTrim(
            template,
            Ingredient.of(ItemTags.TRIMMABLE_ARMOR),
            Ingredient.of(ItemTags.TRIM_MATERIALS),
            RecipeCategory.MISC
        );
    }

    protected final void planksFromLog(Supplier<? extends ItemLike> planks, Ingredient log, int count) {
        this.planksFromLog(planks.get(), log, count);
    }

    protected void planksFromLog(ItemLike planks, Ingredient log, int count) {
        this.planksFromLogBuilder(planks, log, count)
            .group("planks")
            .unlockedBy(getHasName(log), has(log))
            .save(this.writer, getConversionRecipeName(planks, log));
    }

    protected final RecipeBuilder planksFromLogBuilder(Supplier<? extends ItemLike> planks, Ingredient log, int count) {
        return this.planksFromLogBuilder(planks.get(), log, count);
    }

    protected RecipeBuilder planksFromLogBuilder(ItemLike planks, Ingredient log, int count) {
        return ShapelessRecipeBuilder.shapeless(RecipeCategory.BUILDING_BLOCKS, planks, count)
                   .requires(log);
    }

    protected void woodFromLogs(Supplier<? extends ItemLike> wood, Ingredient log) {
        this.woodFromLogs(wood.get(), log);
    }

    protected void woodFromLogs(ItemLike wood, Ingredient log) {
        this.woodFromLogsBuilder(wood, log)
            .group("bark")
            .unlockedBy(getHasName(log), has(log))
            .save(this.writer, getConversionRecipeName(wood, log));
    }

    protected RecipeBuilder woodFromLogsBuilder(Supplier<? extends ItemLike> wood, Ingredient log) {
        return this.woodFromLogsBuilder(wood.get(), log);
    }

    protected RecipeBuilder woodFromLogsBuilder(ItemLike wood, Ingredient log) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, wood, 3)
                   .define('#', log)
                   .pattern("##")
                   .pattern("##");
    }

    protected final void boat(Supplier<? extends ItemLike> boat, Ingredient material) {
        this.boat(boat.get(), material);
    }

    protected void boat(ItemLike boat, Ingredient material) {
        this.boatBuilder(boat, material)
            .group("boat")
            .unlockedBy("in_water", insideOf(Blocks.WATER))
            .save(this.writer);
    }

    protected final RecipeBuilder boatBuilder(Supplier<? extends ItemLike> boat, Ingredient material) {
        return this.boatBuilder(boat.get(), material);
    }

    protected RecipeBuilder boatBuilder(ItemLike boat, Ingredient material) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.TRANSPORTATION, boat)
                   .define('#', material)
                   .pattern("# #")
                   .pattern("###");
    }

    protected final void chestBoat(Supplier<? extends ItemLike> chestBoat, Ingredient boat) {
        this.chestBoat(chestBoat.get(), boat);
    }

    protected void chestBoat(ItemLike chestBoat, Ingredient boat) {
        this.chestBoatBuilder(chestBoat, boat)
            .unlockedBy(getHasName(boat), has(boat))
            .save(this.writer);
    }

    protected final RecipeBuilder chestBoatBuilder(Supplier<? extends ItemLike> chestBoat, Ingredient boat) {
        return this.chestBoatBuilder(chestBoat.get(), boat);
    }

    protected final RecipeBuilder chestBoatBuilder(ItemLike chestBoat, Ingredient boat) {
        return this.chestBoatBuilder(chestBoat, boat, ingredient(Items.CHEST));
    }

    protected final void chestBoat(Supplier<? extends ItemLike> chestBoat, Ingredient boat, Ingredient chest) {
        this.chestBoat(chestBoat.get(), boat, chest);
    }

    protected void chestBoat(ItemLike chestBoat, Ingredient boat, Ingredient chest) {
        this.chestBoatBuilder(chestBoat, boat, chest)
            .unlockedBy(getHasName(boat), has(boat))
            .unlockedBy(getHasName(chest), has(chest))
            .save(this.writer);
    }

    protected final RecipeBuilder chestBoatBuilder(Supplier<? extends ItemLike> chestBoat, Ingredient boat, Ingredient chest) {
        return this.chestBoatBuilder(chestBoat.get(), boat, chest);
    }

    protected RecipeBuilder chestBoatBuilder(ItemLike chestBoat, Ingredient boat, Ingredient chest) {
        return ShapelessRecipeBuilder.shapeless(RecipeCategory.TRANSPORTATION, chestBoat)
                   .requires(chest)
                   .requires(boat)
                   .group("chest_boat");
    }

    protected final void button(Supplier<? extends ItemLike> button, Ingredient material) {
        this.button(button.get(), material);
    }

    protected void button(ItemLike button, Ingredient material) {
        buttonBuilder(button, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder buttonBuilder(Supplier<? extends ItemLike> button, Ingredient material) {
        return buttonBuilder(button.get(), material);
    }

    protected final void door(Supplier<? extends ItemLike> door, Ingredient material) {
        this.door(door.get(), material);
    }

    protected void door(ItemLike door, Ingredient material) {
        doorBuilder(door, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder doorBuilder(Supplier<? extends ItemLike> door, Ingredient material) {
        return doorBuilder(door.get(), material);
    }

    protected final void fence(Supplier<? extends ItemLike> fence, Ingredient material) {
        this.fence(fence.get(), material);
    }

    protected final void fence(ItemLike fence, Ingredient material) {
        if (this.isNetherBrickFence(fence)) {
            this.fence(fence, material, this.getDefaultNetherBricksIngredient(), 3);
        } else {
            this.fence(fence, material, this.getDefaultStickIngredient());
        }
    }

    protected final RecipeBuilder fenceBuilder(Supplier<? extends ItemLike> fence, Ingredient material) {
        return fenceBuilder(fence.get(), material);
    }

    protected static RecipeBuilder fenceBuilder(ItemLike fence, Ingredient material) {
        boolean netherBrick = fence.asItem().getDefaultInstance().is(Tags.Items.FENCES_NETHER_BRICK);
        return netherBrick ? fenceBuilderInternal(fence, material, ingredient(Tags.Items.INGOTS_NETHER_BRICK), 3) : fenceBuilderInternal(fence, material, ingredient(Tags.Items.RODS_WOODEN), 6);
    }

    protected final void fence(Supplier<? extends ItemLike> fence, Ingredient material, Ingredient support) {
        this.fence(fence.get(), material, support);
    }

    protected final void fence(ItemLike fence, Ingredient material, Ingredient support) {
        this.fence(fence, material, support, this.isWoodenFence(fence) ? 6 : 3);
    }

    protected final RecipeBuilder fenceBuilder(Supplier<? extends ItemLike> fence, Ingredient material, Ingredient support) {
        return this.fenceBuilder(fence.get(), material, support);
    }

    protected final RecipeBuilder fenceBuilder(ItemLike fence, Ingredient material, Ingredient support) {
        return this.fenceBuilder(fence, material, support, this.isWoodenFence(fence) ? 6 : 3);
    }

    protected final void fence(Supplier<? extends ItemLike> fence, Ingredient material, Ingredient support, int count) {
        this.fence(fence.get(), material, support, count);
    }

    protected void fence(ItemLike fence, Ingredient material, Ingredient support, int count) {
        this.fenceBuilder(fence, material, support, count)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder fenceBuilder(Supplier<? extends ItemLike> fence, Ingredient material, Ingredient support, int count) {
        return this.fenceBuilder(fence.get(), material, support, count);
    }

    protected RecipeBuilder fenceBuilder(ItemLike fence, Ingredient material, Ingredient support, int count) {
        return fenceBuilderInternal(fence, material, support, count);
    }

    private static RecipeBuilder fenceBuilderInternal(ItemLike fence, Ingredient material, Ingredient support, int count) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, fence, count)
                   .define('W', material)
                   .define('#', support)
                   .pattern("W#W")
                   .pattern("W#W");
    }

    protected final void fenceGate(Supplier<? extends ItemLike> fenceGate, Ingredient material) {
        this.fenceGate(fenceGate.get(), material);
    }

    protected final void fenceGate(ItemLike fenceGate, Ingredient material) {
        this.fenceGate(fenceGate, material, this.getDefaultStickIngredient());
    }

    protected final RecipeBuilder fenceGateBuilder(Supplier<? extends ItemLike> fenceGate, Ingredient material) {
        return fenceGateBuilder(fenceGate.get(), material);
    }

    protected static RecipeBuilder fenceGateBuilder(ItemLike fenceGate, Ingredient material) {
        return fenceGateBuilderInternal(fenceGate, material, ingredient(Tags.Items.RODS_WOODEN));
    }

    protected final void fenceGate(Supplier<? extends ItemLike> fenceGate, Ingredient material, Ingredient support) {
        this.fenceGate(fenceGate.get(), material, support);
    }

    protected void fenceGate(ItemLike fenceGate, Ingredient material, Ingredient support) {
        this.fenceGateBuilder(fenceGate, material, support)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder fenceGateBuilder(Supplier<? extends ItemLike> fenceGate, Ingredient material, Ingredient support) {
        return this.fenceGateBuilder(fenceGate.get(), material, support);
    }

    protected RecipeBuilder fenceGateBuilder(ItemLike fenceGate, Ingredient material, Ingredient support) {
        return fenceGateBuilderInternal(fenceGate, material, support);
    }

    private static RecipeBuilder fenceGateBuilderInternal(ItemLike fenceGate, Ingredient material, Ingredient support) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.REDSTONE, fenceGate)
                   .define('#', support)
                   .define('W', material)
                   .pattern("#W#")
                   .pattern("#W#");
    }

    protected final void pressurePlate(Supplier<? extends ItemLike> pressurePlate, Ingredient material) {
        this.pressurePlate(pressurePlate.get(), material);
    }

    protected void pressurePlate(ItemLike pressurePlate, Ingredient material) {
        this.pressurePlateBuilder(pressurePlate, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder pressurePlateBuilder(Supplier<? extends ItemLike> pressurePlate, Ingredient material) {
        return this.pressurePlateBuilder(pressurePlate.get(), material);
    }

    protected RecipeBuilder pressurePlateBuilder(ItemLike pressurePlate, Ingredient material) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.REDSTONE, pressurePlate)
                   .define('#', material)
                   .pattern("##");
    }

    protected final void slab(RecipeCategory category, Supplier<? extends ItemLike> slab, Ingredient material) {
        this.slab(category, slab.get(), material);
    }

    protected void slab(RecipeCategory category, ItemLike slab, Ingredient material) {
        slabBuilder(category, slab, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder slabBuilder(RecipeCategory category, Supplier<? extends ItemLike> slab, Ingredient material) {
        return slabBuilder(category, slab.get(), material);
    }

    protected final void stairs(Supplier<? extends ItemLike> stairs, Ingredient material) {
        this.stairs(stairs.get(), material);
    }

    protected void stairs(ItemLike stairs, Ingredient material) {
        this.stairsBuilder(stairs, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder stairsBuilder(Supplier<? extends ItemLike> stairs, Ingredient material) {
        return this.stairsBuilder(stairs.get(), material);
    }

    protected RecipeBuilder stairsBuilder(ItemLike stairs, Ingredient material) {
        return stairBuilder(stairs, material);
    }

    /** @deprecated Use {@link #stairsBuilder(ItemLike, Ingredient)} */
    @Deprecated
    protected final RecipeBuilder stairBuilder(Supplier<? extends ItemLike> stairs, Ingredient material) {
        return this.stairsBuilder(stairs, material);
    }

    protected final void trapdoor(Supplier<? extends ItemLike> trapdoor, Ingredient material) {
        this.trapdoor(trapdoor.get(), material);
    }

    protected void trapdoor(ItemLike trapdoor, Ingredient material) {
        trapdoorBuilder(trapdoor, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder trapdoorBuilder(Supplier<? extends ItemLike> trapdoor, Ingredient material) {
        return trapdoorBuilder(trapdoor.get(), material);
    }

    protected final void sign(Supplier<? extends ItemLike> sign, Ingredient material) {
        this.sign(sign.get(), material);
    }

    protected final void sign(ItemLike sign, Ingredient material) {
        this.sign(sign, material, this.getDefaultStickIngredient());
    }

    protected final RecipeBuilder signBuilder(Supplier<? extends ItemLike> sign, Ingredient material) {
        return signBuilder(sign.get(), material);
    }

    protected static RecipeBuilder signBuilder(ItemLike sign, Ingredient material) {
        return signBuilderInternal(sign, material, ingredient(Tags.Items.RODS_WOODEN));
    }

    protected final void sign(Supplier<? extends ItemLike> sign, Ingredient material, Ingredient stick) {
        this.sign(sign.get(), material, stick);
    }

    protected void sign(ItemLike sign, Ingredient material, Ingredient stick) {
        this.signBuilder(sign, material, stick)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder signBuilder(Supplier<? extends ItemLike> sign, Ingredient material, Ingredient stick) {
        return this.signBuilder(sign.get(), material, stick);
    }

    protected RecipeBuilder signBuilder(ItemLike sign, Ingredient material, Ingredient stick) {
        return signBuilderInternal(sign, material, stick);
    }

    private static RecipeBuilder signBuilderInternal(ItemLike sign, Ingredient material, Ingredient stick) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, sign, 3)
                   .group("sign")
                   .define('#', material)
                   .define('X', stick)
                   .pattern("###")
                   .pattern("###")
                   .pattern(" X ");
    }

    protected final void hangingSign(Supplier<? extends ItemLike> sign, Ingredient material) {
        this.hangingSign(sign.get(), material);
    }

    protected final void hangingSign(ItemLike sign, Ingredient material) {
        this.hangingSign(sign, material, ingredient(Items.CHAIN));
    }

    protected final RecipeBuilder hangingSignBuilder(Supplier<? extends ItemLike> sign, Ingredient material) {
        return this.hangingSignBuilder(sign.get(), material);
    }

    protected final RecipeBuilder hangingSignBuilder(ItemLike sign, Ingredient material) {
        return this.hangingSignBuilder(sign, material, ingredient(Items.CHAIN));
    }

    protected final void hangingSign(Supplier<? extends ItemLike> sign, Ingredient material, Ingredient chain) {
        this.hangingSign(sign.get(), material, chain);
    }

    protected void hangingSign(ItemLike sign, Ingredient material, Ingredient chain) {
        this.hangingSignBuilder(sign, material, chain)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder hangingSignBuilder(Supplier<? extends ItemLike> sign, Ingredient material, Ingredient chain) {
        return this.hangingSignBuilder(sign.get(), material, chain);
    }

    protected RecipeBuilder hangingSignBuilder(ItemLike sign, Ingredient material, Ingredient chain) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, sign, 6)
                   .group("hanging_sign")
                   .define('#', material)
                   .define('X', chain)
                   .pattern("X X")
                   .pattern("###")
                   .pattern("###");
    }

    protected final void coloredWoolFromWhiteWoolAndDye(Supplier<? extends ItemLike> dyedWool, Ingredient dye) {
        this.coloredWoolFromWhiteWoolAndDye(dyedWool.get(), dye);
    }

    protected void coloredWoolFromWhiteWoolAndDye(ItemLike dyedWool, Ingredient dye) {
        this.coloredWoolFromWhiteWoolAndDyeBuilder(dyedWool, dye)
            .group("wool")
            .unlockedBy(getHasName(Blocks.WHITE_WOOL), has(Blocks.WHITE_WOOL))
            .save(this.writer);
    }

    protected final RecipeBuilder coloredWoolFromWhiteWoolAndDyeBuilder(Supplier<? extends ItemLike> dyedWool, Ingredient dye) {
        return this.coloredWoolFromWhiteWoolAndDyeBuilder(dyedWool.get(), dye);
    }

    protected RecipeBuilder coloredWoolFromWhiteWoolAndDyeBuilder(ItemLike dyedWool, Ingredient dye) {
        return ShapelessRecipeBuilder.shapeless(RecipeCategory.BUILDING_BLOCKS, dyedWool)
                   .requires(dye)
                   .requires(Blocks.WHITE_WOOL);
    }

    protected final void carpet(Supplier<? extends ItemLike> carpet, Ingredient material) {
        this.carpet(carpet.get(), material);
    }

    protected void carpet(ItemLike carpet, Ingredient material) {
        this.carpetBuilder(carpet, material)
            .group("carpet")
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder carpetBuilder(Supplier<? extends ItemLike> carpet, Ingredient material) {
        return this.carpetBuilder(carpet.get(), material);
    }

    protected RecipeBuilder carpetBuilder(ItemLike carpet, Ingredient material) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, carpet, 3)
                   .define('#', material)
                   .pattern("##");
    }

    protected final void coloredCarpetFromWhiteCarpetAndDye(Supplier<? extends ItemLike> dyedCarpet, Ingredient dye) {
        this.coloredCarpetFromWhiteCarpetAndDye(dyedCarpet.get(), dye);
    }

    protected void coloredCarpetFromWhiteCarpetAndDye(ItemLike dyedCarpet, Ingredient dye) {
        this.coloredCarpetFromWhiteCarpetAndDyeBuilder(dyedCarpet, dye)
            .group("carpet")
            .unlockedBy(getHasName(Blocks.WHITE_CARPET), has(Blocks.WHITE_CARPET))
            .unlockedBy(getHasName(dye), has(dye))
            .save(this.writer, getName(dyedCarpet) + "_from_" + getName(Blocks.WHITE_CARPET) + "_and_" + getName(dye));
    }

    protected final RecipeBuilder coloredCarpetFromWhiteCarpetAndDyeBuilder(Supplier<? extends ItemLike> dyedCarpet, Ingredient dye) {
        return this.coloredCarpetFromWhiteCarpetAndDyeBuilder(dyedCarpet.get(), dye);
    }

    protected RecipeBuilder coloredCarpetFromWhiteCarpetAndDyeBuilder(ItemLike dyedCarpet, Ingredient dye) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, dyedCarpet, 8)
                   .define('#', Blocks.WHITE_CARPET)
                   .define('$', dye)
                   .pattern("###")
                   .pattern("#$#")
                   .pattern("###");
    }

    protected final void bedFromPlanksAndWool(Supplier<? extends ItemLike> bed, Ingredient wool) {
        this.bedFromPlanksAndWool(bed.get(), wool);
    }

    protected void bedFromPlanksAndWool(ItemLike bed, Ingredient wool) {
        this.bedFromPlanksAndWoolBuilder(bed, wool)
            .group("bed")
            .unlockedBy(getHasName(wool), has(wool))
            .save(this.writer);
    }

    protected final RecipeBuilder bedFromPlanksAndWoolBuilder(Supplier<? extends ItemLike> bed, Ingredient wool) {
        return this.bedFromPlanksAndWoolBuilder(bed.get(), wool);
    }

    protected RecipeBuilder bedFromPlanksAndWoolBuilder(ItemLike bed, Ingredient wool) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, bed).define('#', wool)
                   .define('X', this.getDefaultPlanksIngredient())
                   .pattern("###")
                   .pattern("XXX");
    }

    protected final void bedFromWhiteBedAndDye(Supplier<? extends ItemLike> dyedBed, Ingredient dye) {
        this.bedFromWhiteBedAndDye(dyedBed.get(), dye);
    }

    protected void bedFromWhiteBedAndDye(ItemLike dyedBed, Ingredient dye) {
        this.bedFromWhiteBedAndDyeBuilder(dyedBed, dye)
            .group("dyed_bed")
            .unlockedBy("has_bed", has(Items.WHITE_BED))
            .save(this.writer, getName(dyedBed) + "_from_" + getName(Items.WHITE_BED) + "_and_" + getName(dye));
    }

    protected final RecipeBuilder bedFromWhiteBedAndDyeBuilder(Supplier<? extends ItemLike> dyedBed, Ingredient dye) {
        return this.bedFromWhiteBedAndDyeBuilder(dyedBed.get(), dye);
    }

    protected RecipeBuilder bedFromWhiteBedAndDyeBuilder(ItemLike dyedBed, Ingredient dye) {
        return ShapelessRecipeBuilder.shapeless(RecipeCategory.DECORATIONS, dyedBed)
                   .requires(Items.WHITE_BED)
                   .requires(dye);
    }

    protected final void banner(Supplier<? extends ItemLike> banner, Ingredient material) {
        this.banner(banner.get(), material);
    }

    protected final void banner(ItemLike banner, Ingredient material) {
        this.banner(banner, material, this.getDefaultStickIngredient());
    }

    protected final RecipeBuilder bannerBuilder(Supplier<? extends ItemLike> banner, Ingredient material) {
        return this.bannerBuilder(banner.get(), material);
    }

    protected final RecipeBuilder bannerBuilder(ItemLike banner, Ingredient material) {
        return this.bannerBuilder(banner, material, this.getDefaultStickIngredient());
    }

    protected final void banner(Supplier<? extends ItemLike> banner, Ingredient material, Ingredient stick) {
        this.banner(banner.get(), material, stick);
    }

    protected void banner(ItemLike banner, Ingredient material, Ingredient stick) {
        this.bannerBuilder(banner, material, stick)
            .group("banner")
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final RecipeBuilder bannerBuilder(Supplier<? extends ItemLike> banner, Ingredient material, Ingredient stick) {
        return this.bannerBuilder(banner.get(), material, stick);
    }

    protected RecipeBuilder bannerBuilder(ItemLike banner, Ingredient material, Ingredient stick) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, banner).define('#', material)
                   .define('|', stick)
                   .pattern("###")
                   .pattern("###")
                   .pattern(" | ");
    }

    protected final void stainedGlassFromGlassAndDye(Supplier<? extends ItemLike> stainedGlass, Ingredient dye) {
        this.stainedGlassFromGlassAndDye(stainedGlass.get(), dye);
    }

    protected void stainedGlassFromGlassAndDye(ItemLike stainedGlass, Ingredient dye) {
        this.stainedGlassFromGlassAndDyeBuilder(stainedGlass, dye)
            .group("stained_glass")
            .unlockedBy(getHasName(Blocks.GLASS), has(Blocks.GLASS))
            .save(this.writer);
    }

    protected final RecipeBuilder stainedGlassFromGlassAndDyeBuilder(Supplier<? extends ItemLike> stainedGlass, Ingredient dye) {
        return this.stainedGlassFromGlassAndDyeBuilder(stainedGlass.get(), dye);
    }

    protected RecipeBuilder stainedGlassFromGlassAndDyeBuilder(ItemLike stainedGlass, Ingredient dye) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, stainedGlass, 8)
                   .define('#', Blocks.GLASS)
                   .define('X', dye)
                   .pattern("###")
                   .pattern("#X#")
                   .pattern("###");
    }

    protected final void stainedGlassPaneFromStainedGlass(Supplier<? extends ItemLike> stainedGlassPane, Ingredient stainedGlass) {
        this.stainedGlassPaneFromStainedGlass(stainedGlassPane.get(), stainedGlass);
    }

    protected void stainedGlassPaneFromStainedGlass(ItemLike stainedGlassPane, Ingredient stainedGlass) {
        this.stainedGlassPaneFromStainedGlassBuilder(stainedGlassPane, stainedGlass)
            .group("stained_glass_pane")
            .unlockedBy(getHasName(stainedGlass), has(stainedGlass))
            .save(this.writer);
    }

    protected final RecipeBuilder stainedGlassPaneFromStainedGlassBuilder(Supplier<? extends ItemLike> stainedGlassPane, Ingredient stainedGlass) {
        return this.stainedGlassPaneFromStainedGlassBuilder(stainedGlassPane.get(), stainedGlass);
    }

    protected RecipeBuilder stainedGlassPaneFromStainedGlassBuilder(ItemLike stainedGlassPane, Ingredient stainedGlass) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, stainedGlassPane, 16)
                   .define('#', stainedGlass)
                   .pattern("###")
                   .pattern("###");
    }

    protected final void stainedGlassPaneFromGlassPaneAndDye(Supplier<? extends ItemLike> stainedGlassPane, Ingredient dye) {
        this.stainedGlassPaneFromGlassPaneAndDye(stainedGlassPane.get(), dye);
    }

    protected void stainedGlassPaneFromGlassPaneAndDye(ItemLike stainedGlassPane, Ingredient dye) {
        this.stainedGlassPaneFromGlassPaneAndDyeBuilder(stainedGlassPane, dye)
            .group("stained_glass_pane")
            .unlockedBy(getHasName(Blocks.GLASS_PANE), has(Blocks.GLASS_PANE))
            .unlockedBy(getHasName(dye), has(dye))
            .save(this.writer, getConversionRecipeName(stainedGlassPane, Blocks.GLASS_PANE));
    }

    protected final RecipeBuilder stainedGlassPaneFromGlassPaneAndDyeBuilder(Supplier<? extends ItemLike> stainedGlassPane, Ingredient dye) {
        return this.stainedGlassPaneFromGlassPaneAndDyeBuilder(stainedGlassPane.get(), dye);
    }

    protected RecipeBuilder stainedGlassPaneFromGlassPaneAndDyeBuilder(ItemLike stainedGlassPane, Ingredient dye) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, stainedGlassPane, 8)
                   .define('#', Blocks.GLASS_PANE)
                   .define('$', dye)
                   .pattern("###")
                   .pattern("#$#")
                   .pattern("###");
    }

    protected final void coloredTerracottaFromTerracottaAndDye(Supplier<? extends ItemLike> coloredTerracotta, Ingredient dye) {
        this.coloredTerracottaFromTerracottaAndDye(coloredTerracotta.get(), dye);
    }

    protected void coloredTerracottaFromTerracottaAndDye(ItemLike coloredTerracotta, Ingredient dye) {
        this.coloredTerracottaFromTerracottaAndDyeBuilder(coloredTerracotta, dye)
            .group("stained_terracotta")
            .unlockedBy(getHasName(Blocks.TERRACOTTA), has(Blocks.TERRACOTTA))
            .save(this.writer);
    }

    protected final RecipeBuilder coloredTerracottaFromTerracottaAndDyeBuilder(Supplier<? extends ItemLike> coloredTerracotta, Ingredient dye) {
        return this.coloredTerracottaFromTerracottaAndDyeBuilder(coloredTerracotta.get(), dye);
    }

    protected RecipeBuilder coloredTerracottaFromTerracottaAndDyeBuilder(ItemLike coloredTerracotta, Ingredient dye) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, coloredTerracotta, 8)
                   .define('#', Blocks.TERRACOTTA)
                   .define('X', dye)
                   .pattern("###")
                   .pattern("#X#")
                   .pattern("###");
    }

    protected final void concretePowder(Supplier<? extends ItemLike> dyedConcretePowder, Ingredient dye) {
        this.concretePowder(dyedConcretePowder.get(), dye);
    }

    protected final void concretePowder(ItemLike dyedConcretePowder, Ingredient dye) {
        this.concretePowder(dyedConcretePowder, dye, this.getDefaultSandIngredient(), this.getDefaultGravelIngredient());
    }

    protected final RecipeBuilder concretePowderBuilder(Supplier<? extends ItemLike> dyedConcretePowder, Ingredient dye) {
        return this.concretePowderBuilder(dyedConcretePowder.get(), dye);
    }

    protected RecipeBuilder concretePowderBuilder(ItemLike dyedConcretePowder, Ingredient dye) {
        return this.concretePowderBuilder(dyedConcretePowder, dye, this.getDefaultSandIngredient(), this.getDefaultGravelIngredient());
    }

    protected final void concretePowder(Supplier<? extends ItemLike> dyedConcretePowder, Ingredient dye, Ingredient sand, Ingredient gravel) {
        this.concretePowder(dyedConcretePowder.get(), dye, sand, gravel);
    }

    protected void concretePowder(ItemLike dyedConcretePowder, Ingredient dye, Ingredient sand, Ingredient gravel) {
        this.concretePowderBuilder(dyedConcretePowder, dye, sand, gravel)
            .group("concrete_powder")
            .unlockedBy(getHasName(Blocks.SAND), has(Blocks.SAND))
            .unlockedBy(getHasName(Blocks.GRAVEL), has(Blocks.GRAVEL))
            .save(this.writer);
    }

    protected final RecipeBuilder concretePowderBuilder(Supplier<? extends ItemLike> dyedConcretePowder, Ingredient dye, Ingredient sand, Ingredient gravel) {
        return this.concretePowderBuilder(dyedConcretePowder.get(), dye, sand, gravel);
    }

    protected RecipeBuilder concretePowderBuilder(ItemLike dyedConcretePowder, Ingredient dye, Ingredient sand, Ingredient gravel) {
        return ShapelessRecipeBuilder.shapeless(RecipeCategory.BUILDING_BLOCKS, dyedConcretePowder, 8)
                   .requires(dye)
                   .requires(sand, 4)
                   .requires(gravel, 4);
    }

    protected final void candle(Supplier<? extends ItemLike> candle, Ingredient dye) {
        this.candle(candle.get(), dye);
    }

    protected void candle(ItemLike candle, Ingredient dye) {
        this.candleBuilder(candle, dye)
            .group("dyed_candle")
            .unlockedBy(getHasName(dye), has(dye))
            .save(this.writer);
    }

    protected final RecipeBuilder candleBuilder(Supplier<? extends ItemLike> candle, Ingredient dye) {
        return this.candleBuilder(candle.get(), dye);
    }

    protected RecipeBuilder candleBuilder(ItemLike candle, Ingredient dye) {
        return ShapelessRecipeBuilder.shapeless(RecipeCategory.DECORATIONS, candle)
                   .requires(Blocks.CANDLE)
                   .requires(dye);
    }

    protected void wall(RecipeCategory category, Supplier<? extends ItemLike> wall, Ingredient material) {
        this.wall(category, wall.get(), material);
    }

    protected void wall(RecipeCategory category, ItemLike wall, Ingredient material) {
        wallBuilder(category, wall, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final ShapedRecipeBuilder wallBuilder(RecipeCategory category, Supplier<? extends ItemLike> wall, Ingredient material) {
        return (ShapedRecipeBuilder) wallBuilder(category, wall.get(), material);
    }

    protected void polished(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient material) {
        this.polished(category, result.get(), material);
    }

    protected void polished(RecipeCategory category, ItemLike result, Ingredient material) {
        polishedBuilder(category, result, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final ShapedRecipeBuilder polishedBuilder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient material) {
        return (ShapedRecipeBuilder) polishedBuilder(category, result.get(), material);
    }

    protected void cut(RecipeCategory category, Supplier<? extends ItemLike> cutResult, Ingredient material) {
        this.cut(category, cutResult.get(), material);
    }

    protected void cut(RecipeCategory category, ItemLike cutResult, Ingredient material) {
        cutBuilder(category, cutResult, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final ShapedRecipeBuilder cutBuilder(RecipeCategory category, Supplier<? extends ItemLike> cutResult, Ingredient material) {
        return cutBuilder(category, cutResult.get(), material);
    }

    protected final void chiseled(RecipeCategory category, Supplier<? extends ItemLike> chiseledResult, Ingredient material) {
        this.chiseled(category, chiseledResult.get(), material);
    }

    protected void chiseled(RecipeCategory category, ItemLike chiseledResult, Ingredient material) {
        chiseledBuilder(category, chiseledResult, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected final ShapedRecipeBuilder chiseledBuilder(RecipeCategory category, Supplier<? extends ItemLike> chiseledResult, Ingredient material) {
        return chiseledBuilder(category, chiseledResult.get(), material);
    }

    protected void mosaic(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient material) {
        this.mosaic(category, result.get(), material);
    }

    protected void mosaic(RecipeCategory category, ItemLike result, Ingredient material) {
        this.mosaicBuilder(category, result, material)
            .unlockedBy(getHasName(material), has(material))
            .save(this.writer);
    }

    protected ShapedRecipeBuilder mosaicBuilder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient material) {
        return this.mosaicBuilder(category, result.get(), material);
    }

    protected ShapedRecipeBuilder mosaicBuilder(RecipeCategory category, ItemLike result, Ingredient material) {
        return ShapedRecipeBuilder.shaped(category, result)
                   .define('#', material)
                   .pattern("#")
                   .pattern("#");
    }


    protected final void storage2x2(RecipeCategory unpackedCategory, Supplier<? extends ItemLike> unpacked, RecipeCategory packedCategory, Supplier<? extends ItemLike> packed) {
        this.storage2x2(unpackedCategory, unpacked.get(), packedCategory, packed.get());
    }

    protected final void storage2x2(RecipeCategory unpackedCategory, Supplier<? extends ItemLike> unpacked, RecipeCategory packedCategory, ItemLike packed) {
        this.storage2x2(unpackedCategory, unpacked.get(), packedCategory, packed);
    }

    protected final void storage2x2(RecipeCategory unpackedCategory, ItemLike unpacked, RecipeCategory packedCategory, Supplier<? extends ItemLike> packed) {
        this.storage2x2(unpackedCategory, unpacked, packedCategory, packed.get());
    }

    protected void storage2x2(RecipeCategory unpackedCategory, ItemLike unpacked, RecipeCategory packedCategory, ItemLike packed) {
        this.shapelessBuilder(unpackedCategory, unpacked, ingredient(packed), 4)
            .unlockedBy(getHasName(packed), has(packed))
            .save(this.writer, getConversionRecipeName(packed, unpacked));
        this.generic2x2Builder(packedCategory, packed, ingredient(unpacked), 1)
            .unlockedBy(getHasName(unpacked), has(unpacked))
            .save(this.writer, getConversionRecipeName(unpacked, packed));
    }

    protected final void storage3x3(RecipeCategory unpackedCategory, Supplier<? extends ItemLike> unpacked, RecipeCategory packedCategory, Supplier<? extends ItemLike> packed) {
        this.storage3x3(unpackedCategory, unpacked.get(), packedCategory, packed.get());
    }

    protected final void storage3x3(RecipeCategory unpackedCategory, Supplier<? extends ItemLike> unpacked, RecipeCategory packedCategory, ItemLike packed) {
        this.storage3x3(unpackedCategory, unpacked.get(), packedCategory, packed);
    }

    protected final void storage3x3(RecipeCategory unpackedCategory, ItemLike unpacked, RecipeCategory packedCategory, Supplier<? extends ItemLike> packed) {
        this.storage3x3(unpackedCategory, unpacked, packedCategory, packed.get());
    }

    protected void storage3x3(RecipeCategory unpackedCategory, ItemLike unpacked, RecipeCategory packedCategory, ItemLike packed) {
        this.shapelessBuilder(unpackedCategory, unpacked, ingredient(packed), 9)
            .unlockedBy(getHasName(packed), has(packed))
            .save(this.writer, getConversionRecipeName(packed, unpacked));
        this.generic3x3Builder(packedCategory, packed, ingredient(unpacked), 1)
            .unlockedBy(getHasName(unpacked), has(unpacked))
            .save(this.writer, getConversionRecipeName(unpacked, packed));
    }

    protected final void copySmithingTemplate(Supplier<? extends ItemLike> template, Ingredient baseItem) {
        this.copySmithingTemplate(template.get(), baseItem);
    }

    protected final void copySmithingTemplate(ItemLike template, Ingredient baseItem) {
        this.copySmithingTemplate(template, baseItem, this.getDefaultDiamondIngredient());
    }

    protected final RecipeBuilder copySmithingTemplateBuilder(Supplier<? extends ItemLike> template, Ingredient baseItem) {
        return this.copySmithingTemplateBuilder(template.get(), baseItem);
    }

    protected final RecipeBuilder copySmithingTemplateBuilder(ItemLike template, Ingredient baseItem) {
        return this.copySmithingTemplateBuilder(template, baseItem, this.getDefaultDiamondIngredient());
    }

    protected final void copySmithingTemplate(Supplier<? extends ItemLike> template, Ingredient baseItem, Ingredient material) {
        this.copySmithingTemplate(template.get(), material, baseItem);
    }

    protected void copySmithingTemplate(ItemLike template, Ingredient material, Ingredient baseItem) {
        this.copySmithingTemplateBuilder(template, material, baseItem)
            .unlockedBy(getHasName(template), has(template))
            .save(this.writer);
    }

    protected final RecipeBuilder copySmithingTemplateBuilder(Supplier<? extends ItemLike> template, Ingredient baseItem, Ingredient material) {
        return this.copySmithingTemplateBuilder(template.get(), material, baseItem);
    }

    protected RecipeBuilder copySmithingTemplateBuilder(ItemLike template, Ingredient baseItem, Ingredient material) {
        return ShapedRecipeBuilder.shaped(RecipeCategory.MISC, template, 2)
                   .define('#', material)
                   .define('C', baseItem)
                   .define('S', template)
                   .pattern("#S#")
                   .pattern("#C#")
                   .pattern("###");
    }


    /* STONECUTTING */

    protected final void stonecutting(RecipeCategory category, Supplier<? extends ItemLike> result, Supplier<? extends ItemLike> material) {
        this.stonecutting(category, result.get(), material.get());
    }

    protected final void stonecutting(RecipeCategory category, Supplier<? extends ItemLike> result, ItemLike material) {
        this.stonecutting(category, result.get(), material);
    }

    protected final void stonecutting(RecipeCategory category, ItemLike result, Supplier<? extends ItemLike> material) {
        this.stonecutting(category, result, material.get());
    }

    protected final void stonecutting(RecipeCategory category, ItemLike result, ItemLike material) {
        this.stonecutting(category, result, material, 1);
    }

    protected final void stonecutting(RecipeCategory category, Supplier<? extends ItemLike> result, Supplier<? extends ItemLike> material, Number resultCount) {
        this.stonecutting(category, result.get(), material.get(), resultCount);
    }

    protected final void stonecutting(RecipeCategory category, Supplier<? extends ItemLike> result, ItemLike material, Number resultCount) {
        this.stonecutting(category, result.get(), material, resultCount);
    }

    protected final void stonecutting(RecipeCategory category, ItemLike result, Supplier<? extends ItemLike> material, Number resultCount) {
        this.stonecutting(category, result, material.get(), resultCount);
    }

    protected void stonecutting(RecipeCategory category, ItemLike result, ItemLike material, Number resultCount) {
        this.stonecutter.register(category, result, material, resultCount.intValue());
    }


    /* COOKING */

    protected final void cooking(Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience) {
        this.cooking(result.get(), ingredient, experience);
    }

    protected void cooking(ItemLike result, Ingredient ingredient, Number experience) {
        this.smeltingOnly(RecipeCategory.FOOD, result, ingredient, experience);
        this.smokingOnly(RecipeCategory.FOOD, result, ingredient, experience);
        this.campfireCookingOnly(result, ingredient, experience);
    }

    protected final void smoking(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience) {
        this.smoking(category, result.get(), ingredient, experience);
    }

    protected void smoking(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience) {
        this.smokingOnly(category, result, ingredient, experience);
        this.smeltingOnly(category, result, ingredient, experience);
    }

    protected final void blasting(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience) {
        this.blasting(category, result.get(), ingredient, experience);
    }

    protected void blasting(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience) {
        this.blastingOnly(category, result, ingredient, experience);
        this.smeltingOnly(category, result, ingredient, experience);
    }

    protected final void campfireCookingOnly(Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience) {
        this.campfireCookingOnly(result.get(), ingredient, experience);
    }

    protected final void campfireCookingOnly(ItemLike result, Ingredient ingredient, Number experience) {
        this.campfireCookingOnly(result, ingredient, experience, this.getDefaultCampfireCookingTime());
    }

    protected final RecipeBuilder campfireCookingBuilder(Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience) {
        return this.campfireCookingBuilder(result.get(), ingredient, experience);
    }

    protected final RecipeBuilder campfireCookingBuilder(ItemLike result, Ingredient ingredient, Number experience) {
        return this.campfireCookingBuilder(result, ingredient, experience, this.getDefaultCampfireCookingTime());
    }

    protected final void campfireCookingOnly(Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        this.campfireCookingOnly(result.get(), ingredient, experience, cookingTime);
    }

    protected void campfireCookingOnly(ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        this.campfireCookingBuilder(result, ingredient, experience, cookingTime)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient) + "_from_campfire_cooking");
    }

    protected final RecipeBuilder campfireCookingBuilder(Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        return this.campfireCookingBuilder(result.get(), ingredient, experience, cookingTime);
    }

    protected RecipeBuilder campfireCookingBuilder(ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        return SimpleCookingRecipeBuilder.campfireCooking(ingredient, RecipeCategory.FOOD, result, experience.floatValue(), cookingTime.intValue());
    }

    protected final void blastingOnly(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience) {
        this.blastingOnly(category, result.get(), ingredient, experience);
    }

    protected final void blastingOnly(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience) {
        this.blastingOnly(category, result, ingredient, experience, this.getDefaultBlastingTime());
    }

    protected final RecipeBuilder blastingBuilder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience) {
        return this.blastingBuilder(category, result.get(), ingredient, experience);
    }

    protected final RecipeBuilder blastingBuilder(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience) {
        return this.blastingBuilder(category, result, ingredient, experience, this.getDefaultBlastingTime());
    }

    protected final void blastingOnly(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        this.blastingOnly(category, result.get(), ingredient, experience, cookingTime);
    }

    protected void blastingOnly(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        this.blastingBuilder(category, result, ingredient, experience, cookingTime)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient) + "_from_blasting");
    }

    protected final RecipeBuilder blastingBuilder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        return this.blastingBuilder(category, result.get(), ingredient, experience, cookingTime);
    }

    protected RecipeBuilder blastingBuilder(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        return SimpleCookingRecipeBuilder.blasting(ingredient, category, result, experience.floatValue(), cookingTime.intValue());
    }

    protected final void smeltingOnly(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience) {
        this.smeltingOnly(category, result.get(), ingredient, experience);
    }

    protected final void smeltingOnly(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience) {
        this.smeltingOnly(category, result, ingredient, experience, this.getDefaultSmeltingTime());
    }

    protected final RecipeBuilder smeltingBuilder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience) {
        return this.smeltingBuilder(category, result.get(), ingredient, experience);
    }

    protected final RecipeBuilder smeltingBuilder(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience) {
        return this.smeltingBuilder(category, result, ingredient, experience, this.getDefaultSmeltingTime());
    }

    protected final void smeltingOnly(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        this.smeltingOnly(category, result.get(), ingredient, experience, cookingTime);
    }

    protected void smeltingOnly(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        this.smeltingBuilder(category, result, ingredient, experience, cookingTime)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient) + "_from_smelting");
    }

    protected final RecipeBuilder smeltingBuilder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        return this.smeltingBuilder(category, result.get(), ingredient, experience, cookingTime);
    }

    protected RecipeBuilder smeltingBuilder(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        return SimpleCookingRecipeBuilder.smelting(ingredient, category, result, experience.floatValue(), cookingTime.intValue());
    }

    protected final void smokingOnly(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience) {
        this.smokingOnly(category, result.get(), ingredient, experience);
    }

    protected final void smokingOnly(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience) {
        this.smokingOnly(category, result, ingredient, experience, this.getDefaultSmokingTime());
    }

    protected final RecipeBuilder smokingBuilder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience) {
        return this.smokingBuilder(category, result.get(), ingredient, experience);
    }

    protected final RecipeBuilder smokingBuilder(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience) {
        return this.smokingBuilder(category, result, ingredient, experience, this.getDefaultSmokingTime());
    }

    protected final void smokingOnly(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        this.smokingOnly(category, result.get(), ingredient, experience, cookingTime);
    }

    protected void smokingOnly(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        SimpleCookingRecipeBuilder.smoking(ingredient, category, result, experience.floatValue(), cookingTime.intValue())
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient) + "_from_smoking");
    }

    protected final RecipeBuilder smokingBuilder(RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        return this.smokingBuilder(category, result.get(), ingredient, experience, cookingTime);
    }

    protected RecipeBuilder smokingBuilder(RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        return SimpleCookingRecipeBuilder.smoking(ingredient, category, result, experience.floatValue(), cookingTime.intValue());
    }

    protected final void customCooking(Supplier<? extends RecipeSerializer<? extends AbstractCookingRecipe>> cookingSerializer, RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        this.customCooking(cookingSerializer.get(), category, result.get(), ingredient, experience, cookingTime);
    }

    protected final void customCooking(Supplier<? extends RecipeSerializer<? extends AbstractCookingRecipe>> cookingSerializer, RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        this.customCooking(cookingSerializer.get(), category, result, ingredient, experience, cookingTime);
    }

    protected final void customCooking(RecipeSerializer<? extends AbstractCookingRecipe> cookingSerializer, RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        this.customCooking(cookingSerializer, category, result.get(), ingredient, experience, cookingTime);
    }

    protected void customCooking(RecipeSerializer<? extends AbstractCookingRecipe> cookingSerializer, RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        var cookingMethod = Objects.requireNonNull(ForgeRegistries.RECIPE_SERIALIZERS.getKey(cookingSerializer), "Cooking serializer must be registered").getPath();
        this.customCookingBuilder(cookingSerializer, category, result, ingredient, experience, cookingTime)
            .unlockedBy(getHasName(ingredient), has(ingredient))
            .save(this.writer, getConversionRecipeName(result, ingredient) + "_from_" + cookingMethod);
    }

    protected final RecipeBuilder customCookingBuilder(Supplier<? extends RecipeSerializer<? extends AbstractCookingRecipe>> cookingSerializer, RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        return this.customCookingBuilder(cookingSerializer.get(), category, result.get(), ingredient, experience, cookingTime);
    }

    protected final RecipeBuilder customCookingBuilder(Supplier<? extends RecipeSerializer<? extends AbstractCookingRecipe>> cookingSerializer, RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        return this.customCookingBuilder(cookingSerializer.get(), category, result, ingredient, experience, cookingTime);
    }

    protected final RecipeBuilder customCookingBuilder(RecipeSerializer<? extends AbstractCookingRecipe> cookingSerializer, RecipeCategory category, Supplier<? extends ItemLike> result, Ingredient ingredient, Number experience, Number cookingTime) {
        return this.customCookingBuilder(cookingSerializer, category, result.get(), ingredient, experience, cookingTime);
    }

    protected RecipeBuilder customCookingBuilder(RecipeSerializer<? extends AbstractCookingRecipe> cookingSerializer, RecipeCategory category, ItemLike result, Ingredient ingredient, Number experience, Number cookingTime) {
        return SimpleCookingRecipeBuilder.generic(ingredient, category, result, experience.floatValue(), cookingTime.intValue(), cookingSerializer);
    }


    /* STONECUTTING PROCESSING */

    /**
     * Stonecutting recipes are handled by this inner class in partial isolation from the main provider. It is
     * responsible for recursively creating transitive stonecutting recipes.
     * <br>
     * For example, if Nightstone can stonecut to Nightstone Bricks, and Nightstone Bricks can stonecut to Nightstone
     * Brick Slabs, then this provider will automatically generate the recipe that allows Nightstone to stonecut to
     * Nightstone Brick Slabs.
     *
     * @author Shadew
     * @author Jonathing
     */
    // TODO clean up... maybe...
    protected class Stonecutter {
        private final Map<ItemLike, Map<ItemLike, Pair<RecipeCategory, Integer>>> recipes = new HashMap<>();
        private final Map<ItemLike, List<Triple<ItemLike, RecipeCategory, Integer>>> compiled = new HashMap<>();
        private final Set<String> flushed = new HashSet<>();

        public void register(RecipeCategory category, ItemLike result, ItemLike material, int count) {
            this.recipes.computeIfAbsent(material, k -> Maps.newHashMap()).put(result, Pair.of(category, count));
        }

        public final void buildRecipes() {
            this.compile();
            this.flush();
        }

        /**
         * Compiles recipes from {@link #recipes} to {@link #compiled}. This takes each map for each item and transforms
         * each entry into a triple containing the resultant item with it. These triples are then put into another map
         * much like the original.
         */
        protected void compile() {
            for (var item : this.recipes.keySet()) {
                var results = new ArrayList<Triple<ItemLike, RecipeCategory, Integer>>();
                this.compile(item, 1, results);
                this.compiled.put(item, results);
            }
        }

        protected void compile(ItemLike from, int countMul, List<Triple<ItemLike, RecipeCategory, Integer>> out) {
            var to = this.recipes.get(from);
            for (Map.Entry<ItemLike, Pair<RecipeCategory, Integer>> entry : to.entrySet()) {
                var item = entry.getKey();
                var category = entry.getValue().getFirst();
                int count = entry.getValue().getSecond() * countMul;
                out.add(Triple.of(item, category, count));

                // Recursively walk recipes map
                // If you can stonecut stone to bricks and bricks to bricks slab, you can stonecut stone to bricks slab
                if (this.recipes.containsKey(item)) {
                    this.compile(item, count, out);
                }
            }
        }

        protected void flush() {
            for (var entry : this.compiled.entrySet()) {
                var from = entry.getKey();

                var results = entry.getValue();
                for (var result : results) {
                    var to = result.getLeft();
                    var category = result.getMiddle();
                    int count = result.getRight();

                    var name = getConversionRecipeName(from, to) + "_from_stonecutting";

                    // Sometimes multiple ways lead to the same item, meaning a recipe can be registered multiple times.
                    // We track all the recipe IDs we flushed in a set so that we only register recipes that we didn't
                    // register already. We can assume that when a recipe is registered twice, they are exactly the same and
                    // the latter one can be safely omitted...
                    if (!this.flushed.contains(name)) {
                        SingleItemRecipeBuilder.stonecutting(ingredient(from), category, to, count)
                            .unlockedBy(getHasName(to), has(from))
                            .save(CrypticRecipeProvider.this.writer, name);
                        this.flushed.add(name);
                    }
                }
            }
        }
    }


    /* INGREDIENTS */

    @SafeVarargs
    protected static Ingredient ingredient(Supplier<? extends ItemLike>... item) {
        return Ingredient.of(Arrays.stream(item).map(Supplier::get).toArray(ItemLike[]::new));
    }

    protected static Ingredient ingredient(ItemLike... item) {
        return Ingredient.of(item);
    }

    protected static Ingredient ingredient(ItemStack... item) {
        return Ingredient.of(item);
    }

    protected static Ingredient ingredient(Stream<ItemStack> item) {
        return Ingredient.of(item);
    }

    protected static Ingredient ingredient(TagKey<Item> item) {
        return Ingredient.of(item);
    }

    @SafeVarargs
    protected static Ingredient ingredient(TagKey<Item>... item) {
        return ingredient(Arrays.stream(item).map(Ingredient::of).toArray(Ingredient[]::new));
    }

    protected static Ingredient ingredient(Ingredient... item) {
        return CompoundIngredient.of(item);
    }


    /* TRIGGERS */

    protected static EnterBlockTrigger.TriggerInstance insideOf(Supplier<? extends Block> block) {
        return RecipeProvider.insideOf(block.get());
    }

    protected static InventoryChangeTrigger.TriggerInstance has(MinMaxBounds.Ints count, Supplier<? extends ItemLike> item) {
        return RecipeProvider.has(count, item.get());
    }

    protected static InventoryChangeTrigger.TriggerInstance has(Supplier<? extends ItemLike> item) {
        return RecipeProvider.has(item.get());
    }

    protected static InventoryChangeTrigger.TriggerInstance has(Ingredient ingredient) {
        final var tags = new HashSet<TagKey<Item>>();
        final var itemLikes = new HashSet<ItemLike>();
        for (Ingredient.Value value : ingredient.values) {
            if (value instanceof Ingredient.TagValue tag) {
                tags.add(tag.tag);
            } else {
                value.getItems().forEach(i -> itemLikes.add(i.getItem()));
            }
        }

        final var predicates = new ArrayList<ItemPredicate>(tags.size() + itemLikes.size());
        tags.forEach(tag -> predicates.add(ItemPredicate.Builder.item().of(tag).build()));
        itemLikes.forEach(item -> predicates.add(ItemPredicate.Builder.item().of(item).build()));

        return inventoryTrigger(predicates.toArray(ItemPredicate[]::new));
    }


    /* NAMING */

    public static String getHasName(Supplier<? extends ItemLike> item) {
        return RecipeProvider.getHasName(item.get());
    }

    public static String getHasName(TagKey<Item> item) {
        return "has_" + getName(item);
    }

    public static String getHasName(Ingredient item) {
        return "has_" + ((CrypticIngredient) item).cmreg$getName();
    }

    @SafeVarargs
    public static String getName(Supplier<? extends ItemLike>... item) {
        return getName(Arrays.stream(item).map(Supplier::get).toArray(ItemLike[]::new));
    }

    public static String getName(Supplier<? extends ItemLike> item) {
        return getName(item.get());
    }

    @SuppressWarnings("DuplicatedCode")
    public static String getName(ItemLike... item) {
        if (item.length == 0) return "empty";

        var builder = new StringBuilder();
        for (int i = 0; i < item.length; i++) {
            builder.append(getName(item[i]));
            if (i != item.length - 1)
                builder.append("_and_");
        }
        return builder.toString();
    }

    @SuppressWarnings("DataFlowIssue")
    public static String getName(ItemLike item) {
        return ForgeRegistries.ITEMS.getKey(item.asItem()).getPath();
    }

    public static String getName(ItemStack... item) {
        return getName(Arrays.stream(item).map(ItemStack::getItem).toArray(Item[]::new));
    }

    public static String getName(ItemStack item) {
        return getName(item.getItem());
    }

    @SafeVarargs
    @SuppressWarnings("DuplicatedCode")
    public static String getName(TagKey<Item>... tag) {
        if (tag.length == 0) return "empty";

        var builder = new StringBuilder();
        for (int i = 0; i < tag.length; i++) {
            builder.append(getName(tag[i]));
            if (i != tag.length - 1)
                builder.append("_and_");
        }
        return builder.toString();
    }

    public static String getName(TagKey<Item> tag) {
        return tag.location().getPath();
    }

    @SuppressWarnings("DuplicatedCode")
    public static String getName(Ingredient... ingredient) {
        if (ingredient.length == 0) return "empty";

        var builder = new StringBuilder();
        for (int i = 0; i < ingredient.length; i++) {
            builder.append(getName(ingredient[i]));
            if (i != ingredient.length - 1)
                builder.append("_and_");
        }
        return builder.toString();
    }

    public static String getName(Ingredient ingredient) {
        return ((CrypticIngredient) ingredient).cmreg$getName();
    }

    public static String getSimpleRecipeName(Supplier<? extends ItemLike> item) {
        return RecipeProvider.getSimpleRecipeName(item.get());
    }

    public static String getConversionRecipeName(Supplier<? extends ItemLike> result, Supplier<? extends ItemLike> ingredient) {
        return RecipeProvider.getConversionRecipeName(result.get(), ingredient.get());
    }

    @SafeVarargs
    public static String getConversionRecipeName(Supplier<? extends ItemLike> result, Supplier<? extends ItemLike>... ingredient) {
        return getName(result) + "_from_" + getName(ingredient);
    }

    @SafeVarargs
    public static String getConversionRecipeName(ItemLike result, Supplier<? extends ItemLike>... ingredient) {
        return getName(result) + "_from_" + getName(ingredient);
    }

    public static String getConversionRecipeName(Supplier<? extends ItemLike> result, ItemLike... ingredient) {
        return getName(result) + "_from_" + getName(ingredient);
    }

    public static String getConversionRecipeName(ItemLike result, ItemLike... ingredient) {
        return getName(result) + "_from_" + getName(ingredient);
    }

    @SafeVarargs
    public static String getConversionRecipeName(Supplier<? extends ItemLike> result, TagKey<Item>... ingredient) {
        return getName(result) + "_from_" + getName(ingredient);
    }

    @SafeVarargs
    public static String getConversionRecipeName(ItemLike result, TagKey<Item>... ingredient) {
        return getName(result) + "_from_" + getName(ingredient);
    }

    public static String getConversionRecipeName(Supplier<? extends ItemLike> result, TagKey<Item> ingredient) {
        return getName(result) + "_from_" + ingredient.location().getPath();
    }

    public static String getConversionRecipeName(ItemLike result, TagKey<Item> ingredient) {
        return getName(result) + "_from_" + ingredient.location().getPath();
    }

    public static String getConversionRecipeName(Supplier<? extends ItemLike> result, Ingredient ingredient) {
        return getName(result) + "_from_" + getName(ingredient);
    }

    public static String getConversionRecipeName(ItemLike result, Ingredient ingredient) {
        return getName(result) + "_from_" + getName(ingredient);
    }

    public static String getConversionRecipeName(Supplier<? extends ItemLike> result, Ingredient... ingredient) {
        return getName(result) + "_from_" + getName(ingredient);
    }

    public static String getConversionRecipeName(ItemLike result, Ingredient... ingredient) {
        return getName(result) + "_from_" + getName(ingredient);
    }

    public static String getSmeltingRecipeName(Supplier<? extends ItemLike> item) {
        return RecipeProvider.getSmeltingRecipeName(item.get());
    }

    public static String getBlastingRecipeName(Supplier<? extends ItemLike> item) {
        return RecipeProvider.getBlastingRecipeName(item.get());
    }
}
