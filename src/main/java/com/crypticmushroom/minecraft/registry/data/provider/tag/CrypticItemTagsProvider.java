package com.crypticmushroom.minecraft.registry.data.provider.tag;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.TagRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.registries.RegistriesDatapackGenerator;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.data.event.GatherDataEvent;

import java.util.concurrent.CompletableFuture;

/**
 * This provider exists as the item tag implementation of a {@link ICrypticTagsProvider} using an
 * {@link ItemTagsProvider} as the superclass. The easiest way to generate your mod's tags (given you are using the
 * builders properly) is with
 * {@link CrypticRegistry#addAllTagProviders(String, GatherDataEvent, RegistriesDatapackGenerator)}.
 *
 * @author Jonathing
 */
public class CrypticItemTagsProvider extends ItemTagsProvider implements ICrypticIntrinsicHolderTagsProvider<Item> {
    private final String modId;
    private final GatherDataEvent event;

    public CrypticItemTagsProvider(String modId, GatherDataEvent event, CompletableFuture<TagLookup<Block>> blockTags) {
        super(event.getGenerator().getPackOutput(), event.getLookupProvider(), blockTags, modId, event.getExistingFileHelper());

        this.event = event;
        this.modId = modId;
    }

    @Override
    public TagRegistry.Item getTagRegistry() {
        return (TagRegistry.Item) TagRegistry.get(RegistryDirectory.ITEM);
    }

    @Override
    public String getModId() {
        return this.modId;
    }

    @Override
    public String getName() {
        return ICrypticIntrinsicHolderTagsProvider.super.getName();
    }

    @Override
    public GatherDataEvent getEvent() {
        return this.event;
    }

    @Override
    protected void addTags(HolderLookup.Provider provider) {
        this.copyBlockTags(provider);

        this.addRegisteredTags(provider);
    }

    // NOTE: All entries that are copied from block tags contain our blocks only.
    private void copyBlockTags(HolderLookup.Provider provider) {
        DataRegistry.forLinkedTags(this.getModId(), this::copy);
    }

    @Override
    public void add(TagKey<Item> tag) {
        this.tag(tag);
    }

    @Override
    public void add(TagKey<Item> parent, TagKey<Item> child) {
        this.tag(parent).addTag(child);
    }

    @Override
    public void add(TagKey<Item> tag, Item item) {
        this.tag(tag).add(item);
    }
}
