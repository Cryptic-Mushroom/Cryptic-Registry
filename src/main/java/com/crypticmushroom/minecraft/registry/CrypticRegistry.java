package com.crypticmushroom.minecraft.registry;

import com.crypticmushroom.minecraft.registry.coremod.hook.impl.CrypticDeferredRegister;
import com.crypticmushroom.minecraft.registry.coremod.hook.impl.CrypticForgeItemTagsProvider;
import com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft.accessor.RegistriesDatapackGeneratorAccessor;
import com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft.accessor.TagsProviderAccessor;
import com.crypticmushroom.minecraft.registry.coremod.reflection.ReflectionUtil;
import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import com.crypticmushroom.minecraft.registry.data.provider.lang.CrypticLanguageProvider;
import com.crypticmushroom.minecraft.registry.data.provider.metadata.CrypticPackMetadataGenerator;
import com.crypticmushroom.minecraft.registry.data.provider.tag.CrypticBlockTagsProvider;
import com.crypticmushroom.minecraft.registry.data.provider.tag.CrypticItemTagsProvider;
import com.crypticmushroom.minecraft.registry.data.provider.tag.CrypticTagsProvider;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.TagRegistry;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceRegister;
import com.crypticmushroom.minecraft.registry.data.resource.CustomResourceRegister;
import com.crypticmushroom.minecraft.registry.data.tag.CrypticTagClass;
import com.crypticmushroom.minecraft.registry.util.Register;
import com.crypticmushroom.minecraft.registry.util.PluralString;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.google.common.collect.ImmutableList;
import com.google.common.reflect.Reflection;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Lifecycle;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.data.registries.RegistriesDatapackGenerator;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.data.tags.UpdateOneTwentyItemTagsProvider;
import net.minecraft.data.tags.VanillaItemTagsProvider;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.data.ForgeItemTagsProvider;
import net.minecraftforge.common.util.NonNullSupplier;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * The heart of the Cryptic Registry library mod.
 *
 * @author Jonathing
 */
public final class CrypticRegistry {
    @Internal public static final Logger LOGGER = LogManager.getLogger("CrypticRegistry");
    @Internal public static final Marker MARKER = MarkerManager.getMarker("REGISTRY");

    private static final HashMap<String, ModInfo> MOD_INFOS = new HashMap<>();
    private static final RegistryMap.Deferred DEFERRED_REGISTERS = new RegistryMap.Deferred();
    private static final RegistryMap.DataResource DATA_RESOURCE_REGISTERS = new RegistryMap.DataResource();
    private static final RegistryMap.CustomResource CUSTOM_RESOURCE_REGISTERS = new RegistryMap.CustomResource();
    private static final DataBuilderMap DATA_BUILDERS = new DataBuilderMap();
    private static final ClassMap CLASSES = new ClassMap();
    private static final ModIdNamedMap<NonNullSupplier<? extends CrypticTagClass<?>>> TAG_CLASSES = new ModIdNamedMap<>();

    public static <T> DeferredRegister<T> create(RegistryInfo.Static<T> registry) {
        return create(registry, null, null);
    }

    public static <T> DeferredRegister<T> create(RegistryInfo.Static<T> registry, @Nullable Class<?> clazz) {
        return create(registry, clazz, null);
    }

    public static <T> DeferredRegister<T> create(RegistryInfo.Static<T> registry, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        return create(registry, null, tagClass);
    }

    public static <T> DeferredRegister<T> create(RegistryInfo.Static<T> registry, @Nullable Class<?> clazz, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        // create the deferred register
        var modId = attemptGetModIdFromAnnotation();
        var register = DeferredRegister.create(registry.getKey(), modId);
        return create(modId, registry, register, clazz, tagClass);
    }

    public static <T> DeferredRegister<T> createOptional(RegistryInfo.Static<T> registry) {
        return createOptional(registry, null, null);
    }

    public static <T> DeferredRegister<T> createOptional(RegistryInfo.Static<T> registry, @Nullable Class<?> clazz) {
        return createOptional(registry, clazz, null);
    }

    public static <T> DeferredRegister<T> createOptional(RegistryInfo.Static<T> registry, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        return createOptional(registry, null, tagClass);
    }

    public static <T> DeferredRegister<T> createOptional(RegistryInfo.Static<T> registry, @Nullable Class<?> clazz, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        // create the deferred register
        var modId = attemptGetModIdFromAnnotation();
        var register = DeferredRegister.createOptional(registry.getKey(), modId);
        return create(modId, registry, register, clazz, tagClass);
    }

    private static <T> DeferredRegister<T> create(String modId, RegistryInfo.Static<T> registry, DeferredRegister<T> register, @Nullable Class<?> clazz, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        // save the register for later
        DEFERRED_REGISTERS.compute(modId, registry.name, register);

        // add the class to the list of classes to initialize
        if (clazz != null) CLASSES.put(modId, registry.name, clazz);
        if (tagClass != null) TAG_CLASSES.put(modId, registry.name, tagClass);

        return register;
    }


    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry) {
        return create(registry, Lifecycle.stable());
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, Lifecycle lifecycle) {
        return create(registry, lifecycle, (Class<?>) null);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        return create(registry, Lifecycle.stable(), tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, Lifecycle lifecycle, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        return create(registry, lifecycle, (Class<?>) null, tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass, Lifecycle lifecycle) {
        return create(registry, lifecycle, tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, @Nullable Class<?> clazz) {
        return create(registry, Lifecycle.stable(), clazz);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, Lifecycle lifecycle, @Nullable Class<?> clazz) {
        return create(registry, lifecycle, clazz, (NonNullSupplier<? extends CrypticTagClass<?>>) null);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, @Nullable Class<?> clazz, Lifecycle lifecycle) {
        return create(registry, lifecycle, clazz);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, @Nullable Class<?> clazz, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        return create(registry, Lifecycle.stable(), clazz, tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, @Nullable Class<?> clazz, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass, Lifecycle lifecycle) {
        return create(registry, lifecycle, clazz, tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, Lifecycle lifecycle, @Nullable Class<?> clazz, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        // save the register for later
        var modId = attemptGetModIdFromAnnotation();
        var register = new DataResourceRegister<>(registry, modId, lifecycle);
        return create(modId, registry, register, lifecycle, clazz, tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, RegistrySetBuilder.RegistryBootstrap<T> bootstrap) {
        return create(registry, bootstrap, (Lifecycle) null);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, RegistrySetBuilder.RegistryBootstrap<T> bootstrap, Lifecycle lifecycle) {
        return create(registry, bootstrap, lifecycle, null);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, Lifecycle lifecycle, RegistrySetBuilder.RegistryBootstrap<T> bootstrap) {
        return create(registry, bootstrap, lifecycle, null);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, RegistrySetBuilder.RegistryBootstrap<T> bootstrap, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        return create(registry, bootstrap, Lifecycle.stable(), tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, RegistrySetBuilder.RegistryBootstrap<T> bootstrap, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass, Lifecycle lifecycle) {
        return create(registry, bootstrap, lifecycle, tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, Lifecycle lifecycle, RegistrySetBuilder.RegistryBootstrap<T> bootstrap, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        return create(registry, bootstrap, lifecycle, tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, RegistrySetBuilder.RegistryBootstrap<T> bootstrap, Lifecycle lifecycle, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        // save the register for later
        var modId = attemptGetModIdFromAnnotation();
        var register = new DataResourceRegister<>(registry, modId, lifecycle) {
            @Override
            protected void bootstrap(BootstapContext<T> ctx) {
                bootstrap.run(ctx);
                super.bootstrap(ctx);
            }
        };
        return create(modId, registry, register, lifecycle, null, tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, @Nullable Class<?> clazz, RegistrySetBuilder.RegistryBootstrap<T> bootstrap) {
        return create(registry, clazz, bootstrap, (Lifecycle) null);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, @Nullable Class<?> clazz, RegistrySetBuilder.RegistryBootstrap<T> bootstrap, Lifecycle lifecycle) {
        return create(registry, clazz, bootstrap, lifecycle, null);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, Lifecycle lifecycle, @Nullable Class<?> clazz, RegistrySetBuilder.RegistryBootstrap<T> bootstrap) {
        return create(registry, clazz, bootstrap, lifecycle, null);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, @Nullable Class<?> clazz, RegistrySetBuilder.RegistryBootstrap<T> bootstrap, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        return create(registry, clazz, bootstrap, Lifecycle.stable(), tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, @Nullable Class<?> clazz, RegistrySetBuilder.RegistryBootstrap<T> bootstrap, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass, Lifecycle lifecycle) {
        return create(registry, clazz, bootstrap, lifecycle, tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, Lifecycle lifecycle, @Nullable Class<?> clazz, RegistrySetBuilder.RegistryBootstrap<T> bootstrap, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        return create(registry, clazz, bootstrap, lifecycle, tagClass);
    }

    public static <T> DataResourceRegister<T> create(RegistryInfo.Dynamic<T> registry, @Nullable Class<?> clazz, RegistrySetBuilder.RegistryBootstrap<T> bootstrap, Lifecycle lifecycle, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        // save the register for later
        var modId = attemptGetModIdFromAnnotation();
        var register = new DataResourceRegister<>(registry, modId, lifecycle) {
            @Override
            protected void bootstrap(BootstapContext<T> ctx) {
                bootstrap.run(ctx);
                super.bootstrap(ctx);
            }
        };
        return create(modId, registry, register, lifecycle, clazz, tagClass);
    }

    private static <T> DataResourceRegister<T> create(String modId, RegistryInfo.Dynamic<T> registry, DataResourceRegister<T> register, Lifecycle lifecycle, @Nullable Class<?> clazz, @Nullable NonNullSupplier<? extends CrypticTagClass<?>> tagClass) {
        // save the register for later
        DATA_RESOURCE_REGISTERS.put(modId, registry.name, register);

        // register the bootstrap
        DATA_BUILDERS.put(modId, register, lifecycle);

        // add the class to the list of classes to initialize
        if (clazz != null) CLASSES.put(modId, registry.name, clazz);
        if (tagClass != null) TAG_CLASSES.put(modId, registry.name, tagClass);

        return register;
    }


    public static <T> CustomResourceRegister<T, ?> create(RegistryInfo.Custom<T> registry) {
        return create(registry, null);
    }

    public static <T> CustomResourceRegister<T, ?> create(RegistryInfo.Custom<T> registry, @Nullable Class<?> clazz) {
        var modId = attemptGetModIdFromAnnotation();
        var register = registry.makeRegister(modId);

        CUSTOM_RESOURCE_REGISTERS.put(modId, registry.name, register);

        // add the class to the list of classes to initialize
        if (clazz != null) CLASSES.put(modId, registry.name, clazz);

        return register;
    }


    /**
     * This method prepares Cryptic Registry's registration functions for the given mod's ID and its event bus. This
     * should be called by the end-developer in their mod class's constructor with their appropriate registry class and
     * event bus.
     * <p>
     * This method will register all enabled deferred registers (tied with the mod ID from the registry class) with the
     * given event bus, as well as statically initialize all registered classes and
     * {@link CrypticTagClass tag classes}.
     *
     * @param clazz    The main registry class to initialize and prepare for Cryptic Registry (must be annotated with
     *                 {@link CrypticRegistryClass}).
     * @param eventBus The event bus to register the mod's deferred registers to.
     */
    public static void prepare(Class<?> clazz, IEventBus eventBus) {
        if (!clazz.isAnnotationPresent(CrypticRegistryClass.class)) {
            throw new IllegalArgumentException("Cannot prepare Cryptic Registry with a class that is not annotated with @CrypticRegistryClass");
        }

        // make sure our classes are already initialized
        Reflection.initialize(DataRegistry.class, TagRegistry.class, LocalizedNameRegistry.class);

        // initialize the desired class
        Reflection.initialize(clazz);

        // get the mod ID
        var classInfo = clazz.getAnnotation(CrypticRegistryClass.class);
        var modId = classInfo.modId();
        MOD_INFOS.put(modId, new ModInfo(
            modId,
            classInfo.name()
        ));

        // register deferred registries to event bus
        DEFERRED_REGISTERS.forEachCasted(modId, (name, register) -> {
            LOGGER.debug(MARKER, "Adding deferred registry to the event bus: {} - {}", modId, name.plural());
            register.register(eventBus);
        });

        // register static registries to event bus
        CUSTOM_RESOURCE_REGISTERS.forEach(modId, (name, register) -> {
            LOGGER.debug(MARKER, "Adding static registry to the event bus: {} - {}", modId, name.plural());
            register.register(eventBus);
        });

        // register other event listeners
        eventBus.<CreativeModeTabEvent.BuildContents>addListener(event -> onCreativeModeTabBuildContents(modId, event));

        // Initialize classes
        CLASSES.forEach(modId, (name, linkedClass) -> {
            LOGGER.debug(MARKER, "Initializing linked registry class: {} - {} - {}", modId, name.plural(), linkedClass.getName());
            Reflection.initialize(linkedClass);
        });
        TAG_CLASSES.forEach(modId, (name, supplier) -> {
            var tagClass = supplier.get();
            LOGGER.debug(MARKER, "Data registering tags in tag class: {} - {} - {}", modId, name.plural(), tagClass.getClass().getName());
            tagClass.dataRegisterTags();
        });
    }

    private static void onCreativeModeTabBuildContents(String modId, CreativeModeTabEvent.BuildContents event) {
        DataRegistry.forCreativeModeTabBuildOptions(modId, (item, options) -> {
            if (options.tab() == null || !options.tab().get().equals(event.getTab())) return;

            @Nullable var stack = options.conditions().apply(event.getParameters(), event.getEntries(), item.get());
            if (stack != null) event.accept(stack, options.visibility());
        });
    }

    public static String attemptGetModIdFromAnnotation() {
        try {
            @Nullable Class<?> clazz = null;
            for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
                clazz = Class.forName(element.getClassName());
                if (!clazz.isAnnotationPresent(CrypticRegistryClass.class)) {
                    clazz = null;
                } else {
                    break;
                }
            }

            if (clazz == null) {
                throw new IllegalAccessException("Failed to find a @CrypticRegistryClass annotation present in the current thread");
            }

            return clazz.getAnnotation(CrypticRegistryClass.class).modId();
        } catch (ClassNotFoundException | IllegalAccessException e) {
            throw new RuntimeException("Failed to create a CrypticRegistry deferred register", e);
        }
    }

    public static @Nullable String attemptGetModIdFromSavedClass() {
        for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
            var modId = CLASSES.contains(element.getClassName());
            if (modId != null) {
                return modId;
            }
        }

        return null;
    }

    public static ModInfo getModInfo(String modId) {
        @Nullable var info = MOD_INFOS.get(modId);
        if (info == null) {
            if (!ReflectionUtil.threadContains(ForgeItemTagsProvider.class, "m_6055_"))
                LOGGER.warn("Attempted to get mod info that doesn't exist for mod ID: {}", modId);
            return new ModInfo(modId);
        }

        return info;
    }

    /**
     * Gets the deferred register based on the given mod ID and registry key.
     *
     * @param modId    The mod ID to find the deferred register for.
     * @param registry The registry info whose type to search for.
     * @param <T>      The type of the deferred register.
     * @return The deferred register if found, or {@code null} otherwise.
     */
    public static <T> @Nullable DeferredRegister<T> getRegister(String modId, RegistryInfo.Static<T> registry) {
        return DEFERRED_REGISTERS.getRegistry(modId, registry.getKey());
    }

    public static <T, A> @Nullable CustomResourceRegister<T, A> getRegister(String modId, RegistryInfo.Custom<T> registry) {
        return CUSTOM_RESOURCE_REGISTERS.getRegistry(modId, registry);
    }

    public static <T> @Nullable DataResourceRegister<T> getRegister(String modId, RegistryInfo.Dynamic<T> registry) {
        return DATA_RESOURCE_REGISTERS.getRegistry(modId, registry.getKey());
    }

    public static RegistrySetBuilder getDataBuilder(String modId) {
        return DATA_BUILDERS.get(modId);
    }

    public record ModInfo(String modId, String name) implements Comparable<ModInfo> {
        public ModInfo(String modId) {
            this(modId, modId);
        }

        @Override
        public int compareTo(ModInfo o) {
            return this.modId.compareTo(o.modId);
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof ModInfo o && this.modId.equals(o.modId);
        }
    }


    public static void addAllTagProviders(String modId, GatherDataEvent event, RegistriesDatapackGenerator registries) {
        var mod = new CrypticBlockTagsProvider(modId, event);
        mod.addToGenerator();
        new CrypticItemTagsProvider(modId, event, mod.contentsGetter()).addToGenerator();
        var forge = (CrypticForgeItemTagsProvider) (Object) new ForgeItemTagsProvider(event.getGenerator().getPackOutput(), event.getLookupProvider(), mod.contentsGetter(), event.getExistingFileHelper());
        forge.cmreg$configure(event, modId);
        forge.addToGenerator();
        var vanilla = new AdditionalItemTags.Vanilla(modId, event, mod.contentsGetter());
        vanilla.addToGenerator();
        var update120 = new AdditionalItemTags.UpdateOneTwenty(modId, event, vanilla.contentsGetter(), mod.contentsGetter());
        update120.addToGenerator();

        TagRegistry.forEachNormalRegistry(tagReg -> new CrypticTagsProvider<>(modId, event, tagReg).addToGenerator());
        TagRegistry.forEachDataRegistry(tagReg -> new CrypticTagsProvider<>(modId, event, tagReg, ((RegistriesDatapackGeneratorAccessor) registries).cmreg$registries()).addToGenerator());
    }

    private static final class AdditionalItemTags {
        private static final class Vanilla extends VanillaItemTagsProvider implements ICrypticDataProvider {
            private final GatherDataEvent event;
            private final String modId;

            @SuppressWarnings({"unchecked", "DataFlowIssue"})
            public Vanilla(String modId, GatherDataEvent event, CompletableFuture<TagLookup<Block>> blockTags) {
                super(event.getGenerator().getPackOutput(), event.getLookupProvider(), blockTags);
                ((TagsProviderAccessor<Item>) (Object) this).cmreg$existingFileHelper(event.getExistingFileHelper());
                this.event = event;
                this.modId = modId;
            }

            @Override
            public String getModId() {
                return this.modId;
            }

            @Override
            public String getName() {
                return ICrypticDataProvider.super.getName();
            }

            @Override
            public String getSimpleName() {
                return "Additional Item Tags - Minecraft";
            }

            @Override
            public GatherDataEvent getEvent() {
                return this.event;
            }

            @Override
            public boolean shouldRun() {
                return this.getEvent().includeServer();
            }
        }

        private static final class UpdateOneTwenty extends UpdateOneTwentyItemTagsProvider implements ICrypticDataProvider {
            private final GatherDataEvent event;
            private final String modId;

            @SuppressWarnings({"unchecked", "DataFlowIssue"})
            public UpdateOneTwenty(String modId, GatherDataEvent event, CompletableFuture<TagsProvider.TagLookup<Item>> vanillaItemTags, CompletableFuture<TagLookup<Block>> blockTags) {
                super(event.getGenerator().getPackOutput(), event.getLookupProvider(), vanillaItemTags, blockTags);
                ((TagsProviderAccessor<Item>) (Object) this).cmreg$existingFileHelper(event.getExistingFileHelper());
                this.event = event;
                this.modId = modId;
            }

            @Override
            public String getModId() {
                return this.modId;
            }

            @Override
            public String getName() {
                return ICrypticDataProvider.super.getName();
            }

            @Override
            public String getSimpleName() {
                return "Additional Item Tags - Minecraft 1.20";
            }

            @Override
            public GatherDataEvent getEvent() {
                return this.event;
            }

            @Override
            public boolean shouldRun() {
                return this.getEvent().includeServer();
            }
        }
    }


    private static class RegistryMap<R extends Register<?>> extends ModIdNamedMap<R> {
        final Map<String, Map<ResourceKey<? extends Registry<?>>, R>> registers = new HashMap<>();

        @Override
        public List<Pair<PluralString, R>> put(String modId, PluralString name, R register) {
            var result = super.put(modId, name, register);
            this.registers.compute(modId, (k, v) -> {
                var map = v != null ? v : new HashMap<ResourceKey<? extends Registry<?>>, R>();
                map.compute(register.getRegistryKey(), (k1, v1) -> {
                    if (v1 != null) {
                        LOGGER.warn(MARKER, "Adding register with already added resource key: {}", k1);
                        LOGGER.warn(MARKER, "This might cause some problems with the Registry Object Builder.");
                        LOGGER.warn(MARKER, "Please use only one Deferred Register per registry type!");
                        return v1;
                    }

                    return register;
                });
                return map;
            });
            return result;
        }

        @SuppressWarnings("unchecked")
        protected <T, C> C getRegistry(String modId, ResourceKey<? extends Registry<T>> registryKey) {
            return (C) this.registers.compute(modId, (k, v) -> v != null ? v : new HashMap<>()).get(registryKey);
        }

        private static class Deferred extends RegistryMap<CrypticDeferredRegister<?>> {
            public List<Pair<PluralString, CrypticDeferredRegister<?>>> compute(String modId, PluralString name, DeferredRegister<?> register) {
                return super.put(modId, name, (CrypticDeferredRegister<?>) register);
            }

            public void forEachCasted(String modId, BiConsumer<PluralString, DeferredRegister<?>> action) {
                super.forEach(modId, (name, register) -> action.accept(name, (DeferredRegister<?>) register));
            }
        }

        private static class CustomResource extends ModIdNamedMap<CustomResourceRegister<?, ?>> {
            @SuppressWarnings("unchecked")
            protected <T, A> @Nullable CustomResourceRegister<T, A> getRegistry(String modId, RegistryInfo.Custom<T> registryKey) {
                AtomicReference<CustomResourceRegister<T, A>> result = new AtomicReference<>();
                this.compute(modId, (k, v) -> v != null ? v : new LinkedList<>()).forEach(p -> {
                    if (p.getSecond().registry == registryKey) {
                        result.set((CustomResourceRegister<T, A>) p.getSecond());
                    }
                });
                return result.get();
            }

            public void forEach(String modId, Consumer<CustomResourceRegister<?, ?>> action) {
                this.forEach(modId, (name, register) -> action.accept(register));
            }
        }

        private static class DataResource extends RegistryMap<DataResourceRegister<?>> { }
    }

    private static class DataBuilderMap extends HashMap<String, RegistrySetBuilder> {
        public <T> RegistrySetBuilder put(String modId, DataResourceRegister<T> register, Lifecycle lifecycle) {
            return this.put(modId, register.getRegistryKey(), register, lifecycle);
        }

        public <T> RegistrySetBuilder put(String modId, ResourceKey<? extends Registry<T>> registryKey, RegistrySetBuilder.RegistryBootstrap<T> register, Lifecycle lifecycle) {
            return super.compute(modId, (k, v) -> {
                var builder = v != null ? v : new RegistrySetBuilder();
                builder.add(registryKey, lifecycle, register);
                return builder;
            });
        }
    }

    private static class ClassMap extends ModIdNamedMap<Class<?>> {
        public @Nullable String contains(String name) {
            for (Entry<String, List<Pair<PluralString, Class<?>>>> entry : this.entrySet()) {
                for (Pair<PluralString, Class<?>> pair : entry.getValue()) {
                    if (pair.getSecond().getName().equals(name)) return entry.getKey();
                }
            }

            return null;
        }
    }

    private static class ModIdNamedMap<T> extends HashMap<String, List<Pair<PluralString, T>>> {
        public List<Pair<PluralString, T>> put(String modId, PluralString name, T object) {
            return this.compute(modId, (k, v) -> {
                var list = v != null ? v : new LinkedList<Pair<PluralString, T>>();
                list.add(Pair.of(name, object));
                return list;
            });
        }

        public void forEach(String modId, BiConsumer<PluralString, T> action) {
            this.getOrDefault(modId, ImmutableList.of()).forEach(pair -> action.accept(pair.getFirst(), pair.getSecond()));
        }

        public @Nullable String contains(T object) {
            for (Entry<String, List<Pair<PluralString, T>>> entry : this.entrySet()) {
                for (Pair<PluralString, T> pair : entry.getValue()) {
                    if (object.equals(pair.getSecond())) return entry.getKey();
                }
            }

            return null;
        }
    }


    @Internal
    @Mod(CrypticRegistryInfo.MOD_ID)
    public static final class ForgeMod {
        public ForgeMod() {
            MOD_INFOS.put(CrypticRegistryInfo.MOD_ID, new ModInfo(CrypticRegistryInfo.MOD_ID, CrypticRegistryInfo.NAME));

            FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onGatherData);
        }

        private void onGatherData(GatherDataEvent event) {
            new CrypticPackMetadataGenerator(CrypticRegistryInfo.MOD_ID, event).addToGenerator();
            new CrypticLanguageProvider(CrypticRegistryInfo.MOD_ID, event, "en_us").addToGenerator();
        }
    }
}
