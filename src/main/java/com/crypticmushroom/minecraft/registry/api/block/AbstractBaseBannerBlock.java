package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.world.item.BannerItem;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.BannerBlock;
import net.minecraft.world.level.block.WallBannerBlock;

import java.util.function.Supplier;

public abstract class AbstractBaseBannerBlock extends BannerBlock implements MakesCustomBlockItem<BannerItem> {
    public AbstractBaseBannerBlock(DyeColor color, Properties properties) {
        super(color, properties);
    }

    protected abstract WallBannerBlock getWallBanner();

    @Override
    public BannerItem cmreg$makeBlockItem(Item.Properties properties) {
        return new BannerItem(this, this.getWallBanner(), properties.stacksTo(16));
    }

    public static class Basic extends AbstractBaseBannerBlock {
        private final Supplier<? extends WallBannerBlock> wallBanner;

        public Basic(DyeColor color, Properties properties, Supplier<? extends WallBannerBlock> wallBanner) {
            super(color, properties);
            this.wallBanner = wallBanner;
        }

        @Override
        protected WallBannerBlock getWallBanner() {
            return this.wallBanner.get();
        }
    }
}
