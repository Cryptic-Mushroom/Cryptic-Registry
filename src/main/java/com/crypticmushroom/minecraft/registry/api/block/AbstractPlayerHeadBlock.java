package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.PlayerHeadItem;
import net.minecraft.world.level.block.PlayerHeadBlock;
import net.minecraft.world.level.block.PlayerWallHeadBlock;

import java.util.function.Supplier;

public abstract class AbstractPlayerHeadBlock extends PlayerHeadBlock implements MakesCustomBlockItem<PlayerHeadItem> {
    public AbstractPlayerHeadBlock(Properties properties) {
        super(properties);
    }

    protected abstract PlayerWallHeadBlock getWallSkull();

    @Override
    public PlayerHeadItem cmreg$makeBlockItem(Item.Properties properties) {
        return new PlayerHeadItem(this, this.getWallSkull(), properties);
    }

    public static class Basic extends AbstractPlayerHeadBlock {
        private final Supplier<? extends PlayerWallHeadBlock> wallSkull;

        public Basic(Properties properties, Supplier<? extends PlayerWallHeadBlock> wallSkull) {
            super(properties);
            this.wallSkull = wallSkull;
        }

        @Override
        protected PlayerWallHeadBlock getWallSkull() {
            return this.wallSkull.get();
        }
    }
}
