package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.core.Direction;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.StandingAndWallBlockItem;
import net.minecraft.world.level.block.BaseCoralFanBlock;
import net.minecraft.world.level.block.BaseCoralWallFanBlock;

import java.util.function.Supplier;

public abstract class AbstractBaseCoralFanBlock extends BaseCoralFanBlock implements MakesCustomBlockItem<StandingAndWallBlockItem> {
    public AbstractBaseCoralFanBlock(Properties properties) {
        super(properties);
    }

    protected abstract BaseCoralWallFanBlock getWallFan();

    @Override
    public StandingAndWallBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new StandingAndWallBlockItem(this, this.getWallFan(), properties, Direction.DOWN);
    }

    public static class Basic extends AbstractBaseCoralFanBlock {
        private final Supplier<? extends BaseCoralWallFanBlock> wallFan;

        public Basic(Properties properties, Supplier<? extends BaseCoralWallFanBlock> wallFan) {
            super(properties);
            this.wallFan = wallFan;
        }

        @Override
        protected BaseCoralWallFanBlock getWallFan() {
            return this.wallFan.get();
        }
    }
}
