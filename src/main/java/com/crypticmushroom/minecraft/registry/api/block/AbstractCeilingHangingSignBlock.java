package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.world.item.HangingSignItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.CeilingHangingSignBlock;
import net.minecraft.world.level.block.WallHangingSignBlock;
import net.minecraft.world.level.block.state.properties.WoodType;

import java.util.function.Supplier;

public abstract class AbstractCeilingHangingSignBlock extends CeilingHangingSignBlock implements MakesCustomBlockItem<HangingSignItem> {
    public AbstractCeilingHangingSignBlock(Properties properties, WoodType type) {
        super(properties, type);
    }

    protected abstract WallHangingSignBlock getWallSign();

    @Override
    public HangingSignItem cmreg$makeBlockItem(Item.Properties properties) {
        return new HangingSignItem(this, this.getWallSign(), properties.stacksTo(16));
    }

    public static class Basic extends AbstractCeilingHangingSignBlock {
        private final Supplier<? extends WallHangingSignBlock> wallSign;

        public Basic(Properties properties, WoodType type, Supplier<? extends WallHangingSignBlock> wallSign) {
            super(properties, type);
            this.wallSign = wallSign;
        }

        @Override
        protected WallHangingSignBlock getWallSign() {
            return this.wallSign.get();
        }
    }
}
