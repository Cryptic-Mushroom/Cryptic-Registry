package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.core.Direction;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.StandingAndWallBlockItem;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CoralFanBlock;
import net.minecraft.world.level.block.CoralWallFanBlock;

import java.util.function.Supplier;

public abstract class AbstractCoralFanBlock<B extends Block> extends CoralFanBlock implements CoralHasCustomDeadBlock<B>, MakesCustomBlockItem<StandingAndWallBlockItem> {
    public AbstractCoralFanBlock(Properties properties) {
        super(null, properties);
    }

    protected abstract CoralWallFanBlock getWallFan();

    @Override
    public abstract B getDeadBlock();

    @Override
    public StandingAndWallBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new StandingAndWallBlockItem(this, this.getWallFan(), properties, Direction.DOWN);
    }

    public static class Basic<B extends Block> extends AbstractCoralFanBlock<B> {
        private final Supplier<? extends CoralWallFanBlock> wallFan;
        private final Supplier<B> deadBlock;

        public Basic(Properties properties, Supplier<? extends CoralWallFanBlock> wallFan, Supplier<B> deadBlock) {
            super(properties);
            this.wallFan = wallFan;
            this.deadBlock = deadBlock;
        }

        @Override
        protected CoralWallFanBlock getWallFan() {
            return this.wallFan.get();
        }

        @Override
        public B getDeadBlock() {
            return this.deadBlock.get();
        }
    }
}
