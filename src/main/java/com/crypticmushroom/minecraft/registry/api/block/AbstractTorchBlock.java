package com.crypticmushroom.minecraft.registry.api.block;

import com.crypticmushroom.minecraft.registry.util.Lazy;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.StandingAndWallBlockItem;
import net.minecraft.world.level.block.TorchBlock;
import net.minecraft.world.level.block.WallTorchBlock;

import java.util.function.Supplier;

public abstract class AbstractTorchBlock extends TorchBlock implements TorchHasCustomFlameParticle, MakesCustomBlockItem<StandingAndWallBlockItem> {
    @SuppressWarnings("DataFlowIssue")
    public AbstractTorchBlock(Properties properties) {
        super(properties, null);
    }

    public ParticleOptions getFlameParticle() {
        return ParticleTypes.FLAME;
    }

    protected abstract WallTorchBlock getWallTorch();

    @Override
    public StandingAndWallBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new StandingAndWallBlockItem(this, this.getWallTorch(), properties, Direction.DOWN);
    }

    public static class Basic extends AbstractTorchBlock {
        private final Supplier<? extends WallTorchBlock> wallTorch;
        private final Lazy<? extends ParticleOptions> flameParticle;

        public Basic(Properties properties, Supplier<? extends WallTorchBlock> wallTorch) {
            this(properties, wallTorch, () -> ParticleTypes.FLAME);
        }

        public Basic(Properties properties, Supplier<? extends WallTorchBlock> wallTorch, Supplier<? extends ParticleOptions> flameParticle) {
            super(properties);
            this.wallTorch = wallTorch;
            this.flameParticle = Lazy.of(flameParticle);
        }

        public ParticleOptions getFlameParticle() {
            return this.flameParticle.get();
        }

        @Override
        protected WallTorchBlock getWallTorch() {
            return this.wallTorch.get();
        }
    }
}
