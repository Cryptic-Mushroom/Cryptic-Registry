package com.crypticmushroom.minecraft.registry.api.block;

import com.crypticmushroom.minecraft.registry.util.Lazy;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.StandingAndWallBlockItem;
import net.minecraft.world.level.block.RedstoneTorchBlock;
import net.minecraft.world.level.block.RedstoneWallTorchBlock;

import java.util.function.Supplier;

public abstract class AbstractRedstoneTorchBlock extends RedstoneTorchBlock implements TorchHasCustomFlameParticle, MakesCustomBlockItem<StandingAndWallBlockItem> {
    public AbstractRedstoneTorchBlock(Properties properties) {
        super(properties);
    }

    public ParticleOptions getFlameParticle() {
        return DustParticleOptions.REDSTONE;
    }

    protected abstract RedstoneWallTorchBlock getWallTorch();

    @Override
    public StandingAndWallBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new StandingAndWallBlockItem(this, this.getWallTorch(), properties, Direction.DOWN);
    }

    public static class Basic extends AbstractRedstoneTorchBlock {
        private final Supplier<? extends RedstoneWallTorchBlock> wallTorch;
        private final Lazy<? extends ParticleOptions> flameParticle;

        public Basic(Properties properties, Supplier<? extends RedstoneWallTorchBlock> wallTorch) {
            this(properties, wallTorch, () -> DustParticleOptions.REDSTONE);
        }

        public Basic(Properties properties, Supplier<? extends RedstoneWallTorchBlock> wallTorch, Supplier<? extends ParticleOptions> flameParticle) {
            super(properties);
            this.wallTorch = wallTorch;
            this.flameParticle = Lazy.of(flameParticle);
        }

        public ParticleOptions getFlameParticle() {
            return this.flameParticle.get();
        }

        @Override
        protected RedstoneWallTorchBlock getWallTorch() {
            return this.wallTorch.get();
        }
    }
}
