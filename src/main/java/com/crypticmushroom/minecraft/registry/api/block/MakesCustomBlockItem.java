package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import org.spongepowered.asm.mixin.Unique;

@Unique
public interface MakesCustomBlockItem<I extends BlockItem> {
    I cmreg$makeBlockItem(Item.Properties properties);
}
