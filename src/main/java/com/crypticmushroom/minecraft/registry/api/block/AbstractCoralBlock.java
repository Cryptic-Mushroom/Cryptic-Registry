package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CoralBlock;

import java.util.function.Supplier;

public abstract class AbstractCoralBlock<B extends Block> extends CoralBlock implements CoralHasCustomDeadBlock<B> {
    public AbstractCoralBlock(Properties properties) {
        super(null, properties);
    }

    @Override
    public abstract B getDeadBlock();

    public static class Basic<B extends Block> extends AbstractCoralBlock<B> {
        private final Supplier<B> deadBlock;

        public Basic(Properties properties, Supplier<B> deadBlock) {
            super(properties);
            this.deadBlock = deadBlock;
        }

        @Override
        public B getDeadBlock() {
            return this.deadBlock.get();
        }
    }
}
