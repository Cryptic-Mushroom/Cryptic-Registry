package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.core.Direction;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.StandingAndWallBlockItem;
import net.minecraft.world.level.block.SkullBlock;
import net.minecraft.world.level.block.WallSkullBlock;

import java.util.function.Supplier;

public abstract class AbstractBaseSkullBlock extends SkullBlock implements MakesCustomBlockItem<StandingAndWallBlockItem> {
    public AbstractBaseSkullBlock(Type type, Properties properties) {
        super(type, properties);
    }

    protected abstract WallSkullBlock getWallSkull();

    @Override
    public StandingAndWallBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new StandingAndWallBlockItem(this, this.getWallSkull(), properties, Direction.DOWN);
    }

    public static class Basic extends AbstractBaseSkullBlock {
        private final Supplier<? extends WallSkullBlock> wallSkull;

        public Basic(Type type, Properties properties, Supplier<? extends WallSkullBlock> wallSkull) {
            super(type, properties);
            this.wallSkull = wallSkull;
        }

        @Override
        protected WallSkullBlock getWallSkull() {
            return this.wallSkull.get();
        }
    }
}
