package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CoralWallFanBlock;

import java.util.function.Supplier;

public abstract class AbstractCoralWallFanBlock<B extends Block> extends CoralWallFanBlock implements CoralHasCustomDeadBlock<B> {
    public AbstractCoralWallFanBlock(Properties properties) {
        super(null, properties);
    }

    @Override
    public abstract B getDeadBlock();

    public static class Basic<B extends Block> extends AbstractCoralWallFanBlock<B> {
        private final Supplier<B> deadBlock;

        public Basic(Properties properties, Supplier<B> deadBlock) {
            super(properties);
            this.deadBlock = deadBlock;
        }

        @Override
        public B getDeadBlock() {
            return this.deadBlock.get();
        }
    }
}
