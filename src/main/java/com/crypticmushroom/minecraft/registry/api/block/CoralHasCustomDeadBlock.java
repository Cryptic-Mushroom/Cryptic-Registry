package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.world.level.block.Block;

public interface CoralHasCustomDeadBlock<B extends Block> {
    B getDeadBlock();
}
