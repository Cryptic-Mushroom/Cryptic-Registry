package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.core.particles.ParticleOptions;

public interface TorchHasCustomFlameParticle {
    ParticleOptions getFlameParticle();
}
