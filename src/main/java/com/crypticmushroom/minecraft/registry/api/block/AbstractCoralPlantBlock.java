package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CoralPlantBlock;

import java.util.function.Supplier;

public abstract class AbstractCoralPlantBlock<B extends Block> extends CoralPlantBlock implements CoralHasCustomDeadBlock<B> {
    public AbstractCoralPlantBlock(Properties properties) {
        super(null, properties);
    }

    @Override
    public abstract B getDeadBlock();

    public static class Basic<B extends Block> extends AbstractCoralPlantBlock<B> {
        private final Supplier<B> deadBlock;

        public Basic(Properties properties, Supplier<B> deadBlock) {
            super(properties);
            this.deadBlock = deadBlock;
        }

        @Override
        public B getDeadBlock() {
            return this.deadBlock.get();
        }
    }
}
