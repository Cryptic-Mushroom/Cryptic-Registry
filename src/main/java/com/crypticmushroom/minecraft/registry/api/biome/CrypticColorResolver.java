package com.crypticmushroom.minecraft.registry.api.biome;

import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.level.ColorResolver;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.common.util.LogicalSidedProvider;
import net.minecraftforge.fml.LogicalSide;

/**
 * This interface acts as a spiritual extension of {@link ColorResolver}, with the main difference being that a
 * {@link Holder Holder&lt;Biome&gt;} is used instead of the {@link Biome} itself.
 */
@FunctionalInterface
public interface CrypticColorResolver extends ColorResolver {
    int getColor(Holder<Biome> biome, double x, double z);

    @Override
    default int getColor(Biome biome, double x, double z) {
        Holder<Biome> holder = Holder.direct(biome);

        // try and get a reference holder if we're on server
        if (holder instanceof Holder.Direct<Biome>) {
            try {
                var registry = ((MinecraftServer) LogicalSidedProvider.WORKQUEUE.get(LogicalSide.SERVER)).registryAccess().registryOrThrow(RegistryDirectory.BIOME.getKey());
                holder = registry.getHolderOrThrow(registry.getResourceKey(biome).orElseThrow());
            } catch (Throwable ignored) { }
        }

        // try and get a reference holder if we're on client
        if (holder instanceof Holder.Direct<Biome>) {
            holder = attemptGetHolderFromClient(holder, x, z);
        }

        return this.getColor(holder, x, z);
    }

    // exists so we don't attempt to load client classes on server
    private static Holder<Biome> attemptGetHolderFromClient(Holder<Biome> original, double x, double z) {
        var level = Minecraft.getInstance().level;
        var player = Minecraft.getInstance().player;
        if (level == null || player == null) return original;

        return level.getBiome(BlockPos.containing(x, player.getBlockY(), z));
    }
}
