package com.crypticmushroom.minecraft.registry.api.block;

import com.crypticmushroom.minecraft.registry.util.Lazy;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.world.level.block.WallTorchBlock;

import java.util.function.Supplier;

public abstract class AbstractWallTorchBlock extends WallTorchBlock implements TorchHasCustomFlameParticle {
    @SuppressWarnings("DataFlowIssue")
    public AbstractWallTorchBlock(Properties properties) {
        super(properties, null);
    }

    public ParticleOptions getFlameParticle() {
        return ParticleTypes.FLAME;
    }

    public static class Basic extends AbstractWallTorchBlock {
        private final Lazy<? extends ParticleOptions> flameParticle;

        public Basic(Properties properties) {
            this(properties, () -> ParticleTypes.FLAME);
        }

        public Basic(Properties properties, Supplier<? extends ParticleOptions> flameParticle) {
            super(properties);
            this.flameParticle = Lazy.of(flameParticle);
        }

        public ParticleOptions getFlameParticle() {
            return this.flameParticle.get();
        }
    }
}
