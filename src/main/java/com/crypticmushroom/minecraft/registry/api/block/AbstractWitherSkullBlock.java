package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.core.Direction;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.StandingAndWallBlockItem;
import net.minecraft.world.level.block.WitherSkullBlock;
import net.minecraft.world.level.block.WitherWallSkullBlock;

import java.util.function.Supplier;

public abstract class AbstractWitherSkullBlock extends WitherSkullBlock implements MakesCustomBlockItem<StandingAndWallBlockItem> {
    public AbstractWitherSkullBlock(Properties properties) {
        super(properties);
    }

    protected abstract WitherWallSkullBlock getWallSkull();

    @Override
    public StandingAndWallBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new StandingAndWallBlockItem(this, this.getWallSkull(), properties, Direction.DOWN);
    }

    public static class Basic extends AbstractWitherSkullBlock {
        private final Supplier<? extends WitherWallSkullBlock> wallSkull;

        public Basic(Properties properties, Supplier<? extends WitherWallSkullBlock> wallSkull) {
            super(properties);
            this.wallSkull = wallSkull;
        }

        @Override
        protected WitherWallSkullBlock getWallSkull() {
            return this.wallSkull.get();
        }
    }
}
