package com.crypticmushroom.minecraft.registry.api.block;

import com.crypticmushroom.minecraft.registry.util.Lazy;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.world.level.block.RedstoneWallTorchBlock;

import java.util.function.Supplier;

public abstract class AbstractRedstoneWallTorchBlock extends RedstoneWallTorchBlock implements TorchHasCustomFlameParticle {
    public AbstractRedstoneWallTorchBlock(Properties properties) {
        super(properties);
    }

    public ParticleOptions getFlameParticle() {
        return DustParticleOptions.REDSTONE;
    }

    public static class Basic extends AbstractRedstoneWallTorchBlock {
        private final Lazy<? extends ParticleOptions> flameParticle;

        public Basic(Properties properties) {
            this(properties, () -> DustParticleOptions.REDSTONE);
        }

        public Basic(Properties properties, Supplier<? extends ParticleOptions> flameParticle) {
            super(properties);
            this.flameParticle = Lazy.of(flameParticle);
        }

        public ParticleOptions getFlameParticle() {
            return this.flameParticle.get();
        }
    }
}
