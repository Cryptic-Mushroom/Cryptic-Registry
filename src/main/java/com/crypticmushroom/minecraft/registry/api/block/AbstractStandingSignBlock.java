package com.crypticmushroom.minecraft.registry.api.block;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.SignItem;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.WallSignBlock;
import net.minecraft.world.level.block.state.properties.WoodType;

import java.util.function.Supplier;

public abstract class AbstractStandingSignBlock extends StandingSignBlock implements MakesCustomBlockItem<SignItem> {
    public AbstractStandingSignBlock(Properties properties, WoodType type) {
        super(properties, type);
    }

    protected abstract WallSignBlock getWallSign();

    @Override
    public SignItem cmreg$makeBlockItem(Item.Properties properties) {
        return new SignItem(properties.stacksTo(16), this, this.getWallSign());
    }

    public static class Basic extends AbstractStandingSignBlock {
        private final Supplier<? extends WallSignBlock> wallSign;

        public Basic(Properties properties, WoodType type, Supplier<? extends WallSignBlock> wallSign) {
            super(properties, type);
            this.wallSign = wallSign;
        }

        @Override
        protected WallSignBlock getWallSign() {
            return this.wallSign.get();
        }
    }
}
