package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;

public class DecoratedPotPatternBuilder<SELF extends DecoratedPotPatternBuilder<SELF>> extends RegistryObjectBuilder<String, String, SELF> {
    @Override
    protected RegistryInfo.Static<String> getRegistry() {
        return RegistryDirectory.DECORATED_POT_PATTERN;
    }

    @Override
    protected String buildType() {
        return "%s/%s".formatted(this.getModId(), this.getId());
    }
}
