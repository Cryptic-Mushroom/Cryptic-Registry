package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceKey;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.core.Holder;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.levelgen.flat.FlatLevelGeneratorPreset;
import net.minecraft.world.level.levelgen.flat.FlatLevelGeneratorSettings;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Function;
import java.util.function.Supplier;

public class FlatLevelGeneratorPresetBuilder<SELF extends FlatLevelGeneratorPresetBuilder<SELF>> extends DataResourceBuilder<FlatLevelGeneratorPreset, FlatLevelGeneratorPreset, SELF> {
    private Function<BootstapContext<FlatLevelGeneratorPreset>, ? extends Holder<Item>> displayItem;
    private Function<BootstapContext<FlatLevelGeneratorPreset>, ? extends FlatLevelGeneratorSettings> settings;

    @Override
    protected RegistryInfo.Dynamic<FlatLevelGeneratorPreset> getRegistry() {
        return RegistryDirectory.FLAT_LEVEL_GENERATOR_PRESET;
    }

    @Override
    public DataResourceKey<FlatLevelGeneratorPreset, FlatLevelGeneratorPreset> build() {
        this.checkAttribute(this.displayItem, "display item");
        this.checkAttribute(this.settings, "flat level generator settings");

        return super.build();
    }

    @Override
    @SuppressWarnings("deprecation")
    protected FlatLevelGeneratorPreset buildType(BootstapContext<FlatLevelGeneratorPreset> bootstrap) {
        return new FlatLevelGeneratorPreset(
            this.displayItem.apply(bootstrap),
            this.settings.apply(bootstrap)
        );
    }


    /* FLAT LEVEL GENERATOR PRESET */

    @SuppressWarnings("unchecked")
    public SELF displayItem(RegistryObject<? extends Item> displayItem) {
        return this.displayItem(bootstrap -> (Holder<Item>) displayItem.getHolder().orElseThrow(() -> new IllegalStateException("Failed to get holder of registry object with UD: %s".formatted(displayItem.getId()))));
    }

    public SELF displayItem(Supplier<? extends Item> displayItem) {
        return this.displayItem(bootstrap -> displayItem.get().builtInRegistryHolder());
    }

    @SuppressWarnings("unchecked")
    public SELF displayItem(Function<BootstapContext<FlatLevelGeneratorPreset>, ? extends Holder<Item>> displayItem) {
        this.displayItem = displayItem;
        return (SELF) this;
    }

    public SELF settings(Supplier<? extends FlatLevelGeneratorSettings> settings) {
        return this.settings(bootstrap -> settings.get());
    }

    @SuppressWarnings("unchecked")
    public SELF settings(Function<BootstapContext<FlatLevelGeneratorPreset>, ? extends FlatLevelGeneratorSettings> settings) {
        this.settings = settings;
        return (SELF) this;
    }
}
