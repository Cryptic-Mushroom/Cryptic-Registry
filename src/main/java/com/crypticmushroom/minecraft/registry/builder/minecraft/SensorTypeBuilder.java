package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.entity.ai.sensing.Sensor;
import net.minecraft.world.entity.ai.sensing.SensorType;

import java.util.function.Function;
import java.util.function.Supplier;

public class SensorTypeBuilder<S extends Sensor<?>, T extends SensorType<S>, SELF extends SensorTypeBuilder<S, T, SELF>> extends RegistryObjectBuilder<T, SensorType<?>, SELF> {
    private final Function<Supplier<S>, T> type;
    private final Supplier<S> sensor;

    @SuppressWarnings("unchecked")
    public SensorTypeBuilder(Supplier<S> sensor) {
        this(u -> (T) new SensorType<>(u), sensor);
    }

    public SensorTypeBuilder(Function<Supplier<S>, T> type, Supplier<S> sensor) {
        this.type = type;
        this.sensor = sensor;
    }

    @Override
    protected RegistryInfo.Static<SensorType<?>> getRegistry() {
        return RegistryDirectory.SENSOR_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.sensor);
    }
}
