package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.level.levelgen.feature.Feature;

import java.util.function.Supplier;

/**
 * The feature builder contains various methods that allow for building virtually any type (that extends
 * {@link Feature}) with ease and allows for several automatic data generation features.
 *
 * @param <F>    The type of feature to be built.
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @see RegistryObjectBuilder
 * @since 0.1.0
 */
public class FeatureBuilder<F extends Feature<?>, SELF extends FeatureBuilder<F, SELF>> extends RegistryObjectBuilder<F, Feature<?>, SELF> {
    private final Supplier<F> type;

    public FeatureBuilder(Supplier<F> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<Feature<?>> getRegistry() {
        return RegistryDirectory.FEATURES;
    }

    @Override
    protected F buildType() {
        return this.type.get();
    }
}
