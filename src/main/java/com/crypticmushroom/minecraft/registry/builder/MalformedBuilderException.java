package com.crypticmushroom.minecraft.registry.builder;

/**
 * This exception indicates that a builder cannot finish building due to the class itself being malformed.
 */
public class MalformedBuilderException extends RuntimeException {
    public static final String NAMED_YET_NULL = "This builder was marked as named, yet the localized name registry and/or translation key supplier are null! Check your class implementation.";
    public static final String TAGGED_YET_NULL = "This builder was marked as tagged, yet the tag registry is null! Check your class implementation.";

    public MalformedBuilderException(String message, Throwable cause) {
        super(message, cause);
    }

    public MalformedBuilderException(String message) {
        super(message);
    }
}
