package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceKey;
import com.crypticmushroom.minecraft.registry.util.CrypticUtil;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.core.Holder;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.armortrim.TrimPattern;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;
import java.util.function.Supplier;

public class TrimPatternBuilder<SELF extends TrimPatternBuilder<SELF>> extends DataResourceBuilder.Named<TrimPattern, TrimPattern, SELF> {
    private @Nullable Function<BootstapContext<TrimPattern>, Holder<Item>> templateItem;
    private @Nullable Style style;

    @Override
    public DataResourceKey<TrimPattern, TrimPattern> build() {
        this.checkAttribute(this.templateItem, "template item");
        return super.build();
    }

    @Override
    @SuppressWarnings("DataFlowIssue") // checked by build()
    protected TrimPattern buildType(BootstapContext<TrimPattern> value) {
        var component = Component.translatable(this.getTranslationKey());
        if (this.style != null) component.withStyle(this.style);

        return new TrimPattern(
            this.makeResLoc(),
            this.templateItem.apply(value),
            component
        );
    }

    @Override
    protected RegistryInfo.Dynamic<TrimPattern> getRegistry() {
        return RegistryDirectory.TRIM_PATTERN;
    }

    @Override
    public String getTranslationKey() {
        return CrypticUtil.makeDescriptionId("trim_pattern", this.makeResLoc());
    }


    /* TRIM PATTERN */

    public SELF templateItem(Supplier<? extends Item> ingredient) {
        return this.templateItem(bootstrap -> ingredient.get().builtInRegistryHolder());
    }

    @SuppressWarnings("unchecked")
    public SELF templateItem(RegistryObject<? extends Item> ingredient) {
        return this.templateItem(bootstrap -> (Holder<Item>) ingredient.getHolder().orElseThrow());
    }

    public SELF templateItem(Holder<Item> ingredient) {
        return this.templateItem(bootstrap -> ingredient);
    }

    @SuppressWarnings("unchecked")
    public SELF templateItem(Function<BootstapContext<TrimPattern>, Holder<Item>> ingredient) {
        this.templateItem = ingredient;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF chatStyle(@Nullable Style style) {
        this.style = style;
        return (SELF) this;
    }
}
