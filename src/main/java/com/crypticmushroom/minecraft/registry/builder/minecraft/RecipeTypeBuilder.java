package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeType;

import java.util.function.Function;

public class RecipeTypeBuilder<R extends Recipe<?>, T extends RecipeType<R>, SELF extends RecipeTypeBuilder<R, T, SELF>> extends RegistryObjectBuilder<T, RecipeType<?>, SELF> {
    private final Function<ResourceLocation, T> type;

    @SuppressWarnings("unchecked")
    public RecipeTypeBuilder(Class<R> recipeType) {
        this(id -> (T) RecipeType.<R>simple(id), recipeType);
    }

    public RecipeTypeBuilder(Function<ResourceLocation, T> type, Class<R> recipeType) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<RecipeType<?>> getRegistry() {
        return RegistryDirectory.RECIPE_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.makeResLoc());
    }
}
