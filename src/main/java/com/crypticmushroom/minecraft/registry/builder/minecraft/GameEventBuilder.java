package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.level.gameevent.GameEvent;

import java.util.function.BiFunction;

public class GameEventBuilder<E extends GameEvent, SELF extends GameEventBuilder<E, SELF>> extends RegistryObjectBuilder<E, GameEvent, SELF> {
    private final GameEventFunction<E> type;

    private int notificationRadius = 16;

    public GameEventBuilder() {
        this(GameEventFunction.defaultType());
    }

    public GameEventBuilder(GameEventFunction<E> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<GameEvent> getRegistry() {
        return RegistryDirectory.GAME_EVENT;
    }

    @Override
    protected E buildType() {
        return this.type.apply(this.makeResLocString(), this.notificationRadius);
    }

    public interface GameEventFunction<E extends GameEvent> extends BiFunction<String, Integer, E> {
        @Override
        E apply(String name, Integer notificationRadius);

        default E apply(String name, int notificationRadius) {
            return this.apply(name, (Integer) notificationRadius);
        }

        @SuppressWarnings("unchecked")
        static <E extends GameEvent> GameEventFunction<E> defaultType() {
            return (name, notificationRadius) -> (E) new GameEvent(name, notificationRadius);
        }
    }


    /* GAME EVENT */

    @SuppressWarnings("unchecked")
    public SELF notificationRadius(int notificationRadius) {
        this.notificationRadius = notificationRadius;
        return (SELF) this;
    }
}
