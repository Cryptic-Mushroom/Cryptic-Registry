package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicateType;

import java.util.function.Function;
import java.util.function.Supplier;

public class BlockPredicateTypeBuilder<P extends BlockPredicate, T extends BlockPredicateType<P>, SELF extends BlockPredicateTypeBuilder<P, T, SELF>> extends RegistryObjectBuilder<T, BlockPredicateType<?>, SELF> {
    private final Function<Codec<P>, T> type;
    private final Supplier<Codec<P>> codec;

    @SuppressWarnings("unchecked")
    public BlockPredicateTypeBuilder(Supplier<Codec<P>> codec) {
        this(c -> (T) (BlockPredicateType<P>) () -> c, codec);
    }

    public BlockPredicateTypeBuilder(Function<Codec<P>, T> type, Supplier<Codec<P>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<BlockPredicateType<?>> getRegistry() {
        return RegistryDirectory.BLOCK_PREDICATE_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
