package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.data.provider.model.CrypticItemModelProvider;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.flag.FeatureFlag;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Rarity;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * The item builder contains various methods that allow for building virtually any type (that extends {@link Item}) with
 * ease and allows for several automatic data generation features.
 *
 * @param <I>    The type of item to be built.
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @apiNote This builder is marked as "named" and "tagged". While it is possible, it will be very difficult to
 *     override this behavior in a subclass.
 * @see RegistryObjectBuilder
 * @since 0.1.0
 */
public class ItemBuilder<I extends Item, P extends CrypticItemModelProvider, SELF extends ItemBuilder<I, P, SELF>> extends RegistryObjectBuilder.Named<I, Item, SELF> {
    private final Function<Item.Properties, I> type;

    // FOR ITEM
    private final Item.Properties properties;

    // FOR CREATIVE MODE TAB
    private final List<DataRegistry.CreativeModeTabBuildOptions> tabOptions = new ArrayList<>();

    // FOR VALIDITY
    private boolean hasSetMaxStackCount = false;

    // FOR DATA
    private @Nullable BiFunction<P, RegistryObject<I>, ? extends ItemModelBuilder> itemModelBuilder;

    public ItemBuilder() {
        this(Item.Properties::new);
    }

    @SuppressWarnings("unchecked")
    public ItemBuilder(Supplier<Item.Properties> properties) {
        this(p -> (I) new Item(p), properties);
    }

    public ItemBuilder(Function<Item.Properties, I> type) {
        this(type, Item.Properties::new);
    }

    public ItemBuilder(Function<Item.Properties, I> type, Supplier<Item.Properties> properties) {
        this.type = type;
        this.properties = properties.get();
    }

    @Override
    protected RegistryInfo.Static<Item> getRegistry() {
        return RegistryDirectory.ITEM;
    }

    @Override
    public LocalizedNameRegistry.Normal<Item> getLocalizedNameRegistry() {
        return LocalizedNameRegistry.ITEM;
    }

    @Override
    public RegistryObject<I> build() {
        var registered = super.build();

        // register model (if applicable)
        this.registerModel(registered);

        this.registerCreativeModeTabOptions(registered);

        return registered;
    }

    @Override
    protected I buildType() {
        return this.type.apply(this.properties);
    }

    RegistryObject<I> buildBlockItem(String name) {
        var registered = this.getRegister().register(name, this::buildType);

        // register model (if applicable)
        this.registerModel(registered);

        this.registerCreativeModeTabOptions(registered);

        return registered;
    }

    private void registerModel(RegistryObject<I> item) {
        if (this.itemModelBuilder == null) return;

        DataRegistry.registerItemModelBuilder(this.getModId(), item, this.itemModelBuilder);
    }

    private void registerCreativeModeTabOptions(RegistryObject<I> item) {
        DataRegistry.registerCreativeModeTabBuildOptions(this.getModId(), item, this.tabOptions);
    }

    @SuppressWarnings("unchecked")
    protected Class<P> getBlockStateProviderType() {
        return (Class<P>) CrypticItemModelProvider.class;
    }


    /* MODEL GENERATION */

    @SuppressWarnings("unchecked")
    public SELF model(BiFunction<P, RegistryObject<I>, ? extends ItemModelBuilder> itemModelBuilder) {
        this.itemModelBuilder = itemModelBuilder;

        return (SELF) this;
    }


    /* ITEM PROPERTIES */

    /**
     * @return This builder.
     *
     * @see Item.Properties#food(FoodProperties)
     */
    @SuppressWarnings("unchecked")
    public SELF food(FoodProperties food) {
        this.properties.food(food);
        return (SELF) this;
    }

    /**
     * @return This builder.
     *
     * @see Item.Properties#stacksTo(int)
     */
    @SuppressWarnings("unchecked")
    public SELF maxStackSize(int maxStackSize) {
        try {
            this.properties.stacksTo(maxStackSize);
            this.hasSetMaxStackCount = true;
        } catch (RuntimeException e) {
            throw new IllegalStateException(e);
        }

        return (SELF) this;
    }

    /**
     * @return This builder.
     *
     * @see Item.Properties#durability(int)
     */
    @SuppressWarnings("unchecked")
    public SELF durability(int maxDamage) {
        if (this.hasSetMaxStackCount) {
            throw new IllegalStateException("Unable to have damage AND stack.");
        }

        this.properties.durability(maxDamage);
        return (SELF) this;
    }

    /**
     * @return This builder.
     *
     * @see Item.Properties#craftRemainder(Item)
     */
    @SuppressWarnings("unchecked")
    public SELF craftingItemRemainder(Item craftingItemRemainder) {
        this.properties.craftRemainder(craftingItemRemainder);
        return (SELF) this;
    }

    /**
     * @return This builder.
     */
    @SuppressWarnings("unchecked")
    public SELF creativeModeTab(Supplier<? extends CreativeModeTab> creativeModeTab) {
        this.tabOptions.add(new DataRegistry.CreativeModeTabBuildOptions(creativeModeTab, (parameters, entries, item) -> item.getDefaultInstance()));
        return (SELF) this;
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public final SELF creativeModeTab(Supplier<? extends CreativeModeTab>... creativeModeTab) {
        for (Supplier<? extends CreativeModeTab> c : creativeModeTab) {
            this.creativeModeTab(c);
        }

        return (SELF) this;
    }

    /**
     * @return This builder.
     */
    @SuppressWarnings("unchecked")
    public SELF creativeModeTab(Supplier<? extends CreativeModeTab> creativeModeTab, DataRegistry.OutputOptions options) {
        this.tabOptions.add(new DataRegistry.CreativeModeTabBuildOptions(creativeModeTab, options));
        return (SELF) this;
    }

    /**
     * @return This builder.
     */
    @SuppressWarnings("unchecked")
    public SELF creativeModeTab(Supplier<? extends CreativeModeTab> creativeModeTab, CreativeModeTab.TabVisibility visibility) {
        this.tabOptions.add(new DataRegistry.CreativeModeTabBuildOptions(creativeModeTab, (parameters, entries, item) -> item.getDefaultInstance(), visibility));
        return (SELF) this;
    }

    /**
     * @return This builder.
     */
    @SuppressWarnings("unchecked")
    public SELF creativeModeTab(Supplier<? extends CreativeModeTab> creativeModeTab, DataRegistry.OutputOptions options, CreativeModeTab.TabVisibility visibility) {
        this.tabOptions.add(new DataRegistry.CreativeModeTabBuildOptions(creativeModeTab, options, visibility));
        return (SELF) this;
    }

    /**
     * @return This builder.
     *
     * @see Item.Properties#rarity(Rarity)
     */
    @SuppressWarnings("unchecked")
    public SELF rarity(Rarity rarity) {
        this.properties.rarity(rarity);
        return (SELF) this;
    }

    /**
     * @return This builder.
     *
     * @see Item.Properties#fireResistant()
     */
    @SuppressWarnings("unchecked")
    public SELF fireResistant() {
        this.properties.fireResistant();
        return (SELF) this;
    }

    /**
     * @return This builder.
     *
     * @see Item.Properties#setNoRepair()
     */
    @SuppressWarnings("unchecked")
    public SELF cannotRepair() {
        this.properties.setNoRepair();
        return (SELF) this;
    }

    /**
     * @return This builder.
     *
     * @see Item.Properties#requiredFeatures(FeatureFlag...)
     */
    @SuppressWarnings("unchecked")
    public SELF requiredFeatures(FeatureFlag... featureFlags) {
        this.properties.requiredFeatures(featureFlags);
        return (SELF) this;
    }
}
