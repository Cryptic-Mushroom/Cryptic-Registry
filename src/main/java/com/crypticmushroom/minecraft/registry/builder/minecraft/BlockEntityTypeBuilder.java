package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.datafixers.types.Type;
import com.mojang.datafixers.util.Function3;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.BlockEntityType.BlockEntitySupplier;
import net.minecraftforge.registries.RegistryObject;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * The entity type builder contains various methods that allow for building virtually any type (that extends
 * {@link BlockEntity}) with ease and allows for several automatic data generation features.
 *
 * @param <E>    The type of block entity to be built.
 * @param <T>    The type of block entity type to build.
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @implNote This builder builds a registry object for an {@link BlockEntity}, so bear that in mind when extending
 *     and/or working with this class.
 * @see RegistryObjectBuilder
 * @since 0.1.0
 */
public class BlockEntityTypeBuilder<E extends BlockEntity, T extends BlockEntityType<E>, SELF extends BlockEntityTypeBuilder<E, T, SELF>> extends RegistryObjectBuilder<T, BlockEntityType<?>, SELF> {
    private final BlockEntityTypeFunction<E, T> type;

    private final BlockEntityType.BlockEntitySupplier<E> factory;
    private final List<Supplier<? extends Block>> validBlocks = new LinkedList<>();
    private final Type<?> dataType = null;

    public BlockEntityTypeBuilder(BlockEntityType.BlockEntitySupplier<E> factory) {
        this(BlockEntityTypeFunction.defaultType(factory), factory);
    }

    public BlockEntityTypeBuilder(BlockEntityTypeFunction<E, T> type, BlockEntityType.BlockEntitySupplier<E> factory) {
        this.type = type;
        this.factory = factory;
    }

    @Override
    protected RegistryInfo.Static<BlockEntityType<?>> getRegistry() {
        return RegistryDirectory.BLOCK_ENTITY_TYPE;
    }

    @Override
    public RegistryObject<T> build() {
        if (this.validBlocks.isEmpty()) {
            throw new IncompleteBuilderException("Builder for block entity type %s is incomplete! No valid blocks have been defined.".formatted(this.getId()));
        }

        return super.build();
    }

    @Override
    public T buildType() {
        return this.type.apply(this.factory, this.validBlocks.stream().map(Supplier::get).collect(Collectors.toSet()), this.dataType);
    }

    public interface BlockEntityTypeFunction<E extends BlockEntity, ET extends BlockEntityType<E>> extends Function3<BlockEntitySupplier<E>, Set<Block>, Type<?>, ET> {
        @Override
        ET apply(BlockEntityType.BlockEntitySupplier<E> factory, Set<Block> validBlocks, Type<?> dataType);

        @SuppressWarnings("unchecked")
        static <E extends BlockEntity, ET extends BlockEntityType<E>> BlockEntityTypeFunction<E, ET> defaultType(BlockEntityType.BlockEntitySupplier<E> factory) {
            return (f, validBlocks, dataType) -> (ET) new BlockEntityType<>(f, validBlocks, dataType);
        }
    }


    /* BLOCK ENTITYTYPE BUILDING */

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public final SELF forBlock(Supplier<? extends Block>... block) {
        for (Supplier<? extends Block> b : block) {
            this.forBlock(b);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF forBlock(Supplier<? extends Block> block) {
        this.validBlocks.add(block);
        return (SELF) this;
    }
}
