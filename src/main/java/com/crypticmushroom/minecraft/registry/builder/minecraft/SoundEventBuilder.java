package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.datafixers.util.Pair;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.common.data.SoundDefinition;
import net.minecraftforge.common.data.SoundDefinition.Sound;
import net.minecraftforge.common.data.SoundDefinition.SoundType;
import net.minecraftforge.registries.RegistryObject;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * The sound event builder contains various methods that allow for building virtually any type (that extends
 * {@link SoundEvent}) with ease and allows for several automatic data generation features.
 *
 * @param <E>    The type of sound event to be built.
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @see RegistryObjectBuilder
 * @since 0.1.0
 */
public class SoundEventBuilder<E extends SoundEvent, SELF extends SoundEventBuilder<E, SELF>> extends RegistryObjectBuilder<E, SoundEvent, SELF> {
    /** @see SoundDefinition.Sound#DEFAULT_TYPE */
    @SuppressWarnings("JavadocReference")
    private static final SoundType DEFAULT_SOUND_TYPE = SoundType.SOUND;

    private final Function<ResourceLocation, E> type;
    private final SoundDefinition definition;
    private Pair<String, String> subtitle;

    private final Set<Supplier<Sound>> sounds = new LinkedHashSet<>();

    @SuppressWarnings("unchecked")
    public SoundEventBuilder() {
        this(id -> (E) SoundEvent.createVariableRangeEvent(id));
    }

    public SoundEventBuilder(Function<ResourceLocation, E> type) {
        this.type = type;
        this.definition = SoundDefinition.definition();
    }

    @Override
    protected RegistryInfo.Static<SoundEvent> getRegistry() {
        return RegistryDirectory.SOUND_EVENT;
    }

    @Override
    public RegistryObject<E> build() {
        RegistryObject<E> registered = super.build();

        DataRegistry.registerSoundDefinition(this.getModId(), registered, this::buildDefinition);
        if (this.subtitle != null)
            LocalizedNameRegistry.OTHER.register(this.getModId(), this.subtitle.getFirst(), this.subtitle.getSecond());

        return registered;
    }

    @Override
    protected E buildType() {
        return this.type.apply(this.makeResLoc());
    }

    private SoundDefinition buildDefinition() {
        this.sounds.stream().map(Supplier::get).forEach(this.definition::with);
        return this.definition;
    }


    /* SOUND DEFINITION */

    @Deprecated
    public SELF subtitle(String key, String subtitle) {
        this.subtitle = Pair.of("subtitles." + this.getId(), subtitle);
        return this.subtitle(subtitle);
    }

    @SuppressWarnings("unchecked")
    public SELF subtitle(String subtitle) {
        String key = subtitle;

        // if not an existing subtitle key, add the translation to datagen
        if (!isSubtitleKey(subtitle)) {
            this.subtitle = Pair.of(key = String.format("subtitles.%s.%s", this.getModId(), this.getId()), subtitle);
        }

        this.definition.subtitle(key);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF replace(boolean replace) {
        this.definition.replace(replace);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    private SELF sound(Supplier<Sound> sound) {
        this.sounds.add(sound);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF sound(Sound sound) {
        this.sound(() -> sound);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF sound(Sound... sounds) {
        Arrays.stream(sounds).map(s -> (Supplier<Sound>) () -> s).forEach(this::sound);
        return (SELF) this;
    }

    public SELF sound(String sound) {
        return this.sound(() -> Sound.sound(this.makeResLoc(sound), DEFAULT_SOUND_TYPE));
    }

    public SELF music(String music) {
        return this.sound(() -> Sound.sound(this.makeResLoc(music), DEFAULT_SOUND_TYPE).stream());
    }

    public SELF event(Supplier<SoundEvent> event) {
        return this.event(event, s -> s);
    }

    public SELF event(Supplier<SoundEvent> event, Function<Sound, Sound> buildOptions) {
        return this.sound(() -> buildOptions.apply(Sound.sound(event.get().getLocation(), SoundType.EVENT)));
    }

    public SELF sound(String... sound) {
        Sound[] sounds = Arrays.stream(sound).map(s -> Sound.sound(this.makeResLoc(s), DEFAULT_SOUND_TYPE)).toArray(Sound[]::new);
        return this.sound(sounds);
    }


    private static boolean isSubtitleKey(String s) {
        return s.startsWith("subtitle.") || s.startsWith("subtitles.");
    }
}
