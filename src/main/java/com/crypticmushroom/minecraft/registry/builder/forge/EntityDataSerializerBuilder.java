package com.crypticmushroom.minecraft.registry.builder.forge;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.core.IdMap;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.syncher.EntityDataSerializer;

import java.util.Optional;
import java.util.function.Supplier;

public class EntityDataSerializerBuilder<T, S extends EntityDataSerializer<T>, SELF extends EntityDataSerializerBuilder<T, S, SELF>> extends RegistryObjectBuilder<S, EntityDataSerializer<?>, SELF> {
    private final Supplier<S> type;

    public static <T> EntityDataSerializerBuilder<T, EntityDataSerializer.ForValueType<T>, ?> simple(FriendlyByteBuf.Writer<T> writer, FriendlyByteBuf.Reader<T> reader) {
        return new EntityDataSerializerBuilder<>(() -> (EntityDataSerializer.ForValueType<T>) EntityDataSerializer.simple(writer, reader));
    }

    public static <T> EntityDataSerializerBuilder<Optional<T>, EntityDataSerializer.ForValueType<Optional<T>>, ?> simpleOptional(FriendlyByteBuf.Writer<T> writer, FriendlyByteBuf.Reader<T> reader) {
        return new EntityDataSerializerBuilder<>(() -> (EntityDataSerializer.ForValueType<Optional<T>>) EntityDataSerializer.optional(writer, reader));
    }

    public static <T extends Enum<T>> EntityDataSerializerBuilder<T, EntityDataSerializer.ForValueType<T>, ?> simpleEnum(Class<T> enumType) {
        return new EntityDataSerializerBuilder<>(() -> (EntityDataSerializer.ForValueType<T>) EntityDataSerializer.simpleEnum(enumType));
    }

    public static <T> EntityDataSerializerBuilder<T, EntityDataSerializer.ForValueType<T>, ?> simpleId(IdMap<T> idMap) {
        return new EntityDataSerializerBuilder<>(() -> (EntityDataSerializer.ForValueType<T>) EntityDataSerializer.simpleId(idMap));
    }

    public EntityDataSerializerBuilder(Supplier<S> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<EntityDataSerializer<?>> getRegistry() {
        return RegistryDirectory.ENTITY_DATA_SERIALIZER;
    }

    @Override
    protected S buildType() {
        return this.type.get();
    }
}
