package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.registries.RegistryObject;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.OptionalInt;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class PoiTypeBuilder<SELF extends PoiTypeBuilder<SELF>> extends RegistryObjectBuilder<PoiType, PoiType, SELF> {
    private final List<Supplier<? extends BlockState>> matchingStates = new LinkedList<>();
    private OptionalInt maxTickets = OptionalInt.empty();
    private OptionalInt validRange = OptionalInt.empty();

    @Override
    protected RegistryInfo.Static<PoiType> getRegistry() {
        return RegistryDirectory.POI_TYPE;
    }

    @Override
    public RegistryObject<PoiType> build() {
        this.maxTickets.orElseThrow(() -> new IncompleteBuilderException("Cannot create a POI type without a max tickets value"));
        this.validRange.orElseThrow(() -> new IncompleteBuilderException("Cannot create a POI type without a valid range value"));

        return super.build();
    }

    @Override
    @SuppressWarnings("OptionalGetWithoutIsPresent") // checked by build()
    protected PoiType buildType() {
        return new PoiType(
            this.matchingStates.stream().map(b -> (BlockState) b.get()).collect(Collectors.toSet()),
            this.maxTickets.getAsInt(),
            this.validRange.getAsInt()
        );
    }


    /* POI TYPE */

    @SuppressWarnings("unchecked")
    public SELF matchingState(Supplier<? extends BlockState> matchingState) {
        this.matchingStates.add(matchingState);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF matchingState(Supplier<? extends BlockState>... matchingState) {
        for (Supplier<? extends BlockState> b : matchingState) {
            this.matchingState(b);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF matchingState(Collection<Supplier<? extends BlockState>> matchingStates) {
        matchingStates.forEach(this::matchingState);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF maxTickets(int maxTickets) {
        this.maxTickets = OptionalInt.of(maxTickets);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF validRange(int validRange) {
        this.validRange = OptionalInt.of(validRange);
        return (SELF) this;
    }
}
