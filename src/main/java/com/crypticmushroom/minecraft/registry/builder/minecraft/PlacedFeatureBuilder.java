package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.core.Holder;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public class PlacedFeatureBuilder<SELF extends PlacedFeatureBuilder<SELF>> extends DataResourceBuilder<PlacedFeature, PlacedFeature, PlacedFeatureBuilder<SELF>> {
    private Function<BootstapContext<PlacedFeature>, ? extends Holder<ConfiguredFeature<?, ?>>> feature;
    private final List<Function<BootstapContext<PlacedFeature>, ? extends PlacementModifier>> placements = new LinkedList<>();

    @Override
    protected RegistryInfo.Dynamic<PlacedFeature> getRegistry() {
        return RegistryDirectory.PLACED_FEATURE;
    }

    @Override
    protected PlacedFeature buildType(BootstapContext<PlacedFeature> bootstrap) {
        return new PlacedFeature(this.feature.apply(bootstrap), this.placements.stream().map(p -> (PlacementModifier) p.apply(bootstrap)).toList());
    }


    /* PLACED FEATURE */

    public SELF feature(Supplier<? extends Holder<ConfiguredFeature<?, ?>>> feature) {
        return this.feature(bootstrap -> feature.get());
    }

    @SuppressWarnings("unchecked")
    public SELF feature(Function<BootstapContext<PlacedFeature>, ? extends Holder<ConfiguredFeature<?, ?>>> feature) {
        this.feature = feature;
        return (SELF) this;
    }

    public SELF placement(Supplier<? extends PlacementModifier> placement) {
        return this.placement(bootstrap -> placement.get());
    }

    @SuppressWarnings("unchecked")
    public SELF placement(Function<BootstapContext<PlacedFeature>, ? extends PlacementModifier> placement) {
        this.placements.add(placement);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF placement(Supplier<? extends PlacementModifier>... placement) {
        for (Supplier<? extends PlacementModifier> p : placement) {
            this.placement(p);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF placement(Function<BootstapContext<PlacedFeature>, ? extends PlacementModifier>... placement) {
        for (Function<BootstapContext<PlacedFeature>, ? extends PlacementModifier> p : placement) {
            this.placement(p);
        }

        return (SELF) this;
    }
}
