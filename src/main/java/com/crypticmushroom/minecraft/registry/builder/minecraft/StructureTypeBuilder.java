package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureType;

import java.util.function.Function;
import java.util.function.Supplier;

public class StructureTypeBuilder<S extends Structure, T extends StructureType<S>, SELF extends StructureTypeBuilder<S, T, SELF>> extends RegistryObjectBuilder<T, StructureType<?>, SELF> {
    private final Function<Codec<S>, T> type;
    private final Supplier<Codec<S>> codec;

    @SuppressWarnings("unchecked")
    public StructureTypeBuilder(Supplier<Codec<S>> codec) {
        this(c -> (T) (StructureType<S>) () -> c, codec);
    }

    public StructureTypeBuilder(Function<Codec<S>, T> type, Supplier<Codec<S>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<StructureType<?>> getRegistry() {
        return RegistryDirectory.STRUCTURE_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
