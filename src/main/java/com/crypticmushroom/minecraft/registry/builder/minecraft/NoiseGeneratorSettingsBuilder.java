package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceKey;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.level.biome.Climate.ParameterPoint;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.NoiseRouter;
import net.minecraft.world.level.levelgen.NoiseSettings;
import net.minecraft.world.level.levelgen.SurfaceRules;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * The dimension type builder contains various methods that allow for building noise generator settings (with noise
 * settings) with ease andallows for several automatic data generation features.
 *
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @see RegistryObjectBuilder
 * @since 0.1.0
 */
public class NoiseGeneratorSettingsBuilder<SELF extends NoiseGeneratorSettingsBuilder<SELF>> extends DataResourceBuilder<NoiseGeneratorSettings, NoiseGeneratorSettings, SELF> {
    private Supplier<? extends BlockState> defaultBlock;
    private Supplier<? extends BlockState> defaultFluid;
    private Function<BootstapContext<NoiseGeneratorSettings>, ? extends NoiseRouter> noiseRouter;
    private Function<BootstapContext<NoiseGeneratorSettings>, ? extends SurfaceRules.RuleSource> surfaceRule;
    private final List<Function<BootstapContext<NoiseGeneratorSettings>, ParameterPoint>> spawnTarget = new LinkedList<>();
    private Integer seaLevel;
    private Boolean disableMobGeneration;
    private Boolean aquifersEnabled;
    private Boolean oreVeinsEnabled;
    private Boolean useLegacyRandomSource;

    private Integer minY;
    private Integer height;
    private Integer noiseSizeHorizontal;
    private Integer noiseSizeVertical;

    @Override
    protected RegistryInfo.Dynamic<NoiseGeneratorSettings> getRegistry() {
        return RegistryDirectory.NOISE_GENERATOR_SETTINGS;
    }

    @Override
    public DataResourceKey<NoiseGeneratorSettings, NoiseGeneratorSettings> build() {
        this.checkField(this.defaultBlock, "defaultBlock");
        this.checkField(this.defaultFluid, "defaultFluid");
        this.checkField(this.noiseRouter, "noiseRouter");
        this.checkField(this.surfaceRule, "surfaceRule");
        this.checkField(this.spawnTarget, "spawnTarget");
        this.checkField(this.seaLevel, "seaLevel");
        this.checkField(this.disableMobGeneration, "disableMobGeneration");
        this.checkField(this.aquifersEnabled, "aquifersEnabled");
        this.checkField(this.oreVeinsEnabled, "oreVeinsEnabled");
        this.checkField(this.useLegacyRandomSource, "useLegacyRandomSource");

        this.checkField(this.minY, "minY");
        this.checkField(this.height, "height");
        this.checkField(this.noiseSizeHorizontal, "noiseSizeHorizontal");
        this.checkField(this.noiseSizeVertical, "noiseSizeVertical");

        return super.build();
    }

    @Override
    protected NoiseGeneratorSettings buildType(BootstapContext<NoiseGeneratorSettings> bootstrap) {
        return new NoiseGeneratorSettings(
            NoiseSettings.create(
                this.minY,
                this.height,
                this.noiseSizeHorizontal,
                this.noiseSizeVertical
            ),
            this.defaultBlock.get(),
            this.defaultFluid.get(),
            this.noiseRouter.apply(bootstrap),
            this.surfaceRule.apply(bootstrap),
            this.spawnTarget.stream().map(f -> f.apply(bootstrap)).toList(),
            this.seaLevel,
            this.disableMobGeneration,
            this.aquifersEnabled,
            this.oreVeinsEnabled,
            this.useLegacyRandomSource
        );
    }

    private void checkField(Object object, String name) {
        try {
            Objects.requireNonNull(object, () -> "Field is null: %s".formatted(name));
        } catch (NullPointerException e) {
            throw new IncompleteBuilderException("Noise generator settings builder incomplete! See cause for more info. All attributes must be given a value.", e);
        }
    }


    /* NOISE GENERATOR SETTINGS */

    @SuppressWarnings("unchecked")
    public SELF defaultBlock(Supplier<? extends Block> defaultBlock) {
        this.defaultBlock = () -> defaultBlock.get().defaultBlockState();
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF defaultBlockState(Supplier<? extends BlockState> defaultBlockState) {
        this.defaultBlock = defaultBlockState;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF defaultFluid(Supplier<? extends LiquidBlock> defaultFluid) {
        this.defaultFluid = () -> defaultFluid.get().defaultBlockState();
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF defaultFluidState(Supplier<? extends BlockState> defaultFluidState) {
        this.defaultFluid = defaultFluidState;
        return (SELF) this;
    }

    public SELF noiseRouter(Supplier<NoiseRouter> noiseRouter) {
        return this.noiseRouter(bootstrap -> noiseRouter.get());
    }

    @SuppressWarnings("unchecked")
    public SELF noiseRouter(Function<BootstapContext<NoiseGeneratorSettings>, ? extends NoiseRouter> noiseRouter) {
        this.noiseRouter = noiseRouter;
        return (SELF) this;
    }

    public SELF surfaceRule(Supplier<? extends SurfaceRules.RuleSource> surfaceRule) {
        return this.surfaceRule(bootstrap -> surfaceRule.get());
    }

    @SuppressWarnings("unchecked")
    public SELF surfaceRule(Function<BootstapContext<NoiseGeneratorSettings>, ? extends SurfaceRules.RuleSource> surfaceRule) {
        this.surfaceRule = surfaceRule;
        return (SELF) this;
    }

    public SELF spawnTarget(Supplier<ParameterPoint> spawnTarget) {
        return this.spawnTarget(bootstrap -> spawnTarget.get());
    }

    @SuppressWarnings("unchecked")
    public SELF spawnTarget(Function<BootstapContext<NoiseGeneratorSettings>, ParameterPoint> spawnTarget) {
        this.spawnTarget.add(spawnTarget);
        return (SELF) this;
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public final SELF spawnTarget(Supplier<ParameterPoint>... spawnTarget) {
        for (Supplier<ParameterPoint> s : spawnTarget) {
            this.spawnTarget(s);
        }

        return (SELF) this;
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public final SELF spawnTarget(Function<BootstapContext<NoiseGeneratorSettings>, ParameterPoint>... spawnTarget) {
        for (Function<BootstapContext<NoiseGeneratorSettings>, ParameterPoint> s : spawnTarget) {
            this.spawnTarget(s);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF seaLevel(Integer seaLevel) {
        this.seaLevel = seaLevel;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF disableMobGeneration(Boolean disableMobGeneration) {
        this.disableMobGeneration = disableMobGeneration;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF aquifersEnabled(Boolean aquifersEnabled) {
        this.aquifersEnabled = aquifersEnabled;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF oreVeinsEnabled(Boolean oreVeinsEnabled) {
        this.oreVeinsEnabled = oreVeinsEnabled;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF useLegacyRandomSource(Boolean useLegacyRandomSource) {
        this.useLegacyRandomSource = useLegacyRandomSource;
        return (SELF) this;
    }


    /* NOISE SETTINGS */

    @SuppressWarnings("unchecked")
    public SELF minY(Integer minY) {
        this.minY = minY;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF height(Integer height) {
        this.height = height;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF noiseSizeHorizontal(Integer noiseSizeHorizontal) {
        this.noiseSizeHorizontal = noiseSizeHorizontal;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF noiseSizeVertical(Integer noiseSizeVertical) {
        this.noiseSizeVertical = noiseSizeVertical;
        return (SELF) this;
    }
}
