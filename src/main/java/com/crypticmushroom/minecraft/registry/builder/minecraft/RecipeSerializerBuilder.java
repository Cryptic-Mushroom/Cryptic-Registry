package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;

import java.util.function.Supplier;

public class RecipeSerializerBuilder<R extends Recipe<?>, S extends RecipeSerializer<R>, SELF extends RecipeSerializerBuilder<R, S, SELF>> extends RegistryObjectBuilder<S, RecipeSerializer<?>, SELF> {
    private final Supplier<S> type;

    public RecipeSerializerBuilder(Supplier<S> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<RecipeSerializer<?>> getRegistry() {
        return RegistryDirectory.RECIPE_SERIALIZER;
    }

    @Override
    protected S buildType() {
        return this.type.get();
    }
}
