package com.crypticmushroom.minecraft.registry.builder.forge;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraftforge.common.world.StructureModifier;

import java.util.function.Function;
import java.util.function.Supplier;

public class StructureModifierBuilder<M extends StructureModifier, SELF extends StructureModifierBuilder<M, SELF>> extends DataResourceBuilder<M, StructureModifier, SELF> {
    private final Function<BootstapContext<StructureModifier>, M> type;

    public StructureModifierBuilder(Supplier<M> type) {
        this(bootstrap -> type.get());
    }

    public StructureModifierBuilder(Function<BootstapContext<StructureModifier>, M> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Dynamic<StructureModifier> getRegistry() {
        return RegistryDirectory.STRUCTURE_MODIFIER;
    }

    @Override
    protected M buildType(BootstapContext<StructureModifier> bootstrap) {
        return this.type.apply(bootstrap);
    }
}
