package com.crypticmushroom.minecraft.registry.builder.minecraft;

import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.MaterialColor;
import net.minecraft.world.level.material.PushReaction;

public class MaterialBuilder<SELF extends MaterialBuilder<SELF>> extends Material.Builder {
    @SuppressWarnings("DataFlowIssue")
    public MaterialBuilder() {
        super(null);
    }

    public MaterialBuilder(MaterialColor color) {
        super(color);
    }

    @Override
    @SuppressWarnings("ConstantValue") // it's not constant
    public Material build() {
        if (super.color == null) {
            throw new IllegalStateException("Missing required field: color");
        }

        return super.build();
    }


    /* MATERIAL */

    public SELF mapColor(MaterialColor color) {
        super.color = color;
        return this.self();
    }

    public SELF liquid(boolean liquid) {
        super.liquid = liquid;
        return this.self();
    }

    @Override
    public SELF liquid() {
        super.liquid();
        return this.self();
    }

    public SELF solid(boolean solid) {
        super.solid = solid;
        return this.self();
    }

    @Override
    public SELF nonSolid() {
        super.nonSolid();
        return this.self();
    }

    public SELF blocksMotion(boolean blocksMotion) {
        super.blocksMotion = blocksMotion;
        return this.self();
    }

    @Override
    public SELF noCollider() {
        super.noCollider();
        return this.self();
    }

    public SELF solidBlocking(boolean solidBlocking) {
        super.solidBlocking = solidBlocking;
        return this.self();
    }

    @Override
    public SELF notSolidBlocking() {
        super.notSolidBlocking();
        return this.self();
    }

    public SELF flammable(boolean flammable) {
        super.flammable = flammable;
        return this.self();
    }

    @Override
    public SELF flammable() {
        super.flammable();
        return this.self();
    }

    public SELF replaceable(boolean replaceable) {
        super.replaceable = replaceable;
        return this.self();
    }

    @Override
    public SELF replaceable() {
        super.replaceable();
        return this.self();
    }

    public SELF pushReaction(PushReaction pushReaction) {
        super.pushReaction = pushReaction;
        return this.self();
    }

    @Override
    public SELF destroyOnPush() {
        super.destroyOnPush();
        return this.self();
    }

    @Override
    public SELF notPushable() {
        super.notPushable();
        return this.self();
    }


    /* INTERNAL UTILITY */

    @SuppressWarnings("unchecked")
    protected final SELF self() {
        return (SELF) this;
    }
}
