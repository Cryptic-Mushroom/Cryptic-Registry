package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceKey;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.core.Holder;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureSet;
import net.minecraft.world.level.levelgen.structure.placement.StructurePlacement;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public class StructureSetBuilder<SELF extends StructureSetBuilder<SELF>> extends DataResourceBuilder<StructureSet, StructureSet, SELF> {
    private final List<Function<BootstapContext<StructureSet>, StructureSet.StructureSelectionEntry>> structures = new LinkedList<>();
    private @Nullable Function<BootstapContext<StructureSet>, ? extends StructurePlacement> placement = null;

    @Override
    protected RegistryInfo.Dynamic<StructureSet> getRegistry() {
        return RegistryDirectory.STRUCTURE_SET;
    }

    @Override
    public DataResourceKey<StructureSet, StructureSet> build() {
        //@formatter:off
        if (this.structures.isEmpty())
            throw new IncompleteBuilderException("Cannot create a structure set with no structures");
        if (this.placement == null)
            throw new IncompleteBuilderException("Cannot create a structure set with no placement");
        //@formatter:on

        return super.build();
    }

    @Override
    @SuppressWarnings("DataFlowIssue") // checked in build()
    protected StructureSet buildType(BootstapContext<StructureSet> bootstrap) {
        return new StructureSet(
            this.structures.stream().map(f -> f.apply(bootstrap)).toList(),
            this.placement.apply(bootstrap)
        );
    }


    /* STRUCTURE SET */

    public SELF structure(Supplier<? extends Holder<Structure>> structure) {
        return this.structure(1, structure);
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF structure(Supplier<? extends Holder<Structure>>... structure) {
        for (Supplier<? extends Holder<Structure>> s : structure) {
            this.structure(s);
        }

        return (SELF) this;
    }

    public SELF structure(Function<BootstapContext<StructureSet>, ? extends Holder<Structure>> structure) {
        return this.structure(1, structure);
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF structure(Function<BootstapContext<StructureSet>, ? extends Holder<Structure>>... structure) {
        for (Function<BootstapContext<StructureSet>, ? extends Holder<Structure>> s : structure) {
            this.structure(s);
        }

        return (SELF) this;
    }

    public SELF structure(int weight, Supplier<? extends Holder<Structure>> structure) {
        return this.structure(weight, bootstrap -> structure.get());
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF structure(int weight, Supplier<? extends Holder<Structure>>... structure) {
        for (Supplier<? extends Holder<Structure>> s : structure) {
            this.structure(weight, s);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF structure(int weight, Function<BootstapContext<StructureSet>, ? extends Holder<Structure>> structure) {
        this.structures.add(bootstrap -> new StructureSet.StructureSelectionEntry(structure.apply(bootstrap), weight));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF structure(int weight, Function<BootstapContext<StructureSet>, ? extends Holder<Structure>>... structure) {
        for (Function<BootstapContext<StructureSet>, ? extends Holder<Structure>> s : structure) {
            this.structure(weight, s);
        }

        return (SELF) this;
    }

    public SELF placement(Supplier<? extends StructurePlacement> placement) {
        return this.placement(bootstrap -> placement.get());
    }

    @SuppressWarnings("unchecked")
    public SELF placement(Function<BootstapContext<StructureSet>, ? extends StructurePlacement> placement) {
        this.placement = placement;
        return (SELF) this;
    }
}
