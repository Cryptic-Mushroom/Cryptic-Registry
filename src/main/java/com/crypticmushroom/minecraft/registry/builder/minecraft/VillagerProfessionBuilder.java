package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.google.common.collect.ImmutableSet;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class VillagerProfessionBuilder<SELF extends VillagerProfessionBuilder<SELF>> extends RegistryObjectBuilder<VillagerProfession, VillagerProfession, SELF> {
    private Predicate<Holder<PoiType>> heldJobSite;
    private Predicate<Holder<PoiType>> acquirableJobSite;
    private final List<Supplier<? extends Item>> requestedItems = new LinkedList<>();
    private final List<Supplier<? extends Block>> secondaryPois = new LinkedList<>();
    private Supplier<? extends @Nullable SoundEvent> workSound = () -> null;

    @Override
    protected RegistryInfo.Static<VillagerProfession> getRegistry() {
        return RegistryDirectory.VILLAGER_PROFESSION;
    }

    @Override
    public RegistryObject<VillagerProfession> build() {
        this.checkAttribute(this.heldJobSite, "job site");

        return super.build();
    }

    @Override
    protected VillagerProfession buildType() {
        return new VillagerProfession(
            this.makeResLocString(),
            this.heldJobSite,
            this.acquirableJobSite,
            this.requestedItems.stream().map(i -> (Item) i).collect(ImmutableSet.toImmutableSet()),
            this.secondaryPois.stream().map(b -> (Block) b).collect(ImmutableSet.toImmutableSet()),
            this.workSound.get()
        );
    }

    public interface VillagerProfessionFunction<V extends VillagerProfession> {
        V apply(String name, Predicate<Holder<PoiType>> heldJobSite, Predicate<Holder<PoiType>> acquirableJobSite,
                ImmutableSet<Item> requestedItems, ImmutableSet<Block> secondaryPoi, @Nullable SoundEvent workSound);

        @SuppressWarnings("unchecked")
        static <V extends VillagerProfession> VillagerProfessionFunction<V> defaultType() {
            return (name, heldJobSite, acquirableJobSite, requestedItems, secondaryPoi, workSound) -> (V) new VillagerProfession(name, heldJobSite, acquirableJobSite, requestedItems, secondaryPoi, workSound);
        }
    }


    /* VILLAGER PROFESSION */

    @SuppressWarnings("DataFlowIssue")
    public SELF jobSite(RegistryObject<PoiType> heldJobSite) {
        return this.jobSite(poi -> poi.is(heldJobSite.getKey()));
    }

    public SELF jobSite(TagKey<PoiType> heldJobSite) {
        return this.jobSite(poi -> poi.is(heldJobSite));
    }

    public SELF jobSite(ResourceLocation heldJobSite) {
        return this.jobSite(poi -> poi.is(heldJobSite));
    }

    public SELF jobSite(ResourceKey<PoiType> heldJobSite) {
        return this.jobSite(poi -> poi.is(heldJobSite));
    }

    @SuppressWarnings("unchecked")
    public SELF jobSite(Predicate<Holder<PoiType>> heldJobSite) {
        this.heldJobSite = heldJobSite;
        if (this.acquirableJobSite == null) {
            this.acquirableJobSite = heldJobSite;
        }

        return (SELF) this;
    }

    @SuppressWarnings("DataFlowIssue")
    public SELF acquirableJobSite(RegistryObject<PoiType> acquirableJobSite) {
        return this.acquirableJobSite(poi -> poi.is(acquirableJobSite.getKey()));
    }

    public SELF acquirableJobSite(TagKey<PoiType> acquirableJobSite) {
        return this.acquirableJobSite(poi -> poi.is(acquirableJobSite));
    }

    public SELF acquirableJobSite(ResourceLocation acquirableJobSite) {
        return this.acquirableJobSite(poi -> poi.is(acquirableJobSite));
    }

    public SELF acquirableJobSite(ResourceKey<PoiType> acquirableJobSite) {
        return this.acquirableJobSite(poi -> poi.is(acquirableJobSite));
    }

    @SuppressWarnings("unchecked")
    public SELF acquirableJobSite(Predicate<Holder<PoiType>> acquirableJobSite) {
        this.acquirableJobSite = acquirableJobSite;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF requestedItem(Supplier<? extends Item> requestedItem) {
        this.requestedItems.add(requestedItem);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF requestedItem(Supplier<? extends Item>... requestedItem) {
        for (Supplier<? extends Item> i : requestedItem) {
            this.requestedItem(i);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF requestedItem(Collection<Supplier<? extends Item>> requestedItems) {
        requestedItems.forEach(this::requestedItem);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF secondaryPoi(Supplier<? extends Block> secondaryPoi) {
        this.secondaryPois.add(secondaryPoi);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF secondaryPoi(Supplier<? extends Block>... secondaryPoi) {
        for (Supplier<? extends Block> b : secondaryPoi) {
            this.secondaryPoi(b);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF secondaryPoi(Collection<Supplier<? extends Block>> secondaryPois) {
        secondaryPois.forEach(this::secondaryPoi);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF workSound(Supplier<? extends SoundEvent> workSound) {
        this.workSound = workSound;
        return (SELF) this;
    }
}
