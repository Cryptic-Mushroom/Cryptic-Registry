package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.level.block.entity.BannerPattern;

import java.util.function.Function;

public class BannerPatternBuilder<P extends BannerPattern, SELF extends BannerPatternBuilder<P, SELF>> extends RegistryObjectBuilder<P, BannerPattern, SELF> {
    private final Function<String, P> type;

    @SuppressWarnings("unchecked")
    public BannerPatternBuilder() {
        this(s -> (P) new BannerPattern(s));
    }

    public BannerPatternBuilder(Function<String, P> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<BannerPattern> getRegistry() {
        return RegistryDirectory.BANNER_PATTERN;
    }

    @Override
    protected P buildType() {
        return this.type.apply(this.makeResLocString());
    }
}
