package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceKey;
import com.crypticmushroom.minecraft.registry.util.CrypticUtil;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import it.unimi.dsi.fastutil.objects.Object2FloatFunction;
import net.minecraft.core.Holder;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.world.item.ArmorMaterials;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.armortrim.TrimMaterial;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class TrimMaterialBuilder<SELF extends TrimMaterialBuilder<SELF>> extends DataResourceBuilder.Named<TrimMaterial, TrimMaterial, SELF> {
    private @Nullable Function<BootstapContext<TrimMaterial>, Holder<Item>> ingredient;
    private @Nullable Object2FloatFunction<BootstapContext<TrimMaterial>> itemModelIndex;
    private final List<Map.Entry<Function<BootstapContext<TrimMaterial>, ArmorMaterials>, String>> overrideArmorMaterials = new ArrayList<>();
    private @Nullable Style style;

    @Override
    protected RegistryInfo.Dynamic<TrimMaterial> getRegistry() {
        return RegistryDirectory.TRIM_MATERIAL;
    }

    @Override
    public DataResourceKey<TrimMaterial, TrimMaterial> build() {
        this.checkAttribute(this.ingredient, "ingredient");
        this.checkAttribute(this.itemModelIndex, "item model index");
        return super.build();
    }

    @Override
    @SuppressWarnings("DataFlowIssue") // checked by build()
    protected TrimMaterial buildType(BootstapContext<TrimMaterial> bootstrap) {
        var component = Component.translatable(this.getTranslationKey());
        if (this.style != null) component.withStyle(this.style);

        var overrides = new HashMap<ArmorMaterials, String>();
        this.overrideArmorMaterials.forEach(e -> overrides.put(e.getKey().apply(bootstrap), e.getValue()));

        return new TrimMaterial(
            this.getId(),
            this.ingredient.apply(bootstrap),
            this.itemModelIndex.apply(bootstrap),
            Map.copyOf(overrides),
            component
        );
    }

    @Override
    public String getTranslationKey() {
        return CrypticUtil.makeDescriptionId("trim_material", this.makeResLoc());
    }


    /* TRIM MATERIAL */

    public SELF ingredient(Supplier<? extends Item> ingredient) {
        return this.ingredient(bootstrap -> ingredient.get().builtInRegistryHolder());
    }

    @SuppressWarnings("unchecked")
    public SELF ingredient(RegistryObject<? extends Item> ingredient) {
        return this.ingredient(bootstrap -> (Holder<Item>) ingredient.getHolder().orElseThrow());
    }

    public SELF ingredient(Holder<Item> ingredient) {
        return this.ingredient(bootstrap -> ingredient);
    }

    @SuppressWarnings("unchecked")
    public SELF ingredient(Function<BootstapContext<TrimMaterial>, Holder<Item>> ingredient) {
        this.ingredient = ingredient;
        return (SELF) this;
    }

    public SELF itemModelIndex(float itemModelIndex) {
        return this.itemModelIndex(bootstrap -> itemModelIndex);
    }

    @SuppressWarnings("unchecked")
    public SELF itemModelIndex(Object2FloatFunction<BootstapContext<TrimMaterial>> itemModelIndex) {
        this.itemModelIndex = itemModelIndex;
        return (SELF) this;
    }

    public SELF overrideArmorMaterial(ArmorMaterials armorMaterial, String override) {
        return this.overrideArmorMaterial(bootstrap -> armorMaterial, override);
    }

    @SuppressWarnings("unchecked")
    public SELF overrideArmorMaterial(Function<BootstapContext<TrimMaterial>, ArmorMaterials> armorMaterial, String override) {
        this.overrideArmorMaterials.add(new Map.Entry<>() {
            private String value = override;

            @Override
            public Function<BootstapContext<TrimMaterial>, ArmorMaterials> getKey() {
                return armorMaterial;
            }

            @Override
            public String getValue() {
                return this.value;
            }

            @Override
            public String setValue(String value) {
                var old = this.value;
                this.value = value;
                return old;
            }
        });

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF chatStyle(@Nullable Style style) {
        this.style = style;
        return (SELF) this;
    }
}
