package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.entity.decoration.PaintingVariant;
import net.minecraftforge.registries.RegistryObject;

import java.util.OptionalInt;

public class PaintingVariantBuilder<V extends PaintingVariant, SELF extends PaintingVariantBuilder<V, SELF>> extends RegistryObjectBuilder<V, PaintingVariant, SELF> {
    private final PaintingVariantFunction<V> type;

    private OptionalInt width = OptionalInt.empty();
    private OptionalInt height = OptionalInt.empty();

    public PaintingVariantBuilder() {
        this(PaintingVariantFunction.defaultType());
    }

    public PaintingVariantBuilder(PaintingVariantFunction<V> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<PaintingVariant> getRegistry() {
        return RegistryDirectory.PAINTING_VARIANT;
    }

    @Override
    public RegistryObject<V> build() {
        this.width.ifPresentOrElse(i -> {
            if (i <= 0) {
                throw new IncompleteBuilderException("Cannot create a painting variant with a width of zero or lower");
            }
        }, () -> {
            throw new IncompleteBuilderException("Cannot create a painting variant without a width value");
        });

        this.height.ifPresentOrElse(i -> {
            if (i <= 0) {
                throw new IncompleteBuilderException("Cannot create a painting variant with a height of zero or lower");
            }
        }, () -> {
            throw new IncompleteBuilderException("Cannot create a painting variant without a height value");
        });

        return super.build();
    }

    @Override
    @SuppressWarnings("OptionalGetWithoutIsPresent") // checked by build()
    protected V buildType() {
        return this.type.apply(this.width.getAsInt(), this.height.getAsInt());
    }

    @FunctionalInterface
    public interface PaintingVariantFunction<P extends PaintingVariant> {
        P apply(int width, int height);

        @SuppressWarnings("unchecked")
        static <P extends PaintingVariant> PaintingVariantFunction<P> defaultType() {
            return (w, h) -> (P) new PaintingVariant(w, h);
        }
    }


    /* PAINTING VARIANT */

    @SuppressWarnings("unchecked")
    public SELF width(int width) {
        this.width = OptionalInt.of(width);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF height(int height) {
        this.height = OptionalInt.of(height);
        return (SELF) this;
    }
}
