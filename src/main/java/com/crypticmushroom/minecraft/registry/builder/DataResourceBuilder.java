package com.crypticmushroom.minecraft.registry.builder;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.builder.minecraft.BlockBuilder;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.TagRegistry;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceKey;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceRegister;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Lifecycle;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * <h1>Registry Object Builder</h1>
 *
 * <h2>Purpose</h2>
 * A registry object builder is a tool that is used to aid in the creation of {@link RegistryObject registry objects}.
 * <p>
 * Since its inception, the registry object builder has had two main purposes:
 * <ol>
 *     <li>Serve as a link between dynamic runtime registration and data registration.</li>
 *     <li>Provide additional tools for creating objects that are not normally available with object constructors or builders provided by Forge or Mojang.</li>
 * </ol>
 *
 * <h2>Usage</h2>
 * This class, {@link DataResourceBuilder}, is merely an abstract blueprint to be used in subclasses. A perfect
 * of a fully fledged registry object builder is {@link BlockBuilder}. While at first it might seem daunting, it is
 * clear as to what it accomplishes, and you can read more about it in its documentation. For now, all you need to know
 * is that this abstract class does not exist to be used in implementations where it is a type, but as a skeleton for
 * subclasses that will inherit from this class.
 *
 * <h2>Features</h2>
 * This class, as the base for all other registry object builders, provides these functionalities:
 * <ul>
 *     <li>An ID is kept to later be used in {@link DeferredRegister#register(String, Supplier)} as the {@code name} argument.</li>
 *     <li>A named implementation via {@link Named} allows it to register the built object's localized name to a {@link LocalizedNameRegistry} for automatic data generation.</li>
 *     <li>All implementations have support for tags. It is your responsibility to not irresponsibly tag something that cannot be tagged.</li>
 *     <li>Using the {@link #build(Function)} method, a builder can apply additional options to the type that is to be built.</li>
 * </ul>
 *
 * @param <OBJ>  The type of object to be built.
 * @param <SPR>  The base superclass of the object being built for proper registration.
 * @param <SELF> The type of the builder for casting in the super class (see {@link #id(String)}).
 * @author Jonathing
 * @since 2.0.0
 */
public abstract class DataResourceBuilder<OBJ extends SPR, SPR, SELF extends DataResourceBuilder<OBJ, SPR, SELF>> extends AbstractFunctionBuilder<OBJ, BootstapContext<SPR>, DataResourceKey<OBJ, SPR>, SELF> {
    private final List<TagKey<SPR>> tags = new ArrayList<>();

    private Supplier<Lifecycle> lifecycle = () -> this.getRegister().lifecycle;

    protected abstract RegistryInfo.Dynamic<SPR> getRegistry();

    protected DataResourceRegister<SPR> getRegister() {
        return CrypticRegistry.getRegister(this.getModId(), this.getRegistry());
    }

    /**
     * If this builder is marked as "tagged", the implementation must override this method with the corresponding
     * {@link TagRegistry tag registry}.
     *
     * @return The tag registry to use when building the registry object with its related tags.
     */
    protected TagRegistry<SPR> getTagRegistry() {
        return TagRegistry.get(this.getRegistry());
    }

    /**
     * This method builds the {@link RegistryObject registry object} to be saved to a global variable by the end user.
     * Once this method has been called, this builder has effectively served its purpose and should be out of scope to
     * be picked up by Java's Garbage Collection after the object in question has been registered by the deferred
     * register specified in {@link #getRegister()}.
     *
     * <h2>Execution</h2>
     * The way a registry object builder builds a registry object is first by supplying a lambda reference to
     * {@link AbstractFunctionBuilder#buildType(Object)} to the {@link DeferredRegister deferred register} given by
     * {@link #getRegister()}. The deferred register is then responsible for running the lambda reference to
     * {@link AbstractFunctionBuilder#buildType(Object)} to create the object when necessary.
     *
     * <h2>Data Registration</h2>
     * Once this builder has effectively built the registry object, it will register the necessary data elements for
     * data generation depending on this builder's implementation (i.e. {@link Named named}, tagged, or both. For
     * example, if this builder uses the tagged implementation, it will traverse through the given tags (supplied by
     * {@link #tag(TagKey[])}) and register them in the {@link TagRegistry tag registry} via
     * {@link TagRegistry#register(String, TagKey, ResourceKey[])}.
     *
     * @return The {@link RegistryObject registry object} built by this builder.
     *
     * @throws MalformedBuilderException  If the builder subclass does not properly handle attributes such as "named" or
     *                                    "tagged".
     * @throws IncompleteBuilderException If the builder is incomplete (i.e. missing ID, localized name, etc).
     */
    @Override
    public DataResourceKey<OBJ, SPR> build() {
        this.checkAttribute(this.getId(), "ID");

        var registered = this.register(bootstrap -> this.applyBuildOptions(this.buildType(bootstrap), bootstrap));

        try {
            this.tags.forEach(tag -> this.getTagRegistry().register(this.getModId(), tag, registered));
        } catch (NullPointerException e) {
            throw new MalformedBuilderException("Attempted to register a null tag with the built object", e);
        }

        return registered;
    }

    private DataResourceKey<OBJ, SPR> register(Function<BootstapContext<SPR>, OBJ> object) {
        return this.getRegister().register(this.getId(), object, this.lifecycle.get());
    }


    /* LIFECYCLE */

    @SuppressWarnings("unchecked")
    public SELF lifecycle(Lifecycle lifecycle) {
        this.lifecycle = () -> lifecycle;
        return (SELF) this;
    }


    /* TAGS */

    @SuppressWarnings("unchecked")
    public SELF tag(TagKey<SPR> tag) {
        this.tags.add(tag);
        return (SELF) this;
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public final SELF tag(TagKey<SPR>... tag) {
        for (TagKey<SPR> t : tag) {
            this.tag(t);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public final SELF tag(Collection<TagKey<SPR>> tags) {
        tags.forEach(this::tag);
        return (SELF) this;
    }

    public static abstract class Named<OBJ extends SPR, SPR, SELF extends Named<OBJ, SPR, SELF>> extends DataResourceBuilder<OBJ, SPR, SELF> {
        private String translationKey;
        private String localizedName;

        /**
         * If this builder is marked as "named", the implementation must either override this method with the
         * corresponding {@link LocalizedNameRegistry} or the {@link #getTranslationKey()} method to provide the raw
         * translation key to be saved to data.
         *
         * @return The localized name registry to use when building the registry object with its localized name.
         *
         * @see #getTranslationKey()
         */
        public @Nullable LocalizedNameRegistry.Data<SPR> getLocalizedNameRegistry() {
            return null;
        }

        @Override
        @SuppressWarnings("DuplicatedCode")
        public DataResourceKey<OBJ, SPR> build() {
            var registered = super.build();

            try {
                var localizedName = this.getLocalizedName();
                this.checkAttribute(localizedName, "localized name");
                if (this.getLocalizedNameRegistry() == null) {
                    var translationKey = Objects.requireNonNull(this.getTranslationKey(), MalformedBuilderException.NAMED_YET_NULL);
                    LocalizedNameRegistry.OTHER.register(this.getModId(), translationKey, localizedName);
                } else {
                    this.getLocalizedNameRegistry().register(this.getModId(), registered, localizedName);
                }
            } catch (NullPointerException e) {
                throw new MalformedBuilderException(e.getMessage(), e);
            }

            return registered;
        }

        /**
         * If this builder is marked as "named", the implementation must either override this method to provide the raw
         * translation key to be saved to data or the {@link #getLocalizedNameRegistry()} with the corresponding
         * {@link LocalizedNameRegistry}.
         *
         * @return The raw translation key to use when building the registry object with its localized name.
         *
         * @see #getLocalizedNameRegistry()
         */
        public @Nullable String getTranslationKey() {
            return this.translationKey;
        }

        /**
         * @return The localized name to register in the {@link #getLocalizedNameRegistry() localized name registry}.
         *
         * @implNote This method is ignored if this builder is not marked as "named".
         */
        public String getLocalizedName() {
            return this.localizedName;
        }


        /* NAMED OBJECT */

        /**
         * This method sets the translation key of the object to be built by this builder. It is ignored if this
         * builder's {@link #getLocalizedNameRegistry()} method <em>does not</em> return a {@code null} value.
         *
         * @param translationKey The translation key to set in this builder.
         * @return This builder.
         */
        @SuppressWarnings("unchecked")
        public SELF translationKey(String translationKey) {
            this.translationKey = translationKey;
            return (SELF) this;
        }

        /**
         * This method sets the localized name of the object to be built by this builder.
         *
         * @param localizedName The localized name to set in this builder.
         * @return This builder.
         */
        @SuppressWarnings("unchecked")
        public SELF localizedName(String localizedName) {
            this.localizedName = localizedName;
            return (SELF) this;
        }
    }
}
