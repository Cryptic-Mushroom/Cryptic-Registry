package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.util.valueproviders.IntProvider;
import net.minecraft.util.valueproviders.IntProviderType;

import java.util.function.Function;
import java.util.function.Supplier;

public class IntProviderTypeBuilder<P extends IntProvider, T extends IntProviderType<P>, SELF extends IntProviderTypeBuilder<P, T, SELF>> extends RegistryObjectBuilder<T, IntProviderType<?>, SELF> {
    private final Function<Codec<P>, T> type;
    private final Supplier<Codec<P>> codec;

    @SuppressWarnings("unchecked")
    public IntProviderTypeBuilder(Supplier<Codec<P>> codec) {
        this(c -> (T) (IntProviderType<P>) () -> c, codec);
    }

    public IntProviderTypeBuilder(Function<Codec<P>, T> type, Supplier<Codec<P>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<IntProviderType<?>> getRegistry() {
        return RegistryDirectory.INT_PROVIDER_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
