package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import it.unimi.dsi.fastutil.booleans.Boolean2ObjectFunction;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.RegistryObject;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * The particle type builder contains various methods that allow for building virtually any type (that extends
 * {@link ParticleType}) with ease and allows for several automatic data generation features.
 *
 * @param <T>    The type of particle type to be built.
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @see RegistryObjectBuilder
 * @since 0.1.0
 */
public class ParticleTypeBuilder<O extends ParticleOptions, T extends ParticleType<O>, SELF extends ParticleTypeBuilder<O, T, SELF>> extends RegistryObjectBuilder<T, ParticleType<?>, SELF> {
    private final Boolean2ObjectFunction<T> type;
    private boolean overrideLimiter = false;

    private final List<ResourceLocation> textures = new ArrayList<>();

    @SuppressWarnings("unchecked")
    public ParticleTypeBuilder(@SuppressWarnings("deprecation") Supplier<ParticleOptions.Deserializer<O>> deserializer, Function<ParticleType<O>, Codec<O>> codecFactory) {
        this(b -> (T) new SimpleParticleType<>(b, deserializer.get(), codecFactory));
    }

    public ParticleTypeBuilder(Boolean2ObjectFunction<T> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<ParticleType<?>> getRegistry() {
        return RegistryDirectory.PARTICLE_TYPE;
    }

    @Override
    public RegistryObject<T> build() {
        RegistryObject<T> registered = super.build();

        DataRegistry.registerParticleTextures(this.getModId(), registered, this.textures);

        return registered;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.overrideLimiter);
    }

    private static final class SimpleParticleType<O extends ParticleOptions> extends ParticleType<O> {
        private final Codec<O> codec;

        private SimpleParticleType(boolean overrideLimiter, @SuppressWarnings("deprecation") ParticleOptions.Deserializer<O> deserializer, Function<ParticleType<O>, Codec<O>> codecFactory) {
            super(overrideLimiter, deserializer);
            this.codec = codecFactory.apply(this);
        }

        @Override
        public Codec<O> codec() {
            return this.codec;
        }
    }


    /* PARTICLE TYPE */

    /**
     * @return This builder.
     *
     * @see ParticleType#getOverrideLimiter()
     */
    @SuppressWarnings("unchecked")
    public SELF overrideLimiter() {
        this.overrideLimiter = true;
        return (SELF) this;
    }


    /* DATA */

    @SuppressWarnings("unchecked")
    public SELF texture(String texture) {
        this.textures.add(this.makeResLoc(texture));
        return (SELF) this;
    }

    public SELF texture(String texture, int count) {
        var textures = new String[count];
        for (int i = 0; i < count; i++) {
            textures[i] = texture + "_" + i;
        }
        return this.texture(textures);
    }

    @SuppressWarnings("unchecked")
    public SELF texture(String... textures) {
        for (String texture : textures) {
            this.texture(texture);
        }

        return (SELF) this;
    }
}
