package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.level.levelgen.DensityFunction;

import java.util.function.Function;
import java.util.function.Supplier;

public class DensityFunctionBuilder<D extends DensityFunction, SELF extends DensityFunctionBuilder<D, SELF>> extends DataResourceBuilder<D, DensityFunction, SELF> {
    private final Function<BootstapContext<DensityFunction>, D> type;

    public DensityFunctionBuilder(Supplier<D> type) {
        this(bootstrap -> type.get());
    }

    public DensityFunctionBuilder(Function<BootstapContext<DensityFunction>, D> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Dynamic<DensityFunction> getRegistry() {
        return RegistryDirectory.DENSITY_FUNCTION;
    }

    @Override
    protected D buildType(BootstapContext<DensityFunction> bootstrap) {
        return this.type.apply(bootstrap);
    }
}
