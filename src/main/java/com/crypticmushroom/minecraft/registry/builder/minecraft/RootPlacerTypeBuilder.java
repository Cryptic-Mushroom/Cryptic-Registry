package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.feature.rootplacers.RootPlacer;
import net.minecraft.world.level.levelgen.feature.rootplacers.RootPlacerType;

import java.util.function.Function;
import java.util.function.Supplier;

public class RootPlacerTypeBuilder<P extends RootPlacer, T extends RootPlacerType<P>, SELF extends RootPlacerTypeBuilder<P, T, SELF>> extends RegistryObjectBuilder<T, RootPlacerType<?>, SELF> {
    private final Function<Codec<P>, T> type;
    private final Supplier<Codec<P>> codec;

    @SuppressWarnings("unchecked")
    public RootPlacerTypeBuilder(Supplier<Codec<P>> codec) {
        this(c -> (T) new RootPlacerType<>(c), codec);
    }

    public RootPlacerTypeBuilder(Function<Codec<P>, T> type, Supplier<Codec<P>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<RootPlacerType<?>> getRegistry() {
        return RegistryDirectory.ROOT_PLACER_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
