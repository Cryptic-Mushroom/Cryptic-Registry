package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.structure.templatesystem.PosRuleTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.PosRuleTestType;

import java.util.function.Function;
import java.util.function.Supplier;

public class PosRuleTestTypeBuilder<R extends PosRuleTest, T extends PosRuleTestType<R>, SELF extends PosRuleTestTypeBuilder<R, T, SELF>> extends RegistryObjectBuilder<T, PosRuleTestType<?>, SELF> {
    private final Function<Codec<R>, T> type;
    private final Supplier<Codec<R>> codec;

    @SuppressWarnings("unchecked")
    public PosRuleTestTypeBuilder(Supplier<Codec<R>> codec) {
        this(c -> (T) (PosRuleTestType<R>) () -> c, codec);
    }

    public PosRuleTestTypeBuilder(Function<Codec<R>, T> type, Supplier<Codec<R>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<PosRuleTestType<?>> getRegistry() {
        return RegistryDirectory.POS_RULE_TEST;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
