package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.crypticmushroom.minecraft.registry.util.Type;
import com.mojang.serialization.Codec;
import net.minecraft.world.entity.ai.memory.MemoryModuleType;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

public class MemoryModuleTypeBuilder<M, T extends MemoryModuleType<M>, SELF extends MemoryModuleTypeBuilder<M, T, SELF>> extends RegistryObjectBuilder<T, MemoryModuleType<?>, SELF> {
    private final Function<Optional<Codec<M>>, T> type;
    private final Supplier<@Nullable Codec<M>> codec;

    @SuppressWarnings("unchecked")
    public MemoryModuleTypeBuilder(Type<M> type) {
        this(o -> (T) new MemoryModuleType<>(o), () -> null);
    }

    @SuppressWarnings("unchecked")
    public MemoryModuleTypeBuilder(Class<M> moduleType) {
        this(o -> (T) new MemoryModuleType<>(o), () -> null);
    }

    public MemoryModuleTypeBuilder(Function<Optional<Codec<M>>, T> type) {
        this(type, () -> null);
    }

    public MemoryModuleTypeBuilder(Function<Optional<Codec<M>>, T> type, Type<M> moduleType) {
        this(type);
    }

    public MemoryModuleTypeBuilder(Function<Optional<Codec<M>>, T> type, Class<M> moduleType) {
        this(type);
    }

    @SuppressWarnings("unchecked")
    public MemoryModuleTypeBuilder(Supplier<@Nullable Codec<M>> codec) {
        this(o -> (T) new MemoryModuleType<>(o), codec);
    }

    public MemoryModuleTypeBuilder(Function<Optional<Codec<M>>, T> type, Supplier<@Nullable Codec<M>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<MemoryModuleType<?>> getRegistry() {
        return RegistryDirectory.MEMORY_MODULE_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(Optional.ofNullable(this.codec.get()));
    }
}
