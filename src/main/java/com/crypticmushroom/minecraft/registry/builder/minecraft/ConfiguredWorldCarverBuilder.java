package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.level.levelgen.carver.CarverConfiguration;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.carver.WorldCarver;

import java.util.function.Function;
import java.util.function.Supplier;

public class ConfiguredWorldCarverBuilder<WC extends WorldCarver<CC>, CC extends CarverConfiguration, SELF extends ConfiguredWorldCarverBuilder<WC, CC, SELF>> extends DataResourceBuilder<ConfiguredWorldCarver<CC>, ConfiguredWorldCarver<?>, SELF> {
    private final Function<BootstapContext<ConfiguredWorldCarver<?>>, WC> carver;

    private Function<BootstapContext<ConfiguredWorldCarver<?>>, CC> config;

    public ConfiguredWorldCarverBuilder(Supplier<WC> carver) {
        this(bootstrap -> carver.get());
    }

    public ConfiguredWorldCarverBuilder(Function<BootstapContext<ConfiguredWorldCarver<?>>, WC> carver) {
        this.carver = carver;
    }

    @Override
    protected RegistryInfo.Dynamic<ConfiguredWorldCarver<?>> getRegistry() {
        return RegistryDirectory.CONFIGURED_WORLD_CARVER;
    }

    @Override
    protected ConfiguredWorldCarver<CC> buildType(BootstapContext<ConfiguredWorldCarver<?>> bootstrap) {
        return this.carver.apply(bootstrap).configured(this.config.apply(bootstrap));
    }


    /* CONFIGURED CARVER */

    public SELF config(Supplier<CC> config) {
        return this.config(bootstrap -> config.get());
    }

    @SuppressWarnings("unchecked")
    public SELF config(Function<BootstapContext<ConfiguredWorldCarver<?>>, CC> config) {
        this.config = config;
        return (SELF) this;
    }
}
