package com.crypticmushroom.minecraft.registry.builder;

/**
 * This exception indicates that a builder cannot finish building due to incompleteness.
 */
public class IncompleteBuilderException extends RuntimeException {
    public IncompleteBuilderException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncompleteBuilderException(String message) {
        super(message);
    }
}
