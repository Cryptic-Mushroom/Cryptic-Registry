package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.heightproviders.HeightProvider;
import net.minecraft.world.level.levelgen.heightproviders.HeightProviderType;

import java.util.function.Function;
import java.util.function.Supplier;

public class HeightProviderTypeBuilder<P extends HeightProvider, T extends HeightProviderType<P>, SELF extends HeightProviderTypeBuilder<P, T, SELF>> extends RegistryObjectBuilder<T, HeightProviderType<?>, SELF> {
    private final Function<Codec<P>, T> type;
    private final Supplier<Codec<P>> codec;

    @SuppressWarnings("unchecked")
    public HeightProviderTypeBuilder(Supplier<Codec<P>> codec) {
        this(c -> (T) (HeightProviderType<P>) () -> c, codec);
    }

    public HeightProviderTypeBuilder(Function<Codec<P>, T> type, Supplier<Codec<P>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<HeightProviderType<?>> getRegistry() {
        return RegistryDirectory.HEIGHT_PROVIDER_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
