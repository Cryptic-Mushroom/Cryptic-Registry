package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.coremod.hook.impl.CrypticBlockBehaviourProperties;
import com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.BlockBehaviourPropertiesMixin;
import com.crypticmushroom.minecraft.registry.data.provider.loot.CrypticBlockLootSubProvider;
import com.crypticmushroom.minecraft.registry.data.provider.model.CrypticBlockStateProvider;
import com.crypticmushroom.minecraft.registry.data.provider.model.CrypticItemModelProvider;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.flag.FeatureFlag;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Tier;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockBehaviour.OffsetType;
import net.minecraft.world.level.block.state.BlockBehaviour.Properties;
import net.minecraft.world.level.block.state.BlockBehaviour.StateArgumentPredicate;
import net.minecraft.world.level.block.state.BlockBehaviour.StatePredicate;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.MaterialColor;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.client.model.generators.IGeneratedBlockState;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.registries.RegisterEvent;
import net.minecraftforge.registries.RegistryObject;
import org.apache.commons.lang3.function.TriFunction;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

/**
 * The block builder contains various methods that allow for building virtually any type (that extends {@link Block})
 * with ease and allows for several automatic data generation features.
 *
 * @param <B>    The type of block to be built.
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @apiNote This builder is marked as "named" and "tagged". While it is possible, it will be very difficult to
 *     override this behavior in a subclass.
 * @see RegistryObjectBuilder
 * @since 0.1.0
 */
@SuppressWarnings("rawtypes") // I FUCKING LOVE JAVA GENERICS
public class BlockBuilder<B extends Block, BP extends CrypticBlockStateProvider, IP extends CrypticItemModelProvider, LP extends CrypticBlockLootSubProvider, SELF extends BlockBuilder<B, BP, IP, LP, SELF>> extends RegistryObjectBuilder.Named<B, Block, SELF> {
    /**
     * One of the various problems with building {@link RegistryObject registry objects} for blocks early is that we
     * might want to copy the block properties from one block to another using the
     * {@link Properties#copy(BlockBehaviour)} method. Doing this normally will result in an
     * {@link ExceptionInInitializerError} being thrown since blocks cannot be created this early in then game.
     * <p>
     * The solution is to manually cache the properties of each block being built in our own cache. When a block is
     * built in {@link #build()}, the resource key of the newly built registry object is stored in this map along with
     * the {@link Properties} that were used to build it (since block properties technically act like records, in how
     * they do not need additional static initialization to function).
     * <p>
     * This implementation creates an anonymous class extending {@link HashMap} with an overriden
     * {@link HashMap#get(Object)} method that will clone the stored block properties into a new properties value so
     * that there is no chance of accidentally changing the block properties of another block.
     */
    private static Map<ResourceKey<? extends Block>, Properties> PROPERTIES_CACHE = new HashMap<>() {
        /** @see BlockBehaviourPropertiesMixin#cmreg$clone() */
        private Properties clone(Properties properties) {
            return ((CrypticBlockBehaviourProperties) properties).cmreg$clone();
        }

        @Override
        public Properties get(Object key) {
            return this.clone(Objects.requireNonNull(super.get(key), () -> "No properties cached for " + key));
        }
    };

    private final Function<Properties, B> type;

    // FOR BLOCK
    private final Properties properties;

    // FOR BLOCK ITEM
    private @Nullable Function<Supplier<B>, ? extends ItemBuilder<? extends BlockItem, ?, ?>> blockItem;

    // FOR DATA
    private boolean explosionResistant = false;
    private @Nullable TagKey<Block> mineability;
    private @Nullable TagKey<Block> tier;
    private @Nullable BiFunction<BP, RegistryObject<B>, ? extends IGeneratedBlockState> blockStateBuilder;
    private @Nullable BiConsumer<LP, RegistryObject<B>> lootBuilder;

    /**
     * This constructor creates a new block builder with the default {@link Block} type and the given properties in the
     * form of {@link Properties#of(Material)}
     *
     * @param material The material to use with the block's properties.
     */
    public BlockBuilder(Material material) {
        this(() -> Properties.of(material));
    }

    /**
     * This constructor creates a new block builder with the given type and properties in the form of
     * {@link Properties#of(Material)}
     *
     * @param type     The type of block to build.
     * @param material The material to use with the block's properties.
     */
    public BlockBuilder(Function<Properties, B> type, Material material) {
        this(type, () -> Properties.of(material));
    }

    /**
     * This constructor creates a new block builder with the default {@link Block} type and the given properties in the
     * form of {@link Properties#of(Material, DyeColor)}
     *
     * @param material The material to use with the block's properties.
     * @param color    The matieral color (in the form of a {@link DyeColor}) to use with the block's properties.
     */
    public BlockBuilder(Material material, DyeColor color) {
        this(() -> Properties.of(material, color));
    }

    /**
     * This constructor creates a new block builder with the given type and properties in the form of
     * {@link Properties#of(Material, DyeColor)}
     *
     * @param type     The type of block to build.
     * @param material The material to use with the block's properties.
     * @param color    The matieral color (in the form of a {@link DyeColor}) to use with the block's properties.
     */
    public BlockBuilder(Function<Properties, B> type, Material material, DyeColor color) {
        this(type, () -> Properties.of(material, color));
    }

    /**
     * This constructor creates a new block builder with the default {@link Block} type and the given properties in the
     * form of {@link Properties#of(Material, MaterialColor)}
     *
     * @param material The material to use with the block's properties.
     * @param color    The matieral color to use with the block's properties.
     */
    public BlockBuilder(Material material, MaterialColor color) {
        this(() -> Properties.of(material, color));
    }

    /**
     * This constructor creates a new block builder with the given type and properties in the form of
     * {@link Properties#of(Material, MaterialColor)}
     *
     * @param type     The type of block to build.
     * @param material The material to use with the block's properties.
     * @param color    The matieral color to use with the block's properties.
     */
    public BlockBuilder(Function<Properties, B> type, Material material, MaterialColor color) {
        this(type, () -> Properties.of(material, color));
    }

    /**
     * This constructor creates a new block builder with the default {@link Block} type and the given properties in the
     * form of {@link Properties#of(Material, Function)}
     *
     * @param material The material to use with the block's properties.
     * @param color    The matieral color to use with the block's properties.
     */
    public BlockBuilder(Material material, Function<BlockState, MaterialColor> color) {
        this(() -> Properties.of(material, color));
    }

    /**
     * This constructor creates a new block builder with the given type and properties in the form of
     * {@link Properties#of(Material, Function)}
     *
     * @param type     The type of block to build.
     * @param material The material to use with the block's properties.
     * @param color    The matieral color to use with the block's properties.
     */
    public BlockBuilder(Function<Properties, B> type, Material material, Function<BlockState, MaterialColor> color) {
        this(type, () -> Properties.of(material, color));
    }

    /**
     * This constructor creates a new block builder with the default {@link Block} type and the given block registry
     * object to copy properties from.
     *
     * @param block The block registry object to copy properties from.
     */
    public BlockBuilder(RegistryObject<? extends Block> block) {
        this(() -> PROPERTIES_CACHE.get(block.getKey()));
    }

    /**
     * This constructor creates a new block builder with the default {@link Block} type and the given properties.
     *
     * @param properties The properties to build the block with.
     */
    @SuppressWarnings("unchecked")
    public BlockBuilder(Supplier<Properties> properties) {
        this(p -> (B) new Block(p), properties);
    }

    /**
     * This constructor creates a new block builder with the given type and block registry object to copy properties
     * from.
     *
     * @param type  The type of block to build.
     * @param block The block registry object to copy properties from.
     */
    public BlockBuilder(Function<Properties, B> type, RegistryObject<? extends Block> block) {
        this(type, () -> PROPERTIES_CACHE.get(block.getKey()));
    }

    /**
     * This constructor creates a new block builder with the given type and properties.
     *
     * @param type       The type of block to build.
     * @param properties The properties to build the block with.
     */
    public BlockBuilder(Function<Properties, B> type, Supplier<Properties> properties) {
        this.type = type;
        this.properties = properties.get();
    }

    @Override
    protected RegistryInfo.Static<Block> getRegistry() {
        return RegistryDirectory.BLOCK;
    }

    @Override
    @Nullable
    public LocalizedNameRegistry.Normal<Block> getLocalizedNameRegistry() {
        return LocalizedNameRegistry.BLOCK;
    }

    /**
     * In addition to the functionality provided by {@link RegistryObjectBuilder#build()}, when a block is built, it
     * will copy the properties to a cache, build the block item (if any), and register the mineability (if any) to data
     * registry.
     *
     * @return The built registry object of the block type.
     */
    @Override
    public RegistryObject<B> build() {
        // build object
        var registered = super.build();

        // cache properties
        PROPERTIES_CACHE.put(registered.getKey(), this.properties);

        // register loot table (if applicable)
        this.registerLoot(registered);

        // register block state and model (if applicable)
        this.registerBlockStateAndModel(registered);

        // build blockitem object (if applicable)
        this.buildBlockItem(registered);

        // register mineability for datagen
        this.registerMineability(registered);

        return registered;
    }

    @Override
    protected B buildType() {
        try {
            return this.type.apply(Objects.requireNonNull(this.properties));
        } catch (NullPointerException e) {
            throw new IncompleteBuilderException(String.format("Builder for block %s is incomplete! Missing: block properties", this.getId()), e);
        }
    }

    @SuppressWarnings("unchecked")
    protected Class<BP> getBlockStateProviderType() {
        return (Class<BP>) CrypticBlockStateProvider.class;
    }

    @SuppressWarnings("unchecked")
    protected Class<LP> getLootProviderType() {
        return (Class<LP>) CrypticBlockLootSubProvider.class;
    }

    private void registerBlockStateAndModel(RegistryObject<B> block) {
        if (this.blockStateBuilder == null) return;

        DataRegistry.registerBlockstateBuilder(this.getModId(), block, this.getBlockStateProviderType(), this.blockStateBuilder);
    }

    private void registerLoot(RegistryObject<B> block) {
        if (this.lootBuilder != null)
            DataRegistry.registerBlockLootBuilder(this.getModId(), block, this.getLootProviderType(), this.lootBuilder);

        if (this.explosionResistant)
            DataRegistry.registerExplosionResistant(block);
    }

    private void buildBlockItem(RegistryObject<B> block) {
        if (this.blockItem == null) return;

        var builder = this.blockItem.apply(block).id(this.getId());
        builder.buildBlockItem(this.getId());
    }

    private void registerMineability(RegistryObject<B> block) {
        if (this.mineability == null && this.properties.requiresCorrectToolForDrops) {
            throw new IncompleteBuilderException(String.format("Builder for block %s is incomplete! Missing mineability for a tool-required-for-drops block.", this.getId()));
        }

        if (this.mineability != null) this.getTagRegistry().register(this.getModId(), this.mineability, block);
        if (this.tier != null) this.getTagRegistry().register(this.getModId(), this.tier, block);
    }

    public <I extends Item> ItemBuilder<I, ?, ? extends ItemBuilder<I, ?, ?>> createItemBuilder(Function<Item.Properties, I> type) {
        return new BlockItemBuilder<>(type);
    }

    private final class BlockItemBuilder<I extends Item> extends ItemBuilder<I, IP, BlockItemBuilder<I>> {
        private BlockItemBuilder(Function<Item.Properties, I> type) {
            super(type);
        }

        @Override
        protected String getModId() {
            return BlockBuilder.this.getModId();
        }
    }


    /* BLOCK ITEM */

    protected BlockItem makeBlockItem(Item.Properties properties, B block) {
        return block instanceof MakesCustomBlockItem<?> custom ? custom.cmreg$makeBlockItem(properties) : new BlockItem(block, properties);
    }

    public SELF blockItem(Supplier<? extends CreativeModeTab> creativeTab) {
        return this.blockItem(
            b -> this.createItemBuilder(p -> this.makeBlockItem(p, b.get()))
                     .creativeModeTab(creativeTab)
                     .model((p, i) -> p.inherit(b))
        );
    }

    // TODO I need to keep this non-final and non-safevarargs because it doesn't inherently use another method... sorry
    @SuppressWarnings("unchecked")
    public SELF blockItem(Supplier<? extends CreativeModeTab>... creativeTab) {
        return this.blockItem(
            b -> this.createItemBuilder(p -> this.makeBlockItem(p, b.get()))
                     .creativeModeTab(creativeTab)
                     .model((p, i) -> p.inherit(b))
        );
    }

    @SuppressWarnings("unchecked")
    public SELF blockItem(BiFunction<IP, Supplier<B>, ? extends ItemModelBuilder> model, Supplier<? extends CreativeModeTab> creativeTab) {
        return this.blockItem(
            b -> this.createItemBuilder(p -> this.makeBlockItem(p, b.get()))
                     .creativeModeTab(creativeTab)
                     .model((p, i) -> model.apply((IP) p, b))
        );
    }

    // TODO I need to keep this non-final and non-safevarargs because it doesn't inherently use another method... sorry
    @SuppressWarnings("unchecked")
    public SELF blockItem(BiFunction<IP, Supplier<B>, ? extends ItemModelBuilder> model, Supplier<? extends CreativeModeTab>... creativeTab) {
        return this.blockItem(
            b -> this.createItemBuilder(p -> this.makeBlockItem(p, b.get()))
                     .creativeModeTab(creativeTab)
                     .model((p, i) -> model.apply((IP) p, b))
        );
    }

    @SuppressWarnings("unchecked")
    public SELF blockItem(TriFunction<IP, Supplier<B>, RegistryObject<BlockItem>, ? extends ItemModelBuilder> model, Supplier<? extends CreativeModeTab> creativeTab) {
        return this.blockItem(
            b -> this.createItemBuilder(p -> this.makeBlockItem(p, b.get()))
                     .creativeModeTab(creativeTab)
                     .model((p, i) -> model.apply((IP) p, b, i))
        );
    }

    @SuppressWarnings("unchecked")
    public SELF blockItem(TriFunction<IP, Supplier<B>, RegistryObject<BlockItem>, ? extends ItemModelBuilder> model, Supplier<? extends CreativeModeTab>... creativeTab) {
        return this.blockItem(
            b -> this.createItemBuilder(p -> this.makeBlockItem(p, b.get()))
                     .creativeModeTab(creativeTab)
                     .model((p, i) -> model.apply((IP) p, b, i))
        );
    }

    public SELF blockItem(BiConsumer<ItemBuilder<? extends BlockItem, ?, ?>, Supplier<B>> blockItemBuilderAction) {
        return this.blockItem(b -> {
            var itemBuilder = this.createItemBuilder(p -> this.makeBlockItem(p, b.get())).model((p, i) -> p.inherit(b));
            blockItemBuilderAction.accept(itemBuilder, b);
            return itemBuilder;
        });
    }

    @SuppressWarnings("unchecked")
    public SELF blockItem(Function<Supplier<B>, ? extends ItemBuilder<? extends BlockItem, ?, ?>> blockItem) {
        this.blockItem = blockItem;
        return (SELF) this;
    }


    /* MINEABILITY */

    @SuppressWarnings("unchecked")
    public SELF mineability(TagKey<Block> mineability, boolean mineable) {
        if (!mineability.location().getPath().contains("mineable")) {
            throw new IllegalArgumentException("Must use a tag that includes \"mineable\" in its location for mineability!");
        }

        // cancel if not mineable with tag
        if (!mineable) {
            // if the current tag is the non-mineable one, remove it from the block
            if (this.mineability == mineability) this.mineability = null;

            return (SELF) this;
        }

        // if mineable and current mineability is not null, warn
        if (this.mineability != null && this.mineability != mineability) {
            CrypticRegistry.LOGGER.warn("Mineability tag for block {} was changed from {} to {}!", this.getId(), this.mineability, mineability);
        }

        this.mineability = mineability;
        return (SELF) this;
    }

    public SELF mineability(TagKey<Block> mineability) {
        return this.mineability(mineability, true);
    }

    public SELF mineableWithAxe(boolean mineableWithAxe) {
        return this.mineability(BlockTags.MINEABLE_WITH_AXE, mineableWithAxe);
    }

    public SELF mineableWithAxe() {
        return this.mineableWithAxe(true);
    }

    public SELF mineableWithHoe(boolean mineableWithHoe) {
        return this.mineability(BlockTags.MINEABLE_WITH_HOE, mineableWithHoe);
    }

    public SELF mineableWithHoe() {
        return this.mineableWithHoe(true);
    }

    public SELF mineableWithPickaxe(boolean mineableWithPickaxe) {
        return this.mineability(BlockTags.MINEABLE_WITH_PICKAXE, mineableWithPickaxe);
    }

    public SELF mineableWithPickaxe() {
        return this.mineableWithPickaxe(true);
    }

    public SELF mineableWithShovel(boolean mineableWithShovel) {
        return this.mineability(BlockTags.MINEABLE_WITH_SHOVEL, mineableWithShovel);
    }

    public SELF mineableWithShovel() {
        return this.mineableWithShovel(true);
    }

    @SuppressWarnings("unchecked")
    public SELF needsTier(Tier tier) {
        try {
            this.tier = Objects.requireNonNull(tier.getTag());
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("The tier given for the tool necessity did not have a tag or was null! This cannot happen for vanilla tiers, so double check any custom tag implementations.");
        }

        return (SELF) this;
    }


    /* BLOCK STATE AND MODEL GENERATION */

    @SuppressWarnings("unchecked")
    public SELF blockStateAndModel(BiFunction<BP, RegistryObject<B>, ? extends IGeneratedBlockState> blockStateBuilder) {
        this.blockStateBuilder = blockStateBuilder;

        return (SELF) this;
    }


    /* BLOCK LOOT GENERATION */

    @SuppressWarnings("unchecked")
    public SELF lootTable(BiConsumer<LP, RegistryObject<B>> lootBuilder) {
        this.lootBuilder = lootBuilder;

        return (SELF) this;
    }


    /* BLOCKBEHAVIOR PROPERTIES */

    @SuppressWarnings("unchecked")
    public SELF material(Material material) {
        this.properties.material = material;
        return (SELF) this;
    }

    /** @deprecated Use {@link #mapColor(DyeColor)} */
    @Deprecated(forRemoval = true, since = "1.20.1-*")
    public SELF color(DyeColor color) {
        return this.mapColor(color);
    }

    /** @deprecated Use {@link #mapColor(MaterialColor)} */
    @Deprecated(forRemoval = true, since = "1.20.1-*")
    public SELF color(MaterialColor color) {
        return this.mapColor(color);
    }

    /** @see Properties#of(Material, DyeColor) */
    public SELF mapColor(DyeColor color) {
        return this.mapColor(color.getMaterialColor());
    }

    /** @see Properties#color(MaterialColor) */
    @SuppressWarnings("unchecked")
    public SELF mapColor(MaterialColor color) {
        this.properties.color(color);
        return (SELF) this;
    }

    /** @deprecated Use {@link #noCollision()} */
    @Deprecated
    public SELF noCollission() {
        return this.noCollision();
    }

    /** @see Properties#noCollission() */
    @SuppressWarnings("unchecked")
    public SELF noCollision() {
        this.properties.noCollission();
        return (SELF) this;
    }

    /** @see Properties#noCollission() */
    @SuppressWarnings("unchecked")
    public SELF hasCollision() {
        this.properties.hasCollision = true;
        return (SELF) this;
    }

    /** @see Properties#noOcclusion() */
    @SuppressWarnings("unchecked")
    public SELF noOcclusion() {
        this.properties.noOcclusion();
        return (SELF) this;
    }

    /** @see Properties#noOcclusion() */
    @SuppressWarnings("unchecked")
    public SELF canOcclude() {
        this.properties.canOcclude = true;
        return (SELF) this;
    }

    /** @see Properties#friction(float) */
    @SuppressWarnings("unchecked")
    public SELF friction(Number friction) {
        this.properties.friction(friction.floatValue());
        return (SELF) this;
    }

    /** @see Properties#speedFactor(float) */
    @SuppressWarnings("unchecked")
    public SELF speedFactor(Number speedFactor) {
        this.properties.speedFactor(speedFactor.floatValue());
        return (SELF) this;
    }

    /** @see Properties#jumpFactor(float) */
    @SuppressWarnings("unchecked")
    public SELF jumpFactor(Number jumpFactor) {
        this.properties.jumpFactor(jumpFactor.floatValue());
        return (SELF) this;
    }

    /** @see Properties#sound(SoundType) */
    @SuppressWarnings("unchecked")
    public SELF sound(SoundType soundType) {
        this.properties.sound(soundType);
        return (SELF) this;
    }

    /** @see Properties#lightLevel(ToIntFunction) */
    public SELF lightLevel(int lightEmission) {
        return this.lightLevel(state -> lightEmission);
    }

    /** @see Properties#lightLevel(ToIntFunction) */
    @SuppressWarnings("unchecked")
    public SELF lightLevel(ToIntFunction<BlockState> lightEmission) {
        this.properties.lightLevel(lightEmission);
        return (SELF) this;
    }

    /** @see Properties#strength(float, float) */
    @SuppressWarnings("unchecked")
    @Deprecated(forRemoval = true, since = "2.1.0")
    public SELF strength(Number destroyTime, Number explosionResistance) {
        this.properties.strength(destroyTime.floatValue(), explosionResistance.floatValue());
        return (SELF) this;
    }

    /** @see Properties#strength(float, float) */
    @SuppressWarnings("unchecked")
    public SELF strength(Number destroyTime, Number explosionResistance, boolean itemAlwaysSurvivesExplosion) {
        this.strength(destroyTime, explosionResistance);
        this.explosionResistant = itemAlwaysSurvivesExplosion;
        return (SELF) this;
    }

    /** @see Properties#instabreak() */
    @SuppressWarnings("unchecked")
    public SELF instabreak() {
        this.properties.instabreak();
        return (SELF) this;
    }

    /** @see Properties#strength(float) */
    @SuppressWarnings("unchecked")
    @Deprecated(forRemoval = true, since = "2.1.0")
    public SELF strength(Number strength) {
        this.properties.strength(strength.floatValue());
        return (SELF) this;
    }

    /** @see Properties#strength(float) */
    @SuppressWarnings("unchecked")
    public SELF strength(Number strength, boolean itemAlwaysSurvivesExplosion) {
        this.strength(strength);
        this.explosionResistant = itemAlwaysSurvivesExplosion;
        return (SELF) this;
    }

    /** @see Properties#randomTicks() */
    @SuppressWarnings("unchecked")
    public SELF randomTicks(boolean isRandomlyTicking) {
        this.properties.isRandomlyTicking = isRandomlyTicking;
        return (SELF) this;
    }

    /** @see Properties#randomTicks() */
    @SuppressWarnings("unchecked")
    public SELF randomTicks() {
        this.properties.randomTicks();
        return (SELF) this;
    }

    /** @see Properties#dynamicShape() */
    @SuppressWarnings("unchecked")
    public SELF dynamicShape(boolean dyanmicShape) {
        this.properties.dynamicShape = dyanmicShape;
        return (SELF) this;
    }

    /** @see Properties#dynamicShape() */
    @SuppressWarnings("unchecked")
    public SELF dynamicShape() {
        this.properties.dynamicShape();
        return (SELF) this;
    }

    /**
     * @implNote Feel free to keep using this, but also remember to define the block's empty loot table in data.
     * @see Properties#noLootTable()
     */
    @SuppressWarnings("unchecked")
    public SELF noLootTable() {
        this.properties.noLootTable();
        return (SELF) this;
    }

    /**
     * @see Properties#lootFrom(Supplier)
     * @deprecated If possible, define the loot table in a {@link LootTableProvider}.
     */
    @Deprecated
    @SuppressWarnings("unchecked")
    public SELF lootFrom(Supplier<? extends Block> block) {
        this.properties.lootFrom(block);
        return (SELF) this;
    }

    /** @see Properties#air() */
    @SuppressWarnings("unchecked")
    public SELF air(boolean isAir) {
        this.properties.isAir = isAir;
        return (SELF) this;
    }

    /** @see Properties#air() */
    @SuppressWarnings("unchecked")
    public SELF air() {
        this.properties.air();
        return (SELF) this;
    }

    /** @see Properties#isValidSpawn(StateArgumentPredicate) */
    public SELF isValidSpawn(boolean isValidSpawn) {
        return this.isValidSpawn((state, level, pos, entity) -> isValidSpawn);
    }

    /** @see Properties#isValidSpawn(StateArgumentPredicate) */
    @SuppressWarnings("unchecked")
    public SELF isValidSpawn(StateArgumentPredicate<EntityType<?>> isValidSpawn) {
        this.properties.isValidSpawn(isValidSpawn);
        return (SELF) this;
    }

    /** @see Properties#isRedstoneConductor(StatePredicate) */
    public SELF isRedstoneConductor(boolean isRedstoneConductor) {
        return this.isRedstoneConductor((state, level, pos) -> isRedstoneConductor);
    }

    /** @see Properties#isRedstoneConductor(StatePredicate) */
    @SuppressWarnings("unchecked")
    public SELF isRedstoneConductor(StatePredicate isRedstoneConductor) {
        this.properties.isRedstoneConductor(isRedstoneConductor);
        return (SELF) this;
    }

    /** @see Properties#isSuffocating(StatePredicate) */
    public SELF isSuffocating(boolean isSuffocating) {
        return this.isSuffocating((state, level, pos) -> isSuffocating);
    }

    /** @see Properties#isSuffocating(StatePredicate) */
    @SuppressWarnings("unchecked")
    public SELF isSuffocating(StatePredicate isSuffocating) {
        this.properties.isSuffocating(isSuffocating);
        return (SELF) this;
    }

    /** @see Properties#isViewBlocking(StatePredicate) */
    public SELF isViewBlocking(boolean isViewBlocking) {
        return this.isViewBlocking((state, level, pos) -> isViewBlocking);
    }

    /** @see Properties#isViewBlocking(StatePredicate) */
    @SuppressWarnings("unchecked")
    public SELF isViewBlocking(StatePredicate isViewBlocking) {
        this.properties.isViewBlocking(isViewBlocking);
        return (SELF) this;
    }

    /** @see Properties#hasPostProcess(StatePredicate) */
    public SELF hasPostProcess(boolean hasPostProcess) {
        return this.hasPostProcess((state, level, pos) -> hasPostProcess);
    }

    /** @see Properties#hasPostProcess(StatePredicate) */
    @SuppressWarnings("unchecked")
    public SELF hasPostProcess(StatePredicate hasPostProcess) {
        this.properties.hasPostProcess(hasPostProcess);
        return (SELF) this;
    }

    /** @see Properties#emissiveRendering(StatePredicate) */
    public SELF emissiveRendering(boolean emissiveRendering) {
        return this.emissiveRendering((state, level, pos) -> emissiveRendering);
    }

    /** @see Properties#emissiveRendering(StatePredicate) */
    @SuppressWarnings("unchecked")
    public SELF emissiveRendering(StatePredicate emissiveRendering) {
        this.properties.emissiveRendering(emissiveRendering);
        return (SELF) this;
    }

    /** @see Properties#requiresCorrectToolForDrops() */
    @SuppressWarnings("unchecked")
    public SELF requiresCorrectToolForDrops(boolean requiresCorrectToolForDrops) {
        this.properties.requiresCorrectToolForDrops = requiresCorrectToolForDrops;
        return (SELF) this;
    }

    /** @see Properties#requiresCorrectToolForDrops() */
    @SuppressWarnings("unchecked")
    public SELF requiresCorrectToolForDrops() {
        this.properties.requiresCorrectToolForDrops();
        return (SELF) this;
    }

    /** @see Properties#destroyTime(float) */
    @SuppressWarnings("unchecked")
    public SELF destroyTime(Number destroyTime) {
        this.properties.destroyTime(destroyTime.floatValue());
        return (SELF) this;
    }

    /** @see Properties#explosionResistance(float) */
    @SuppressWarnings("unchecked")
    @Deprecated(forRemoval = true, since = "2.1.0")
    public SELF explosionResistance(Number explosionResistance) {
        this.properties.explosionResistance(explosionResistance.floatValue());
        return (SELF) this;
    }

    /** @see Properties#explosionResistance(float) */
    @SuppressWarnings("unchecked")
    public SELF explosionResistance(Number explosionResistance, boolean itemAlwaysSurvivesExplosion) {
        this.explosionResistance(explosionResistance);
        this.explosionResistant = itemAlwaysSurvivesExplosion;
        return (SELF) this;
    }

    /** @see Properties#offsetType(OffsetType) */
    @SuppressWarnings("unchecked")
    public SELF offsetType(BlockBehaviour.OffsetType offsetType) {
        this.properties.offsetType(offsetType);
        return (SELF) this;
    }

    /** @see Properties#offsetType(OffsetType) */
    public SELF offsetType(Supplier<BlockBehaviour.OffsetType> offsetType) {
        return this.offsetType(offsetType.get());
    }

    /** @see Properties#offsetType(OffsetType) */
    @Deprecated(forRemoval = true, since = "1.19.4-*")
    public SELF offsetType(Function<BlockState, BlockBehaviour.OffsetType> offsetType) {
        return this.offsetType((state, level, pos) -> {
            var type = offsetType.apply(state);
            switch (type) {
                case XYZ -> {
                    Block block = state.getBlock();
                    long i = Mth.getSeed(pos.getX(), 0, pos.getZ());
                    double d0 = ((double) ((float) (i >> 4 & 15L) / 15.0F) - 1.0D) * (double) block.getMaxVerticalOffset();
                    float f = block.getMaxHorizontalOffset();
                    double d1 = Mth.clamp(((double) ((float) (i & 15L) / 15.0F) - 0.5D) * 0.5D, -f, f);
                    double d2 = Mth.clamp(((double) ((float) (i >> 8 & 15L) / 15.0F) - 0.5D) * 0.5D, -f, f);
                    return new Vec3(d1, d0, d2);
                }
                case XZ -> {
                    Block block = state.getBlock();
                    long i = Mth.getSeed(pos.getX(), 0, pos.getZ());
                    float f = block.getMaxHorizontalOffset();
                    double d0 = Mth.clamp(((double) ((float) (i & 15L) / 15.0F) - 0.5D) * 0.5D, -f, f);
                    double d1 = Mth.clamp(((double) ((float) (i >> 8 & 15L) / 15.0F) - 0.5D) * 0.5D, -f, f);
                    return new Vec3(d0, 0.0D, d1);
                }
                default -> {
                    return Vec3.ZERO;
                }
            }
        });
    }

    /** @see Properties#offsetFunction */
    @SuppressWarnings({"unchecked", "JavadocReference"})
    public SELF offsetType(BlockBehaviour.OffsetFunction offsetFunction) {
        this.properties.offsetFunction = Optional.of(offsetFunction);
        return (SELF) this;
    }

    /** @see Properties#noParticlesOnBreak() */
    @SuppressWarnings("unchecked")
    public SELF spawnParticlesOnBreak(boolean spawnParticlesOnBreak) {
        this.properties.spawnParticlesOnBreak = spawnParticlesOnBreak;
        return (SELF) this;
    }

    /** @see Properties#noParticlesOnBreak() */
    @SuppressWarnings("unchecked")
    public SELF noParticlesOnBreak() {
        this.properties.noParticlesOnBreak();
        return (SELF) this;
    }

    /** @see Properties#requiredFeatures(FeatureFlag...) */
    @SuppressWarnings("unchecked")
    public SELF requiredFeatures(FeatureFlag... featureFlags) {
        this.properties.requiredFeatures(featureFlags);
        return (SELF) this;
    }


    /* INTERNAL ONLY */

    @Internal
    public static void cleanUpBlockMemory(RegisterEvent event) {
        if (event.getRegistryKey() == RegistryDirectory.ITEM.getKey()) {
            BlockBuilder.PROPERTIES_CACHE = null;
        }
    }
}
