package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.feature.trunkplacers.TrunkPlacer;
import net.minecraft.world.level.levelgen.feature.trunkplacers.TrunkPlacerType;

import java.util.function.Function;
import java.util.function.Supplier;

public class TrunkPlacerTypeBuilder<P extends TrunkPlacer, T extends TrunkPlacerType<P>, SELF extends TrunkPlacerTypeBuilder<P, T, SELF>> extends RegistryObjectBuilder<T, TrunkPlacerType<?>, SELF> {
    private final Function<Codec<P>, T> type;
    private final Supplier<Codec<P>> codec;

    @SuppressWarnings("unchecked")
    public TrunkPlacerTypeBuilder(Supplier<Codec<P>> codec) {
        this(c -> (T) new TrunkPlacerType<>(c), codec);
    }

    public TrunkPlacerTypeBuilder(Function<Codec<P>, T> type, Supplier<Codec<P>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<TrunkPlacerType<?>> getRegistry() {
        return RegistryDirectory.TRUNK_PLACER_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
