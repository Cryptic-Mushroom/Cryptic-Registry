package com.crypticmushroom.minecraft.registry.builder;

import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;

import java.util.function.Supplier;

/**
 * <h1>Codec Type Builder</h1>
 * <p>
 * There are quite a few registries that merely hold {@link Codec codecs} of a certain type (i.e.
 * {@link net.minecraft.core.registries.Registries#BIOME_SOURCE},
 * {@link net.minecraft.core.registries.Registries#CHUNK_GENERATOR}, etc.). It would be imprudent and a waste of time to
 * build a registry object builder for each one, so then solution was instead to create a builder that accounted for all
 * cases while still being able to provide subtypes of the codec in question or its type. This builder is that
 * solution.
 *
 * @param <OBJ>  The type of the codec's type used in the registry (the part after the {@code ? extends}).
 * @param <CDC>  The codec containing the subtype that we want to build.
 * @param <SELF> The type of this builder for casting in any superclass methods (see {@link RegistryObjectBuilder}).
 */
public class CodecTypeBuilder<OBJ, CDC extends Codec<? extends OBJ>, SELF extends CodecTypeBuilder<OBJ, CDC, SELF>> extends RegistryObjectBuilder<CDC, Codec<? extends OBJ>, SELF> {
    private final RegistryInfo.Static<Codec<? extends OBJ>> registry;
    private final Supplier<CDC> type;

    public CodecTypeBuilder(RegistryInfo.Static<Codec<? extends OBJ>> registry, Class<OBJ> clazz, Supplier<CDC> type) {
        this(registry, type);
    }

    public CodecTypeBuilder(RegistryInfo.Static<Codec<? extends OBJ>> registry, Supplier<CDC> type) {
        this.registry = registry;
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<Codec<? extends OBJ>> getRegistry() {
        return this.registry;
    }

    @Override
    protected CDC buildType() {
        return this.type.get();
    }
}
