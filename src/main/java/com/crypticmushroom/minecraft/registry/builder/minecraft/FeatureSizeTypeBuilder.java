package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.feature.featuresize.FeatureSize;
import net.minecraft.world.level.levelgen.feature.featuresize.FeatureSizeType;

import java.util.function.Function;
import java.util.function.Supplier;

public class FeatureSizeTypeBuilder<S extends FeatureSize, T extends FeatureSizeType<S>, SELF extends FeatureSizeTypeBuilder<S, T, SELF>> extends RegistryObjectBuilder<T, FeatureSizeType<?>, SELF> {
    private final Function<Codec<S>, T> type;
    private final Supplier<Codec<S>> codec;

    @SuppressWarnings("unchecked")
    public FeatureSizeTypeBuilder(Supplier<Codec<S>> codec) {
        this(c -> (T) new FeatureSizeType<>(c), codec);
    }

    public FeatureSizeTypeBuilder(Function<Codec<S>, T> type, Supplier<Codec<S>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<FeatureSizeType<?>> getRegistry() {
        return RegistryDirectory.FEATURE_SIZE_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
