package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import it.unimi.dsi.fastutil.objects.Object2FloatFunction;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.damagesource.DamageEffects;
import net.minecraft.world.damagesource.DamageScaling;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.damagesource.DeathMessageType;

import java.util.function.Function;
import java.util.function.Supplier;

public class DamageTypeBuilder<SELF extends DamageTypeBuilder<SELF>> extends DataResourceBuilder<DamageType, DamageType, SELF> {
    private Function<BootstapContext<DamageType>, String> msgId;
    private Function<BootstapContext<DamageType>, DamageScaling> scaling = boostrap -> DamageScaling.WHEN_CAUSED_BY_LIVING_NON_PLAYER;
    private Object2FloatFunction<BootstapContext<DamageType>> exhaustion;
    private Function<BootstapContext<DamageType>, DamageEffects> effects = bootstrap -> DamageEffects.HURT;
    private Function<BootstapContext<DamageType>, DeathMessageType> deathMessageType = bootstrap -> DeathMessageType.DEFAULT;

    @Override
    protected RegistryInfo.Dynamic<DamageType> getRegistry() {
        return RegistryDirectory.DAMAGE_TYPE;
    }

    @Override
    protected DamageType buildType(BootstapContext<DamageType> bootstrap) {
        return new DamageType(
            this.checkAttribute(this.msgId.apply(bootstrap), "message ID"),
            this.checkAttribute(this.scaling.apply(bootstrap), "damage scaling"),
            this.checkAttribute(this.exhaustion.apply(bootstrap), "exhaustion"),
            this.checkAttribute(this.effects.apply(bootstrap), "damage effects"),
            this.checkAttribute(this.deathMessageType.apply(bootstrap), "death message type")
        );
    }


    /* DAMAGE TYPE */

    public SELF msgId(String msgId) {
        return this.msgId(bootstrap -> msgId);
    }

    public SELF msgId(Supplier<String> msgId) {
        return this.msgId(bootstrap -> msgId.get());
    }

    @SuppressWarnings("unchecked")
    public SELF msgId(Function<BootstapContext<DamageType>, String> msgId) {
        this.msgId = msgId;
        return (SELF) this;
    }

    public SELF scaling(DamageScaling scaling) {
        return this.scaling(bootstrap -> scaling);
    }

    public SELF scaling(Supplier<DamageScaling> scaling) {
        return this.scaling(bootstrap -> scaling.get());
    }

    @SuppressWarnings("unchecked")
    public SELF scaling(Function<BootstapContext<DamageType>, DamageScaling> scaling) {
        this.scaling = scaling;
        return (SELF) this;
    }

    public SELF exhaustion(float exhaustion) {
        return this.exhaustion(bootstrap -> exhaustion);
    }

    public SELF exhaustion(Supplier<Float> exhaustion) {
        return this.exhaustion(bootstrap -> exhaustion.get());
    }

    @SuppressWarnings("unchecked")
    public SELF exhaustion(Object2FloatFunction<BootstapContext<DamageType>> exhaustion) {
        this.exhaustion = exhaustion;
        return (SELF) this;
    }

    public SELF effects(DamageEffects effects) {
        return this.effects(bootstrap -> effects);
    }

    public SELF effects(Supplier<DamageEffects> effects) {
        return this.effects(bootstrap -> effects.get());
    }

    @SuppressWarnings("unchecked")
    public SELF effects(Function<BootstapContext<DamageType>, DamageEffects> effects) {
        this.effects = effects;
        return (SELF) this;
    }

    public SELF deathMessageType(DeathMessageType deathMessageType) {
        return this.deathMessageType(bootstrap -> deathMessageType);
    }

    public SELF deathMessageType(Supplier<DeathMessageType> deathMessageType) {
        return this.deathMessageType(bootstrap -> deathMessageType.get());
    }

    @SuppressWarnings("unchecked")
    public SELF deathMessageType(Function<BootstapContext<DamageType>, DeathMessageType> deathMessageType) {
        this.deathMessageType = deathMessageType;
        return (SELF) this;
    }
}
