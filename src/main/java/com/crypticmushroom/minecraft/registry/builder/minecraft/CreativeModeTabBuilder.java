package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.CustomResourceBuilder;
import com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.accessor.CreativeModeTabBuilderAccessor;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.Style;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.event.CreativeModeTabEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class CreativeModeTabBuilder<T extends CreativeModeTab, SELF extends CreativeModeTabBuilder<T, SELF>> extends CustomResourceBuilder.Named<T, CreativeModeTab, CreativeModeTabEvent.Register, SELF> {
    private final List<UnaryOperator<CreativeModeTab.Builder>> tabBuilderOptions = new ArrayList<>();

    private final List<UnaryOperator<MutableComponent>> titleOptions = new ArrayList<>();

    public CreativeModeTabBuilder() { }

    @SuppressWarnings("unchecked")
    public CreativeModeTabBuilder(Function<CreativeModeTab.Builder, T> type) {
        this.tabBuilderOptions.add(b -> b.withTabFactory((Function<CreativeModeTab.Builder, CreativeModeTab>) type));
    }

    @Override
    public RegistryInfo.Custom<CreativeModeTab> getRegistry() {
        return RegistryDirectory.CREATIVE_MODE_TAB;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected T buildType(CreativeModeTabEvent.Register event) {
        var id = this.makeResLoc();
        return (T) event.registerCreativeModeTab(id, builder -> {
            // localization
            //noinspection DataFlowIssue
            var component = Component.translatable(this.getTranslationKey());
            this.titleOptions.forEach(o -> o.apply(component));
            builder.title(component);

            // builder options
            this.tabBuilderOptions.forEach(o -> o.apply(builder));
        });
    }

    @Override
    public String getTranslationKey() {
        return "tabs.%s.%s".formatted(this.getModId(), this.getId());
    }

    /* CREATIVE MODE TAB */

    @SuppressWarnings("unchecked")
    public SELF titleStyle(UnaryOperator<Style> style) {
        this.titleOptions.add(s -> s.withStyle(style));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF titleStyle(Style style) {
        this.titleOptions.add(s -> s.withStyle(style));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF titleStyle(ChatFormatting... style) {
        this.titleOptions.add(s -> s.withStyle(style));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF titleStyle(ChatFormatting style) {
        this.titleOptions.add(s -> s.withStyle(style));
        return (SELF) this;
    }

    public SELF icon(Supplier<? extends ItemLike> icon) {
        return this.iconStack(() -> icon.get().asItem().getDefaultInstance());
    }

    @SuppressWarnings("unchecked")
    public SELF iconStack(Supplier<ItemStack> icon) {
        this.tabBuilderOptions.add(b -> b.icon(icon));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF displayItems(CreativeModeTab.DisplayItemsGenerator displayItemsGenerator) {
        this.tabBuilderOptions.add(b -> b.displayItems(displayItemsGenerator));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF alignedRight() {
        this.tabBuilderOptions.add(CreativeModeTab.Builder::alignedRight);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF hideTitle() {
        this.tabBuilderOptions.add(CreativeModeTab.Builder::hideTitle);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF noScrollBar() {
        this.tabBuilderOptions.add(CreativeModeTab.Builder::noScrollBar);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF type(CreativeModeTab.Type type) {
        this.tabBuilderOptions.add(b -> ((CreativeModeTabBuilderAccessor) b).cmreg$type(type));
        return (SELF) this;
    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public SELF backgroundSuffix(String backgroundSuffix) {
        this.tabBuilderOptions.add(b -> b.backgroundSuffix(backgroundSuffix));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF background(ResourceLocation background) {
        this.tabBuilderOptions.add(b -> b.withBackgroundLocation(background));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF withSearchBar() {
        this.tabBuilderOptions.add(CreativeModeTab.Builder::withSearchBar);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF withSearchBar(int searchBarWidth) {
        this.tabBuilderOptions.add(b -> b.withSearchBar(searchBarWidth));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF tabsImage(ResourceLocation tabsImage) {
        this.tabBuilderOptions.add(b -> b.withTabsImage(tabsImage));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF labelColor(int labelColor) {
        this.tabBuilderOptions.add(b -> b.withLabelColor(labelColor));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF slotColor(int slotColor) {
        this.tabBuilderOptions.add(b -> b.withSlotColor(slotColor));
        return (SELF) this;
    }
}
