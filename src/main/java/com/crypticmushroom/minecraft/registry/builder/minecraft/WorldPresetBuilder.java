package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.dimension.LevelStem;
import net.minecraft.world.level.levelgen.presets.WorldPreset;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

public class WorldPresetBuilder<P extends WorldPreset, SELF extends WorldPresetBuilder<P, SELF>> extends DataResourceBuilder<P, WorldPreset, SELF> {
    private final Function<Map<ResourceKey<LevelStem>, LevelStem>, P> type;

    private Map<ResourceKey<LevelStem>, BiFunction<BootstapContext<WorldPreset>, ResourceKey<LevelStem>, LevelStem>> dimensions;

    @Deprecated
    @SuppressWarnings("unchecked")
    public WorldPresetBuilder() {
        this(m -> (P) new WorldPreset(m));
    }

    public WorldPresetBuilder(Function<Map<ResourceKey<LevelStem>, LevelStem>, P> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Dynamic<WorldPreset> getRegistry() {
        return RegistryDirectory.WORLD_PRESET;
    }

    @Override
    protected P buildType(BootstapContext<WorldPreset> bootstap) {
        var builtDimensions = new HashMap<ResourceKey<LevelStem>, LevelStem>();
        this.dimensions.forEach((k, v) -> builtDimensions.put(k, v.apply(bootstap, k)));
        return this.type.apply(builtDimensions);
    }


    /* WORLD PRESET */

    @SuppressWarnings("unchecked")
    public SELF dimensions(ResourceKey<LevelStem> key, BiFunction<BootstapContext<WorldPreset>, ResourceKey<LevelStem>, LevelStem> dimensionGetter) {
        this.dimensions.put(key, dimensionGetter);
        return (SELF) this;
    }
}
