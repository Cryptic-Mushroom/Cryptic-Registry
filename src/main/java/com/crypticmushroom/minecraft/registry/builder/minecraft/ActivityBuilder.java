package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.entity.schedule.Activity;

import java.util.function.Function;

public class ActivityBuilder<A extends Activity, SELF extends ActivityBuilder<A, SELF>> extends RegistryObjectBuilder<A, Activity, SELF> {
    private final ActivityFunction<A> type;

    public ActivityBuilder() {
        this(ActivityFunction.defaultType());
    }

    public ActivityBuilder(ActivityFunction<A> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<Activity> getRegistry() {
        return RegistryDirectory.ACTIVITY;
    }

    @Override
    protected A buildType() {
        return this.type.apply(this.makeResLocString());
    }

    public interface ActivityFunction<A extends Activity> extends Function<String, A> {
        @Override
        A apply(String name);

        @SuppressWarnings("unchecked")
        static <A extends Activity> ActivityFunction<A> defaultType() {
            return name -> (A) new Activity(name);
        }
    }
}
