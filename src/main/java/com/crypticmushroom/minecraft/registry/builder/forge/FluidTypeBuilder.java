package com.crypticmushroom.minecraft.registry.builder.forge;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraftforge.client.extensions.common.IClientFluidTypeExtensions;
import net.minecraftforge.common.SoundAction;
import net.minecraftforge.fluids.FluidType;
import org.jetbrains.annotations.Nullable;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class FluidTypeBuilder<T extends FluidType, SELF extends FluidTypeBuilder<T, SELF>> extends RegistryObjectBuilder.Named<T, FluidType, SELF> {
    private final BiFunction<FluidType.Properties, IClientFluidTypeExtensions, T> type;
    private final FluidType.Properties properties;

    private Supplier<IClientFluidTypeExtensions> fluidTypeExtensions;

    public FluidTypeBuilder() {
        this(FluidType.Properties::create);
    }

    @SuppressWarnings("unchecked")
    public FluidTypeBuilder(Supplier<FluidType.Properties> properties) {
        this((p, e) -> (T) new FluidType(p) {
            @Override
            public void initializeClient(Consumer<IClientFluidTypeExtensions> consumer) {
                consumer.accept(e);
            }
        }, properties);
    }

    public FluidTypeBuilder(BiFunction<FluidType.Properties, IClientFluidTypeExtensions, T> type) {
        this(type, FluidType.Properties::create);
    }

    public FluidTypeBuilder(BiFunction<FluidType.Properties, IClientFluidTypeExtensions, T> type, Supplier<FluidType.Properties> properties) {
        this.type = type;
        this.properties = properties.get();
    }

    @Override
    protected RegistryInfo.Static<FluidType> getRegistry() {
        return RegistryDirectory.FLUID_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.properties, this.fluidTypeExtensions.get());
    }


    /* FLUID TYPE */

    @SuppressWarnings("unchecked")
    public SELF clientExtensions(Supplier<IClientFluidTypeExtensions> fluidTypeExtensions) {
        this.fluidTypeExtensions = fluidTypeExtensions;
        return (SELF) this;
    }

    /**
     * Sets the identifier representing the name of the fluid type.
     *
     * @param descriptionId the identifier representing the name of the fluid type
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF descriptionId(String descriptionId) {
        this.properties.descriptionId(descriptionId);
        return (SELF) this;
    }

    /**
     * Sets how much the velocity of the fluid should be scaled by.
     *
     * @param motionScale a scalar to multiply to the fluid velocity
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF motionScale(double motionScale) {
        this.properties.motionScale(motionScale);
        return (SELF) this;
    }

    /**
     * Sets whether the fluid can push an entity.
     *
     * @param canPushEntity if the fluid can push an entity
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF canPushEntity(boolean canPushEntity) {
        this.properties.canPushEntity(canPushEntity);
        return (SELF) this;
    }

    /**
     * Sets whether the fluid can be swum in.
     *
     * @param canSwim if the fluid can be swum in
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF canSwim(boolean canSwim) {
        this.properties.canSwim(canSwim);
        return (SELF) this;
    }

    /**
     * Sets whether the fluid can drown something.
     *
     * @param canDrown if the fluid can drown something
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF canDrown(boolean canDrown) {
        this.properties.canDrown(canDrown);
        return (SELF) this;
    }

    /**
     * Sets how much the fluid should scale the damage done when hitting the ground per tick.
     *
     * @param fallDistanceModifier a scalar to multiply to the fall damage
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF fallDistanceModifier(float fallDistanceModifier) {
        this.properties.fallDistanceModifier(fallDistanceModifier);
        return (SELF) this;
    }

    /**
     * Sets whether the fluid can extinguish.
     *
     * @param canExtinguish if the fluid can extinguish
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF canExtinguish(boolean canExtinguish) {
        this.properties.canExtinguish(canExtinguish);
        return (SELF) this;
    }

    /**
     * Sets whether the fluid can create a source.
     *
     * @param canConvertToSource if the fluid can create a source
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF canConvertToSource(boolean canConvertToSource) {
        this.properties.canConvertToSource(canConvertToSource);
        return (SELF) this;
    }

    /**
     * Sets whether the fluid supports boating.
     *
     * @param supportsBoating if the fluid supports boating
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF supportsBoating(boolean supportsBoating) {
        this.properties.supportsBoating(supportsBoating);
        return (SELF) this;
    }

    /**
     * Sets the path type of this fluid.
     *
     * @param pathType the path type of this fluid
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF pathType(@Nullable BlockPathTypes pathType) {
        this.properties.pathType(pathType);
        return (SELF) this;
    }

    /**
     * Sets the path type of the adjacent fluid. Path types with a negative malus are not traversable. Pathfinding will
     * favor paths consisting of a lower malus.
     *
     * @param adjacentPathType the path type of this fluid
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF adjacentPathType(@Nullable BlockPathTypes adjacentPathType) {
        this.properties.adjacentPathType(adjacentPathType);
        return (SELF) this;
    }

    /**
     * Sets a sound to play when a certain action is performed.
     *
     * @param action the action being performed
     * @param sound  the sound to play when performing the action
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF sound(SoundAction action, SoundEvent sound) {
        this.properties.sound(action, sound);
        return (SELF) this;
    }

    /**
     * Sets whether the fluid can hydrate.
     *
     * <p>Hydration is an arbitrary word which depends on the implementation.
     *
     * @param canHydrate if the fluid can hydrate
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF canHydrate(boolean canHydrate) {
        this.properties.canHydrate(canHydrate);
        return (SELF) this;
    }

    /**
     * Sets the light level emitted by the fluid.
     *
     * @param lightLevel the light level emitted by the fluid
     * @return This builder
     *
     * @throws IllegalArgumentException if light level is not between [0,15]
     */
    @SuppressWarnings("unchecked")
    public SELF lightLevel(int lightLevel) {
        this.properties.lightLevel(lightLevel);
        return (SELF) this;
    }

    /**
     * Sets the density of the fluid.
     *
     * @param density the density of the fluid
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF density(int density) {
        this.properties.density(density);
        return (SELF) this;
    }

    /**
     * Sets the temperature of the fluid.
     *
     * @param temperature the temperature of the fluid
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF temperature(int temperature) {
        this.properties.temperature(temperature);
        return (SELF) this;
    }

    /**
     * Sets the viscosity, or thickness, of the fluid.
     *
     * @param viscosity the viscosity of the fluid
     * @return This builder
     *
     * @throws IllegalArgumentException if viscosity is negative
     */
    @SuppressWarnings("unchecked")
    public SELF viscosity(int viscosity) {
        this.properties.viscosity(viscosity);
        return (SELF) this;
    }

    /**
     * Sets the rarity of the fluid.
     *
     * @param rarity the rarity of the fluid
     * @return This builder
     */
    @SuppressWarnings("unchecked")
    public SELF rarity(Rarity rarity) {
        this.properties.rarity(rarity);
        return (SELF) this;
    }
}
