package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.accessor.EntityTypeAccessor;
import com.crypticmushroom.minecraft.registry.data.provider.loot.CrypticEntityLootSubProvider;
import com.crypticmushroom.minecraft.registry.data.registry.DataRegistry;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.google.common.collect.ImmutableSet;
import com.mojang.datafixers.util.Function15;
import net.minecraft.Util;
import net.minecraft.util.datafix.fixes.References;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.flag.FeatureFlag;
import net.minecraft.world.flag.FeatureFlagSet;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.network.PlayMessages;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

/**
 * The entity type builder contains various methods that allow for building virtually any type (that extends
 * {@link Entity}) with ease and allows for several automatic data generation features.
 *
 * @param <E>    The type of entity to be built.
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @apiNote This builder is marked as "named" and "tagged". While it is possible, it will be very difficult to
 *     override this behavior in a subclass.
 * @implNote This builder builds a registry object for an {@link EntityType}, so bear that in mind when extending
 *     and/or working with this class.
 * @see RegistryObjectBuilder
 * @see EntityType.Builder
 * @since 0.1.0
 */
public class EntityTypeBuilder<E extends Entity, T extends EntityType<E>, LP extends CrypticEntityLootSubProvider, SELF extends EntityTypeBuilder<E, T, LP, SELF>> extends RegistryObjectBuilder.Named<T, EntityType<?>, SELF> {
    private final EntityTypeFunction<E, T> type;

    private final EntityType.EntityFactory<E> factory;
    private MobCategory category;
    private final List<Supplier<Block>> immuneTo = new LinkedList<>();
    private boolean serialize = true;
    private boolean summon = true;
    private boolean fireImmune;
    private boolean canSpawnFarFromPlayer;
    private EntityDimensions dimensions = EntityDimensions.scalable(0.6F, 1.8F);
    private FeatureFlagSet featureFlags = FeatureFlags.VANILLA_SET;

    private Predicate<EntityType<?>> velocityUpdateSupplier = e -> ((EntityTypeAccessor) e).cmreg$defaultVelocitySupplier();
    private ToIntFunction<EntityType<?>> trackingRangeSupplier = e -> ((EntityTypeAccessor) e).cmreg$defaultTrackingRangeSupplier();
    private ToIntFunction<EntityType<?>> updateIntervalSupplier = e -> ((EntityTypeAccessor) e).cmreg$defaultUpdateIntervalSupplier();
    private BiFunction<PlayMessages.SpawnEntity, Level, E> customClientFactory;

    // FOR DATA
    private @Nullable BiConsumer<LP, RegistryObject<T>> lootBuilder;

    public EntityTypeBuilder(EntityType.EntityFactory<E> factory) {
        this(EntityTypeFunction.defaultType(factory), factory);
    }

    public EntityTypeBuilder(EntityTypeFunction<E, T> type, EntityType.EntityFactory<E> factory) {
        this.type = type;
        this.factory = factory;
    }

    @Override
    protected RegistryInfo.Static<EntityType<?>> getRegistry() {
        return RegistryDirectory.ENTITY_TYPE;
    }

    @Override
    public LocalizedNameRegistry.Normal<EntityType<?>> getLocalizedNameRegistry() {
        return LocalizedNameRegistry.ENTITY_TYPE;
    }

    @Override
    public RegistryObject<T> build() {
        var registered = super.build();

        // register loot table (if applicable)
        this.registerLoot(registered);

        return registered;
    }

    @Override
    public T buildType() {
        if (this.serialize) {
            Util.fetchChoiceType(References.ENTITY_TREE, this.makeResLocString());
        }

        return this.type.apply(
            this.factory,
            this.category,
            this.serialize,
            this.summon,
            this.fireImmune,
            this.canSpawnFarFromPlayer,
            this.immuneTo.stream().map(Supplier::get).collect(ImmutableSet.toImmutableSet()),
            this.dimensions,
            5,
            3,
            this.featureFlags,
            this.velocityUpdateSupplier,
            this.trackingRangeSupplier,
            this.updateIntervalSupplier,
            this.customClientFactory
        );
    }

    @SuppressWarnings("unchecked")
    protected Class<LP> getLootProviderType() {
        return (Class<LP>) CrypticEntityLootSubProvider.class;
    }

    private void registerLoot(RegistryObject<T> entityType) {
        if (this.lootBuilder == null) return;

        DataRegistry.registerEntityLootBuilder(this.getModId(), entityType, this.getLootProviderType(), this.lootBuilder);
    }

    @FunctionalInterface
    public interface EntityTypeFunction<E extends Entity, ET extends EntityType<E>> extends Function15<EntityType.EntityFactory<E>, MobCategory, Boolean, Boolean, Boolean, Boolean, ImmutableSet<Block>, EntityDimensions, Integer, Integer, FeatureFlagSet, Predicate<EntityType<?>>, ToIntFunction<EntityType<?>>, ToIntFunction<EntityType<?>>, BiFunction<PlayMessages.SpawnEntity, Level, E>, ET> {
        @Override
        ET apply(EntityType.EntityFactory<E> factory, MobCategory category, Boolean serialize, Boolean summon,
                 Boolean fireImmune, Boolean canSpawnFarFromPlayer, ImmutableSet<Block> immuneTo,
                 EntityDimensions dimensions, Integer clientTrackingRange, Integer updateInterval,
                 FeatureFlagSet featureFlagSet,
                 Predicate<EntityType<?>> velocityUpdateSupplier, ToIntFunction<EntityType<?>> trackingRangeSupplier,
                 ToIntFunction<EntityType<?>> updateIntervalSupplier,
                 BiFunction<PlayMessages.SpawnEntity, Level, E> customClientFactory);

        default ET apply(EntityType.EntityFactory<E> factory, MobCategory category, boolean serialize, boolean summon,
                         boolean fireImmune, boolean canSpawnFarFromPlayer, ImmutableSet<Block> immuneTo,
                         EntityDimensions dimensions, int clientTrackingRange, int updateInterval,
                         FeatureFlagSet featureFlagSet,
                         Predicate<EntityType<?>> velocityUpdateSupplier,
                         ToIntFunction<EntityType<?>> trackingRangeSupplier,
                         ToIntFunction<EntityType<?>> updateIntervalSupplier,
                         BiFunction<PlayMessages.SpawnEntity, Level, E> customClientFactory) {
            return this.apply(factory, category, (Boolean) serialize, (Boolean) summon, (Boolean) fireImmune, (Boolean) canSpawnFarFromPlayer, immuneTo, dimensions, (Integer) clientTrackingRange, (Integer) updateInterval, featureFlagSet, velocityUpdateSupplier, trackingRangeSupplier, updateIntervalSupplier, customClientFactory);
        }

        @SuppressWarnings("unchecked")
        static <E extends Entity, ET extends EntityType<E>> EntityTypeFunction<E, ET> defaultType(EntityType.EntityFactory<E> factory) {
            return (f, category, serialize, summon, fireImmune, canSpawnFarFromPlayer, immuneTo, dimensions, clientTrackingRange, updateInterval, featureFlagSet, velocityUpdateSupplier, trackingRangeSupplier, updateIntervalSupplier, customClientFactory) -> (ET) new EntityType<>(f, category, serialize, summon, fireImmune, canSpawnFarFromPlayer, immuneTo, dimensions, clientTrackingRange, updateInterval, featureFlagSet, velocityUpdateSupplier, trackingRangeSupplier, updateIntervalSupplier, customClientFactory);
        }
    }


    /* ENTITY LOOT GENERATION */

    @SuppressWarnings("unchecked")
    public SELF lootTable(BiConsumer<LP, RegistryObject<T>> lootBuilder) {
        this.lootBuilder = lootBuilder;

        return (SELF) this;
    }


    /* ENTITYTYPE BUILDING */

    @SuppressWarnings("unchecked")
    public SELF category(MobCategory category) {
        this.category = category;
        return (SELF) this;
    }

    public SELF sized(float width, float height) {
        return this.sized(EntityDimensions.scalable(width, height));
    }

    @SuppressWarnings("unchecked")
    public SELF sized(EntityDimensions dimensions) {
        this.dimensions = dimensions;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF noSummon() {
        this.summon = false;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF noSave() {
        this.serialize = false;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF fireImmune() {
        this.fireImmune = true;
        return (SELF) this;
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public final SELF immuneTo(Supplier<Block>... block) {
        for (Supplier<Block> b : block) {
            this.immuneTo(b);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF immuneTo(Supplier<Block> block) {
        this.immuneTo.add(block);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF canSpawnFarFromPlayer() {
        this.canSpawnFarFromPlayer = true;
        return (SELF) this;
    }

    public SELF clientTrackingRange(int clientTrackingRange) {
        return this.clientTrackingRange(t -> clientTrackingRange);
    }

    @SuppressWarnings("unchecked")
    public SELF clientTrackingRange(ToIntFunction<EntityType<?>> clientTrackingRange) {
        this.trackingRangeSupplier = clientTrackingRange;
        return (SELF) this;
    }

    public SELF updateInterval(int updateInterval) {
        return this.updateInterval(t -> updateInterval);
    }

    @SuppressWarnings("unchecked")
    public SELF updateInterval(ToIntFunction<EntityType<?>> updateInterval) {
        this.updateIntervalSupplier = updateInterval;
        return (SELF) this;
    }

    public SELF shouldReceiveVelocityUpdates(boolean shouldReceiveVelocityUpdates) {
        return this.shouldReceiveVelocityUpdates(t -> shouldReceiveVelocityUpdates);
    }

    @SuppressWarnings("unchecked")
    public SELF shouldReceiveVelocityUpdates(Predicate<EntityType<?>> shouldReceiveVelocityUpdates) {
        this.velocityUpdateSupplier = shouldReceiveVelocityUpdates;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF customClientFactory(BiFunction<PlayMessages.SpawnEntity, Level, E> customClientFactory) {
        this.customClientFactory = customClientFactory;
        return (SELF) this;
    }

    public SELF requiredFeatures(FeatureFlag... requiredFeatures) {
        this.featureFlags = FeatureFlags.REGISTRY.subset(requiredFeatures);
        return (SELF) this;
    }
}
