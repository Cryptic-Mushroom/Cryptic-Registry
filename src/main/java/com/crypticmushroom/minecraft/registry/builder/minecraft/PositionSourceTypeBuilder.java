package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.level.gameevent.PositionSource;
import net.minecraft.world.level.gameevent.PositionSourceType;

import java.util.function.Supplier;

public class PositionSourceTypeBuilder<S extends PositionSource, T extends PositionSourceType<S>, SELF extends PositionSourceTypeBuilder<S, T, SELF>> extends RegistryObjectBuilder<T, PositionSourceType<?>, SELF> {
    private final Supplier<T> type;

    public PositionSourceTypeBuilder(Supplier<T> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<PositionSourceType<?>> getRegistry() {
        return RegistryDirectory.POSITION_SOURCE_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.get();
    }
}
