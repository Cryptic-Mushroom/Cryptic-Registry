package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Supplier;

public class PotionBuilder<P extends Potion, SELF extends PotionBuilder<P, SELF>> extends RegistryObjectBuilder<P, Potion, SELF> {
    private final BiFunction<String, MobEffectInstance[], P> type;

    private final List<Supplier<? extends MobEffectInstance>> instances = new LinkedList<>();

    private String localizedName = null;
    private @Nullable String tippedArrowLocalization;
    private @Nullable String potionLocalization;
    private @Nullable String splashPotionLocalization;
    private @Nullable String lingeringPotionLocalization;

    @SuppressWarnings("unchecked")
    public PotionBuilder() {
        this((s, e) -> (P) new Potion(s, e));
    }

    public PotionBuilder(BiFunction<String, MobEffectInstance[], P> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<Potion> getRegistry() {
        return RegistryDirectory.POTION;
    }

    @Override
    public RegistryObject<P> build() {
        var registered = super.build();

        this.checkAttribute(this.localizedName, "localized name");

        LocalizedNameRegistry.OTHER.register(this.getModId(), "item.minecraft.tipped_arrow.effect." + this.getId(), Objects.requireNonNullElseGet(this.tippedArrowLocalization, () -> "Arrow of " + this.localizedName));
        LocalizedNameRegistry.OTHER.register(this.getModId(), "item.minecraft.potion.effect." + this.getId(), Objects.requireNonNullElseGet(this.potionLocalization, () -> "Potion of " + this.localizedName));
        LocalizedNameRegistry.OTHER.register(this.getModId(), "item.minecraft.splash_potion.effect." + this.getId(), Objects.requireNonNullElseGet(this.splashPotionLocalization, () -> "Splash Potion of " + this.localizedName));
        LocalizedNameRegistry.OTHER.register(this.getModId(), "item.minecraft.lingering_potion.effect." + this.getId(), Objects.requireNonNullElseGet(this.lingeringPotionLocalization, () -> "Lingering Potion of " + this.localizedName));

        return registered;
    }

    @Override
    protected P buildType() {
        return this.type.apply(this.getId(), this.instances.stream().map(e -> (MobEffectInstance) e.get()).toArray(MobEffectInstance[]::new));
    }


    /* POTION */

    @SuppressWarnings("unchecked")
    public SELF effect(Supplier<? extends MobEffectInstance> effect) {
        this.instances.add(effect);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF effect(Supplier<? extends MobEffectInstance>... effect) {
        for (Supplier<? extends MobEffectInstance> p : effect) {
            this.effect(p);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF effect(Collection<Supplier<? extends MobEffectInstance>> effect) {
        effect.forEach(this::effect);
        return (SELF) this;
    }


    /* LOCALIZATION */

    @SuppressWarnings("unchecked")
    public SELF localizedName(String localizedName) {
        this.localizedName = localizedName;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF tippedArrowLocalization(String tippedArrowLocalization) {
        this.tippedArrowLocalization = tippedArrowLocalization;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF potionLocalization(String potionLocalization) {
        this.potionLocalization = potionLocalization;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF splashPotionLocalization(String splashPotionLocalization) {
        this.splashPotionLocalization = splashPotionLocalization;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF lingeringPotionLocalization(String lingeringPotionLocalization) {
        this.lingeringPotionLocalization = lingeringPotionLocalization;
        return (SELF) this;
    }
}
