package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProviderType;

import java.util.function.Function;
import java.util.function.Supplier;

public class BlockStateProviderTypeBuilder<P extends BlockStateProvider, T extends BlockStateProviderType<P>, SELF extends BlockStateProviderTypeBuilder<P, T, SELF>> extends RegistryObjectBuilder<T, BlockStateProviderType<?>, SELF> {
    private final Function<Codec<P>, T> type;
    private final Supplier<Codec<P>> codec;

    @SuppressWarnings("unchecked")
    public BlockStateProviderTypeBuilder(Supplier<Codec<P>> codec) {
        this(c -> (T) new BlockStateProviderType<>(c), codec);
    }

    public BlockStateProviderTypeBuilder(Function<Codec<P>, T> type, Supplier<Codec<P>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<BlockStateProviderType<?>> getRegistry() {
        return RegistryDirectory.BLOCKSTATE_PROVIDER_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
