package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.stats.StatFormatter;
import net.minecraft.stats.Stats;

public class StatBuilder<SELF extends StatBuilder<SELF>> {
    private final StatLocationBuilder builder = new StatLocationBuilder();

    public ResourceLocation build() {
        return this.builder.build().getId();
    }


    /* CUSTOM STAT */

    @SuppressWarnings("unchecked")
    public SELF id(String id) {
        this.builder.id(id);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF formatter(StatFormatter formatter) {
        this.builder.formatter(formatter);
        return (SELF) this;
    }

    protected static final class StatLocationBuilder extends RegistryObjectBuilder<ResourceLocation, ResourceLocation, StatLocationBuilder> {
        private StatFormatter formatter;

        @Override
        protected RegistryInfo.Static<ResourceLocation> getRegistry() {
            return RegistryDirectory.CUSTOM_STAT;
        }

        @Override
        protected ResourceLocation buildType() {
            var id = this.makeResLoc();
            Stats.CUSTOM.get(id, this.formatter);
            return id;
        }

        public StatLocationBuilder formatter(StatFormatter formatter) {
            this.formatter = formatter;
            return this;
        }
    }
}
