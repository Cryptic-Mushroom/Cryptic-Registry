package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.core.Holder;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.item.Instrument;
import net.minecraftforge.registries.RegistryObject;

import java.util.Objects;
import java.util.function.Supplier;

public class InstrumentBuilder<SELF extends InstrumentBuilder<SELF>> extends RegistryObjectBuilder<Instrument, Instrument, SELF> {
    private Supplier<Holder<SoundEvent>> soundEvent = null;
    private Integer useDuration = null;
    private Float range = null;

    @Override
    protected RegistryInfo.Static<Instrument> getRegistry() {
        return RegistryDirectory.INSTRUMENT;
    }

    @Override
    public RegistryObject<Instrument> build() {
        try {
            Objects.requireNonNull(this.soundEvent, "Cannot create an instrument type without a sound event");
            Objects.requireNonNull(this.useDuration, "Cannot create an instrument type without a use duration");
            Objects.requireNonNull(this.range, "Cannot create an instrument type without a range");
        } catch (NullPointerException e) {
            throw new IncompleteBuilderException(e.getMessage());
        }

        return super.build();
    }

    @Override
    protected Instrument buildType() {
        return new Instrument(
            this.soundEvent.get(),
            this.useDuration,
            this.range
        );
    }


    /* INSTRUMENT */

    @SuppressWarnings("unchecked")
    public SELF soundEvent(RegistryObject<? extends SoundEvent> soundEvent) {
        return this.soundEvent(() -> (Holder<SoundEvent>) soundEvent.getHolder().orElseThrow());
    }

    @SuppressWarnings("unchecked")
    public SELF soundEvent(Supplier<Holder<SoundEvent>> soundEvent) {
        this.soundEvent = soundEvent;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF useDuration(int useDuration) {
        this.useDuration = useDuration;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF range(Number range) {
        this.range = range.floatValue();
        return (SELF) this;
    }
}
