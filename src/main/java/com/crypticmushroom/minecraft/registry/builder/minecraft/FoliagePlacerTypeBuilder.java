package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.feature.foliageplacers.FoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.FoliagePlacerType;

import java.util.function.Function;
import java.util.function.Supplier;

public class FoliagePlacerTypeBuilder<P extends FoliagePlacer, T extends FoliagePlacerType<P>, SELF extends FoliagePlacerTypeBuilder<P, T, SELF>> extends RegistryObjectBuilder<T, FoliagePlacerType<?>, SELF> {
    private final Function<Codec<P>, T> type;
    private final Supplier<Codec<P>> codec;

    @SuppressWarnings("unchecked")
    public FoliagePlacerTypeBuilder(Supplier<Codec<P>> codec) {
        this(c -> (T) new FoliagePlacerType<>(c), codec);
    }

    public FoliagePlacerTypeBuilder(Function<Codec<P>, T> type, Supplier<Codec<P>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<FoliagePlacerType<?>> getRegistry() {
        return RegistryDirectory.FOLIAGE_PLACER_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
