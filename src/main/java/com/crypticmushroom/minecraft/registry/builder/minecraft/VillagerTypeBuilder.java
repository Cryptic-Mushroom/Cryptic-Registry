package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.entity.npc.VillagerType;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.registries.RegistryObject;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

public class VillagerTypeBuilder<SELF extends VillagerTypeBuilder<SELF>> extends RegistryObjectBuilder<VillagerType, VillagerType, SELF> {
    private final List<Supplier<? extends ResourceKey<Biome>>> biomes = new LinkedList<>();

    @Override
    protected RegistryInfo.Static<VillagerType> getRegistry() {
        return RegistryDirectory.VILLAGER_TYPE;
    }

    @Override
    protected VillagerType buildType() {
        var result = new VillagerType(this.makeResLocString());
        this.biomes.forEach(b -> VillagerType.BY_BIOME.put(b.get(), result));
        return result;
    }

    @SuppressWarnings("unchecked")
    public SELF biome(Supplier<? extends ResourceKey<Biome>> biome) {
        this.biomes.add(biome);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF biome(Supplier<? extends ResourceKey<Biome>>... biome) {
        for (Supplier<? extends ResourceKey<Biome>> b : biome) {
            this.biome(b);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF biome(Collection<Supplier<? extends ResourceKey<Biome>>> biomes) {
        biomes.forEach(this::biome);
        return (SELF) this;
    }

    public SELF biome(RegistryObject<Biome> biome) {
        return this.biome(biome::getKey);
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF biome(RegistryObject<Biome>... biome) {
        for (RegistryObject<Biome> b : biome) {
            this.biome(b);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF biomeRegObjs(Collection<RegistryObject<Biome>> biomes) {
        biomes.forEach(this::biome);
        return (SELF) this;
    }
}
