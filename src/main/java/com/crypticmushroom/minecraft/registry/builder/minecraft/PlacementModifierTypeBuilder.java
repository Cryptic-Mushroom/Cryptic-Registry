package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.PlacementModifierType;

import java.util.function.Function;
import java.util.function.Supplier;

public class PlacementModifierTypeBuilder<M extends PlacementModifier, T extends PlacementModifierType<M>, SELF extends PlacementModifierTypeBuilder<M, T, SELF>> extends RegistryObjectBuilder<T, PlacementModifierType<?>, SELF> {
    private final Function<Codec<M>, T> type;
    private final Supplier<Codec<M>> codec;

    @SuppressWarnings("unchecked")
    public PlacementModifierTypeBuilder(Supplier<Codec<M>> codec) {
        this(c -> (T) (PlacementModifierType<M>) () -> c, codec);
    }

    public PlacementModifierTypeBuilder(Function<Codec<M>, T> type, Supplier<Codec<M>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<PlacementModifierType<?>> getRegistry() {
        return RegistryDirectory.PLACEMENT_MODIFIER_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
