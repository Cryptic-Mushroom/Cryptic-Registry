package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.brigadier.arguments.ArgumentType;
import net.minecraft.commands.synchronization.ArgumentTypeInfo;
import net.minecraft.commands.synchronization.ArgumentTypeInfos;

import java.util.function.Supplier;

public class ArgumentTypeInfoBuilder<A extends ArgumentType<?>, T extends ArgumentTypeInfo.Template<A>, I extends ArgumentTypeInfo<A, T>, SELF extends ArgumentTypeInfoBuilder<A, T, I, SELF>> extends RegistryObjectBuilder<I, ArgumentTypeInfo<?, ?>, SELF> {
    private final Supplier<I> type;
    private final Class<A> argumentType;

    public ArgumentTypeInfoBuilder(Supplier<I> type, Class<A> argumentType, Class<T> templateType) {
        this(type, argumentType);
    }

    public ArgumentTypeInfoBuilder(Supplier<I> type, Class<A> argumentType) {
        this.type = type;
        this.argumentType = argumentType;
    }

    @Override
    protected RegistryInfo.Static<ArgumentTypeInfo<?, ?>> getRegistry() {
        return RegistryDirectory.COMMAND_ARGUMENT_TYPE;
    }

    @Override
    protected I buildType() {
        return ArgumentTypeInfos.registerByClass(this.argumentType, this.type.get());
    }
}
