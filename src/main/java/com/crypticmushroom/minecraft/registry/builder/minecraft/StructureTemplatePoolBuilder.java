package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceKey;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.datafixers.util.Pair;
import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.data.worldgen.Pools;
import net.minecraft.world.level.levelgen.structure.pools.StructurePoolElement;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool.Projection;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class StructureTemplatePoolBuilder<P extends StructureTemplatePool, SELF extends StructureTemplatePoolBuilder<P, SELF>> extends DataResourceBuilder<P, StructureTemplatePool, SELF> {
    private final StructureTemplatePoolFunction<P> type;

    private Function<BootstapContext<StructureTemplatePool>, Holder<StructureTemplatePool>> fallback = bootstrap -> bootstrap.lookup(Registries.TEMPLATE_POOL).getOrThrow(Pools.EMPTY);
    private final List<TemplateElement> elements = new ArrayList<>();
    private @Nullable StructureTemplatePool.Projection projection = null;

    public StructureTemplatePoolBuilder() {
        this(StructureTemplatePoolFunction.defaultType());
    }

    public StructureTemplatePoolBuilder(StructureTemplatePoolFunction<P> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Dynamic<StructureTemplatePool> getRegistry() {
        return RegistryDirectory.STRUCTURE_TEMPLATE_POOL;
    }

    @Override
    public DataResourceKey<P, StructureTemplatePool> build() {
        //@formatter:off
        if (this.elements.isEmpty())
            throw new IncompleteBuilderException("Cannot create a structure template pool without any template elements");
        //@formatter:on

        return super.build();
    }

    @Override
    protected P buildType(BootstapContext<StructureTemplatePool> bootstrap) {
        return this.type.apply(
            this.fallback.apply(bootstrap),
            this.elements.stream().map(e -> e.apply(bootstrap, this.projection)).toList()
        );
    }

    @FunctionalInterface
    public interface StructureTemplatePoolFunction<P extends StructureTemplatePool> extends BiFunction<Holder<StructureTemplatePool>, List<Pair<StructurePoolElement, Integer>>, P> {
        @Override
        P apply(Holder<StructureTemplatePool> fallback, List<Pair<StructurePoolElement, Integer>> rawTemplates);

        @SuppressWarnings("unchecked")
        static <P extends StructureTemplatePool> StructureTemplatePoolFunction<P> defaultType() {
            return (fallback, rawTemplates) -> (P) new StructureTemplatePool(fallback, rawTemplates);
        }
    }


    /* STRUCTURE TEMPLATE POOL */

    @SuppressWarnings("unchecked")
    public SELF fallback(RegistryObject<? extends StructureTemplatePool> fallback) {
        return this.fallback(() -> (Holder<StructureTemplatePool>) fallback.getHolder().orElseThrow());
    }

    public SELF fallback(Supplier<Holder<StructureTemplatePool>> fallback) {
        return this.fallback(bootstrap -> fallback.get());
    }

    @SuppressWarnings("unchecked")
    public SELF fallback(Function<BootstapContext<StructureTemplatePool>, Holder<StructureTemplatePool>> fallback) {
        this.fallback = fallback;
        return (SELF) this;
    }

    public SELF element(Supplier<? extends StructurePoolElement> element) {
        return this.element(1, element);
    }

    public SELF element(int weight, Supplier<? extends StructurePoolElement> element) {
        return this.element(weight, (b, p) -> element.get());
    }

    public SELF element(Function<StructureTemplatePool.Projection, ? extends StructurePoolElement> element) {
        return this.element(1, element);
    }

    public SELF element(int weight, Function<StructureTemplatePool.Projection, ? extends StructurePoolElement> element) {
        return this.element(weight, (b, p) -> element.apply(p));
    }

    public SELF element(BiFunction<BootstapContext<StructureTemplatePool>, StructureTemplatePool.Projection, ? extends StructurePoolElement> element) {
        return this.element(1, element);
    }

    @SuppressWarnings("unchecked")
    public SELF element(int weight, BiFunction<BootstapContext<StructureTemplatePool>, StructureTemplatePool.Projection, ? extends StructurePoolElement> element) {
        this.elements.add(new TemplateElement(element, weight));
        return (SELF) this;
    }

    @SafeVarargs
    public final SELF element(Supplier<? extends StructurePoolElement>... element) {
        return this.element(1, element);
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF element(int weight, Supplier<? extends StructurePoolElement>... element) {
        for (Supplier<? extends StructurePoolElement> e : element) {
            this.element(weight, e);
        }

        return (SELF) this;
    }

    @SafeVarargs
    public final SELF element(Function<StructureTemplatePool.Projection, ? extends StructurePoolElement>... element) {
        return this.element(1, element);
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF element(int weight, Function<StructureTemplatePool.Projection, ? extends StructurePoolElement>... element) {
        for (Function<StructureTemplatePool.Projection, ? extends StructurePoolElement> e : element) {
            this.element(weight, e);
        }

        return (SELF) this;
    }

    public SELF elementRaw(Collection<Supplier<? extends StructurePoolElement>> elements) {
        return this.elementRaw(1, elements);
    }

    @SuppressWarnings("unchecked")
    public SELF elementRaw(int weight, Collection<Supplier<? extends StructurePoolElement>> elements) {
        elements.forEach(e -> this.element(weight, e));
        return (SELF) this;
    }

    public SELF element(Collection<Function<StructureTemplatePool.Projection, ? extends StructurePoolElement>> elements) {
        return this.element(1, elements);
    }

    @SuppressWarnings("unchecked")
    public SELF element(int weight, Collection<Function<StructureTemplatePool.Projection, ? extends StructurePoolElement>> elements) {
        elements.forEach(e -> this.element(weight, e));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF projection(StructureTemplatePool.Projection projection) {
        this.projection = projection;
        return (SELF) this;
    }

    private static class TemplateElement implements BiFunction<BootstapContext<StructureTemplatePool>, StructureTemplatePool.Projection, Pair<StructurePoolElement, Integer>> {
        private final BiFunction<BootstapContext<StructureTemplatePool>, StructureTemplatePool.Projection, ? extends StructurePoolElement> template;
        private final int weight;
        private final boolean raw;

        private TemplateElement(Function<BootstapContext<StructureTemplatePool>, ? extends StructurePoolElement> template, int weight) {
            this((b, p) -> template.apply(b), weight, true);
        }

        private TemplateElement(BiFunction<BootstapContext<StructureTemplatePool>, StructureTemplatePool.Projection, ? extends StructurePoolElement> template, int weight) {
            this(template, weight, false);
        }

        private TemplateElement(BiFunction<BootstapContext<StructureTemplatePool>, StructureTemplatePool.Projection, ? extends StructurePoolElement> template, int weight, boolean raw) {
            this.template = template;
            this.weight = weight;
            this.raw = raw;
        }

        @Override
        public Pair<StructurePoolElement, Integer> apply(BootstapContext<StructureTemplatePool> bootstrap, @Nullable Projection projection) {
            if (projection == null && !this.raw) {
                throw new IllegalStateException("Attempted to create a structure template that requires a projection without one");
            }

            return Pair.of(this.template.apply(bootstrap, projection), this.weight);
        }
    }
}
