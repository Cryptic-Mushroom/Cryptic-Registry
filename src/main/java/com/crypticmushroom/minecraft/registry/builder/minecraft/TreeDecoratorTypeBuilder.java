package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.feature.treedecorators.TreeDecorator;
import net.minecraft.world.level.levelgen.feature.treedecorators.TreeDecoratorType;

import java.util.function.Function;
import java.util.function.Supplier;

public class TreeDecoratorTypeBuilder<D extends TreeDecorator, T extends TreeDecoratorType<D>, SELF extends TreeDecoratorTypeBuilder<D, T, SELF>> extends RegistryObjectBuilder<T, TreeDecoratorType<?>, SELF> {
    private final Function<Codec<D>, T> type;
    private final Supplier<Codec<D>> codec;

    @SuppressWarnings("unchecked")
    public TreeDecoratorTypeBuilder(Supplier<Codec<D>> codec) {
        this(c -> (T) new TreeDecoratorType<>(c), codec);
    }

    public TreeDecoratorTypeBuilder(Function<Codec<D>, T> type, Supplier<Codec<D>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<TreeDecoratorType<?>> getRegistry() {
        return RegistryDirectory.TREE_DECORATOR_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
