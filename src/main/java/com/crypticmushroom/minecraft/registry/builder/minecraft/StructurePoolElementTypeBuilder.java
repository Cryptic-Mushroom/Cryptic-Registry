package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.structure.pools.StructurePoolElement;
import net.minecraft.world.level.levelgen.structure.pools.StructurePoolElementType;

import java.util.function.Function;
import java.util.function.Supplier;

public class StructurePoolElementTypeBuilder<E extends StructurePoolElement, T extends StructurePoolElementType<E>, SELF extends StructurePoolElementTypeBuilder<E, T, SELF>> extends RegistryObjectBuilder<T, StructurePoolElementType<?>, SELF> {
    private final Function<Codec<E>, T> type;
    private final Supplier<Codec<E>> codec;

    @SuppressWarnings("unchecked")
    public StructurePoolElementTypeBuilder(Supplier<Codec<E>> codec) {
        this(c -> (T) (StructurePoolElementType<E>) () -> c, codec);
    }

    public StructurePoolElementTypeBuilder(Function<Codec<E>, T> type, Supplier<Codec<E>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<StructurePoolElementType<?>> getRegistry() {
        return RegistryDirectory.STRUCTURE_POOL_ELEMENT_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
