package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceKey;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleList;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.level.levelgen.synth.NormalNoise;

import java.util.Collection;
import java.util.OptionalInt;

/**
 * The noise builder contains various methods that allow for building
 * {@link NormalNoise.NoiseParameters noise parameters} with ease and allows for automatic data generation features.
 *
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @see RegistryObjectBuilder
 * @since 0.1.0
 */
public class NoiseParametersBuilder<SELF extends NoiseParametersBuilder<SELF>> extends DataResourceBuilder<NormalNoise.NoiseParameters, NormalNoise.NoiseParameters, SELF> {
    private OptionalInt firstOctave = OptionalInt.empty();
    private final DoubleList amplitudes = new DoubleArrayList();

    @Override
    protected RegistryInfo.Dynamic<NormalNoise.NoiseParameters> getRegistry() {
        return RegistryDirectory.NOISE;
    }

    @Override
    public DataResourceKey<NormalNoise.NoiseParameters, NormalNoise.NoiseParameters> build() {
        //@formatter:off
        this.firstOctave.orElseThrow(() -> new IncompleteBuilderException("Cannot create noise parameters without the first octave"));
        if (this.amplitudes.isEmpty())
            throw new IncompleteBuilderException("Cannot create noise parameters without any amplitudes");
        //@formatter:on

        return super.build();
    }

    @Override
    @SuppressWarnings("OptionalGetWithoutIsPresent") // checked by build()
    protected NormalNoise.NoiseParameters buildType(BootstapContext<NormalNoise.NoiseParameters> bootstrap) {
        return new NormalNoise.NoiseParameters(
            this.firstOctave.getAsInt(),
            this.amplitudes
        );
    }


    /* NOISE PARAMETERS */

    @SuppressWarnings("unchecked")
    public SELF firstOctave(int firstOctave) {
        this.firstOctave = OptionalInt.of(firstOctave);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF amplitude(double amplitude) {
        this.amplitudes.add(amplitude);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public final SELF amplitude(double... amplitude) {
        for (double a : amplitude) {
            this.amplitude(a);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public final SELF amplitude(Collection<Double> amplitudes) {
        amplitudes.forEach(this::amplitude);
        return (SELF) this;
    }
}
