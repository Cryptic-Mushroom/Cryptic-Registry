package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentCategory;
import org.apache.commons.lang3.function.TriFunction;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

public class EnchantmentBuilder<E extends Enchantment, SELF extends EnchantmentBuilder<E, SELF>> extends RegistryObjectBuilder.Named<E, Enchantment, SELF> {
    private final TriFunction<Enchantment.Rarity, EnchantmentCategory, EquipmentSlot[], E> type;

    private final List<EquipmentSlot> slots = new LinkedList<>();
    private @Nullable Enchantment.Rarity rarity = null;
    private @Nullable EnchantmentCategory category = null;

    public EnchantmentBuilder(Supplier<E> type) {
        this.type = (rarity, category, slots) -> type.get();
    }

    public EnchantmentBuilder(TriFunction<Enchantment.Rarity, EnchantmentCategory, EquipmentSlot[], E> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<Enchantment> getRegistry() {
        return RegistryDirectory.ENCHANTMENT;
    }

    @Override
    public LocalizedNameRegistry.Normal<Enchantment> getLocalizedNameRegistry() {
        return LocalizedNameRegistry.ENCHANTMENT;
    }

    @Override
    protected E buildType() {
        return this.type.apply(this.rarity, this.category, this.slots.toArray(EquipmentSlot[]::new));
    }


    /* ENCHANTMENT */

    @SuppressWarnings("unchecked")
    public SELF slot(EquipmentSlot slot) {
        this.slots.add(slot);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF slot(EquipmentSlot... slot) {
        for (EquipmentSlot s : slot) {
            this.slot(s);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF slot(Iterable<EquipmentSlot> effect) {
        effect.forEach(this::slot);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF rarity(Enchantment.Rarity rarity) {
        this.rarity = rarity;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF category(EnchantmentCategory category) {
        this.category = category;
        return (SELF) this;
    }
}
