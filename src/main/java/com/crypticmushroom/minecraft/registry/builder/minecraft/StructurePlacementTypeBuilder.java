package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.structure.placement.StructurePlacement;
import net.minecraft.world.level.levelgen.structure.placement.StructurePlacementType;

import java.util.function.Function;
import java.util.function.Supplier;

public class StructurePlacementTypeBuilder<P extends StructurePlacement, T extends StructurePlacementType<P>, SELF extends StructurePlacementTypeBuilder<P, T, SELF>> extends RegistryObjectBuilder<T, StructurePlacementType<?>, SELF> {
    private final Function<Codec<P>, T> type;
    private final Supplier<Codec<P>> codec;

    @SuppressWarnings("unchecked")
    public StructurePlacementTypeBuilder(Supplier<Codec<P>> codec) {
        this(c -> (T) (StructurePlacementType<P>) () -> c, codec);
    }

    public StructurePlacementTypeBuilder(Function<Codec<P>, T> type, Supplier<Codec<P>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<StructurePlacementType<?>> getRegistry() {
        return RegistryDirectory.STRUCTURE_PLACEMENT_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
