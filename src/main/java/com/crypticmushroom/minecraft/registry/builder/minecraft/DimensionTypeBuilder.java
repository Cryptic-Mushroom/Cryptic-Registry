package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceKey;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.util.valueproviders.IntProvider;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.dimension.DimensionType.MonsterSettings;

import java.util.Objects;
import java.util.OptionalLong;

/**
 * The dimension type builder contains various methods that allow for building a dimension type with ease andallows for
 * several automatic data generation features.
 *
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @see RegistryObjectBuilder
 * @since 0.1.0
 */
public class DimensionTypeBuilder<SELF extends DimensionTypeBuilder<SELF>> extends DataResourceBuilder<DimensionType, DimensionType, SELF> {
    @SuppressWarnings("OptionalAssignedToNull") private OptionalLong fixedTime = null;
    private Boolean hasSkyLight = null;
    private Boolean hasCeiling = null;
    private Boolean ultraWarm = null;
    private Boolean natural = null;
    private Double coordinateScale = null;
    private Boolean bedWorks = null;
    private Boolean respawnAnchorWorks = null;
    private Integer minY = null;
    private Integer height = null;
    private Integer logicalHeight = null;
    private TagKey<Block> infiniburn = null;
    private ResourceLocation effectsLocation = null;
    private Float ambientLight = null;

    private Boolean piglinSafe = null;
    private Boolean hasRaids = null;
    private IntProvider monsterSpawnLightTest = null;
    private Integer monsterSpawnBlockLightLimit = null;

    @Override
    protected RegistryInfo.Dynamic<DimensionType> getRegistry() {
        return RegistryDirectory.DIMENSION_TYPE;
    }

    @Override
    public DataResourceKey<DimensionType, DimensionType> build() {
        this.checkField(this.fixedTime, "fixedTime");
        this.checkField(this.hasSkyLight, "hasSkyLight");
        this.checkField(this.hasCeiling, "hasCeiling");
        this.checkField(this.ultraWarm, "ultraWarm");
        this.checkField(this.natural, "natural");
        this.checkField(this.coordinateScale, "coordinateScale");
        this.checkField(this.bedWorks, "bedWorks");
        this.checkField(this.respawnAnchorWorks, "respawnAnchorWorks");
        this.checkField(this.minY, "minY");
        this.checkField(this.height, "height");
        this.checkField(this.logicalHeight, "logicalHeight");
        this.checkField(this.infiniburn, "infiniburn");
        this.checkField(this.effectsLocation, "effectsLocation");
        this.checkField(this.ambientLight, "ambientLight");

        this.checkField(this.piglinSafe, "piglinSafe");
        this.checkField(this.hasRaids, "hasRaids");
        this.checkField(this.monsterSpawnLightTest, "monsterSpawnLightTest");
        this.checkField(this.monsterSpawnBlockLightLimit, "monsterSpawnBlockLightLimit");

        return super.build();
    }

    @Override
    protected DimensionType buildType(BootstapContext<DimensionType> bootstrap) {
        return new DimensionType(
            this.fixedTime,
            this.hasSkyLight,
            this.hasCeiling,
            this.ultraWarm,
            this.natural,
            this.coordinateScale,
            this.bedWorks,
            this.respawnAnchorWorks,
            this.minY,
            this.height,
            this.logicalHeight,
            this.infiniburn,
            this.effectsLocation,
            this.ambientLight,
            new MonsterSettings(
                this.piglinSafe,
                this.hasRaids,
                this.monsterSpawnLightTest,
                this.monsterSpawnBlockLightLimit
            )
        );
    }

    private void checkField(Object object, String name) {
        try {
            Objects.requireNonNull(object, () -> "Field is null: %s".formatted(name));
        } catch (NullPointerException e) {
            throw new IncompleteBuilderException("Dimension type builder incomplete! See cause for more info. All attributes must be given a value.", e);
        }
    }


    /* DIMENSION TYPE */

    @SuppressWarnings("unchecked")
    public SELF fixedTime(OptionalLong fixedTime) {
        this.fixedTime = fixedTime;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF fixedTime(long fixedTime) {
        this.fixedTime = OptionalLong.of(fixedTime);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF fixedTime() {
        this.fixedTime = OptionalLong.empty();
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF hasSkyLight(boolean hasSkyLight) {
        this.hasSkyLight = hasSkyLight;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF hasCeiling(boolean hasCeiling) {
        this.hasCeiling = hasCeiling;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF ultraWarm(boolean ultraWarm) {
        this.ultraWarm = ultraWarm;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF natural(boolean natural) {
        this.natural = natural;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF coordinateScale(double coordinateScale) {
        this.coordinateScale = coordinateScale;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF bedWorks(boolean bedWorks) {
        this.bedWorks = bedWorks;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF respawnAnchorWorks(boolean respawnAnchorWorks) {
        this.respawnAnchorWorks = respawnAnchorWorks;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF minY(int minY) {
        this.minY = minY;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF height(int height) {
        this.height = height;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF logicalHeight(int logicalHeight) {
        this.logicalHeight = logicalHeight;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF infiniburn(TagKey<Block> infiniburn) {
        this.infiniburn = infiniburn;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF effectsLocation(ResourceLocation effectsLocation) {
        this.effectsLocation = effectsLocation;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF effectsLocation(String effectsLocation) {
        this.effectsLocation = this.makeResLoc(effectsLocation);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF ambientLight(float ambientLight) {
        this.ambientLight = ambientLight;
        return (SELF) this;
    }


    /* MONSTER SETTINGS */

    @SuppressWarnings("unchecked")
    public SELF piglinSafe(boolean piglinSafe) {
        this.piglinSafe = piglinSafe;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF hasRaids(boolean hasRaids) {
        this.hasRaids = hasRaids;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF monsterSpawnLightTest(IntProvider monsterSpawnLightTest) {
        this.monsterSpawnLightTest = monsterSpawnLightTest;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF monsterSpawnLightTest(int lowLightLevel, int highLightLevel) {
        this.monsterSpawnLightTest = UniformInt.of(lowLightLevel, highLightLevel);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF monsterSpawnBlockLightLimit(int monsterSpawnBlockLightLimit) {
        this.monsterSpawnBlockLightLimit = monsterSpawnBlockLightLimit;
        return (SELF) this;
    }
}
