package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.animal.CatVariant;
import net.minecraftforge.registries.RegistryObject;

public class CatVariantBuilder<SELF extends CatVariantBuilder<SELF>> extends RegistryObjectBuilder<CatVariant, CatVariant, SELF> {
    private ResourceLocation texture;

    @Override
    protected RegistryInfo.Static<CatVariant> getRegistry() {
        return RegistryDirectory.CAT_VARIANT;
    }

    @Override
    public RegistryObject<CatVariant> build() {
        if (this.texture == null)
            throw new IncompleteBuilderException("Cannot create a cat variant without a texture");

        return super.build();
    }

    @Override
    protected CatVariant buildType() {
        return new CatVariant(this.texture);
    }


    /* CAT VARIANT */

    public SELF texture(String texture) {
        return this.texture(this.makeResLoc(texture));
    }

    @SuppressWarnings("unchecked")
    public SELF texture(ResourceLocation texture) {
        this.texture = texture;
        return (SELF) this;
    }
}
