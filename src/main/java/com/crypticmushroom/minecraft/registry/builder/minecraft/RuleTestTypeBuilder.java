package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleTestType;

import java.util.function.Function;
import java.util.function.Supplier;

public class RuleTestTypeBuilder<R extends RuleTest, T extends RuleTestType<R>, SELF extends RuleTestTypeBuilder<R, T, SELF>> extends RegistryObjectBuilder<T, RuleTestType<?>, SELF> {
    private final Function<Codec<R>, T> type;
    private final Supplier<Codec<R>> codec;

    @SuppressWarnings("unchecked")
    public RuleTestTypeBuilder(Supplier<Codec<R>> codec) {
        this(c -> (T) (RuleTestType<R>) () -> c, codec);
    }

    public RuleTestTypeBuilder(Function<Codec<R>, T> type, Supplier<Codec<R>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<RuleTestType<?>> getRegistry() {
        return RegistryDirectory.RULE_TEST_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
