package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import it.unimi.dsi.fastutil.objects.Object2BooleanFunction;
import it.unimi.dsi.fastutil.objects.Object2FloatFunction;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biome.TemperatureModifier;
import net.minecraft.world.level.biome.BiomeGenerationSettings;
import net.minecraft.world.level.biome.BiomeSpecialEffects;
import net.minecraft.world.level.biome.MobSpawnSettings;
import org.jetbrains.annotations.Nullable;

import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * The biome builder acts idendical to a {@link Biome.BiomeBuilder}, but has the additional feature of adding tags to a
 * biome.
 *
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @apiNote This builder is marked as "tagged". You may override this default behavior in a subclass.
 * @see RegistryObjectBuilder
 * @see Biome.BiomeBuilder
 * @since 0.1.0
 */
@SuppressWarnings("unchecked")
public class BiomeBuilder<SELF extends BiomeBuilder<SELF>> extends DataResourceBuilder<Biome, Biome, SELF> {
    private Object2BooleanFunction<BootstapContext<Biome>> hasPrecipitation = bootstrap -> true;
    private Function<BootstapContext<Biome>, @Nullable Float> temperature = bootstrap -> null;
    private Function<BootstapContext<Biome>, @Nullable Float> downfall = bootstrap -> null;
    private Function<BootstapContext<Biome>, @Nullable BiomeSpecialEffects> specialEffects = bootstrap -> null;
    private Function<BootstapContext<Biome>, @Nullable MobSpawnSettings> mobSpawnSettings = bootstrap -> null;
    private Function<BootstapContext<Biome>, @Nullable BiomeGenerationSettings> generationSettings = bootstrap -> null;
    private Function<BootstapContext<Biome>, TemperatureModifier> temperatureSettings = bootstrap -> Biome.TemperatureModifier.NONE;

    @Override
    protected RegistryInfo.Dynamic<Biome> getRegistry() {
        return RegistryDirectory.BIOME;
    }

    @SuppressWarnings("DataFlowIssue")
    @Override
    protected Biome buildType(BootstapContext<Biome> bootstrap) {
        try {
            return new Biome.BiomeBuilder()
                       .hasPrecipitation(this.hasPrecipitation.apply(bootstrap))
                       .temperature(this.temperature.apply(bootstrap))
                       .downfall(this.downfall.apply(bootstrap))
                       .specialEffects(this.specialEffects.apply(bootstrap))
                       .mobSpawnSettings(this.mobSpawnSettings.apply(bootstrap))
                       .generationSettings(this.generationSettings.apply(bootstrap))
                       .temperatureAdjustment(this.temperatureSettings.apply(bootstrap))
                       .build();
        } catch (IllegalStateException | NullPointerException e) {
            throw new IncompleteBuilderException("Failed to build biome!", e);
        }
    }


    /* BIOME */

    /**
     * @return This builder.
     *
     * @see #hasPrecipitation(boolean)
     */
    @Deprecated(forRemoval = true, since = "1.19.4-*")
    public SELF precipitation(Biome.Precipitation precipitation) {
        return this.hasPrecipitation(precipitation != Biome.Precipitation.NONE);
    }

    public SELF hasPrecipitation(boolean hasPercipitation) {
        return this.hasPrecipitation(bootstrap -> hasPercipitation);
    }

    public SELF hasPrecipitation(BooleanSupplier hasPrecipitation) {
        return this.hasPrecipitation(boostrap -> hasPrecipitation.getAsBoolean());
    }

    public SELF hasPrecipitation(Object2BooleanFunction<BootstapContext<Biome>> hasPrecipitation) {
        this.hasPrecipitation = hasPrecipitation;
        return (SELF) this;
    }

    public SELF temperature(float temperature) {
        return this.temperature(bootstrap -> temperature);
    }

    public SELF temperature(Supplier<Float> temperature) {
        return this.temperature(bootstrap -> temperature.get());
    }

    public SELF temperature(Object2FloatFunction<BootstapContext<Biome>> temperature) {
        this.temperature = temperature;
        return (SELF) this;
    }

    public SELF downfall(float downfall) {
        return this.downfall(bootstrap -> downfall);
    }

    public SELF downfall(Supplier<Float> downfall) {
        return this.downfall(bootstrap -> downfall.get());
    }

    public SELF downfall(Object2FloatFunction<BootstapContext<Biome>> downfall) {
        this.downfall = downfall;
        return (SELF) this;
    }

    public SELF specialEffects(BiomeSpecialEffects effects) {
        return this.specialEffects(bootstrap -> effects);
    }

    public SELF specialEffects(Supplier<BiomeSpecialEffects> effects) {
        return this.specialEffects(bootstrap -> effects.get());
    }

    public SELF specialEffects(Function<BootstapContext<Biome>, BiomeSpecialEffects> effects) {
        this.specialEffects = effects;
        return (SELF) this;
    }

    public SELF mobSpawnSettings(MobSpawnSettings mobSpawnSettings) {
        return this.mobSpawnSettings(bootstrap -> mobSpawnSettings);
    }

    public SELF mobSpawnSettings(Supplier<MobSpawnSettings> mobSpawnSettings) {
        return this.mobSpawnSettings(bootstrap -> mobSpawnSettings.get());
    }

    public SELF mobSpawnSettings(Function<BootstapContext<Biome>, MobSpawnSettings> mobSpawnSettings) {
        this.mobSpawnSettings = mobSpawnSettings;
        return (SELF) this;
    }

    public SELF generationSettings(BiomeGenerationSettings generationSettings) {
        return this.generationSettings(bootstrap -> generationSettings);
    }

    public SELF generationSettings(Supplier<BiomeGenerationSettings> generationSettings) {
        return this.generationSettings(bootstrap -> generationSettings.get());
    }

    public SELF generationSettings(Function<BootstapContext<Biome>, BiomeGenerationSettings> generationSettings) {
        this.generationSettings = generationSettings;
        return (SELF) this;
    }

    public SELF temperatureAdjustment(TemperatureModifier temperatureSettings) {
        return this.temperatureAdjustment(bootstrap -> temperatureSettings);
    }

    public SELF temperatureAdjustment(Supplier<TemperatureModifier> temperatureSettings) {
        return this.temperatureAdjustment(bootstrap -> temperatureSettings.get());
    }

    public SELF temperatureAdjustment(Function<BootstapContext<Biome>, TemperatureModifier> temperatureSettings) {
        this.temperatureSettings = temperatureSettings;
        return (SELF) this;
    }
}
