package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceType;
import net.minecraftforge.registries.RegistryObject;

import java.util.Locale;
import java.util.function.Supplier;

public class StructurePieceTypeBuilder<T extends StructurePieceType, SELF extends StructurePieceTypeBuilder<T, SELF>> extends RegistryObjectBuilder<T, StructurePieceType, SELF> {
    private final Supplier<T> type;

    public StructurePieceTypeBuilder(Supplier<T> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<StructurePieceType> getRegistry() {
        return RegistryDirectory.STRUCTURE_PIECE_TYPE;
    }

    @Override
    public RegistryObject<T> build() {
        this.id(this.getId().toLowerCase(Locale.ROOT));
        return super.build();
    }

    @Override
    protected T buildType() {
        return this.type.get();
    }
}
