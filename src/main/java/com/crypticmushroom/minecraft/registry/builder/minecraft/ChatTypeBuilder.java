package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.data.resource.DataResourceKey;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.network.chat.ChatType;
import net.minecraft.network.chat.ChatTypeDecoration;
import net.minecraftforge.common.util.NonNullSupplier;

public class ChatTypeBuilder<SELF extends ChatTypeBuilder<SELF>> extends DataResourceBuilder<ChatType, ChatType, SELF> {
    private NonNullSupplier<ChatTypeDecoration> chat;
    private NonNullSupplier<ChatTypeDecoration> narration;

    @Override
    protected RegistryInfo.Dynamic<ChatType> getRegistry() {
        return RegistryDirectory.CHAT_TYPE;
    }

    @Override
    public DataResourceKey<ChatType, ChatType> build() {
        if (this.chat == null) {
            CrypticRegistry.LOGGER.warn("Chat decoration not specified for chat type builder {}! Using the game's default. Please specify the chat decoration in the builder even if it is the default.", this.getId());
            this.chat = () -> ChatType.DEFAULT_CHAT_DECORATION;
        }

        if (this.narration == null) {
            CrypticRegistry.LOGGER.warn("Chat decoration for narration not specified for chat type builder {}! Using the game's default. Please specify the chat decoration in the builder even if it is the default.", this.getId());
            this.narration = () -> ChatType.DEFAULT_CHAT_DECORATION;
        }

        return super.build();
    }

    @Override
    protected ChatType buildType(BootstapContext<ChatType> bootstrap) {
        return new ChatType(this.chat.get(), this.narration.get());
    }


    /* CHAT TYPE */

    @SuppressWarnings("unchecked")
    public SELF chat(NonNullSupplier<ChatTypeDecoration> chat) {
        this.chat = chat;
        return (SELF) this;
    }
}
