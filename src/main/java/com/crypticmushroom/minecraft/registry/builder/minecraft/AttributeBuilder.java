package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.datafixers.util.Function4;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.RangedAttribute;
import net.minecraftforge.registries.RegistryObject;

import java.util.OptionalDouble;

public class AttributeBuilder<A extends RangedAttribute, SELF extends AttributeBuilder<A, SELF>> extends RegistryObjectBuilder.Named<A, Attribute, SELF> {
    private final AttributeFunction<A> type;

    private OptionalDouble defaultValue = OptionalDouble.empty();
    private OptionalDouble min = OptionalDouble.empty();
    private OptionalDouble max = OptionalDouble.empty();

    public AttributeBuilder() {
        this(AttributeFunction.defaultType());
    }

    public AttributeBuilder(AttributeFunction<A> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<Attribute> getRegistry() {
        return RegistryDirectory.ATTRIBUTE;
    }

    @Override
    public LocalizedNameRegistry.Normal<Attribute> getLocalizedNameRegistry() {
        return LocalizedNameRegistry.ATTRIBUTE;
    }

    @Override
    public String getTranslationKey() {
        return String.format("attribute.%s.%s", this.getModId(), this.getId());
    }

    @Override
    public RegistryObject<A> build() {
        this.defaultValue.orElseThrow(() -> new IncompleteBuilderException("Cannot create an attribute without a default value"));
        this.min.orElseThrow(() -> new IncompleteBuilderException("Cannot create an attribute without a minimum value"));
        this.max.orElseThrow(() -> new IncompleteBuilderException("Cannot create an attribute without a maximum value"));

        return super.build();
    }

    @Override
    @SuppressWarnings("OptionalGetWithoutIsPresent") // checked in build()
    protected A buildType() {
        return this.type.apply(this.getTranslationKey(), this.defaultValue.getAsDouble(), this.min.getAsDouble(), this.max.getAsDouble());
    }

    @FunctionalInterface
    public interface AttributeFunction<A extends RangedAttribute> extends Function4<String, Double, Double, Double, A> {
        @Override
        A apply(String descriptionId, Double defaultValue, Double min, Double max);

        default A apply(String descriptionId, double defaultValue, double min, double max) {
            return this.apply(descriptionId, (Double) defaultValue, (Double) min, (Double) max);
        }

        @SuppressWarnings("unchecked")
        static <A extends RangedAttribute> AttributeFunction<A> defaultType() {
            return (s, d, a, b) -> (A) new RangedAttribute(s, d, a, b);
        }
    }


    /* ATTRIBUTE */

    @SuppressWarnings("unchecked")
    public SELF defaultValue(double defaultValue) {
        this.defaultValue = OptionalDouble.of(defaultValue);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF min(double min) {
        this.min = OptionalDouble.of(min);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF max(double max) {
        this.max = OptionalDouble.of(max);
        return (SELF) this;
    }
}
