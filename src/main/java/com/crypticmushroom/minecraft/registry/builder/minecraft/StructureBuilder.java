package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.core.HolderSet;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureSpawnOverride;
import net.minecraft.world.level.levelgen.structure.TerrainAdjustment;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class StructureBuilder<S extends Structure, SELF extends StructureBuilder<S, SELF>> extends DataResourceBuilder<S, Structure, SELF> {
    private final Function<Structure.StructureSettings, S> type;

    private static final Function<BootstapContext<Structure>, HolderSet<Biome>> EMPTY_BIOME_HOLDER_SET = bootstrap -> HolderSet.direct(List.of());
    private Function<BootstapContext<Structure>, HolderSet<Biome>> biomes = EMPTY_BIOME_HOLDER_SET;
    private final Map<MobCategory, StructureSpawnOverride> spawnOverrides = new HashMap<>();
    private @Nullable GenerationStep.Decoration step = null;
    private @Nullable TerrainAdjustment terrainAdaptation = null;

    public StructureBuilder(Function<Structure.StructureSettings, S> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Dynamic<Structure> getRegistry() {
        return RegistryDirectory.STRUCTURE;
    }

    @Override
    protected S buildType(BootstapContext<Structure> bootstrap) {
        return this.type.apply(new Structure.StructureSettings(
            this.biomes.apply(bootstrap),
            this.spawnOverrides,
            this.step != null ? this.step : GenerationStep.Decoration.RAW_GENERATION,
            this.terrainAdaptation != null ? this.terrainAdaptation : TerrainAdjustment.NONE
        ));
    }


    /* STRUCTURE SETTINGS */

    public SELF biome(@Nullable Supplier<HolderSet<Biome>> biome) {
        return this.biome(biome != null ? bootstrap -> biome.get() : EMPTY_BIOME_HOLDER_SET);
    }

    @SuppressWarnings("unchecked")
    public SELF biome(@Nullable Function<BootstapContext<Structure>, HolderSet<Biome>> biome) {
        this.biomes = biome != null ? biome : EMPTY_BIOME_HOLDER_SET;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF biome(MobCategory mobCategory, StructureSpawnOverride override) {
        this.spawnOverrides.put(mobCategory, override);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF biome(Map<MobCategory, StructureSpawnOverride> overrides) {
        overrides.forEach(this::biome);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF step(@Nullable GenerationStep.Decoration step) {
        this.step = step;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF terrainAdaptation(@Nullable TerrainAdjustment terrainAdaptation) {
        this.terrainAdaptation = terrainAdaptation;
        return (SELF) this;
    }
}
