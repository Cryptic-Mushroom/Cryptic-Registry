package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.entity.schedule.Activity;
import net.minecraft.world.entity.schedule.Schedule;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class ScheduleBuilder<S extends Schedule, SELF extends ScheduleBuilder<S, SELF>> extends RegistryObjectBuilder<S, Schedule, SELF> {
    private final Supplier<S> type;
    private final List<ActivityTransition> transitions = new ArrayList<>();

    @SuppressWarnings("unchecked")
    public ScheduleBuilder() {
        this(() -> (S) new Schedule());
    }

    public ScheduleBuilder(Supplier<S> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<Schedule> getRegistry() {
        return RegistryDirectory.SCHEDULE;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected S buildType() {
        var builder = new net.minecraft.world.entity.schedule.ScheduleBuilder(this.type.get());
        this.transitions.forEach(t -> builder.changeActivityAt(t.getTime(), t.getActivity()));
        return (S) builder.build();
    }


    /* SCHEDULE */

    @SuppressWarnings("unchecked")
    public SELF changeActivityAt(int duration, Supplier<? extends Activity> activity) {
        this.transitions.add(new ActivityTransition(duration, activity));
        return (SELF) this;
    }

    protected static class ActivityTransition {
        private final int time;
        private final Supplier<? extends Activity> activity;

        public ActivityTransition(int time, Supplier<? extends Activity> activity) {
            this.time = time;
            this.activity = activity;
        }

        public int getTime() {
            return this.time;
        }

        public Activity getActivity() {
            return this.activity.get();
        }
    }
}
