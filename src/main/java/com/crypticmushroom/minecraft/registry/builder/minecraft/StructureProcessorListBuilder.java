package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorList;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public class StructureProcessorListBuilder<L extends StructureProcessorList, SELF extends StructureProcessorListBuilder<L, SELF>> extends DataResourceBuilder<L, StructureProcessorList, SELF> {
    private final Function<List<StructureProcessor>, L> type;

    private final List<Function<BootstapContext<StructureProcessorList>, ? extends StructureProcessor>> processors = new LinkedList<>();

    @SuppressWarnings("unchecked")
    public StructureProcessorListBuilder() {
        this(list -> (L) new StructureProcessorList(list));
    }

    public StructureProcessorListBuilder(Function<List<StructureProcessor>, L> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Dynamic<StructureProcessorList> getRegistry() {
        return RegistryDirectory.STRUCTURE_PROCESSOR_LIST;
    }

    @Override
    protected L buildType(BootstapContext<StructureProcessorList> bootstrap) {
        return this.type.apply(this.processors.stream().map(p -> (StructureProcessor) p.apply(bootstrap)).toList());
    }


    /* STRUCTURE PROCESSOR LIST */

    public SELF processor(Supplier<? extends StructureProcessor> processor) {
        return this.processor(bootstrap -> processor.get());
    }

    @SuppressWarnings("unchecked")
    public SELF processor(Function<BootstapContext<StructureProcessorList>, ? extends StructureProcessor> processor) {
        this.processors.add(processor);
        return (SELF) this;
    }

    @SafeVarargs
    public final SELF processor(Supplier<? extends StructureProcessor>... processor) {
        for (Supplier<? extends StructureProcessor> p : processor) {
            this.processor(p);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    public final SELF processor(Function<BootstapContext<StructureProcessorList>, ? extends StructureProcessor>... processor) {
        for (Function<BootstapContext<StructureProcessorList>, ? extends StructureProcessor> p : processor) {
            this.processor(p);
        }

        return (SELF) this;
    }
}
