package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.client.gui.screens.MenuScreens;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.screens.inventory.MenuAccess;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import org.apache.commons.lang3.function.TriFunction;

import java.util.function.Function;
import java.util.function.Supplier;

public class MenuTypeBuilder<M extends AbstractContainerMenu, S extends Screen & MenuAccess<M>, T extends MenuType<M>, SELF extends MenuTypeBuilder<M, S, T, SELF>> extends RegistryObjectBuilder<T, MenuType<?>, SELF> {
    private final Function<MenuType.MenuSupplier<M>, T> type;
    private final MenuType.MenuSupplier<M> menuType;
    private final Supplier<TriFunction<M, Inventory, Component, S>> screenType;

    @SuppressWarnings("unchecked")
    public MenuTypeBuilder(MenuType.MenuSupplier<M> menuType, Supplier<TriFunction<M, Inventory, Component, S>> screenType) {
        this(menu -> (T) new MenuType<>(menu, FeatureFlags.VANILLA_SET), menuType, screenType);
    }

    public MenuTypeBuilder(Function<MenuType.MenuSupplier<M>, T> type, MenuType.MenuSupplier<M> menuType, Supplier<TriFunction<M, Inventory, Component, S>> screenType) {
        this.type = type;
        this.menuType = menuType;
        this.screenType = screenType;
    }

    @Override
    protected RegistryInfo.Static<MenuType<?>> getRegistry() {
        return RegistryDirectory.MENU_TYPE;
    }

    @Override
    protected T buildType() {
        var result = this.type.apply(this.menuType);
        DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> MenuScreens.register(result, MenuTypeBuilder.this.screenType.get()::apply));
        return result;
    }

}
