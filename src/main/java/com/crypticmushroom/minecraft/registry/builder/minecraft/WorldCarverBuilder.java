package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.carver.CarverConfiguration;
import net.minecraft.world.level.levelgen.carver.WorldCarver;

import java.util.function.Function;
import java.util.function.Supplier;

public class WorldCarverBuilder<WC extends WorldCarver<CC>, CC extends CarverConfiguration, SELF extends WorldCarverBuilder<WC, CC, SELF>> extends RegistryObjectBuilder<WC, WorldCarver<?>, SELF> {
    private final Function<Codec<CC>, WC> type;

    private Supplier<Codec<CC>> codec;

    // codec class type necessary due to issues with nested generic types
    public WorldCarverBuilder(Function<Codec<CC>, WC> type, Class<CC> codecType) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<WorldCarver<?>> getRegistry() {
        return RegistryDirectory.WORLD_CARVER;
    }

    @Override
    protected WC buildType() {
        return this.type.apply(this.codec.get());
    }


    /* CARVER */

    @SuppressWarnings("unchecked")
    public SELF codec(Supplier<Codec<CC>> codec) {
        this.codec = codec;
        return (SELF) this;
    }
}
