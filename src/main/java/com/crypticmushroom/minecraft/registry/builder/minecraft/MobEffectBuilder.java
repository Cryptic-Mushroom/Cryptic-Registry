package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectCategory;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;

import java.util.Objects;
import java.util.OptionalInt;
import java.util.function.BiFunction;
import java.util.function.Supplier;

public class MobEffectBuilder<E extends MobEffect, SELF extends MobEffectBuilder<E, SELF>> extends RegistryObjectBuilder.Named<E, MobEffect, SELF> {
    private final BiFunction<MobEffectCategory, Integer, E> type;
    private boolean needsParameters = true;

    private MobEffectCategory category = null;
    private OptionalInt color = OptionalInt.empty();

    @SuppressWarnings("unchecked")
    public MobEffectBuilder() {
        this((category, color) -> (E) new SimpleMobEffect(category, color));
    }

    public MobEffectBuilder(Supplier<E> type) {
        this.type = (category, color) -> type.get();
        this.needsParameters = false;
    }

    public MobEffectBuilder(BiFunction<MobEffectCategory, Integer, E> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<MobEffect> getRegistry() {
        return RegistryDirectory.MOB_EFFECT;
    }

    @Override
    public LocalizedNameRegistry.Normal<MobEffect> getLocalizedNameRegistry() {
        return LocalizedNameRegistry.MOB_EFFECT;
    }

    @Override
    protected E buildType() {
        if (this.needsParameters) {
            try {
                Objects.requireNonNull(this.category, "Mob effect category cannot be null for an effect without a pre-defined category");
                this.color.orElseThrow(() -> new NullPointerException("Potion color cannot be null for an effect without a pre-defined color"));
            } catch (NullPointerException e) {
                throw new IncompleteBuilderException("Mob effect builder incomplete: " + e.getMessage(), e);
            }
        }

        var built = this.type.apply(this.category, this.color.orElse(0));

        if (!this.needsParameters) {
            boolean affected = false;
            if (this.category != null) {
                CrypticRegistry.LOGGER.warn("Mob effect category is defined for a mob effect type that does not need it!");
                affected = true;
            }
            if (this.color.isPresent()) {
                CrypticRegistry.LOGGER.warn("Potion color is defined for a mob effect type that does not need it!");
                affected = true;
            }

            if (affected) {
                CrypticRegistry.LOGGER.warn("The relevant mob effect type is: {}", built.getClass().getName());
            }
        }

        return built;
    }

    @SuppressWarnings("unchecked")
    public SELF category(MobEffectCategory category) {
        this.category = category;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF color(int color) {
        this.color = OptionalInt.of(color);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF attributeModifier(Supplier<Attribute> attribute, String uuid, double amount, AttributeModifier.Operation operation) {
        this.addBuildOptions(effect -> (E) effect.addAttributeModifier(attribute.get(), uuid, amount, operation));
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF factorDataFactory(Supplier<MobEffectInstance.FactorData> factorDataFactory) {
        this.addBuildOptions(effect -> (E) effect.setFactorDataFactory(factorDataFactory));
        return (SELF) this;
    }

    private static final class SimpleMobEffect extends MobEffect {
        private SimpleMobEffect(MobEffectCategory category, int color) {
            super(category, color);
        }
    }
}
