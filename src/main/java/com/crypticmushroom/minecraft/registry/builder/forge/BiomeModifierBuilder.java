package com.crypticmushroom.minecraft.registry.builder.forge;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraftforge.common.world.BiomeModifier;

import java.util.function.Function;
import java.util.function.Supplier;

public class BiomeModifierBuilder<M extends BiomeModifier, SELF extends BiomeModifierBuilder<M, SELF>> extends DataResourceBuilder<M, BiomeModifier, SELF> {
    private final Function<BootstapContext<BiomeModifier>, M> type;

    public BiomeModifierBuilder(Supplier<M> type) {
        this(bootstrap -> type.get());
    }

    public BiomeModifierBuilder(Function<BootstapContext<BiomeModifier>, M> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Dynamic<BiomeModifier> getRegistry() {
        return RegistryDirectory.BIOME_MODIFIER;
    }

    @Override
    protected M buildType(BootstapContext<BiomeModifier> bootstrap) {
        return this.type.apply(bootstrap);
    }
}
