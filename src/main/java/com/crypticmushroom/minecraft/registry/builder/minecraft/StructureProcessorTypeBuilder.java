package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorType;

import java.util.function.Function;
import java.util.function.Supplier;

public class StructureProcessorTypeBuilder<P extends StructureProcessor, T extends StructureProcessorType<P>, SELF extends StructureProcessorTypeBuilder<P, T, SELF>> extends RegistryObjectBuilder<T, StructureProcessorType<?>, SELF> {
    private final Function<Codec<P>, T> type;
    private final Supplier<Codec<P>> codec;

    @SuppressWarnings("unchecked")
    public StructureProcessorTypeBuilder(Supplier<Codec<P>> codec) {
        this(c -> (T) (StructureProcessorType<P>) () -> c, codec);
    }

    public StructureProcessorTypeBuilder(Function<Codec<P>, T> type, Supplier<Codec<P>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<StructureProcessorType<?>> getRegistry() {
        return RegistryDirectory.STRUCTURE_PROCESSOR_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
