package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.DataResourceBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;

import java.util.function.Function;
import java.util.function.Supplier;

public class ConfiguredFeatureBuilder<F extends Feature<FC>, FC extends FeatureConfiguration, SELF extends ConfiguredFeatureBuilder<F, FC, SELF>> extends DataResourceBuilder<ConfiguredFeature<FC, F>, ConfiguredFeature<?, ?>, ConfiguredFeatureBuilder<F, FC, SELF>> {
    private final Function<BootstapContext<ConfiguredFeature<?, ?>>, F> feature;
    private final Function<BootstapContext<ConfiguredFeature<?, ?>>, FC> config;

    public ConfiguredFeatureBuilder(Supplier<F> feature, Supplier<FC> config) {
        this(bootstrap -> feature.get(), bootstrap -> config.get());
    }

    public ConfiguredFeatureBuilder(Function<BootstapContext<ConfiguredFeature<?, ?>>, F> feature, Supplier<FC> config) {
        this(feature, bootstrap -> config.get());
    }

    public ConfiguredFeatureBuilder(Supplier<F> feature, Function<BootstapContext<ConfiguredFeature<?, ?>>, FC> config) {
        this(bootstrap -> feature.get(), config);
    }

    public ConfiguredFeatureBuilder(Function<BootstapContext<ConfiguredFeature<?, ?>>, F> feature, Function<BootstapContext<ConfiguredFeature<?, ?>>, FC> config) {
        this.feature = feature;
        this.config = config;
    }

    @Override
    protected RegistryInfo.Dynamic<ConfiguredFeature<?, ?>> getRegistry() {
        return RegistryDirectory.CONFIGURED_FEATURE;
    }

    @Override
    protected ConfiguredFeature<FC, F> buildType(BootstapContext<ConfiguredFeature<?, ?>> bootstrap) {
        return new ConfiguredFeature<>(this.feature.apply(bootstrap), this.config.apply(bootstrap));
    }
}
