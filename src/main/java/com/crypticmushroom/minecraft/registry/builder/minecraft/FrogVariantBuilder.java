package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.animal.FrogVariant;
import net.minecraftforge.registries.RegistryObject;

public class FrogVariantBuilder<SELF extends FrogVariantBuilder<SELF>> extends RegistryObjectBuilder<FrogVariant, FrogVariant, SELF> {
    private ResourceLocation texture;

    @Override
    protected RegistryInfo.Static<FrogVariant> getRegistry() {
        return RegistryDirectory.FROG_VARIANT;
    }

    @Override
    public RegistryObject<FrogVariant> build() {
        if (this.texture == null)
            throw new IncompleteBuilderException("Cannot create a cat variant without a texture");

        return super.build();
    }

    @Override
    protected FrogVariant buildType() {
        return new FrogVariant(this.texture);
    }


    /* CAT VARIANT */

    public SELF texture(String texture) {
        return this.texture(this.makeResLoc(texture));
    }

    @SuppressWarnings("unchecked")
    public SELF texture(ResourceLocation texture) {
        this.texture = texture;
        return (SELF) this;
    }
}
