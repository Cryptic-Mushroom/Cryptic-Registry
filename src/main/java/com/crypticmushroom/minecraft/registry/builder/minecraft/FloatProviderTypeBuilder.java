package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.serialization.Codec;
import net.minecraft.util.valueproviders.FloatProvider;
import net.minecraft.util.valueproviders.FloatProviderType;

import java.util.function.Function;
import java.util.function.Supplier;

public class FloatProviderTypeBuilder<P extends FloatProvider, T extends FloatProviderType<P>, SELF extends FloatProviderTypeBuilder<P, T, SELF>> extends RegistryObjectBuilder<T, FloatProviderType<?>, SELF> {
    private final Function<Codec<P>, T> type;
    private final Supplier<Codec<P>> codec;

    @SuppressWarnings("unchecked")
    public FloatProviderTypeBuilder(Supplier<Codec<P>> codec) {
        this(c -> (T) (FloatProviderType<P>) () -> c, codec);
    }

    public FloatProviderTypeBuilder(Function<Codec<P>, T> type, Supplier<Codec<P>> codec) {
        this.type = type;
        this.codec = codec;
    }

    @Override
    protected RegistryInfo.Static<FloatProviderType<?>> getRegistry() {
        return RegistryDirectory.FLOAT_PROVIDER_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.codec.get());
    }
}
