package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.level.storage.loot.Serializer;
import net.minecraft.world.level.storage.loot.SerializerType;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Function;
import java.util.function.Supplier;

public class SerializerTypeBuilder<T, S extends SerializerType<T>, A extends S, SELF extends SerializerTypeBuilder<T, S, A, SELF>> extends RegistryObjectBuilder<A, S, SELF> {
    private final RegistryInfo.Static<S> registry;
    private final Function<Serializer<? extends T>, A> type;

    private Supplier<Serializer<? extends T>> serializer;

    public SerializerTypeBuilder(RegistryInfo.Static<S> registry, Function<Serializer<? extends T>, A> type) {
        this.registry = registry;
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<S> getRegistry() {
        return this.registry;
    }

    @Override
    public RegistryObject<A> build() {
        this.checkAttribute(this.serializer, "serializer");

        return super.build();
    }

    @Override
    protected A buildType() {
        return this.type.apply(this.serializer.get());
    }


    /* SERIALIZER TYPE */

    @SuppressWarnings("unchecked")
    public SELF serializer(Supplier<Serializer<? extends T>> serializer) {
        this.serializer = serializer;
        return (SELF) this;
    }
}
