package com.crypticmushroom.minecraft.registry.builder;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import net.minecraft.resources.ResourceLocation;
import org.jetbrains.annotations.ApiStatus.Internal;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

// fuck off this isn't for you
// use a pre-existing subclass like a good little boy
public abstract class AbstractSupplierBuilder<OBJ, BLD extends Supplier<OBJ>, SELF extends AbstractSupplierBuilder<OBJ, BLD, SELF>> {
    private final String modId; // why is this variable final yet private? some subclasses need to override getModId().
    private String id;
    private final List<Function<OBJ, OBJ>> buildOptions = new ArrayList<>();

    protected AbstractSupplierBuilder() {
        this.modId = CrypticRegistry.attemptGetModIdFromSavedClass();

        // don't mixin into my library mod. if you want to avoid this, make a subclass and override this method
        if (this.getModId() == null) {
            throw new MalformedBuilderException("Cannot create a builder from a class that has not been linked with a registry");
        }
    }

    /**
     * Adds the given build options to the internal list of build options to be applied when the object is
     * {@link #build() built}.
     *
     * @param buildOptions The build options to queue.
     * @implNote This method is final to maintain the integrity of build options.
     */
    protected final void addBuildOptions(Function<OBJ, OBJ> buildOptions) {
        this.buildOptions.add(buildOptions);
    }

    /**
     * Applies all of the queued build options to the given object.
     *
     * @param object The object to apply build options to.
     * @return The result of the build options applied onto the object.
     *
     * @implNote This method is final to maintain the integrity of build options.
     * @apiNote This method should only be used by the direct subclass of this class, and not by any specific or
     *     external builders.
     */
    @Internal
    protected final OBJ applyBuildOptions(OBJ object) {
        var built = object;
        for (Function<OBJ, OBJ> buildOption : this.buildOptions) {
            built = buildOption.apply(built);
        }
        return built;
    }

    /**
     * This method adds the given build options using {@link #addBuildOptions(Function)} and then proceeds to build the
     * {@link BLD supplier} using {@link #build()}.
     *
     * @param buildOptions The build options to apply onto the built object.
     * @return The supplier for the object.
     *
     * @implNote This method is final to maintain the integrity of build options.
     */
    public final BLD build(Function<OBJ, OBJ> buildOptions) {
        this.addBuildOptions(buildOptions);
        return this.build();
    }

    /**
     * This method creates the {@link BLD supplier} used to store the object in the registry class. <strong>It does not
     * create the resulting object like {@link #buildType()} does.</strong>
     *
     * @return The supplier for the object to be created at registration.
     */
    public abstract BLD build();

    /**
     * This method is responsible for creating the object that will be contained by the resulting {@link BLD supplier}
     * provided by {@link #build()}.
     *
     * @return The resulting object to be created at registration.
     */
    protected abstract OBJ buildType();

    /**
     * @return A resource location containing the {@link #modId mod ID} and the builder object's ID.
     */
    protected final ResourceLocation makeResLoc() {
        return this.makeResLoc(this.getId());
    }

    /**
     * @return A resource location containing the {@link #modId mod ID} and the given path.
     */
    protected final ResourceLocation makeResLoc(String path) {
        return new ResourceLocation(this.makeResLocString(path));
    }

    /**
     * @return A resource location containing the {@link #modId mod ID} and the builder object's ID, in the form of a
     *     {@link String}.
     */
    protected final String makeResLocString() {
        return this.makeResLocString(this.getId());
    }

    /**
     * @return A resource location containing the {@link #modId mod ID} and the given path, in the form of a
     *     {@link String}.
     */
    protected final String makeResLocString(String path) {
        return path.indexOf(':') >= 0 ? path : this.getModId() + ":" + path;
    }

    /**
     * @return The ID of this builder's object.
     */
    protected final String getId() {
        return this.id;
    }

    /**
     * @return The mod ID used for this builder (and, by extension, any linked registers).
     */
    protected String getModId() {
        return this.modId;
    }

    @Internal
    protected <O> O checkAttribute(O object, String name) {
        try {
            return Objects.requireNonNull(object);
        } catch (NullPointerException e) {
            throw new IncompleteBuilderException("Cannot create object with ID %s with null or missing value: %s".formatted(this.getId(), name), e);
        }
    }


    /* BASE OBJECT */

    /**
     * Sets the ID to build the builder's object with.
     *
     * @param id The ID to mark this builder's object with.
     * @return This builder.
     */
    @SuppressWarnings("unchecked")
    public SELF id(String id) {
        this.id = id;
        return (SELF) this;
    }
}
