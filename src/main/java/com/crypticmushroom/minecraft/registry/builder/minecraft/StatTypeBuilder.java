package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.data.registry.LocalizedNameRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.core.Registry;
import net.minecraft.stats.StatType;

import java.util.function.Function;

public class StatTypeBuilder<S, T extends StatType<S>, SELF extends StatTypeBuilder<S, T, SELF>> extends RegistryObjectBuilder.Named<T, StatType<?>, SELF> {
    private final Function<Registry<S>, T> type;
    private final Registry<S> registry;

    @SuppressWarnings("unchecked")
    public StatTypeBuilder(Registry<S> registry) {
        this(r -> (T) new StatType<>(r), registry);
    }

    public StatTypeBuilder(Function<Registry<S>, T> type, Registry<S> registry) {
        this.type = type;
        this.registry = registry;
    }

    @Override
    protected RegistryInfo.Static<StatType<?>> getRegistry() {
        return RegistryDirectory.STAT_TYPE;
    }

    @Override
    public LocalizedNameRegistry.Normal<StatType<?>> getLocalizedNameRegistry() {
        return LocalizedNameRegistry.STAT_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.apply(this.registry);
    }
}
