package com.crypticmushroom.minecraft.registry.builder.forge;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraftforge.registries.holdersets.HolderSetType;

import java.util.function.Supplier;

public class HolderSetTypeBuilder<T extends HolderSetType, SELF extends HolderSetTypeBuilder<T, SELF>> extends RegistryObjectBuilder<T, HolderSetType, SELF> {
    private final Supplier<T> type;

    public HolderSetTypeBuilder(Supplier<T> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<HolderSetType> getRegistry() {
        return RegistryDirectory.HOLDER_SET_TYPE;
    }

    @Override
    protected T buildType() {
        return this.type.get();
    }
}
