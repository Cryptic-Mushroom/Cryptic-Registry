package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.fluids.FluidType;
import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.fluids.ForgeFlowingFluid.Properties;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

public class FluidBuilder<F extends Fluid, SELF extends FluidBuilder<F, SELF>> extends RegistryObjectBuilder<F, Fluid, SELF> {
    private final Function<ForgeFlowingFluid.@Nullable Properties, F> type;
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType") // necessary to handle incompatible types
    private final Optional<ForgeFlowingFluid.Properties> properties;

    @Deprecated
    public FluidBuilder(Supplier<F> type) {
        this.type = p -> type.get();
        this.properties = Optional.empty();
    }

    public FluidBuilder(Function<ForgeFlowingFluid.Properties, F> type, Supplier<? extends FluidType> fluidType, String still, String flowing) {
        this.type = type;
        this.properties = Optional.of(new Properties(
            fluidType,
            () -> RegistryDirectory.FLUID.getForgeRegistry().getValue(this.makeResLoc(still)),
            () -> RegistryDirectory.FLUID.getForgeRegistry().getValue(this.makeResLoc(flowing))
        ));
    }

    @Override
    protected RegistryInfo.Static<Fluid> getRegistry() {
        return RegistryDirectory.FLUID;
    }

    @Override
    protected F buildType() {
        return this.type.apply(this.properties.orElse(null));
    }


    /* FLUID */

    private void warnUnsupportedType() {
        CrypticRegistry.LOGGER.warn("Attempted to change properties for a fluid builder for fluid {} that does not hold a type of ForgeFlowingFluid! If you are certain you are using your own fluid that doesn't extend that class, don't use any builder methods that mutate properties!", this.getId());
    }

    @SuppressWarnings("unchecked")
    public SELF bucket(Supplier<? extends Item> bucket) {
        this.properties.ifPresentOrElse(p -> p.bucket(bucket), this::warnUnsupportedType);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF block(Supplier<? extends LiquidBlock> block) {
        this.properties.ifPresentOrElse(p -> p.block(block), this::warnUnsupportedType);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF slopeFindDistance(int slopeFindDistance) {
        this.properties.ifPresentOrElse(p -> p.slopeFindDistance(slopeFindDistance), this::warnUnsupportedType);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF levelDecreasePerBlock(int levelDecreasePerBlock) {
        this.properties.ifPresentOrElse(p -> p.levelDecreasePerBlock(levelDecreasePerBlock), this::warnUnsupportedType);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF explosionResistance(float explosionResistance) {
        this.properties.ifPresentOrElse(p -> p.explosionResistance(explosionResistance), this::warnUnsupportedType);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF tickRate(int tickRate) {
        this.properties.ifPresentOrElse(p -> p.tickRate(tickRate), this::warnUnsupportedType);
        return (SELF) this;
    }
}
