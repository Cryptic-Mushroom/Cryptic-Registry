package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.RegistryObjectBuilder;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import com.crypticmushroom.minecraft.registry.directory.RegistryInfo;
import com.mojang.datafixers.util.Function7;
import net.minecraft.world.level.chunk.ChunkStatus;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.EnumSet;
import java.util.function.Supplier;

public class ChunkStatusBuilder<S extends ChunkStatus, SELF extends ChunkStatusBuilder<S, SELF>> extends RegistryObjectBuilder<S, ChunkStatus, SELF> {
    private final ChunkStatusFunction<S> type;

    private Supplier<? extends @Nullable ChunkStatus> parent = () -> null;
    private Integer range = null;
    private final EnumSet<Heightmap.Types> heightmapsAfter = EnumSet.noneOf(Heightmap.Types.class);
    private @Nullable ChunkStatus.ChunkType chunkType = null;
    private @Nullable Supplier<? extends ChunkStatus.GenerationTask> generationTask = null;
    private @Nullable Supplier<? extends ChunkStatus.LoadingTask> loadingTask = () -> ChunkStatus.PASSTHROUGH_LOAD_TASK;

    public ChunkStatusBuilder() {
        this(ChunkStatusFunction.defaultType());
    }

    public ChunkStatusBuilder(ChunkStatusFunction<S> type) {
        this.type = type;
    }

    @Override
    protected RegistryInfo.Static<ChunkStatus> getRegistry() {
        return RegistryDirectory.CHUNK_STATUS;
    }

    @Override
    public RegistryObject<S> build() {
        this.checkAttribute(this.range, "range");
        this.checkAttribute(this.chunkType, "chunk type");
        this.checkAttribute(this.generationTask, "generation task");
        this.checkAttribute(this.loadingTask, "loading type");

        return super.build();
    }

    @Override
    @SuppressWarnings("DataFlowIssue") // checked in build()
    protected S buildType() {
        return this.type.apply(
            this.makeResLocString(),
            this.parent.get(),
            this.range,
            this.heightmapsAfter,
            this.chunkType,
            this.generationTask.get(),
            this.loadingTask.get()
        );
    }

    @FunctionalInterface
    public interface ChunkStatusFunction<C extends ChunkStatus> extends Function7<String, @Nullable ChunkStatus, Integer, EnumSet<Heightmap.Types>, ChunkStatus.ChunkType, ChunkStatus.GenerationTask, ChunkStatus.LoadingTask, C> {
        @Override
        C apply(String name, @Nullable ChunkStatus parent, Integer range, EnumSet<Heightmap.Types> heightmapsAfter,
                ChunkStatus.ChunkType chunkType, ChunkStatus.GenerationTask generationTask,
                ChunkStatus.LoadingTask loadingTask);

        default C apply(String name, @Nullable ChunkStatus parent, int range, EnumSet<Heightmap.Types> heightmapsAfter,
                        ChunkStatus.ChunkType chunkType, ChunkStatus.GenerationTask generationTask,
                        ChunkStatus.LoadingTask loadingTask) {
            return this.apply(name, parent, (Integer) range, heightmapsAfter, chunkType, generationTask, loadingTask);
        }

        @SuppressWarnings("unchecked")
        static <C extends ChunkStatus> ChunkStatusFunction<C> defaultType() {
            return (name, parent, range, heightmapsAfter, chunkType, generationTask, loadingTask) -> (C) new ChunkStatus(name, parent, range, heightmapsAfter, chunkType, generationTask, loadingTask);
        }
    }


    /* CHUNK STATUS */

    @SuppressWarnings("unchecked")
    public SELF parent(Supplier<? extends ChunkStatus> parent) {
        this.parent = parent;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF range(int range) {
        this.range = range;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF heightmapAfter(Heightmap.Types heightmapAfter) {
        this.heightmapsAfter.add(heightmapAfter);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public final SELF heightmapAfter(Heightmap.Types... heightmapAfter) {
        for (Heightmap.Types t : heightmapAfter) {
            this.heightmapAfter(t);
        }

        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF heightmapAfter(Collection<Heightmap.Types> heightmapsAfter) {
        heightmapsAfter.forEach(this::heightmapAfter);
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF chunkType(ChunkStatus.ChunkType chunkType) {
        this.chunkType = chunkType;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF generationTask(Supplier<? extends ChunkStatus.GenerationTask> generationTask) {
        this.generationTask = generationTask;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF loadingTask(Supplier<? extends ChunkStatus.LoadingTask> loadingTask) {
        this.loadingTask = loadingTask;
        return (SELF) this;
    }
}
