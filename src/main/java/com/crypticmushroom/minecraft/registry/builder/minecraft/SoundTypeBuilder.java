package com.crypticmushroom.minecraft.registry.builder.minecraft;

import com.crypticmushroom.minecraft.registry.builder.IncompleteBuilderException;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.common.util.ForgeSoundType;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * The sound type builder contains various methods that allow for building a {@link ForgeSoundType} with ease.
 *
 * @param <SELF> The type of the builder for casting in the super class.
 * @author Jonathing
 * @since 0.1.0
 */
public class SoundTypeBuilder<S extends ForgeSoundType, SELF extends SoundTypeBuilder<S, SELF>> {
    private final ForgeSoundTypeFunction<S> type;

    private float volume = 1.0F;
    private float pitch = 1.0F;

    private Supplier<SoundEvent> breakSound;
    private Supplier<SoundEvent> stepSound;
    private Supplier<SoundEvent> placeSound;
    private Supplier<SoundEvent> hitSound;
    private Supplier<SoundEvent> fallSound;

    public SoundTypeBuilder() {
        this(ForgeSoundTypeFunction.defaultType());
    }

    public SoundTypeBuilder(ForgeSoundTypeFunction<S> type) {
        this.type = type;
    }

    public S build() {
        try {
            return this.type.create(
                this.volume, this.pitch,
                Objects.requireNonNull(this.breakSound, "Missing break sound supplier"),
                Objects.requireNonNull(this.stepSound, "Missing step sound supplier"),
                Objects.requireNonNull(this.placeSound, "Missing place sound supplier"),
                Objects.requireNonNull(this.hitSound, "Missing hit sound supplier"),
                Objects.requireNonNull(this.fallSound, "Missing fall sound supplier")
            );
        } catch (NullPointerException e) {
            throw new IncompleteBuilderException("Builder for a sound type is incomplete! Missing a required sound event.", e);
        }
    }

    @FunctionalInterface
    public interface ForgeSoundTypeFunction<T extends ForgeSoundType> {
        T create(float volume, float pitch, Supplier<SoundEvent> breakSound, Supplier<SoundEvent> stepSound,
                 Supplier<SoundEvent> placeSound, Supplier<SoundEvent> hitSound, Supplier<SoundEvent> fallSound);

        @SuppressWarnings("unchecked")
        static <T extends ForgeSoundType> ForgeSoundTypeFunction<T> defaultType() {
            return (volume, pitch, breakSound, stepSound, placeSound, hitSound, fallSound) -> (T) new ForgeSoundType(volume, pitch, breakSound, stepSound, placeSound, hitSound, fallSound);
        }
    }


    /* SOUND TYPE */

    @SuppressWarnings("unchecked")
    public SELF volume(float volume) {
        this.volume = volume;
        return (SELF) this;
    }

    public SELF volume(double volume) {
        return this.volume((float) volume);
    }

    @SuppressWarnings("unchecked")
    public SELF pitch(float pitch) {
        this.pitch = pitch;
        return (SELF) this;
    }

    public SELF pitch(double pitch) {
        return this.pitch((float) pitch);
    }

    @SuppressWarnings("unchecked")
    public SELF breakSound(Supplier<SoundEvent> breakSound) {
        this.breakSound = breakSound;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF stepSound(Supplier<SoundEvent> stepSound) {
        this.stepSound = stepSound;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF placeSound(Supplier<SoundEvent> placeSound) {
        this.placeSound = placeSound;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF hitSound(Supplier<SoundEvent> hitSound) {
        this.hitSound = hitSound;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF fallSound(Supplier<SoundEvent> fallSound) {
        this.fallSound = fallSound;
        return (SELF) this;
    }

    @SuppressWarnings("unchecked")
    public SELF allSounds(Supplier<SoundEvent> defaultSound) {
        this.breakSound = defaultSound;
        this.stepSound = defaultSound;
        this.placeSound = defaultSound;
        this.hitSound = defaultSound;
        this.fallSound = defaultSound;
        return (SELF) this;
    }
}
