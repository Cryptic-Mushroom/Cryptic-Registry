package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CakeBlock;
import net.minecraft.world.level.block.ShulkerBoxBlock;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;

@Unique
@Mixin({
    ShulkerBoxBlock.class,
    CakeBlock.class
})
public class MakesSingleStackBlockItem implements MakesCustomBlockItem<BlockItem> {
    @Override
    public BlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new BlockItem((Block) (Object) this, properties.stacksTo(1));
    }
}
