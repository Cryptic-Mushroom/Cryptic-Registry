package com.crypticmushroom.minecraft.registry.coremod.hook.impl;

import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import net.minecraftforge.common.data.ForgeItemTagsProvider;
import net.minecraftforge.data.event.GatherDataEvent;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.spongepowered.asm.mixin.Overwrite;

/**
 * This additional interface is designed to be used {@link Internal internally} and <strong>only for
 * {@link ForgeItemTagsProvider}</strong> via coremodding.
 *
 * @author Jonathing
 */
@Internal
public interface CrypticForgeItemTagsProvider extends ICrypticDataProvider {
    /**
     * Internally configures the affected {@link ForgeItemTagsProvider} for data generation with Cryptic Registry.
     *
     * @param event The event to add the provider to.
     * @param modId The mod ID to configure the provider to.
     */
    void cmreg$configure(GatherDataEvent event, String modId);

    /**
     * @throws UnsupportedOperationException If this method is applied to {@link ForgeItemTagsProvider} and is not
     *                                       manually overwritten in a mixin class.
     * @deprecated Due to Java bytecode manipulation limitations when it comes to using superinterfaces, this method
     *     <strong>cannot be overriden</strong> in {@link ForgeItemTagsProvider}. The mixin class for it must
     *     {@link Overwrite manually overwrite} the method and use its own implementation.
     */
    @Override
    @Deprecated
    @SuppressWarnings("override")
    default String getName() {
        throw new UnsupportedOperationException("getName() must be manually overwritten in a mixin for ForgeItemTagsProvider");
    }

    @Override
    default String getSimpleName() {
        return "Additional Item Tags - Forge";
    }

    @Override
    default boolean shouldRun() {
        return this.getEvent().includeServer();
    }
}
