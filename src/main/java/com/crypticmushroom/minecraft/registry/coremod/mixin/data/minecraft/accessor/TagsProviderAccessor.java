package com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft.accessor;

import net.minecraft.data.tags.TagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(TagsProvider.class)
public interface TagsProviderAccessor<T> {
    @Accessor(value = "existingFileHelper", remap = false)
    @Mutable
    void cmreg$existingFileHelper(ExistingFileHelper existingFileHelper);
}
