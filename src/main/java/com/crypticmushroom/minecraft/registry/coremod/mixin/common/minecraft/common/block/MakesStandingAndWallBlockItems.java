package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.StandingAndWallBlockItem;
import net.minecraft.world.level.block.BaseCoralFanBlock;
import net.minecraft.world.level.block.CoralFanBlock;
import net.minecraft.world.level.block.CoralWallFanBlock;
import net.minecraft.world.level.block.SkullBlock;
import net.minecraft.world.level.block.TorchBlock;
import net.minecraft.world.level.block.WallSkullBlock;
import org.spongepowered.asm.mixin.Mixin;

@Mixin({
    TorchBlock.class,
    BaseCoralFanBlock.class,
    CoralFanBlock.class,
    CoralWallFanBlock.class,
    SkullBlock.class,
    WallSkullBlock.class
})
public class MakesStandingAndWallBlockItems implements MakesCustomBlockItem<StandingAndWallBlockItem> {
    @Override
    public StandingAndWallBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        throw new IllegalStateException(
            "Cryptic Registry does not support creating a block item for %s. ".formatted(this.getClass().getSimpleName()) +
                "Please use the implementations found in com.cryptic.mushroom.minecraft.registry.api.block " +
                "or implement MakesCustomBlockItem<? extends StandingAndWallBlockItem> in your %s implementation.".formatted(this.getClass().getSimpleName()));
    }
}
