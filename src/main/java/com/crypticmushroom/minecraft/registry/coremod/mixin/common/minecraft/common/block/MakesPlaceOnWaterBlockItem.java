package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.PlaceOnWaterBlockItem;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FrogspawnBlock;
import net.minecraft.world.level.block.WaterlilyBlock;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;

@Unique
@Mixin({
    WaterlilyBlock.class,
    FrogspawnBlock.class
})
public class MakesPlaceOnWaterBlockItem implements MakesCustomBlockItem<PlaceOnWaterBlockItem> {
    @Override
    public PlaceOnWaterBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new PlaceOnWaterBlockItem((Block) (Object) this, properties);
    }
}
