package com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft;

import com.crypticmushroom.minecraft.registry.coremod.hook.RecipeBuilderHooks;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.SpecialRecipeBuilder;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import java.util.function.Consumer;

@Mixin(SpecialRecipeBuilder.class)
public abstract class SpecialRecipeBuilderMixin {
    @ModifyVariable(
        method = "save(Ljava/util/function/Consumer;Ljava/lang/String;)V",
        at = @At("HEAD"),
        index = 2,
        argsOnly = true
    )
    private String modifyId(String id, Consumer<FinishedRecipe> recipeConsumer) {
        return RecipeBuilderHooks.replaceResLoc(id, recipeConsumer);
    }
}
