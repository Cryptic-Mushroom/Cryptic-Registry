package com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft;

import com.crypticmushroom.minecraft.registry.coremod.hook.RecipeBuilderHooks;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.SmithingTransformRecipeBuilder;
import net.minecraft.resources.ResourceLocation;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;

import java.util.function.Consumer;

@Mixin(SmithingTransformRecipeBuilder.class)
public abstract class SmithingTransformRecipeBuilderMixin {
    @ModifyArg(
        method = "save(Ljava/util/function/Consumer;Ljava/lang/String;)V",
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/data/recipes/SmithingTransformRecipeBuilder.save(Ljava/util/function/Consumer;Lnet/minecraft/resources/ResourceLocation;)V"
        ),
        index = 1
    )
    private ResourceLocation modifyId(Consumer<FinishedRecipe> recipeConsumer, ResourceLocation id) {
        return RecipeBuilderHooks.replaceResLoc(id, recipeConsumer);
    }
}
