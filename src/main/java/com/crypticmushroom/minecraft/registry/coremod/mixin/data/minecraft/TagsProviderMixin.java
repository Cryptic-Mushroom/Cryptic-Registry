package com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft;

import com.crypticmushroom.minecraft.registry.data.provider.tag.CrypticItemTagsProvider;
import com.crypticmushroom.minecraft.registry.data.provider.tag.CrypticTagsProvider;
import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import com.llamalad7.mixinextras.sugar.Local;
import com.llamalad7.mixinextras.sugar.ref.LocalRef;
import net.minecraft.data.DataProvider;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.tags.TagBuilder;
import net.minecraft.tags.TagEntry;
import net.minecraft.tags.TagKey;
import org.jetbrains.annotations.Contract;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;

import java.util.List;

/**
 * This mixin allows us to inject bytecode into the {@link TagsProvider} class to perform various custom operations
 * required by the {@link CrypticTagsProvider}.
 *
 * @author Jonathing
 */
@Mixin(TagsProvider.class)
public abstract class TagsProviderMixin<T> implements DataProvider {
    @Shadow(remap = false) protected @Final String modId;

    @Contract("null -> fail; _ -> !null")
    @Shadow
    protected abstract TagBuilder getOrCreateRawBuilder(TagKey<T> tag);

    @Contract("-> !null")
    @Shadow
    @SuppressWarnings("override")
    public abstract String getName();

    @SuppressWarnings({"ConstantValue", "rawtypes"})
    @WrapOperation(
        method = "lambda$run$6(Ljava/util/function/Predicate;Ljava/util/function/Predicate;Lnet/minecraft/data/CachedOutput;Ljava/util/Map$Entry;)Ljava/util/concurrent/CompletableFuture;",
        remap = false,
        at = @At(
            value = "INVOKE",
            target = "java/util/List.isEmpty()Z"
        )
    )
    private boolean allowMissingReferencesForCmreg(List<TagEntry> list1, Operation<Boolean> original, @Local(ordinal = 0) LocalRef<List<TagEntry>> list) {
        if (!((TagsProvider) (Object) this instanceof CrypticItemTagsProvider)) return original.call(list1);
        list.set(list.get().stream().filter(o -> !list1.contains(o)).toList());
        return true;
    }
}
