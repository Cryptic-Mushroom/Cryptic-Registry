package com.crypticmushroom.minecraft.registry.coremod.hook.impl;

import com.crypticmushroom.minecraft.registry.util.Register;

public interface CrypticDeferredRegister<T> extends Register<T> { }
