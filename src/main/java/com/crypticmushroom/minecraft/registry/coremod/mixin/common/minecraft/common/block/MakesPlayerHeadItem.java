package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.PlayerHeadItem;
import net.minecraft.world.level.block.PlayerHeadBlock;
import net.minecraft.world.level.block.PlayerWallHeadBlock;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;

@Unique
@Mixin({
    PlayerHeadBlock.class,
    PlayerWallHeadBlock.class
})
public class MakesPlayerHeadItem implements MakesCustomBlockItem<PlayerHeadItem> {
    @Override
    public PlayerHeadItem cmreg$makeBlockItem(Item.Properties properties) {
        throw new IllegalStateException(
            "Cryptic Registry does not support traditionally creating player heads. " +
                "Please use the implementations found in com.cryptic.mushroom.minecraft.registry.api.block " +
                "or implement MakesCustomBlockItem<? extends PlayerHeadItem> in your player head implementation.");
    }
}
