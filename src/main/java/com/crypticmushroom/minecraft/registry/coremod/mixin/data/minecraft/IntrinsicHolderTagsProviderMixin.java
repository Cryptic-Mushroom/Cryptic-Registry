package com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft;

import com.crypticmushroom.minecraft.registry.coremod.hook.ItemTagsProviderHooks;
import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import net.minecraft.data.tags.IntrinsicHolderTagsProvider;
import net.minecraft.tags.TagBuilder;
import net.minecraft.tags.TagKey;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(IntrinsicHolderTagsProvider.class)
public abstract class IntrinsicHolderTagsProviderMixin<T> {
    /**
     * This method wrapper allows us to prevent the creation of {@link TagBuilder tag builders} that are doomed to be
     * empty on data generation.
     *
     * @param provider {@code this}
     * @param tag      The tag for the builder.
     * @return The original builder to be created, or a dud that can't be tracked.
     */
    @WrapOperation(
        method = "tag(Lnet/minecraft/tags/TagKey;)Lnet/minecraft/data/tags/IntrinsicHolderTagsProvider$IntrinsicTagAppender;",
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/data/tags/IntrinsicHolderTagsProvider.getOrCreateRawBuilder(Lnet/minecraft/tags/TagKey;)Lnet/minecraft/tags/TagBuilder;"
        )
    )
    private TagBuilder preventVanillaTagTracking(IntrinsicHolderTagsProvider<T> provider, TagKey<T> tag, Operation<TagBuilder> original) {
        return ItemTagsProviderHooks.isVanillaOrForgeProvider(provider)
               ? TagBuilder.create() : original.call(provider, tag);
    }
}
