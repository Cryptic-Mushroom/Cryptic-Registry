package com.crypticmushroom.minecraft.registry.coremod.hook;

import com.crypticmushroom.minecraft.registry.data.provider.recipe.CrypticRecipeProvider;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.resources.ResourceLocation;

import java.util.function.Consumer;

/** @see net.minecraft.data.recipes.RecipeBuilder */
@SuppressWarnings("unused")
public final class RecipeBuilderHooks {
    public static String replaceResLoc(String original, Consumer<FinishedRecipe> finishedRecipeConsumer) {
        return replaceResLoc(new ResourceLocation(original), finishedRecipeConsumer).toString();
    }

    public static ResourceLocation replaceResLoc(ResourceLocation original, Consumer<FinishedRecipe> finishedRecipeConsumer) {
        return replaceResLoc(original, finishedRecipeConsumer, original.getPath());
    }

    /** @see net.minecraft.data.recipes.RecipeBuilder#save(Consumer, String) */
    public static ResourceLocation replaceResLoc(ResourceLocation original, Consumer<FinishedRecipe> finishedRecipeConsumer, String recipeId) {
        return original.getNamespace().equals("minecraft") && finishedRecipeConsumer instanceof CrypticRecipeProvider.CrypticRecipeWriter writer
               ? new ResourceLocation(writer.getModId(), recipeId)
               : original;
    }
}
