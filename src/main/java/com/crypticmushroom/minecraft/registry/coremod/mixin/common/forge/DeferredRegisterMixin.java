package com.crypticmushroom.minecraft.registry.coremod.mixin.common.forge;

import com.crypticmushroom.minecraft.registry.coremod.hook.impl.CrypticDeferredRegister;
import net.minecraftforge.registries.DeferredRegister;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(value = DeferredRegister.class, remap = false)
public abstract class DeferredRegisterMixin<T> implements CrypticDeferredRegister<T> { }
