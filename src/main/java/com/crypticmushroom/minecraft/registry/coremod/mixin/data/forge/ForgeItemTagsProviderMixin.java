package com.crypticmushroom.minecraft.registry.coremod.mixin.data.forge;

import com.crypticmushroom.minecraft.registry.coremod.hook.impl.CrypticForgeItemTagsProvider;
import com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft.ItemTagsProviderMixin;
import net.minecraftforge.common.data.ForgeItemTagsProvider;
import net.minecraftforge.data.event.GatherDataEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

/**
 * This mixin allows us to inject bytecode into the {@link ForgeItemTagsProvider} class to perform various custom
 * operations required to properly generate additional item tags for Cryptic Registry.
 *
 * @author Jonathing
 */
@Mixin(ForgeItemTagsProvider.class)
public abstract class ForgeItemTagsProviderMixin extends ItemTagsProviderMixin implements CrypticForgeItemTagsProvider {
    private GatherDataEvent cmdatareg_event;
    private String cmdatareg_modId;

    /**
     * @author Jonathing
     * @reason Clarification during Cryptic Registry datagen that the ForgeItemTagsProvider instance is only used to
     *     generate additional item tag files.
     */
    @Overwrite
    @SuppressWarnings({"override", "deprecation"})
    public String getName() {
        return String.format("%s - %s", this.getDataOwnerName(), this.getSimpleName());
    }

    @Override
    public void cmreg$configure(GatherDataEvent event, String modId) {
        this.cmdatareg_event = event;
        this.cmdatareg_modId = modId;
    }

    @Override
    @SuppressWarnings("AddedMixinMembersNamePattern")
    public String getModId() {
        return this.cmdatareg_modId;
    }

    @Override
    @SuppressWarnings("AddedMixinMembersNamePattern")
    public GatherDataEvent getEvent() {
        return this.cmdatareg_event;
    }
}
