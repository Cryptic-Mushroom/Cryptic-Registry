package com.crypticmushroom.minecraft.registry.coremod.mixin.data.forge;

import com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft.IngredientMixin;
import com.crypticmushroom.minecraft.registry.data.provider.recipe.CrypticRecipeProvider;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.common.crafting.DifferenceIngredient;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(value = DifferenceIngredient.class, remap = false)
public abstract class DifferenceIngredientMixin extends IngredientMixin {
    @Shadow private @Final Ingredient base;
    @Shadow private @Final Ingredient subtracted;

    @Inject(
        method = "<init>(Lnet/minecraft/world/item/crafting/Ingredient;Lnet/minecraft/world/item/crafting/Ingredient;)V",
        at = @At("RETURN")
    )
    private void afterConstruction(Ingredient base, Ingredient subtracted, CallbackInfo callback) {
        this.cmreg$name = cmreg$createName(this.base, this.subtracted);
    }

    @Unique
    private static String cmreg$createName(Ingredient base, Ingredient subtracted) {
        return CrypticRecipeProvider.getName(base) + "_without_" + CrypticRecipeProvider.getName(subtracted);
    }
}
