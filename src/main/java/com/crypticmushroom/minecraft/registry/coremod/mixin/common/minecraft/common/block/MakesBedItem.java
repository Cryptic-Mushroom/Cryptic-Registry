package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.BedItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.BedBlock;
import org.spongepowered.asm.mixin.Mixin;

@Mixin({
    BedBlock.class
})
public class MakesBedItem implements MakesCustomBlockItem<BedItem> {
    @Override
    public BedItem cmreg$makeBlockItem(Item.Properties properties) {
        return new BedItem((BedBlock) (Object) this, properties.stacksTo(1));
    }
}
