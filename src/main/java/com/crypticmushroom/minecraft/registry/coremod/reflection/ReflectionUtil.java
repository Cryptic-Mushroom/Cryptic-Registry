package com.crypticmushroom.minecraft.registry.coremod.reflection;

import com.google.common.reflect.ClassPath;
import cpw.mods.modlauncher.api.INameMappingService.Domain;
import net.minecraftforge.fml.util.ObfuscationReflectionHelper;

import java.io.IOException;

import static com.crypticmushroom.minecraft.registry.coremod.CrypticCoreMod.LOGGER;
import static com.crypticmushroom.minecraft.registry.coremod.CrypticCoreMod.REFLECTION_MARKER;

public final class ReflectionUtil {
    /**
     * This method initializes all top-level classes at the given package name.
     */
    public static void initPackage(String packageName) {
        try {
            var classes = ClassPath.from(ClassLoader.getSystemClassLoader()).getTopLevelClasses(packageName);
            classes.forEach(c -> {
                LOGGER.debug(REFLECTION_MARKER, "Initializing class via reflection: {}", c);
                try {
                    Class.forName(c.getName());
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean threadContains(Class<?> clazz, String methodName) {
        return threadContains(Thread.currentThread().getStackTrace(), clazz.getName(), methodName);
    }

    public static boolean threadContains(Thread thread, Class<?> clazz, String methodName) {
        return threadContains(thread.getStackTrace(), clazz.getName(), methodName);
    }

    public static boolean threadContains(StackTraceElement[] elements, Class<?> clazz, String methodName) {
        return threadContains(elements, clazz.getName(), methodName);
    }

    public static boolean threadContains(String className, String methodName) {
        return threadContains(Thread.currentThread().getStackTrace(), className, methodName);
    }

    public static boolean threadContains(Thread thread, String className, String methodName) {
        return threadContains(thread.getStackTrace(), className, methodName);
    }

    public static boolean threadContains(StackTraceElement[] elements, String className, String methodName) {
        methodName = ObfuscationReflectionHelper.remapName(Domain.METHOD, methodName);
        for (StackTraceElement element : elements) {
            boolean clazz = element.getClassName().equals(className);
            boolean method = element.getMethodName().equals(methodName);
            if (clazz && method) {
                return true;
            }
        }

        return false;
    }
}
