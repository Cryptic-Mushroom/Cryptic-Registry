package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.accessor;

import net.minecraft.world.item.CreativeModeTab;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(CreativeModeTab.Builder.class)
public interface CreativeModeTabBuilderAccessor {
    @Invoker("type")
    CreativeModeTab.Builder cmreg$type(CreativeModeTab.Type type);
}
