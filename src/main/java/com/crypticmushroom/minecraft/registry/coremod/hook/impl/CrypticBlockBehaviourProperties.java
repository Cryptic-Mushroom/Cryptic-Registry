package com.crypticmushroom.minecraft.registry.coremod.hook.impl;

import net.minecraft.world.level.block.state.BlockBehaviour;

public interface CrypticBlockBehaviourProperties {
    BlockBehaviour.Properties cmreg$clone();
}
