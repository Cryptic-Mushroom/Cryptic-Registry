package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.BannerItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.BannerBlock;
import net.minecraft.world.level.block.WallBannerBlock;
import org.spongepowered.asm.mixin.Mixin;

@Mixin({
    BannerBlock.class,
    WallBannerBlock.class
})
public class MakesBannerItem implements MakesCustomBlockItem<BannerItem> {
    @Override
    public BannerItem cmreg$makeBlockItem(Item.Properties properties) {
        throw new IllegalStateException(
            "Cryptic Registry does not support creating a block item for banners. " +
                "Please use the implementations found in com.cryptic.mushroom.minecraft.registry.api.block " +
                "or implement MakesCustomBlockItem<? extends BannerItem> in your banner implementation.");
    }
}
