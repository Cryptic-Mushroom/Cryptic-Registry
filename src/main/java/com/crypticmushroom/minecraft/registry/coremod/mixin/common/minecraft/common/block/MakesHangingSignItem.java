package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.HangingSignItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.CeilingHangingSignBlock;
import net.minecraft.world.level.block.WallHangingSignBlock;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;

@Unique
@Mixin({
    CeilingHangingSignBlock.class,
    WallHangingSignBlock.class
})
public class MakesHangingSignItem implements MakesCustomBlockItem<HangingSignItem> {
    @Override
    public HangingSignItem cmreg$makeBlockItem(Item.Properties properties) {
        throw new IllegalStateException(
            "Cryptic Registry does not support creating a block item for hanging signs. " +
                "Please use the implementations found in com.cryptic.mushroom.minecraft.registry.api.block " +
                "or implement MakesCustomBlockItem<? extends HangingSignItem> in your hanging sign implementation.");
    }
}
