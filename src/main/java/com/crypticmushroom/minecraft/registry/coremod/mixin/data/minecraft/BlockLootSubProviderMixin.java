package com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import net.minecraft.advancements.critereon.ItemPredicate;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.world.item.Items;
import net.minecraftforge.common.Tags;
import org.jetbrains.annotations.Nullable;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Slice;

import java.util.List;

@Mixin(BlockLootSubProvider.class)
public abstract class BlockLootSubProviderMixin {
    @ModifyExpressionValue(
        method = "<clinit>()V",
        slice = @Slice(
            from = @At(
                value = "FIELD",
                target = "net/minecraft/world/item/Items.SHEARS:Lnet/minecraft/world/item/Item;",
                opcode = Opcodes.GETSTATIC
            )
        ),
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/advancements/critereon/ItemPredicate$Builder.of([Lnet/minecraft/world/level/ItemLike;)Lnet/minecraft/advancements/critereon/ItemPredicate$Builder;",
            ordinal = 0
        )
    )
    private static ItemPredicate.Builder replaceShearsItemWithTag(ItemPredicate.Builder original) {
        var shears = List.of(
            Items.SHEARS
        );

        @Nullable var items = original.items;
        if (items == null) return original;

        return !items.isEmpty() && items.stream().filter(i -> !shears.contains(i)).toArray().length == 0
               ? ItemPredicate.Builder.item().of(Tags.Items.SHEARS)
               : original;
    }
}
