package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemNameBlockItem;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CaveVinesBlock;
import net.minecraft.world.level.block.CocoaBlock;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.NetherWartBlock;
import net.minecraft.world.level.block.RedStoneWireBlock;
import net.minecraft.world.level.block.StemBlock;
import net.minecraft.world.level.block.SweetBerryBushBlock;
import net.minecraft.world.level.block.TripWireBlock;
import org.spongepowered.asm.mixin.Mixin;

@Mixin({
    RedStoneWireBlock.class,
    TripWireBlock.class,
    CropBlock.class,
    CocoaBlock.class,
    StemBlock.class,
    NetherWartBlock.class,
    SweetBerryBushBlock.class,
    CaveVinesBlock.class
})
public class MakesItemNameBlockItem implements MakesCustomBlockItem<ItemNameBlockItem> {
    @Override
    public ItemNameBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new ItemNameBlockItem((Block) (Object) this, properties);
    }
}
