package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.DoubleHighBlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.BedBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.DoublePlantBlock;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;

@Unique
@Mixin({
    DoublePlantBlock.class,
    DoorBlock.class
})
public class MakesDoubleHighBlockItem implements MakesCustomBlockItem<DoubleHighBlockItem> {
    @Override
    public DoubleHighBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new DoubleHighBlockItem((Block) (Object) this, properties);
    }
}
