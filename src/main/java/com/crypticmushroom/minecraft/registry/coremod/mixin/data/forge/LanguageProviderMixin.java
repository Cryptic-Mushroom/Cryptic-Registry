package com.crypticmushroom.minecraft.registry.coremod.mixin.data.forge;

import com.crypticmushroom.minecraft.registry.CrypticRegistry;
import com.crypticmushroom.minecraft.registry.coremod.CrypticCoreMod;
import com.crypticmushroom.minecraft.registry.data.provider.lang.CrypticLanguageProvider;
import net.minecraft.data.DataProvider;
import net.minecraftforge.common.data.LanguageProvider;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/**
 * This mixin class injects additional bytecode into the {@link LanguageProvider} for additional functionality performed
 * by {@link CrypticLanguageProvider}.
 *
 * @author Jonathing
 */
@Mixin(value = LanguageProvider.class, remap = false)
public abstract class LanguageProviderMixin implements DataProvider {
    @Shadow private @Final String locale;

    /**
     * This injection allows the language provider to replace keys in the language provider without throwing an
     * exception. This allows for directly inheriting from a superclass of a language provider and manually overriding
     * specific translations (i.e. "Shovel" is in {@code en_us}, but "Spade" can be overridden in a {@code en_uk}
     * provider that extends from an implementation of the former).
     *
     * @param key      The translation key to add to the provider.
     * @param value    The raw translation value to add to the provider.
     * @param callback The callback info for Mixin.
     */
    @Inject(
        method = "add(Ljava/lang/String;Ljava/lang/String;)V",
        at = @At(
            value = "INVOKE",
            target = "java/lang/IllegalStateException.<init>(Ljava/lang/String;)V"
        ),
        cancellable = true
    )
    private void add(String key, String value, CallbackInfo callback) {
        CrypticRegistry.LOGGER.warn(CrypticCoreMod.COREMOD_MARKER, "Allowing replacement of key {} in {}", key, this.locale);
        callback.cancel();
    }
}
