package com.crypticmushroom.minecraft.registry.coremod.mixin.data.forge;

import com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft.IngredientMixin;
import com.crypticmushroom.minecraft.registry.data.provider.recipe.CrypticRecipeProvider;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.crafting.StrictNBTIngredient;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(value = StrictNBTIngredient.class, remap = false)
public abstract class StrictNBTIngredientMixin extends IngredientMixin {
    @Shadow private @Final ItemStack stack;

    @Inject(
        method = "<init>(Lnet/minecraft/world/item/ItemStack;)V",
        at = @At("RETURN")
    )
    private void afterConstruction(ItemStack stack, CallbackInfo callback) {
        this.cmreg$name = cmreg$createName(this.stack);
    }

    @Unique
    private static String cmreg$createName(ItemStack stack) {
        return "strict_nbt_of_" + CrypticRecipeProvider.getName(stack);
    }
}
