package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ScaffoldingBlockItem;
import net.minecraft.world.level.block.ScaffoldingBlock;
import org.spongepowered.asm.mixin.Mixin;

@Mixin({
    ScaffoldingBlock.class
})
public class MakesScaffoldingBlockItem implements MakesCustomBlockItem<ScaffoldingBlockItem> {
    @Override
    public ScaffoldingBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new ScaffoldingBlockItem((ScaffoldingBlock) (Object) this, properties);
    }
}
