package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SignItem;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.WallSignBlock;
import org.spongepowered.asm.mixin.Mixin;

@Mixin({
    StandingSignBlock.class,
    WallSignBlock.class
})
public class MakesSignItem implements MakesCustomBlockItem<SignItem> {
    @Override
    public SignItem cmreg$makeBlockItem(Item.Properties properties) {
        throw new IllegalStateException(
            "Cryptic Registry does not support creating a block item for signs. " +
                "Please use the implementations found in com.cryptic.mushroom.minecraft.registry.api.block " +
                "or implement MakesCustomBlockItem<? extends SignItem> in your sign implementation.");
    }
}
