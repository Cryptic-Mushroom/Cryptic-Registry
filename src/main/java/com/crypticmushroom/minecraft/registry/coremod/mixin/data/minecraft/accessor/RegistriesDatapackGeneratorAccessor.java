package com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft.accessor;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.registries.RegistriesDatapackGenerator;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.concurrent.CompletableFuture;

@Mixin(RegistriesDatapackGenerator.class)
public interface RegistriesDatapackGeneratorAccessor {
    @Accessor("registries")
    CompletableFuture<HolderLookup.Provider> cmreg$registries();
}
