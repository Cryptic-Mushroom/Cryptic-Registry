package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.accessor;

import net.minecraft.world.entity.EntityType;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(EntityType.class)
public interface EntityTypeAccessor {
    @Invoker(value = "defaultVelocitySupplier", remap = false)
    boolean cmreg$defaultVelocitySupplier();

    @Invoker(value = "defaultTrackingRangeSupplier", remap = false)
    int cmreg$defaultTrackingRangeSupplier();

    @Invoker(value = "defaultUpdateIntervalSupplier", remap = false)
    int cmreg$defaultUpdateIntervalSupplier();
}
