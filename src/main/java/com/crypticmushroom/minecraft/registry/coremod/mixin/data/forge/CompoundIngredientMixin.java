package com.crypticmushroom.minecraft.registry.coremod.mixin.data.forge;

import com.crypticmushroom.minecraft.registry.coremod.hook.impl.CrypticIngredient;
import com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft.IngredientMixin;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.common.crafting.CompoundIngredient;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.List;

@Mixin(value = CompoundIngredient.class, remap = false)
public abstract class CompoundIngredientMixin extends IngredientMixin {
    @Shadow private List<Ingredient> children;

    @Inject(
        method = "<init>(Ljava/util/List;)V",
        at = @At("RETURN")
    )
    private void afterConstruction(List<Ingredient> children, CallbackInfo callback) {
        this.cmreg$name = cmreg$createName(this.children);
    }

    @Unique
    private static @Nullable String cmreg$createName(List<Ingredient> values) {
        var builder = new StringBuilder();
        for (int i = 0; i < values.size(); i++) {
            builder.append(((CrypticIngredient) values.get(i)).cmreg$getName());
            if (i != values.size() - 1)
                builder.append("_or_");
        }
        var result = builder.toString();
        return result.isEmpty() ? null : result;
    }
}
