package com.crypticmushroom.minecraft.registry.coremod;

import com.crypticmushroom.minecraft.registry.CrypticRegistryInfo;
import cpw.mods.modlauncher.Launcher;
import cpw.mods.modlauncher.api.IEnvironment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.Nullable;
import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.Mixins;
import org.spongepowered.asm.mixin.connect.IMixinConnector;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.List;
import java.util.Set;

/**
 * <h1>CoreModding in Cryptic Registry</h1>
 * <p>
 * Due to Cryptic Registry's invasive nature, coremodding is an inevitability. Therefore, it is imperative that the
 * coremodding strategies used by it are as safe as possible.
 *
 * <h2>Mixin</h2>
 * <p>
 * Mixin is the framework used by Cryptic Registry to coremod the game. The other available transformer is the CoreMods
 * framework provided by Forge to use JS in a sandbox, but Mixin is far safer since it can account for other mixins that
 * could be modifying the same class. Additionally, it easily allows us to check system properties at runtime to decide
 * exactly which mixins should be applied (see {@link #getMixins()}).
 *
 * <h2>Access Transformation</h2>
 * <p>
 * Since most of the access transformation is only required during data generation, Cryptic Registry opts to use
 * {@link Accessor Mixin accessors} as virtual (non-static) access transformers so they can be applied dynamically. It
 * is also safer to use accessors over traditional access transformers provided by Forge, since if there is some sort of
 * {@link IllegalAccessException access issue}, Mixin can quickly diagnose which accessor(s) are responsible.
 *
 * <h2>Mixin Connector and Config Plugin</h2>
 * <p>
 * To further prevent end-user meddling with raw files in the mod's JAR, this class contains the connection process to
 * the {@code cryptic_registry.mixins.json} file as well as as much configuration for our mixins as possible.
 */
@Internal
public final class CrypticCoreMod implements IMixinConnector, IMixinConfigPlugin {
    public static final Logger LOGGER = LogManager.getLogger("CrypticRegistry");

    public static final Marker
        COREMOD_MARKER = MarkerManager.getMarker("COREMOD"),
        HOOKS_MARKER = MarkerManager.getMarker("HOOKS").addParents(COREMOD_MARKER),
        REFLECTION_MARKER = MarkerManager.getMarker("REFLECTION").addParents(COREMOD_MARKER);

    @Override
    public void connect() {
        boolean data = Launcher.INSTANCE.environment().getProperty(IEnvironment.Keys.LAUNCHTARGET.get()).orElse("MISSING").equalsIgnoreCase("forgedatauserdev");

        Mixins.addConfiguration("META-INF/mixin/" + CrypticRegistryInfo.MOD_ID + ".mixins.json");
        if (data) {
            LOGGER.warn("Modlauncher has started userdev data generation. Datagen-only mixins will apply.");
            Mixins.addConfiguration("META-INF/mixin/" + CrypticRegistryInfo.MOD_ID + ".data.mixins.json");
        }
    }

    @Override
    public @Nullable List<String> getMixins() {
        return null;
    }

    @Override
    public void onLoad(String mixinPackage) { }

    @Override
    public String getRefMapperConfig() {
        return "META-INF/mixin/" + CrypticRegistryInfo.MOD_ID + ".refmap.json";
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        return true;
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) { }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) { }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) { }
}
