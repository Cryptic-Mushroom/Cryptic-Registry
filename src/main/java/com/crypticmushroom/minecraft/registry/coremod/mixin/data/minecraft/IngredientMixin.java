package com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft;

import com.crypticmushroom.minecraft.registry.coremod.hook.impl.CrypticIngredient;
import com.crypticmushroom.minecraft.registry.data.provider.recipe.CrypticRecipeProvider;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Arrays;
import java.util.stream.Stream;

@Mixin(Ingredient.class)
public abstract class IngredientMixin implements CrypticIngredient {
    @Unique protected String cmreg$name = null;

    @Inject(
        method = "<init>(Ljava/util/stream/Stream;)V",
        at = @At("RETURN")
    )
    private void afterConstruction(Stream<? extends Ingredient.Value> stream, CallbackInfo callback) {
        this.cmreg$name = cmreg$createName(((Ingredient) (Object) this).values);
    }

    @Unique
    private static @Nullable String cmreg$createName(Ingredient.Value[] values) {
        var builder = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            if (values[i] instanceof Ingredient.ItemValue item) {
                builder.append(CrypticRecipeProvider.getName(item.item.getItem()));
            } else if (values[i] instanceof Ingredient.TagValue tag) {
                builder.append(CrypticRecipeProvider.getName(tag.tag));
            } else {
                continue;
            }

            if (i != values.length - 1)
                builder.append("_and_");
        }
        var result = builder.toString();
        return result.isEmpty() ? null : result;
    }

    @Override
    public String cmreg$getName() {
        if (this.cmreg$name == null) {
            this.cmreg$name = CrypticRecipeProvider.getName(Arrays.stream(((Ingredient) (Object) this).getItems()).map(ItemStack::getItem).distinct().toArray(Item[]::new));
        }

        return this.cmreg$name;
    }
}
