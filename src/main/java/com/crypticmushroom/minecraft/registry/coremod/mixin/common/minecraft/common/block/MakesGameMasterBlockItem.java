package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common.block;

import com.crypticmushroom.minecraft.registry.api.block.MakesCustomBlockItem;
import net.minecraft.world.item.GameMasterBlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.GameMasterBlock;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;

@Unique
@Mixin({
    GameMasterBlock.class
})
public interface MakesGameMasterBlockItem extends MakesCustomBlockItem<GameMasterBlockItem> {
    @Override
    default GameMasterBlockItem cmreg$makeBlockItem(Item.Properties properties) {
        return new GameMasterBlockItem(((Block) this), properties.rarity(Rarity.EPIC));
    }
}
