package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.common;

import com.crypticmushroom.minecraft.registry.coremod.hook.impl.CrypticBlockBehaviourProperties;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.flag.FeatureFlagSet;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.MaterialColor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.ToIntFunction;

@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
@Mixin(BlockBehaviour.Properties.class)
public abstract class BlockBehaviourPropertiesMixin implements CrypticBlockBehaviourProperties {
    @Shadow public Material material;
    @Shadow Function<BlockState, MaterialColor> materialColor;
    @Shadow public boolean hasCollision;
    @Shadow SoundType soundType;
    @Shadow ToIntFunction<BlockState> lightEmission;
    @Shadow float explosionResistance;
    @Shadow float destroyTime;
    @Shadow public boolean requiresCorrectToolForDrops;
    @Shadow public boolean isRandomlyTicking;
    @Shadow float friction;
    @Shadow float speedFactor;
    @Shadow float jumpFactor;
    /** Sets loot table information */
    @Shadow ResourceLocation drops;
    @Shadow public boolean canOcclude;
    @Shadow public boolean isAir;
    @Shadow public boolean spawnParticlesOnBreak;
    @Shadow(remap = false) private java.util.function.Supplier<ResourceLocation> lootTableSupplier;
    @Shadow BlockBehaviour.StateArgumentPredicate<EntityType<?>> isValidSpawn;
    @Shadow BlockBehaviour.StatePredicate isRedstoneConductor;
    @Shadow BlockBehaviour.StatePredicate isSuffocating;
    /** If it blocks vision on the client side. */
    @Shadow BlockBehaviour.StatePredicate isViewBlocking;
    @Shadow BlockBehaviour.StatePredicate hasPostProcess;
    @Shadow BlockBehaviour.StatePredicate emissiveRendering;
    @Shadow public boolean dynamicShape;
    @Shadow FeatureFlagSet requiredFeatures;
    @Shadow public Optional<BlockBehaviour.OffsetFunction> offsetFunction;

    @Unique
    @Override
    public BlockBehaviour.Properties cmreg$clone() {
        var result = BlockBehaviour.Properties.of(this.material, this.materialColor);
        var clone = (BlockBehaviourPropertiesMixin) (Object) result;

        clone.material = this.material;
        clone.destroyTime = this.destroyTime;
        clone.explosionResistance = this.explosionResistance;
        clone.hasCollision = this.hasCollision;
        clone.isRandomlyTicking = this.isRandomlyTicking;
        clone.lightEmission = this.lightEmission;
        clone.materialColor = this.materialColor;
        clone.soundType = this.soundType;
        clone.friction = this.friction;
        clone.speedFactor = this.speedFactor;
        clone.dynamicShape = this.dynamicShape;
        clone.canOcclude = this.canOcclude;
        clone.isAir = this.isAir;
        clone.requiresCorrectToolForDrops = this.requiresCorrectToolForDrops;
        clone.offsetFunction = this.offsetFunction;
        clone.spawnParticlesOnBreak = this.spawnParticlesOnBreak;
        clone.requiredFeatures = this.requiredFeatures;

        // NOT IN copy()
        clone.jumpFactor = this.jumpFactor;
        clone.drops = this.drops;
        clone.lootTableSupplier = this.lootTableSupplier;
        clone.isValidSpawn = this.isValidSpawn;
        clone.isRedstoneConductor = this.isRedstoneConductor;
        clone.isSuffocating = this.isSuffocating;
        clone.isViewBlocking = this.isViewBlocking;
        clone.hasPostProcess = this.hasPostProcess;
        clone.emissiveRendering = this.emissiveRendering;

        return result;
    }
}
