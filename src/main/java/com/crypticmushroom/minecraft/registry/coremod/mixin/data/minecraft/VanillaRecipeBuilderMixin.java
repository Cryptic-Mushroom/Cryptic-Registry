package com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft;

import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.ShapelessRecipeBuilder;
import net.minecraft.data.recipes.SimpleCookingRecipeBuilder;
import net.minecraft.data.recipes.SingleItemRecipeBuilder;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.ForgeRegistries;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin({
    ShapedRecipeBuilder.class,
    ShapelessRecipeBuilder.class,
    SimpleCookingRecipeBuilder.class,
    SingleItemRecipeBuilder.class
})
public abstract class VanillaRecipeBuilderMixin implements RecipeBuilder {
    @Shadow(
        remap = false,
        aliases = {
            "f_126106_", // ShapedRecipeBuilder
            "f_126173_", // ShapelessRecipeBuilder
            "f_126235_", // SimpleCookingRecipeBuilder
            "f_126302_" // SingleItemRecipeBuilder
        }
    )
    private @Final Item result;

    @ModifyConstant(
        method = "save(Ljava/util/function/Consumer;Lnet/minecraft/resources/ResourceLocation;)V",
        constant = @Constant(stringValue = "")
    )
    private String modifyGroup(String group) {
        return ForgeRegistries.ITEMS.getKey(this.result).getPath();
    }
}
