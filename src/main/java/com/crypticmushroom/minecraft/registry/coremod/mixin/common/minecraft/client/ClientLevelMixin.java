package com.crypticmushroom.minecraft.registry.coremod.mixin.common.minecraft.client;

import com.crypticmushroom.minecraft.registry.api.biome.CrypticColorResolver;
import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import com.llamalad7.mixinextras.sugar.Local;
import com.llamalad7.mixinextras.sugar.Share;
import com.llamalad7.mixinextras.sugar.ref.LocalBooleanRef;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.ColorResolver;
import net.minecraft.world.level.biome.Biome;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ClientLevel.class)
public abstract class ClientLevelMixin {
    @Inject(
        method = "calculateBlockTint(Lnet/minecraft/core/BlockPos;Lnet/minecraft/world/level/ColorResolver;)I",
        at = @At("HEAD")
    )
    private void checkColorResolver(BlockPos blockPos, ColorResolver colorResolver, CallbackInfoReturnable<Integer> callback, @Share("cryptic") LocalBooleanRef cryptic) {
        cryptic.set(colorResolver instanceof CrypticColorResolver);
    }

    @WrapOperation(
        method = "calculateBlockTint(Lnet/minecraft/core/BlockPos;Lnet/minecraft/world/level/ColorResolver;)I",
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/world/level/ColorResolver.getColor(Lnet/minecraft/world/level/biome/Biome;DD)I",
            ordinal = 0
        )
    )
    private int getColor(ColorResolver colorResolver, Biome biome, double x, double z, Operation<Integer> original, BlockPos blockPos, ColorResolver ignored, @Share("cryptic") LocalBooleanRef cryptic) {
        return cryptic.get()
               ? ((CrypticColorResolver) colorResolver).getColor(((ClientLevel) (Object) this).getBiome(blockPos), x, z)
               : original.call(colorResolver, biome, x, z);
    }

    @WrapOperation(
        method = "calculateBlockTint(Lnet/minecraft/core/BlockPos;Lnet/minecraft/world/level/ColorResolver;)I",
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/world/level/ColorResolver.getColor(Lnet/minecraft/world/level/biome/Biome;DD)I",
            ordinal = 1
        )
    )
    private int getColor$biomeBlend(ColorResolver colorResolver, Biome biome, double x, double z, Operation<Integer> original, @Local(index = 10) BlockPos.MutableBlockPos blockpos$mutableblockpos, @Share("cryptic") LocalBooleanRef cryptic) {
        return cryptic.get()
               ? ((CrypticColorResolver) colorResolver).getColor(((ClientLevel) (Object) this).getBiome(blockpos$mutableblockpos), x, z)
               : original.call(colorResolver, biome, x, z);
    }
}
