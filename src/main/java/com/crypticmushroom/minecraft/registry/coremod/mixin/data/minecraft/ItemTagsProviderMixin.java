package com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft;

import com.crypticmushroom.minecraft.registry.coremod.hook.ItemTagsProviderHooks;
import com.crypticmushroom.minecraft.registry.data.provider.ICrypticDataProvider;
import net.minecraft.client.multiplayer.ClientPacketListener;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.tags.TagBuilder;
import net.minecraft.tags.TagEntry;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.ForgeRegistries;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.ArrayList;


/**
 * This mixin allows us to inject bytecode into the {@link ClientPacketListener} class to perform various custom
 * operations.
 *
 * @author Jonathing
 */
@Mixin(ItemTagsProvider.class)
public abstract class ItemTagsProviderMixin extends TagsProviderMixin<Item> {
    @Inject(
        method = "lambda$createContentsProvider$3(Lnet/minecraft/data/tags/TagsProvider$TagLookup;Lnet/minecraft/tags/TagKey;Lnet/minecraft/tags/TagKey;)V",
        at = @At("HEAD"),
        cancellable = true
    )
    private void copyItemTags(TagsProvider.TagLookup<Block> lookup, TagKey<Block> blockTag, TagKey<Item> itemTag, CallbackInfo callback) {
        if (!(this instanceof ICrypticDataProvider cryptic)) return;

        var tags = lookup.apply(blockTag).orElse(TagBuilder.create()).build();
        if (!tags.isEmpty()) {
            var entries = new ArrayList<TagEntry>(tags.size());
            tags.forEach(entry -> {
                if (entry.verifyIfPresent(ForgeRegistries.ITEMS::containsKey, tag -> ItemTagsProviderHooks.isItemTag(cryptic.getModId(), tag))) {
                    entries.add(entry);
                }
            });

            if (!entries.isEmpty()) {
                TagBuilder itemTagBuilder = this.getOrCreateRawBuilder(itemTag);
                entries.forEach(itemTagBuilder::add);
            }
        }

        callback.cancel();
    }
}
