package com.crypticmushroom.minecraft.registry.coremod.mixin.data.forge;

import com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft.IngredientMixin;
import com.crypticmushroom.minecraft.registry.data.provider.recipe.CrypticRecipeProvider;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.Item;
import net.minecraftforge.common.crafting.PartialNBTIngredient;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Set;

@Mixin(value = PartialNBTIngredient.class, remap = false)
public abstract class PartialNBTIngredientMixin extends IngredientMixin {
    @Shadow private @Final Set<Item> items;

    @Inject(
        method = "<init>(Ljava/util/Set;Lnet/minecraft/nbt/CompoundTag;)V",
        at = @At("RETURN")
    )
    private void afterConstruction(Set<Item> items, CompoundTag nbt, CallbackInfo callback) {
        this.cmreg$name = cmreg$createName(this.items);
    }

    @Unique
    private static @Nullable String cmreg$createName(Set<Item> values) {
        var builder = new StringBuilder().append("partial_nbt_of_");
        var iterator = values.iterator();
        while (iterator.hasNext()) {
            builder.append(CrypticRecipeProvider.getName(iterator.next()));
            if (iterator.hasNext())
                builder.append("_and_");
        }
        var result = builder.toString();
        return result.equals("partial_nbt_of_") ? null : result;
    }
}
