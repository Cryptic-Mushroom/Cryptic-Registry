package com.crypticmushroom.minecraft.registry.coremod.mixin.data.forge.accessor;

import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.client.model.generators.ModelBuilder;
import net.minecraftforge.client.model.generators.ModelProvider;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.function.Function;

@Mixin(value = ModelProvider.class, remap = false)
public interface ModelProviderAccessor<T extends ModelBuilder<T>> {
    @Accessor("factory")
    @Mutable
    void cmreg$factory(Function<ResourceLocation, T> factory);
}
