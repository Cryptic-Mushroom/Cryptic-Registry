package com.crypticmushroom.minecraft.registry.coremod.hook;

import com.crypticmushroom.minecraft.registry.coremod.reflection.ReflectionUtil;
import com.crypticmushroom.minecraft.registry.data.registry.TagRegistry;
import com.crypticmushroom.minecraft.registry.directory.RegistryDirectory;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.data.tags.UpdateOneTwentyItemTagsProvider;
import net.minecraft.data.tags.VanillaItemTagsProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.ForgeItemTagsProvider;
import org.jetbrains.annotations.ApiStatus.Internal;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static com.crypticmushroom.minecraft.registry.coremod.CrypticCoreMod.LOGGER;
import static com.crypticmushroom.minecraft.registry.coremod.CrypticCoreMod.REFLECTION_MARKER;

/**
 * This utility class contains additional hooks for the
 * {@link com.crypticmushroom.minecraft.registry.coremod.mixin.data.minecraft.ItemTagsProviderMixin
 * ItemTagsProviderMixin} class.
 *
 * @author Jonathing
 */
@Internal
public final class ItemTagsProviderHooks {
    @SuppressWarnings("unchecked")
    private static final Set<ResourceLocation> VANILLA_AND_FORGE_ITEM_TAGS = new HashSet<>() {
        {
            LOGGER.debug(REFLECTION_MARKER, "Gathering all vanilla and Forge item tags");
            var classes = Map.of(
                "vanilla", ItemTags.class,
                "forge", Tags.Items.class
            );
            for (Map.Entry<String, Class<?>> entry : classes.entrySet()) {
                var origin = entry.getKey();
                var clazz = entry.getValue();
                for (Field field : clazz.getFields()) {
                    TagKey<Item> tag;
                    try {
                        tag = Objects.requireNonNull((TagKey<Item>) field.get(null));
                    } catch (IllegalArgumentException | IllegalAccessException | ClassCastException |
                             SecurityException | NullPointerException e) {
                        continue;
                    }

                    LOGGER.trace(REFLECTION_MARKER, "Found {} item tag: {}", origin, tag.toString());
                    this.add(tag.location());
                }
            }
        }
    };

    /**
     * Checks if a given tag's ID is a vanilla tag, a forge tag, or if it is in the registered item tag class contained
     * in the {@link TagRegistry.Item item tag registry}.
     *
     * @param modId The mod ID to check the tag class for.
     * @param id    The ID of the item tag to validate.
     * @return If the given ID is of a valid item tag.
     */
    public static boolean isItemTag(String modId, ResourceLocation id) {
        var itemTagRegistry = (TagRegistry.Item) TagRegistry.get(RegistryDirectory.ITEM);
        return VANILLA_AND_FORGE_ITEM_TAGS.contains(id) || itemTagRegistry.contains(modId, id);
    }

    /**
     * Checks if a given {@link StackTraceElement} array (usually given by {@link Thread#getStackTrace()}) contains a
     * method call to {@link VanillaItemTagsProvider#addTags(HolderLookup.Provider)},
     * {@link VanillaItemTagsProvider#addTags(HolderLookup.Provider)}, or
     * {@link ForgeItemTagsProvider#addTags(HolderLookup.Provider)}.
     *
     * @param elements The array of stack trace elements.
     * @return If the stack trace contains a method call as described.
     */
    @SuppressWarnings("JavadocReference")
    public static boolean containsVanillaOrForgeMethod(StackTraceElement[] elements) {
        return ReflectionUtil.threadContains(elements, VanillaItemTagsProvider.class, "m_6577_")
                   || ReflectionUtil.threadContains(elements, UpdateOneTwentyItemTagsProvider.class, "m_6577_")
                   || ReflectionUtil.threadContains(elements, ForgeItemTagsProvider.class, "m_6577_");
    }

    @SuppressWarnings("rawtypes")
    public static boolean isVanillaOrForgeProvider(TagsProvider provider) {
        return provider instanceof VanillaItemTagsProvider
                   || provider instanceof UpdateOneTwentyItemTagsProvider
                   || provider instanceof ForgeItemTagsProvider;
    }
}
