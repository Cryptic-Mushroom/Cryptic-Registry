ModsDotGroovy.make {
    modLoader = 'javafml'
    loaderVersion = this.buildProperties.loader_version_range
    issueTrackerUrl = 'https://gitlab.com/cryptic-mushroom/minecraft/cryptic-registry/-/issues'
    license = this.buildProperties.mod_license
    put('showAsResourcePack', false)

    mod {
        modId = this.buildProperties.mod_id
        version = this.buildProperties.mod_version
        displayName = this.buildProperties.mod_name
        //updateJsonUrl = 'https://raw.githubusercontent.com/Cryptic-Mushroom/The-Midnight/rewrite/versioninfo/update.json'
        displayUrl = 'https://www.curseforge.com/minecraft/mc-mods/cryptic-registry'
        //logoFile = 'logo.png'
        credits = "${this.buildProperties.mod_vendor}, bconlon"
        authors = ['Jonathing']
        description =
            '''Cryptic Registry is an in-house library mod which assists and automates much of the data-generation process for our mods.'''
        displayTest = DisplayTest.MATCH_VERSION

        dependencies {
            minecraft = this.buildProperties.mc_version_range
            forge = this.buildProperties.forge_version_range
        }
    }
}